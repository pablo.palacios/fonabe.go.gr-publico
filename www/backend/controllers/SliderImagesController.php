<?php

namespace backend\controllers;

use Yii;
use common\models\SliderImages;
use common\models\SliderImagesSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\ImageUploadForm;
use yii\web\UploadedFile;

/**
 * Class SliderImagesController, gestión de imágenes de Slide
 *
 * @package backend\controllers
 */
class SliderImagesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update', 'delete'],
                        'roles' => ['SUPERADMIN','ADMIN'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Maestro de Slides
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SliderImagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Crea registro de Slides
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SliderImages();
        $model->image_link = '#';
        $uploadModel = new ImageUploadForm();
        $isImageUploaded = false;
        if (Yii::$app->request->isPost) {
            $uploadModel->imageFile = UploadedFile::getInstance($uploadModel, 'imageFile');
            $model->load(Yii::$app->request->post());
            $img_save = $uploadModel->upload();
            if ($img_save !== null) {
                $isImageUploaded = true;
                $model->image_url = $img_save->name;
                if ($model->save() && $isImageUploaded) {
                    return $this->redirect([
                        'index'
                    ]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'uploadModel' => $uploadModel,
        ]);
    }

    /**
     * Actualiza registro de Slides
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $post = $request->post();
        $model = $this->findModel($id);
        $uploadModel = new ImageUploadForm();
        $isImageUploaded = false;

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            $file = $_FILES['ImageUploadForm'];
            if( $file['name']['imageFile'] !=='' ){
                $uploadModel->imageFile = UploadedFile::getInstance($uploadModel, 'imageFile');
                $img_save = $uploadModel->upload();
                $model->image_url = $img_save->name;
            }
            $isImageUploaded = true;
                if ($model->save() && $isImageUploaded) {
                    return $this->redirect([
                        'index'
                    ]);
                }
        }
        return $this->render('update', [
            'model' => $model,
            'uploadModel' => $uploadModel,
        ]);
    }

    /**
     * Elimina registro de Slides
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }


    /**
     * Busca registro de Slides
     *
     * @param integer $id
     * @return SliderImages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SliderImages::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('El registro no fue encontrado.');
    }
}
