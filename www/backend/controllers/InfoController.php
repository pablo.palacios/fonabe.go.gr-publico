<?php

namespace backend\controllers;

use common\models\StaticPages;
use Yii;
use common\models\Info;
use common\models\InfoSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * Class InfoController, gestión de Información adicional
 *
 * @package backend\controllers
 */
class InfoController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update', 'delete'],
                        'roles' => ['SUPERADMIN', 'ADMIN', 'GESTOR_SECCIONES', 'GESTOR_CONTENIDOS'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Maestro de Información.
     *
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new InfoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Crea un nuevo registro
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionCreate() {
        $model = new Info();
        $pages = $this->findCategoyStaticPagesArray();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {

                Yii::$app->session->setFlash('success', 'Se ha guardado correctamente su página');
            } else {

                Yii::$app->session->setFlash('error', 'Ha ocurrido un error, por favor intente de nuevo');
            }
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('create', [
                    'model' => $model,
                    'pages' => $pages,
        ]);
    }

    /**
     * Actualiza el registro
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $pages = $this->findCategoyStaticPagesArray();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {

                Yii::$app->session->setFlash('success', 'Se ha guardado correctamente su página');
            } else {

                Yii::$app->session->setFlash('error', 'Ha ocurrido un error, por favor intente de nuevo');
            }
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
                    'pages' => $pages,
        ]);
    }

    /**
     * Elimina el registro
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Encuentra el registro por ID
     *
     * @param integer $id
     * @return Info the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Info::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('El registro no fue encontrado.');
    }

    /**
     * Obtiene las Categorías y páginas para Dropdown
     *
     * @return array
     * @throws NotFoundHttpException
     */
    protected function findCategoyStaticPagesArray() {
        if (($model = StaticPages::find()->select('id, title, category_id')->where(['has_md_info' => true])->orderBy(['category_id' => 'asc'])->all() ) !== null) {
            $ret_model = [];
            foreach ($model as $key => $value) {
                array_push($ret_model, ['id' => $value->id, 'title' => $value->category->title . ' | ' . $value->title]);
            }
            return ArrayHelper::map($ret_model, 'id', 'title');
        }
        throw new NotFoundHttpException('Se produjo un error');
    }

}
