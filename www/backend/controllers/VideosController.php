<?php

namespace backend\controllers;

use common\models\StaticPages;
use Yii;
use common\models\Videos;
use common\models\VideosSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * Class VideosController, gestión de registros de Video
 *
 * @package backend\controllers
 */
class VideosController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update', 'delete'],
                        'roles' => ['SUPERADMIN','ADMIN','GESTOR_SECCIONES','GESTOR_CONTENIDOS'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Maestro de Videos
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VideosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Crea registro de Videos
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionCreate()
    {
        $model = new Videos();
        $pages = $this->findCategoyStaticPagesArray();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
            'pages' => $pages,
        ]);
    }

    /**
     * Actualiza registro de Videos
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $pages = $this->findCategoyStaticPagesArray();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'pages' => $pages,
        ]);
    }

    /**
     * Elimina registro de Videos
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Busca el registro de Video por ID
     *
     * @param integer $id
     * @return Videos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Videos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('El registro no fue encontrado.');
    }
    /**
     * Obtiene las Categorías y páginas para Dropdown
     *
     * @return array
     * @throws NotFoundHttpException
     */
    protected function findCategoyStaticPagesArray()
    {
        if (($model = StaticPages::find()->select('id, title, category_id')->where(['has_md_video'=>true])->orderBy(['category_id'=>'asc'])->all() ) !== null) {
            $ret_model = [];
            foreach ($model as $key => $value) {
                array_push($ret_model, ['id' => $value->id, 'title' => $value->category->title . ' | ' . $value->title] );
            }
            return ArrayHelper::map($ret_model,'id', 'title');
        }
        throw new NotFoundHttpException('Se produjo un error');
    }
}
