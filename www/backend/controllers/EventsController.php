<?php

namespace backend\controllers;

use Throwable;
use Yii;
use DateInterval;
use DateTime;
use Exception;
use common\models\Events;
use common\models\EventsSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * Class EventsController, gestión de Eventos para calendario
 *
 * @package backend\controllers
 */
class EventsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'update'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'update'],
                        'roles' => ['SUPERADMIN','ADMIN','GESTOR_SECCIONES'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Maestro de Eventos.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Crea nuevo Evento
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Events();
        $save_me = false;
        if ($model->load(Yii::$app->request->post()) && $request->isPost ) {
            try {
                list($day, $month, $year) = explode('-', $model->date);
                list($hour, $minute) = explode(':', $model->time);
                $hour_range = $model->hours;
                $save_me = true;
            } catch (Exception $e) {
                Yii::$app->session->setFlash('error', 'Seleccione la fecha del Evento ' . $e->getMessage());
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }

        if ($save_me && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }


    }

    /**
     * Actualiza los Eventos.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Exception
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $save_me = false;
        if ($model->load(Yii::$app->request->post()) && $request->isPost ) {
            try {
                list($day, $month, $year) = explode('-', $model->date);
                list($hour, $minute) = explode(':', $model->time);
                $hour_range = $model->hours;
                $save_me = true;
            } catch (Exception $e) {
            } catch (Exception $e) {
                Yii::$app->session->setFlash('error', 'Seleccione la fecha del Evento');
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }

        if ($save_me && $model->save()) {
            Yii::$app->session->setFlash('info', 'Su evento se ha guardado correctamente');
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Elimina el Evento.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Exception
     * @throws Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Busca el registro de Evento por ID
     *
     * @param integer $id
     * @return Events the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Events::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('El registro no fue encontrado.');
    }
}
