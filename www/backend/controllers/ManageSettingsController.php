<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use backend\models\SettingsForm;

/**
 * Class ManageSettingsController, gestiona las configuraciones generales del sistema
 *
 * @package backend\controllers
 */
class ManageSettingsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update', 'delete'],
                        'roles' => ['SUPERADMIN'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Formulario de Configuraciones
     *
     * @return string
     */
    public function actionIndex()
    {
        $settings = Yii::$app->settings;
        $model = new SettingsForm();

        //MAIN
        $model->adminUrl = $settings->get('main', 'admin_url');
        $model->frontend_url = $settings->get('main', 'frontend_url');

        $model->correoInstitucionalUrl = $settings->get('main', 'correo_institucional_url');
        $model->siteMainTitle = $settings->get('main', 'site_main_title');
        $model->facebookUrl = $settings->get('main', 'facebook_url');
        $model->twitterUrl = $settings->get('main', 'twitter_url');
        $model->copyString = $settings->get('main', 'copy_string');
        $model->email = $settings->get('mail', 'email');
        $model->pagination = $settings->get('main', 'pagination');

        //Blue box
        $model->urlLink0 = $settings->get('index', 'urlLink0');
        $model->urlLink1 = $settings->get('index', 'urlLink1');
        $model->urlLink2 = $settings->get('index', 'urlLink2');
        $model->urlLink3 = $settings->get('index', 'urlLink3');

        $model->link0_label = $settings->get('index', 'link0_label');
        $model->link1_label = $settings->get('index', 'link1_label');
        $model->link2_label = $settings->get('index', 'link2_label');
        $model->link3_label = $settings->get('index', 'link3_label');

        //Datos abiertos
        $model->link_url_1 = $settings->get('data', 'link_url_1');
        $model->link_url_2 = $settings->get('data', 'link_url_2');
        $model->link_url_3 = $settings->get('data', 'link_url_3');
        $model->link_label_1 = $settings->get('data', 'link_label_1');
        $model->link_label_2 = $settings->get('data', 'link_label_2');
        $model->link_label_3 = $settings->get('data', 'link_label_3');

        return $this->render('settings', ['model' => $model]);
    }

    /**
     * Actualiza los registros de Configuración
     *
     * @return \yii\web\Response
     */
    public function actionUpdate()
    {
        $settings = Yii::$app->settings;
        $request = Yii::$app->request;
        $post = $request->post();
        $model = new SettingsForm();

        if($model->load($post)){
            //MAIN
            $settings->set('main', 'admin_url', $model->adminUrl);
            $settings->set('main', 'frontend_url', $model->frontend_url);

            $settings->set('main', 'correo_institucional_url', $model->correoInstitucionalUrl);
            $settings->set('main', 'site_main_title', $model->siteMainTitle);
            $settings->set('main', 'facebook_url', $model->facebookUrl);
            $settings->set('main', 'twitter_url', $model->twitterUrl);
            $settings->set('main', 'copy_string', $model->copyString);
            $settings->set('mail', 'email', $model->email);
            $settings->set('main', 'pagination', $model->pagination);

            //INDEX
            $settings->set('index', 'urlLink0', $model->urlLink0);
            $settings->set('index', 'urlLink1', $model->urlLink1);
            $settings->set('index', 'urlLink2', $model->urlLink2);
            $settings->set('index', 'urlLink3', $model->urlLink3);

            $settings->set('index', 'link0_label', $model->link0_label);
            $settings->set('index', 'link1_label', $model->link1_label);
            $settings->set('index', 'link2_label', $model->link2_label);
            $settings->set('index', 'link3_label', $model->link3_label);

            //DATOS ABIERTOS
            $settings->set('data', 'link_url_1', $model->link_url_1);
            $settings->set('data', 'link_url_2', $model->link_url_2);
            $settings->set('data', 'link_url_3', $model->link_url_3);

            $settings->set('data', 'link_label_1', $model->link_label_1);
            $settings->set('data', 'link_label_2', $model->link_label_2);
            $settings->set('data', 'link_label_3', $model->link_label_3);
            $settings->invalidateCache();
            Yii::$app->session->setFlash('success', 'Las configuraciones del sitio fueron almacenadas correctamente');
        }

        Yii::$app->cache->flush();
        Yii::$app->cacheFrontend->flush();
        return $this->redirect('/manage-settings');
    }
}
