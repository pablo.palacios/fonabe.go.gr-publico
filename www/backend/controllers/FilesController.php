<?php

namespace backend\controllers;

use backend\models\FileUploadForm;
use common\models\StaticPages;
use Throwable;
use Yii;
use common\models\Files;
use common\models\FilesSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\UploadedFile;
use common\models\Info;

/**
 * Class FilesController, gestión de Documentos
 *
 * @package backend\controllers
 */
class FilesController extends Controller {

  public function behaviors() {
    return [
        'access' => [
            'class' => AccessControl::className(),
            'only' => ['index', 'create', 'update', 'delete'],
            'rules' => [
                [
                    'allow' => true,
                    'actions' => ['index', 'create', 'update', 'delete'],
                    'roles' => ['SUPERADMIN', 'ADMIN', 'GESTOR_SECCIONES', 'GESTOR_CONTENIDOS'],
                ],
            ],
        ],
        'verbs' => [
            'class' => VerbFilter::className(),
            'actions' => [
                'delete' => ['POST'],
            ],
        ],
    ];
  }

  /**
   * Maestro de Documentos.
   * @return mixed
   */
  public function actionIndex() {
    $searchModel = new FilesSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Crea un Documento
   *
   * @return mixed
   * @throws NotFoundHttpException
   */
  public function actionCreate() {
    $model = new Files();
    $pages = $this->findCategoyStaticPagesArray();
    $uploadModel = new FileUploadForm();
    $isDocumentUploaded = false;
    if ($model->load(Yii::$app->request->post())) {
      //escenario para solicitar como requerido el campo de página secundaria
      if ($model->page_id == Info::ID_TIPO_BECA) {
        $model->scenario = 'pagina_secundaria';
      }
      $uploadModel->documentFile = UploadedFile::getInstance($uploadModel, 'documentFile');
      $doc_save = $uploadModel->upload();
      if ($doc_save !== null) {
        $isDocumentUploaded = true;
        $model->file_url = $doc_save->name;
        if ($model->save() && $isDocumentUploaded) {
          Yii::$app->session->setFlash('success', 'Su documento fue creado correctamente');
          return $this->redirect([
                      'index'
          ]);
        }
      }
    }
    return $this->render('create', [
                'model' => $model,
                'pages' => $pages,
                'uploadModel' => $uploadModel,
    ]);
  }

  /**
   * Actualiza el Documento.
   *
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionUpdate($id) {
    $request = Yii::$app->request;
    $model = $this->findModel($id);
    $pages = $this->findCategoyStaticPagesArray();
    $uploadModel = new FileUploadForm();
    $isDocumentUploaded = false;

    if ($model->load(Yii::$app->request->post())) {
      //escenario para solicitar como requerido el campo de página secundaria
      if ($model->page_id == Info::ID_TIPO_BECA) {
        $model->scenario = 'pagina_secundaria';
      }
      $file = $_FILES['FileUploadForm'];
      if ($file['name']['documentFile'] !== '') {
        $uploadModel->documentFile = UploadedFile::getInstance($uploadModel, 'documentFile');
        $doc_save = $uploadModel->upload();
        if (isset($doc_save->name)) {
          $model->file_url = $doc_save->name;
        }
      }
      $isDocumentUploaded = true;
      if ($model->save() && $isDocumentUploaded) {
        Yii::$app->session->setFlash('success', 'Su documento fue creado correctamente');
        return $this->redirect([
                    'index'
        ]);
      }
    }
    return $this->render('update', [
                'model' => $model,
                'pages' => $pages,
                'uploadModel' => $uploadModel,
    ]);
  }

  /**
   * Elimina el Documento
   *
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   * @throws \Exception
   * @throws Throwable
   * @throws \yii\db\StaleObjectException
   */
  public function actionDelete($id) {
    $this->findModel($id)->delete();
    return $this->redirect(['index']);
  }

  /**
   * Busca el Documento por ID
   *
   * @param integer $id
   * @return Files the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id) {
    if (($model = Files::findOne($id)) !== null) {
      return $model;
    }
    throw new NotFoundHttpException('La página que busca no fue encontrada.');
  }

  /**
   * Obtiene las Categorías y páginas para Dropdown
   *
   * @return array
   * @throws NotFoundHttpException
   */
  protected function findCategoyStaticPagesArray() {
    if (($model = StaticPages::find()->select('id, title, category_id')->where(['has_md_documents' => true])->orderBy(['category_id' => 'asc'])->all() ) !== null) {
      $ret_model = [];
      foreach ($model as $key => $value) {
        array_push($ret_model, ['id' => $value->id, 'title' => $value->category->title . ' | ' . $value->title]);
      }
      return ArrayHelper::map($ret_model, 'id', 'title');
    }
    throw new NotFoundHttpException('El registro no fue encontrado');
  }

  /*
   * Action para cargar combo de páginas secundarias
   *
   * Las páginas secundarias únicamente se cargan si es para tipos de becas.
   */

  public function actionSubpage() {
    $out = [];
    if (isset($_POST['depdrop_parents'])) {
      $parents = $_POST['depdrop_parents'];
      if ($parents != NULL) {
        $subpage_id = $parents[0];
        $out = FALSE;
        //sólo cuando se selecciona una página tipo de beca se llena
        //el combo de selección por depdrop
        if ($subpage_id == Info::ID_TIPO_BECA) {
          $out = Info::getAllInfoPages();
        }
        echo Json::encode(['output' => $out, 'selected' => '']);
        return;
      }
    }
    echo Json::encode(['output' => '', 'selected' => '']);
  }

}
