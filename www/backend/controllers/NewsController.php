<?php

namespace backend\controllers;

use Yii;
use backend\models\ImageUploadForm;
use common\models\News;
use common\models\NewsSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use kartik\grid\EditableColumnAction;
use yii\web\UploadedFile;

/**
 * Class NewsController, gestión de Noticias
 *
 * @package backend\controllers
 */
class NewsController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update', 'delete'],
                        'roles' => ['SUPERADMIN','ADMIN','GESTOR_SECCIONES','GESTOR_CONTENIDOS'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Maestro de Noticias
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Crea Noticia
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();
        $uploadModel = new ImageUploadForm();
        $isImageUploaded = false;

        if (Yii::$app->request->isPost) {
            $uploadModel->imageFile = UploadedFile::getInstance($uploadModel, 'imageFile');
            $model->load(Yii::$app->request->post());
            $img_save = $uploadModel->upload();
            if ($img_save !== null) {
                $isImageUploaded = true;
                $model->url_imagen = $img_save->name;
                $model->published = false;
                if ($model->save() && $isImageUploaded) {
                    Yii::$app->session->setFlash('success', 'Su nueva noticia fue creada correctamente');
                    return $this->redirect([
                        'index'
                    ]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'uploadModel' => $uploadModel,
        ]);
    }

    /**
     * Actualiza la Noticia
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $post = $request->post();
        $model = $this->findModel($id);
        $uploadModel = new ImageUploadForm();
        $isImageUploaded = false;

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            $file = $_FILES['ImageUploadForm'];
            if( $file['name']['imageFile'] !=='' ){
                $uploadModel->imageFile = UploadedFile::getInstance($uploadModel, 'imageFile');
                $img_save = $uploadModel->upload();
                $model->url_imagen = $img_save->name;
            }
            $isImageUploaded = true;
                if ($model->save() && $isImageUploaded) {
                    Yii::$app->session->setFlash('success', 'Su noticia fue actualizada correctamente');
                    return $this->redirect([
                        'index'
                    ]);
                }
        }

        return $this->render('update', [
            'model' => $model,
            'uploadModel' => $uploadModel,
        ]);
    }

    /**
     * Elimina la noticia
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Cambia el estado de la noticia
     *
     * @return \yii\web\Response
     */
    public function actionPublishedStatus()
    {
        return $this->redirect(['index']);
    }

    /**
     * Encuentra la noticia por id
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Cambia estado PUBLICADO
     * @return array
     */
    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'published' => [
                'class' => EditableColumnAction::className(),
                'modelClass' => News::className(),
                'outputValue' => function ($model, $attribute, $key, $index) {
                         return ($model->$attribute)?'Si':'No';
                },
                'outputMessage' => function($model, $attribute, $key, $index) {
                    return '';
                },
            ]
        ]);
    }

}
