<?php
namespace backend\models;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class ImageUploadForm
 *
 * @package backend\models
 */
class ImageUploadForm extends Model
{

    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * Caraga la imágen
     * @return null / imageFile
     */
    public function upload()
    {
        $unique_name = uniqid('', true);
        if ($this->validate()) {
            $file = $unique_name . '.' . $this->imageFile->extension;
            $path = Yii::getAlias('@frontend') . '\web\img-uploads\\';
            $this->imageFile->saveAs( $path . $file);
            $this->imageFile->name= $file;
            return $this->imageFile;
        } else {
            return null;
        }
    }
}