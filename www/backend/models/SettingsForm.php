<?php

namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * Class SettingsForm
 *
 * @package backend\models
 */
class SettingsForm extends Model
{
    public $adminUrl;
    public $frontend_url;

    public $correoInstitucionalUrl;
    public $siteMainTitle;
    public $facebookUrl;
    public $twitterUrl;
    public $copyString;
    public $email;
    public $pagination;

    public $urlLink0; //Blue box
    public $urlLink1; //Blue box
    public $urlLink2; //Blue box
    public $urlLink3; //Blue box

    public $link0_label; //Blue box
    public $link1_label; //Blue box
    public $link2_label; //Blue box
    public $link3_label; //Blue box

    public $link_url_1; //Datos abiertos
    public $link_url_2; //Datos abiertos
    public $link_url_3; //Datos abiertos

    public $link_label_1; //Datos abiertos
    public $link_label_2; //Datos abiertos
    public $link_label_3; //Datos abiertos

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'adminUrl',
                'frontend_url',
                'correoInstitucionalUrl',
                'siteMainTitle',
                'facebookUrl',
                'twitterUrl',
                'copyString',
                'email',
                'pagination',

                //Blue box
                'urlLink0',
                'urlLink1',
                'urlLink2',
                'urlLink3',

                'link0_label',
                'link1_label',
                'link2_label',
                'link3_label',

                //Datos Abiertos
                'link_url_1',
                'link_url_2',
                'link_url_3',

                'link_label_1',
                'link_label_2',
                'link_label_3',

            ], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            //Generales
            'adminUrl' => 'Url Administrativo',
            'frontend_url' => 'Url Frontend',
            'correoInstitucionalUrl' => 'Url Correo Institucional',
            'siteMainTitle' => 'Titulo del Sitio',
            'facebookUrl' => 'Url de Facebook',
            'twitterUrl' => 'Url de Twitter',
            'copyString' => 'Texto derechos de Copia',
            'email' => 'Correo Institucional',
            'pagination' => 'Paginación',

            //Blue box
            'urlLink0' => 'Url link 0 página inicial',
            'urlLink1' => 'Url link 1 página inicial',
            'urlLink2' => 'Url link 2 página inicial',
            'urlLink3' => 'Url link 3 página inicial',

            'link0_label' => 'Etiqueta link 0 página inicial',
            'link1_label' => 'Etiqueta link 1 página inicial',
            'link2_label' => 'Etiqueta link 2 página inicial',
            'link3_label' => 'Etiqueta link 3 página inicial',

            //Datos Abiertos
            'link_url_1' => 'Url 1 Datos Abiertos',
            'link_url_2' => 'Url 2 Datos Abiertos',
            'link_url_3' => 'Url 3 Datos Abiertos',

            'link_label_1' => 'Label 1 Datos Abiertos', //Logros
            'link_label_2' => 'Label 2 Datos Abiertos', //Financiero
            'link_label_3' => 'Label 3 Datos Abiertos', //Demografía
        ];
    }
}