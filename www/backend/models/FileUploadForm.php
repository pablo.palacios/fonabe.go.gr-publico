<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class FileUploadForm
 *
 * @package backend\models
 */
class FileUploadForm extends Model {

    public $documentFile;

    public function rules() {
        return [
            [['documentFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf, doc, docx,xls,xlsx'],
        ];
    }

    /**
     * Carga del documento
     * @return null
     */
    public function upload() {
        $unique_name = uniqid('', true);
        if ($this->validate()) {
            $file = $unique_name . '.' . $this->documentFile->extension;
            $path = Yii::getAlias('@frontend') . '\web\file-uploads\\';
            $this->documentFile->saveAs($path . $file);
            $this->documentFile->name = $file;
            return $this->documentFile;
        } else {
            return null;
        }
    }

}
