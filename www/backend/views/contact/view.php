<?php
/* @var $this yii\web\View */
/* @var $model common\models\Contact */

use yii\helpers\Html;
use kartik\detail\DetailView;

$this->title = $model->first_name . ' ' . $model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Consultas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->first_name . ' ' . $model->last_name;
?>
<div class="well">
    <?php
    echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'first_name',
            'last_name',
            'identification',
            'phone',
            'email:email',
            'subject',
            'detail',
            'created_at:date',
        ],
    ])
    ?>
    <div class="form-group">
<?php echo Html::a('Cerrar', ['index'], ['class' => 'btn btn-warning']); ?>
    </div>
</div>
