<?php

use kartik\grid\GridView;
use kartik\grid\ActionColumn;
use yii2mod\alert\Alert;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Consultas';
$this->params['breadcrumbs'][] = $this->title;
echo Alert::widget();
?>
<div class="well">
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'autoXlFormat'=>true,
        'export'=>[
            'fontAwesome'=>true,
            'showConfirmAlert'=>false,
            'target'=>GridView::TARGET_BLANK
        ],

        'responsive' => true,
        'hover' => true,
        'pjax' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'first_name',
            'last_name',
            'email:email',
            'subject',
            'created_at:date',
            [
                'class' => ActionColumn::className(),
                'template' => '{view}',
            ]
        ],
        'panel'=>[
            'type'=>'primary',
            'heading'=>'Consultas recibidas'
        ],
        'exportConfig' => [ 'html'=>false, 'csv'=>true, 'txt'=>true, 'xls'=>false,   'json'=>false,],
    ]);
    ?>
</div>
