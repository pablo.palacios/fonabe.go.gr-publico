<?php
/* @var $model \backend\models\SettingsForm */
/* @var $this \yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii2mod\alert\Alert;

$this->title = 'Configuraciones Generales de FONABE';
echo Alert::widget();
?>
<div class="well">
<?php
$form = ActiveForm::begin([
    'action' => 'manage-settings/update',
    'id' => 'settings-form',
]);

echo $form->field($model, 'adminUrl');
echo $form->field($model, 'frontend_url');
echo $form->field($model, 'correoInstitucionalUrl');
echo $form->field($model, 'siteMainTitle');
echo $form->field($model, 'facebookUrl');
echo $form->field($model, 'twitterUrl');
echo $form->field($model, 'copyString');
echo $form->field($model, 'email');
echo $form->field($model, 'pagination');

//Blue box
echo $form->field($model, 'link0_label');
echo $form->field($model, 'urlLink0');

echo $form->field($model, 'link1_label');
echo $form->field($model, 'urlLink1');

echo $form->field($model, 'link2_label');
echo $form->field($model, 'urlLink2');

echo $form->field($model, 'link3_label');
echo $form->field($model, 'urlLink3');



echo $form->field($model, 'link_label_1');
echo $form->field($model, 'link_url_1');

echo $form->field($model, 'link_label_2');
echo $form->field($model, 'link_url_2');

echo $form->field($model, 'link_label_3');
echo $form->field($model, 'link_url_3');

?>
    <div class="form-group">
        <?php echo  Html::a('Cancelar', ['/'], ['class' => 'btn btn-warning']);?>
        <?php echo  Html::submitButton('Guardar', [
                'data-confirm'=> 'Está seguro que desea sobre escribir la información?',
            'class' => 'btn btn-success'
        ]); ?>
    </div>
<?php
ActiveForm::end();
?>
</div>
