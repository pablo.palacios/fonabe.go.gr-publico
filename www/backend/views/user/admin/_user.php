<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */


echo $form->field($user, 'email')->textInput(['maxlength' => 255]);
echo $form->field($user, 'username')->textInput(['maxlength' => 255]);
echo $form->field($user, 'password')->passwordInput();
