<?php
/*
 * This file is part of the Dektrium project
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */
?>

<?php $this->beginContent('@dektrium/user/views/admin/update.php', ['user' => $user]) ?>

<table class="table">
    <tr>
        <td>
            <strong>
                <?php echo Yii::t('user', 'Registration time') ?>:
            </strong>
        </td>
        <td><?php echo Yii::t('user', '{0, date, MMMM dd, YYYY HH:mm}', [$user->created_at]) ?>
        </td>
    </tr>
    <?php if ($user->registration_ip !== null) { ?>
        <tr>
            <td>
                <strong>
                    <?php echo Yii::t('user', 'Registration IP') ?>:
                </strong>
            </td>
            <td>
                <?php echo $user->registration_ip ?>
            </td>
        </tr>
    <?php } ?>
    <tr>
        <td>
            <strong>
                <?php echo Yii::t('user', 'Confirmation status') ?>:
            </strong>
        </td>
        <?php if ($user->isConfirmed) { ?>
            <td class="text-success">
                <?php echo Yii::t('user', 'Confirmed at {0, date, MMMM dd, YYYY HH:mm}', [$user->confirmed_at]) ?>
            </td>
        <?php } else { ?>
            <td class="text-danger">
                <?php echo Yii::t('user', 'Unconfirmed') ?>
            </td>
        <?php } ?>
    </tr>
    <tr>
        <td>
            <strong>
                <?php echo Yii::t('user', 'Block status') ?>:
            </strong>
        </td>
        <?php if ($user->isBlocked) { ?>
            <td class="text-danger">
                <?php echo Yii::t('user', 'Blocked at {0, date, MMMM dd, YYYY HH:mm}', [$user->blocked_at]) ?>
            </td>
        <?php } else { ?>
            <td class="text-success">
                <?php echo Yii::t('user', 'Not blocked') ?>
            </td>
        <?php } ?>
    </tr>
</table>

<?php
$this->endContent();
