<?php

use yii\bootstrap\Alert;
/*
 * This file is part of the Dektrium project
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */
use dektrium\rbac\widgets\Assignments;

$this->beginContent('@dektrium/user/views/admin/update.php', ['user' => $user]);
echo Alert::widget([
    'options' => [
        'class' => 'alert-info alert-dismissible',
    ],
    'body' => Yii::t('user', 'You can assign multiple roles or permissions to user by using the form below'),
]);
echo Assignments::widget(['userId' => $user->id]);
$this->endContent();
