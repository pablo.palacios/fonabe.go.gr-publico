<?php
/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var dektrium\user\models\User
 */
echo Yii::t('user', 'Hola');
?>,

<?php echo Yii::t('user', 'Su cuenta {0} tiene una nueva contraseña', Yii::$app->name) ?>.
<?php echo Yii::t('user', 'Se ha generado la siguiente contraseña: ') ?>:
<?php echo $user->password ?>

<?php echo Yii::t('user', 'Si no ha solicitado este email, por favor ignorarlo') ?>.
