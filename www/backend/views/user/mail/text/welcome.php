<?php
/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var dektrium\user\models\User
 */
echo Yii::t('user', 'Hola, ')
?>,

<?= Yii::t('user', 'Su cuenta en FONABE {0} ha sido creada', Yii::$app->name) ?>.
<?php if ($module->enableGeneratingPassword): ?>
    <?= Yii::t('user', 'Hemos generado la siguiente contraseña para usted') ?>:
    <?= $user->password ?>
<?php endif ?>

<?php if ($token !== null): ?>
    <?= Yii::t('user', 'In order to complete your registration, please click the link below') ?>.

    <?= $token->url ?>

    <?= Yii::t('user', 'If you cannot click the link, please try pasting the text into your browser') ?>.
<?php endif ?>

<?= Yii::t('user', 'If you did not make this request you can ignore this email') ?>.
