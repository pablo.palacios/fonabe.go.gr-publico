<?php
/* @var $this yii\web\View */
/* @var $model common\models\Info */
/* @var $form yii\widgets\ActiveForm */

use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->registerJs("CKEDITOR.plugins.addExternal('sourcearea', '/sourcearea/plugin.js', '');");
$this->registerJs("CKEDITOR.plugins.addExternal('sourcedialog', '/sourcedialog/plugin.js', '');");
?>
<div class="info-form">
    <?php $form = ActiveForm::begin();
    echo $form->field($model, 'page_id')->dropDownList(
        $pages,
        ['prompt' => 'Seleccione la página...'],
        ['options' =>
            [
                $model->id => ['selected' => true]
            ]
        ]
    )->label('Página relacionada');

    echo $form->field($model, 'title')->textInput();
    echo  $form->field($model, 'excerpt')
        ->widget(CKEditor::className(), [
            'options' => ['rows' => 4],
            'preset' => 'custom',
            'clientOptions' => [
                'extraPlugins' => 'sourcearea',
            ]
        ]);
    echo  $form->field($model, 'content')
        ->widget(CKEditor::className(), [
            'options' => ['rows' => 6],
            'preset' => 'custom',
            'clientOptions' => [
                'extraPlugins' => 'sourcearea',
            ]
        ]); ?>

    <div class="form-group">
        <?php echo  Html::a('Cerrar', ['index'], ['class' => 'btn btn-warning']);?>
        <?php echo  Html::submitButton('Guardar', ['class' => 'btn btn-success']); ?>
    </div>
    <?php
    ActiveForm::end();
    ?>
</div>

