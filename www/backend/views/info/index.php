<?php
/* @var $this yii\web\View */
/* @var $searchModel common\models\InfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\grid\ActionColumn;
use yii2mod\alert\Alert;

$this->title = 'Páginas de Información Adicional';
$this->params['breadcrumbs'][] = $this->title;
echo Alert::widget();
?>
<div class="well">
    <?php
    echo Html::tag('p', Html::a('Crear item de información adicional', ['create'], ['class' => 'btn btn-success']));
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsive'=>true,
        'hover'=>true,
        'pjax'=>true,
        'columns' => [

                [
                    'attribute'=> 'title',
                    'format'=> 'raw',
                    'value'=>function($data) {
                        if(!empty($data->page->category->title)){
                         return $data->page->category->title . ' / ' . $data->page->title . ' / ' . $data->title;                            
                        }
                    },
                ],
                [
                    'class' => ActionColumn::className(),
                    'template' => '{update}{delete}',
                    'buttons' => [
                        'delete' => function($url, $model){
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], [
                                'class' => '',
                                'data' => [
                                    'title' => 'Confirmación',
                                    'confirm' => 'Está seguro, ¿Desea eliminar el evento seleccionado?.',
                                    'method' => 'post',
                                ],
                            ]);
                        }
                    ]
                ]

        ],
    ]); ?>
</div>
