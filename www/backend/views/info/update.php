<?php

use yii\helpers\Html;

$this->title = 'Editar página de información';
$this->params['breadcrumbs'][] = ['label' => 'Información', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

echo Html::tag('div', $this->render('_form', ['model' => $model, 'pages' => $pages,]), ['class' => 'well']);
