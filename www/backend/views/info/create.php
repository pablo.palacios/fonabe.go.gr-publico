<?php
/* @var $this yii\web\View */
/* @var $model common\models\Info */

use yii\helpers\Html;

$this->title = 'Crear página de información';
$this->params['breadcrumbs'][] = ['label' => 'Información', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

echo Html::tag('div', $this->render('_form', [ 'model' => $model, 'pages' => $pages,]), ['class' => 'well']);