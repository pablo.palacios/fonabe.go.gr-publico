<?php

use yii\helpers\Html;

$this->title = $name;
?>
<section class="content">

    <div class="error-page">
        <h2 class="headline text-info"><i class="fa fa-warning text-yellow"></i></h2>

        <div class="error-content">
            <h3><?php echo  $name ?></h3>
            <p>
                <?php echo  nl2br(Html::encode($message)) ?>
            </p>
        </div>
    </div>
</section>
