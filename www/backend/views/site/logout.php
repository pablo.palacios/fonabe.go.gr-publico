<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
$this->title = 'Salir de la administración';
?>

<div class="well">
    <?php
    $form = ActiveForm::begin(['action' => '/user/logout']);;
    ?>
    <div class="form-group">
        <?php echo Html::submitButton('SALIR', ['class' => 'btn btn-success']); ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
