<?php

use yii\helpers\Html;

$this->title = "Editando: {$model->title}";
$this->params['breadcrumbs'][] = ['label' => 'Eventos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id];
$this->params['breadcrumbs'][] = 'Editar';

echo Html::tag('div', $this->render('_form', ['model' => $model,]), ['class' => 'well', 'style' => 'max-width: 850px;']);

