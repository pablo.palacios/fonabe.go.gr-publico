<?php
/* @var $this yii\web\View */
/* @var $model common\models\Events */
/* @var $form yii\widgets\ActiveForm */

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\widgets\DatePicker;
use kartik\widgets\TimePicker;
use kartik\widgets\RangeInput;

$now_dt = new DateTime();



$form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]);
?>
<div class="row">
    <div class="col-md-12">
<?php
echo Form::widget([
    'model'=>$model,
    'form'=>$form,
    'columns'=>1,
    'attributes'=>[
        'title'=>['type'=>Form::INPUT_TEXT, 'options'=>['placeholder'=>'Escriba el títu;o de su evento']],
        'description'=>['type'=>Form::INPUT_TEXTAREA, 'options'=>['placeholder'=>'Escriba los detalles del evento']],
    ]
]);
?>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <label class="control-label">Fecha del evento</label>
        <?php
        echo DatePicker::widget([
            'model'=>$model,
            'attribute'=>'date',
            'type' => DatePicker::TYPE_INLINE,
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd'
            ],
            'options' => [
                'class' => 'hide'
            ]
        ]);
        ?>
    </div>
    <div class="col-md-6 center-block">
        <label class="control-label">Hora de inicio del evento</label>
        <?php

        echo TimePicker::widget([
            'model'=>$model,
            'attribute'=>'time',
            'value' => (!isset($model->date)) ? false: Yii::$app->formatter->format($model->date, 'date'),
            'pluginOptions' => [
                'minuteStep' => 30,
                'showSeconds' => false,
                'showMeridian' => false
            ],
            'options' => [
                'readonly' => true,
            ],
        ]);?>
        <br>
        <label class="control-label">Duración del evento</label>
        <?php
        echo RangeInput::widget([
            'model'=>$model,
            'attribute'=>'hours',
            'value' => 1,
            'options' => [
                    'placeholder' => 'Horas (1 a 8)',
                    'readonly' => true,
                ],
            'html5Options' => ['min' => 1, 'max' => 8],
            'addon' => ['append' => ['content' => 'horas']]
        ]);

        echo '</div>';

        ?>

    </div>
</div>
<div class="form-group">
    <?php echo Html::a('Cancelar', ['index'], ['class' => 'btn btn-warning']); ?>
    <?php echo Html::submitButton('Guardar', ['class' => 'btn btn-success']); ?>
</div>
<?php ActiveForm::end(); ?>
