<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\grid\ActionColumn;
use yii2mod\alert\Alert;

/* @var $this yii\web\View */
/* @var $searchModel common\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Eventos';
$this->params['breadcrumbs'][] = $this->title;
echo Alert::widget();
?>
<div class="well">
    <?php

    echo Html::tag('p', Html::a('Crear evento nuevo', ['create'], ['class' => 'btn btn-success']));
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsive'=>true,
        'hover'=>true,
        'pjax'=>true,
        'columns' => [
            [
                'attribute' => 'title',
                'filter' => true,
                'width' => '300px',
            ],
            [
                'attribute' => 'date',
                'filter' => false,
                'format'=>'date',
            ],
            [
                'attribute' => 'time',
                'filter' => false,
                'format'=>'time'
            ],
            [
                'attribute' => 'hours',
                'filter' => false,
                'format'=>'integer'
            ],
            [
                'class' => ActionColumn::className(),
                'template' => '{update}{delete}',
                'buttons' => [
                    'delete' => function($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], [
                            'class' => '',
                            'data' => [
                                'title' => 'Confirmación',
                                'confirm' => 'Está seguro, ¿Desea eliminar el evento seleccionado?.',
                                'method' => 'post',
                            ],
                        ]);
                    }
                ]
            ]
        ],
    ]);

    ?>
</div>
