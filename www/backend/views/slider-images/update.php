<?php

use yii\helpers\Html;

$this->title = "Actualizando: {$model->slide_text}";
$this->params['breadcrumbs'][] = ['label' => 'Imágenes Slider', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id];
$this->params['breadcrumbs'][] = 'Editar';

echo Html::tag('div', $this->render('_form', ['model' => $model, 'uploadModel' => $uploadModel,]), ['class' => 'well']);
