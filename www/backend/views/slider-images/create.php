<?php
/* @var $this yii\web\View */
/* @var $model common\models\SliderImages */
/* @var $uploadModel \backend\models\ImageUploadForm */

use yii\helpers\Html;

$this->title = 'Cargar una imagen';
$this->params['breadcrumbs'][] = ['label' => 'Imágenes Slider', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
echo Html::tag('div', $this->render('_form', [ 'model' => $model, 'uploadModel' => $uploadModel, ]), ['class' => 'well']);
