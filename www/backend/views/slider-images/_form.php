<?php
/* @var $this yii\web\View */
/* @var $model common\models\SliderImages */
/* @var $form yii\widgets\ActiveForm */
/* @var $uploadModel \backend\models\ImageUploadForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

$this->registerJsFile("/js/backend_slider.js", ['depends' => [\yii\web\JqueryAsset::className()]]);

$form = ActiveForm::begin([
                'options' => ['enctype' => 'multipart/form-data']
        ]);
echo $form->field($model, 'image_link')->textInput();
echo $form->field($model, 'slide_text')->textInput();

echo Html::activeLabel($model, 'image_url');
echo  Html::error($uploadModel, 'imageFile',['class'=>'text-danger']);
echo FileInput::widget([
    'model' => $uploadModel,
    'attribute' => 'imageFile',
    'pluginOptions' => [
        'showCaption' => false,
        'showRemove' => false,
        'showUpload' => true,
        'browseLabel' =>  ' Selecciona la imagen',
        'uploadLabel' =>  ' SUBIR LA IMAGEN Y GUARDAR ',
        'uploadClass' =>  'btn btn-primary',
        'uploadAsync'=> true,
    ],
    'options' => [
        'id' => 'gt-uploaderImg',
        'multiple' => false
    ]
]);
?>
<br>
<div class="form-group">
    <?php echo  Html::submitButton('Guardar', ['class' => 'btn btn-success','id' => 'submit-btn',]); ?>
    <?php echo Html::a('Cancelar', ['index'], ['class' => 'btn btn-warning']); ?>
</div>
<?php ActiveForm::end(); ?>


