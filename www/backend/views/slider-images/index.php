<?php
/* @var $this yii\web\View */
/* @var $searchModel common\models\SliderImagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\grid\ActionColumn;

$this->title = 'Imágenes del Slider';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="well">
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsive'=>true,
        'hover'=>true,
        'pjax'=>true,
        'columns' => [
            'slide_text',
            [
                'attribute' => 'image_link',
                'filter' => false,
                'format'=>'url'
            ],
            [
                'attribute' => 'image_url',
                'filter' => false,
                'format' => 'html',
                'value'=>function($data) {
                        $settings = Yii::$app->settings;
                        $path = $settings->get('main', 'frontend_url') . '/img-uploads/'. $data->image_url;
                        return Html::img($path, ['style'=>'width:100px']);
                    },
            ],
            [
                'class' => ActionColumn::className(),
                'template' => '{update}',
            ]
        ],
    ]);
    ?>
</div>