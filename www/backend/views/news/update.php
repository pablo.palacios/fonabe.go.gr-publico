<?php

use yii\helpers\Html;

$this->title = "Edición: {$model->title}";
$this->params['breadcrumbs'][] = ['label' => 'Noticias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="well">

    <?php
    echo $this->render('_form', [
        'model' => $model,
        'uploadModel' => $uploadModel,
    ])
    ?>

</div>
