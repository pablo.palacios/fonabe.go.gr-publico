<?php
$this->title = 'Nueva Noticia';
$this->params['breadcrumbs'][] = ['label' => 'Noticias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="well">
    <?php echo  $this->render('_form', [
        'model' => $model,
        'uploadModel' => $uploadModel,
    ]) ?>
</div>
