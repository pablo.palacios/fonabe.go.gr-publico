<?php

use kartik\editable\Editable;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\grid\ActionColumn;
use yii2mod\alert\Alert;
use yii\helpers\BaseStringHelper;

$this->title = 'Administración Noticias';
$this->params['breadcrumbs'][] = $this->title;
echo Alert::widget();
?>
<div class="well">
    <p>
        <?php echo Html::a('Crear una noticia nueva', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsive' => true,
        'hover' => true,
        'pjax' => true,
        'columns' => [
            [
                'attribute' => 'title',
                'value' => function($model) {
                    return BaseStringHelper::truncate($model->title, 50);
                }
            ],
            [
                'attribute' => 'slug',
                'value' => function($model) {
                    return BaseStringHelper::truncate($model->slug, 50);
                }
            ],
            'author_name',
            [
                'attribute' => 'updated_at',
                'filter' => false,
                'format' => 'datetime'
            ],
            [
                'attribute' => 'created_at',
                'filter' => false,
                'format' => 'datetime'
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'published',
                'filter' => true,
                'editableOptions' => [
                    'header' => '¿Publicado?',
                    'size' => 'sm',
                    'formOptions' => ['action' => ['/news/published']],
                    'inputType' => Editable::INPUT_SWITCH,
                    'options' => [
                        'pluginOptions' => [
                            'onText' => 'Si',
                            'offText' => 'No',
                            'autoclose' => true
                        ]
                    ],
                ],
                'format' => ['boolean'],
            ],
            [
                'attribute' => 'url_imagen',
                'filter' => false,
                'format' => 'html',
                'value' => function($data) {
                    $settings = Yii::$app->settings;
                    $path = $settings->get('main', 'frontend_url') . '/img-uploads/' . $data->url_imagen;
                    return Html::img($path, ['style' => 'width:100px']);
                },
            ],
            [
                'class' => ActionColumn::className(),
                'template' => '{update}{delete}',
                'buttons' => [
                    'delete' => function($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], [
                                    'class' => '',
                                    'data' => [
                                        'title' => 'Confirmación',
                                        'confirm' => 'Está seguro, ¿Desea eliminar el evento seleccionado?.',
                                        'method' => 'post',
                                    ],
                        ]);
                    }
                ]
            ]
        ],
    ])
    ?>
</div>
