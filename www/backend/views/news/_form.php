<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use kartik\widgets\Spinner;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\SliderImages */
/* @var $form yii\widgets\ActiveForm */
/* @var $uploadModel \backend\models\ImageUploadForm */

$this->registerJs("CKEDITOR.plugins.addExternal('sourcearea', '/sourcearea/plugin.js', '');");
$this->registerJs("CKEDITOR.plugins.addExternal('sourcedialog', '/sourcedialog/plugin.js', '');");
$this->registerJsFile("/js/backend_news.js", ['depends' => [\yii\web\JqueryAsset::className()]]);
$isEditable = false;
?>
<div id="main-form" style="display: none">
    <?php
    $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]);
    echo $form->field($model, 'title')->textInput(['readonly'=>$isEditable, 'disabled' => $isEditable]);
    echo $form->field($model, 'slug')->textInput(['readonly'=>true]);
    echo $form->field($model, 'extract')->textarea();
    echo  $form->field($model, 'content')
        ->widget(CKEditor::className(), [
            'options' => ['rows' => 6],
            'preset' => 'custom',
            'clientOptions' => [
                'extraPlugins' => 'sourcearea',
            ]
        ]);
    echo  $form->field($model, 'author_name')->textInput();
    echo Html::activeLabel($model, 'image_url');
    echo  Html::error($uploadModel, 'imageFile',['class'=>'text-danger']);
    echo FileInput::widget([
        'model' => $uploadModel,
        'attribute' => 'imageFile',
        'pluginOptions' => [
            'showCaption' => false,
            'showRemove' => false,
            'showUpload' => true,
            'browseLabel' =>  ($model->isNewRecord)?' Selecciona la imagen para GUARDAR':' Seleccione una imagen, si desea cambiar la actual',
            'uploadLabel' =>  ' SUBIR LA IMAGEN Y GUARDAR ',
            'uploadClass' =>  'btn btn-primary',
            'uploadAsync'=> true,
        ],
        'options' => [
            'id' => 'gt-uploaderImg',
            'multiple' => false
        ]
    ]);
    ?>
    <br>
    <div class="form-group">
        <?php if (!$model->isNewRecord){
            echo  Html::submitButton('Guardar', ['class' => 'btn btn-success','id' => 'submit-btn',]);
        } ?>
        <?php echo  Html::a('Cancelar', ['index'], ['class' => 'btn btn-warning']);?>
    </div>
    <?php
    ActiveForm::end();
    ?>
</div>
<div id="spinner">
    <?php echo Spinner::widget(['preset' => 'large', 'align' => 'center']);?>
</div>