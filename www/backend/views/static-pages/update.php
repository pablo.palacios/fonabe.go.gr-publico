<?php
$this->title = "Actualizar: {$model->category->title} / {$model->title}";
$this->params['breadcrumbs'][] = ['label' => 'Páginas estáticas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="well">
    <?php
    echo $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
