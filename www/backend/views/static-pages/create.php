<?php
use yii\helpers\Html;
$this->title = 'Crear una página nueva';
$this->params['breadcrumbs'][] = ['label' => 'Páginas estáticas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="static-pages-create">
    <?php echo  $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
