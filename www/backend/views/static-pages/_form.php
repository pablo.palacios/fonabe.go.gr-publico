<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use kartik\widgets\Spinner;
use  \yii2mod\alert\Alert;
echo Alert::widget();

$this->registerJs("CKEDITOR.plugins.addExternal('sourcearea', '/sourcearea/plugin.js', '');");
$this->registerJs("CKEDITOR.plugins.addExternal('sourcedialog', '/sourcedialog/plugin.js', '');");
$this->registerJsFile("/js/backend_static_pages.js", ['depends' => [\yii\web\JqueryAsset::className()]]);
$isEditable = $model->editable;
?>
<div id="main-form" style="display: none">
    <?php
    $form = ActiveForm::begin();
    echo $form->field($model, 'title')->textInput(['readonly'=>$isEditable, 'disabled' => $isEditable]);
    echo $form->field($model, 'slug')->textInput(['readonly'=>$isEditable, 'disabled' => $isEditable]);
    echo $form->field($model, 'excerpt')->textarea();
    echo  $form->field($model, 'content')
        ->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'custom',
        'clientOptions' => [
            'extraPlugins' => 'sourcearea',
            'allowedContent' => true,
        ]
    ]);
    ?>
    <div class="form-group">
        <?php echo  Html::a('Cerrar', ['index'], ['class' => 'btn btn-warning']);?>
        <?php echo  Html::submitButton('Guardar', ['class' => 'btn btn-success']); ?>
    </div>
    <?php
    ActiveForm::end();
    ?>
</div>
<div id="spinner">
    <?php echo Spinner::widget(['preset' => 'large', 'align' => 'center']);?>
</div>