<?php
/* @var $this yii\web\View */
/* @var $searchModel common\models\StaticPagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\grid\GridView;
use kartik\grid\ActionColumn;
use  \yii2mod\alert\Alert;

echo Alert::widget();
$this->title = 'Páginas Estáticas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="well">
    <?php
    echo  GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'responsive'=>true,
            'hover'=>true,
            'pjax'=>true,
            'columns' => [
                [
                    'attribute'=> 'title',
                    'format'=> 'raw',
                    'value'=>function($data) {
                        return $data->category->title . ' / ' . $data->title;
                    },
                ],
                'slug',
                [
                    'class' => ActionColumn::className(),
                    'template' => '{update}',
                ]
            ],
        ]); ?>
</div>
