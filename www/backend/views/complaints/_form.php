<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Complaints */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="complaints-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo  $form->field($model, 'first_name')->textInput() ?>

    <?php echo  $form->field($model, 'last_name')->textInput() ?>

    <?php echo  $form->field($model, 'identification_card')->textInput() ?>

    <?php echo  $form->field($model, 'phone')->textInput() ?>

    <?php echo  $form->field($model, 'address')->textInput() ?>

    <?php echo  $form->field($model, 'email')->textInput() ?>

    <?php echo  $form->field($model, 'fax')->textInput() ?>

    <?php echo  $form->field($model, 'place_of_work')->textInput() ?>

    <?php echo  $form->field($model, 'details')->textInput() ?>

    <?php echo  $form->field($model, 'reported_name_01')->textInput() ?>

    <?php echo  $form->field($model, 'reported_name_02')->textInput() ?>

    <?php echo  $form->field($model, 'reported_name_03')->textInput() ?>

    <?php echo  $form->field($model, 'reported_location_01')->textInput() ?>

    <?php echo  $form->field($model, 'reported_location_02')->textInput() ?>

    <?php echo  $form->field($model, 'reported_location_03')->textInput() ?>

    <?php echo  $form->field($model, 'evidence_01')->textInput() ?>

    <?php echo  $form->field($model, 'evidence_02')->textInput() ?>

    <?php echo  $form->field($model, 'evidence_03')->textInput() ?>

    <?php echo  $form->field($model, 'evidence_location_01')->textInput() ?>

    <?php echo  $form->field($model, 'evidence_location_02')->textInput() ?>

    <?php echo  $form->field($model, 'evidence_location_03')->textInput() ?>

    <?php echo  $form->field($model, 'witness_01')->textInput() ?>

    <?php echo  $form->field($model, 'witness_02')->textInput() ?>

    <?php echo  $form->field($model, 'witness_03')->textInput() ?>

    <?php echo  $form->field($model, 'witness_location_01')->textInput() ?>

    <?php echo  $form->field($model, 'witness_location_02')->textInput() ?>

    <?php echo  $form->field($model, 'witness_location_03')->textInput() ?>

    <div class="form-group">
        <?php echo  Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
