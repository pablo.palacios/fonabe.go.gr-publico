<?php
/* @var $this yii\web\View */
/* @var $searchModel common\models\ComplaintsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\grid\GridView;
use kartik\grid\ActionColumn;
use yii2mod\alert\Alert;

$this->title = 'Denuncias';
$this->params['breadcrumbs'][] = $this->title;
echo Alert::widget();
?>
<div class="well">
    <?php echo  GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'autoXlFormat'=>true,
        'export'=>[
            'fontAwesome'=>true,
            'showConfirmAlert'=>true,
            'target'=>GridView::TARGET_BLANK
        ],

        'responsive'=>true,
        'hover'=>true,
        'pjax'=>true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'first_name',
            'last_name',
            'identification_card',
            'email:email',
            'created_at:date',
            [
                'class' => ActionColumn::className(),
                'template' => '{view}',
            ]
        ],
        'panel'=>[
            'type'=>'primary',
            'heading'=>'Denuncias recibidas'
        ],
        'exportConfig' => [ 'html'=>false, 'csv'=>true, 'txt'=>true, 'xls'=>false,   'json'=>false,],
    ]); ?>
</div>
