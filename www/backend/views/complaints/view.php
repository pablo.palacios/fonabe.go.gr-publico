<?php
/* @var $this yii\web\View */
/* @var $model common\models\Complaints */

use yii\helpers\Html;
use kartik\detail\DetailView;

$this->title = $model->first_name . ' ' . $model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Denuncias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->first_name . ' ' . $model->last_name;
?>
<div class="well">
    <?php echo  DetailView::widget([
        'model' => $model,
        'attributes' => [
            'first_name',
            'last_name',
            'identification_card',
            'phone',
            'address',
            'email:email',
            'fax',
            'place_of_work',
            'details',
            'reported_name_01',
            'reported_name_02',
            'reported_name_03',
            'reported_location_01',
            'reported_location_02',
            'reported_location_03',
            'evidence_01',
            'evidence_02',
            'evidence_03',
            'evidence_location_01',
            'evidence_location_02',
            'evidence_location_03',
            'witness_01',
            'witness_02',
            'witness_03',
            'witness_location_01',
            'witness_location_02',
            'witness_location_03',
        ],
    ]) ?>
    <div class="form-group">
        <?php echo Html::a('Cerrar', ['index'], ['class' => 'btn btn-warning']); ?>
    </div>
</div>
