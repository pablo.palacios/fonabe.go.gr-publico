<?php

use yii\helpers\Html;

$this->title = 'Crear un nuevo histórico';
$this->params['breadcrumbs'][] = ['label' => 'Históricos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
echo Html::tag('div', $this->render('_form', ['model' => $model,]), ['class' => 'well']);
