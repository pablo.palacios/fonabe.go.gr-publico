<?php
/* @var $this yii\web\View */
/* @var $model common\models\History */

/* @var $form yii\widgets\ActiveForm */

use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii2mod\alert\Alert;

echo Alert::widget();

$this->registerJs("CKEDITOR.plugins.addExternal('sourcearea', '/sourcearea/plugin.js', '');");
$this->registerJs("CKEDITOR.plugins.addExternal('sourcedialog', '/sourcedialog/plugin.js', '');");
$this->registerJsFile("/js/backend_static_pages.js", ['depends' => [\yii\web\JqueryAsset::className()]]);

$form = ActiveForm::begin();
echo $form->field($model, 'year')->textInput();
echo $form->field($model, 'title')->textInput();
echo $form->field($model, 'content')
    ->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'custom',
        'clientOptions' => [
            'extraPlugins' => 'sourcearea',
        ]
    ]);

?>
<div class="form-group">
    <?php echo Html::a('Salir', ['index'], ['class' => 'btn btn-warning']); ?>
    <?php echo Html::submitButton('Guardar', ['class' => 'btn btn-success']); ?>
</div>
<?php ActiveForm::end(); ?>
