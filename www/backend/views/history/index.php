<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\grid\ActionColumn;

$this->title = 'Historias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="well">
    <p>
        <?php echo Html::a('Crear un Histórico', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'year',
            'title',
            [
                'class' => ActionColumn::className(),
                'template' => '{update} {delete}',
                'buttons' => [
                    'delete' => function($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], [
                                    'class' => '',
                                    'data' => [
                                        'title' => 'Confirmación',
                                        'confirm' => 'Está seguro, ¿Desea eliminar el elemento seleccionado?.',
                                        'method' => 'post',
                                    ],
                        ]);
                    }
                ]
            ]
        ],
    ]);
    ?>
</div>
