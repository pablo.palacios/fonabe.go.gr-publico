<?php

use yii\helpers\Html;
use yii\helpers\BaseStringHelper;

$title = "Actualizando: {$model->title}";
$this->title = BaseStringHelper::truncateWords($title, 6);
$this->params['breadcrumbs'][] = ['label' => 'Histórico', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id];
$this->params['breadcrumbs'][] = 'Actualizar';

echo Html::tag('div', $this->render('_form', ['model' => $model,]), ['class' => 'well']);
