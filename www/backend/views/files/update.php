<?php

use yii\helpers\Html;
use \yii\helpers\BaseStringHelper;

$title = "Editar documento: {$model->title}";
$this->title = BaseStringHelper::truncateWords($title, 4);
$this->params['breadcrumbs'][] = ['label' => 'Documentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
echo Html::tag('div', $this->render('_form', ['model' => $model, 'pages' => $pages, 'uploadModel' => $uploadModel,]), ['class' => 'well']);
