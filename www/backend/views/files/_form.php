<?php

use kartik\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\FileInput;
use \yii\web\JqueryAsset;
use kartik\widgets\DepDrop;
use yii\helpers\Url;

$this->registerJsFile('/js/backend_files.js', ['depends' => [JqueryAsset::className()]]);
$now_dt = new DateTime();
$form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data']
        ]);

echo $form->field($model, 'page_id')->dropDownList(
        $pages, [
    'prompt' => 'Seleccione la página...'
        ], [
    'options' =>
    [
        $model->id => ['selected' => true]
    ],
        ]
)->label('Página relacionada');

// Dependent Dropdown
//Se carga el valor en caso de ser un update
echo $form->field($model, 'page_information_id')->widget(DepDrop::classname(), [
    'data' => (!$model->isNewRecord) ? [$model->page_information_id => 'selected'] : [],
    'options' => [
        'id' => 'info-id',
    ],
    'pluginOptions' => [
        'initialize' => true,
        'depends' => ['files-page_id'],
        'placeholder' => '',
        'url' => Url::to(['subpage'])
    ]
]);


echo $form->field($model, 'title')->textInput();
echo $form->field($model, 'description')->textInput();

echo Html::activeLabel($model, 'file_date');
echo Html::error($model, 'file_date', ['class' => 'text-danger']);

echo DatePicker::widget([
    'model' => $model,
    'attribute' => 'file_date',
    'type' => DatePicker::TYPE_INLINE,
    'value' => (!isset($model->file_date)) ? false : Yii::$app->formatter->format($model->file_date, 'date'),
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd'
    ],
    'options' => [
        'class' => 'hide'
    ]
]);
?>
<br>
<?php
echo Html::activeLabel($model, 'file_url');
echo Html::error($uploadModel, 'documentFile', ['class' => 'text-danger']);
echo FileInput::widget([
    'model' => $uploadModel,
    'attribute' => 'documentFile',
    'pluginOptions' => [
        'showCaption' => false,
        'showRemove' => false,
        'showUpload' => true,
        'browseLabel' => ($model->isNewRecord) ? ' Seleccione el documento para GUARDAR' : ' Seleccione un documento, si desea cambiar el actual',
        'uploadLabel' => ' SUBIR EL DOCUMENTO Y GUARDAR ',
        'uploadClass' => 'btn btn-primary',
        'uploadAsync' => true,
    ],
    'options' => [
        'id' => 'gt-uploaderImg',
        'multiple' => false,
        'options' => ['accept' => 'image/*']
    ]
]);
?>
<br>
<div class="form-group">
    <?php
    if (!$model->isNewRecord) {
        echo Html::submitButton('Guardar', ['class' => 'btn btn-success', 'id' => 'submit-btn',]);
    }
    ?>
    <?php echo Html::a('Cancelar', ['index'], ['class' => 'btn btn-warning']); ?>
</div>
<?php
ActiveForm::end();
