<?php
/* @var $this yii\web\View */
/* @var $model common\models\Files */
/* @var $pages common\models\StaticPages */

use yii\helpers\Html;

$this->title = 'Cargar documentos';
$this->params['breadcrumbs'][] = ['label' => 'Documentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

echo Html::tag('div', $this->render('_form', [ 'model' => $model,'pages' => $pages, 'uploadModel' => $uploadModel, ]), ['class' => 'well']);
