<?php
/* @var $this yii\web\View */
/* @var $searchModel common\models\FilesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\grid\ActionColumn;
use yii2mod\alert\Alert;

$this->title = 'Documentos';
$this->params['breadcrumbs'][] = $this->title;
echo Alert::widget();
?>
<div class="well">
    <?php
    echo Html::tag('p', Html::a('Nuevo documento', ['create'], ['class' => 'btn btn-success']));

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsive' => true,
        'hover' => true,
        'pjax' => true,
        'columns' => [
            [
                'label' => 'Página relacionada',
                'attribute' => 'page',
                'value' => function($model) {
                    if (!empty($model->page)) {
                        return $model->page->category->title . ' | ' . $model->page->title;
                    }
                }
            ],
            'title',
            [
                'attribute' => 'file_url',
                'filter' => false,
                'format' => 'text'
            ],
            [
                'attribute' => 'file_date',
                'filter' => false,
                'format' => 'date'
            ],
            [
                'attribute' => 'hit_counter',
                'filter' => false,
                'format' => 'integer'
            ],
            [
                'attribute' => 'download_counter',
                'filter' => false,
                'format' => 'integer'
            ],
            [
                'class' => ActionColumn::className(),
                'template' => '{update}{delete}',
                'buttons' => [
                    'delete' => function($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], [
                                    'class' => '',
                                    'data' => [
                                        'title' => 'Confirmación',
                                        'confirm' => 'Está seguro, ¿Desea eliminar el evento seleccionado?.',
                                        'method' => 'post',
                                    ],
                        ]);
                    }
                ]
            ]
        ],
    ]);
    ?>
</div>