<?php

use yii\grid\GridView;
use yii\widgets\Pjax;
use  \yii2mod\alert\Alert;

$this->title = 'Categorías';
$this->params['breadcrumbs'][] = $this->title;

echo Alert::widget();
?>
<div class="well">
    <?php Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            ['class' => yii\grid\ActionColumn::className(), 'template' => '{update}']
        ],
    ]);
    Pjax::end(); ?>
</div>
