<?php
$this->title = "Actualizar: {$model->title}";
$this->params['breadcrumbs'][] = ['label' => 'Editar categorías', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="well">
    <?php
    echo $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
