<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="category-form">
    <?php
    $form = ActiveForm::begin();
    echo  $form->field($model, 'title')->textInput();
    echo  $form->field($model, 'slug')->textInput();
    ?>
    <div class="form-group">
        <?php echo  Html::a('Cancelar', ['index'], ['class' => 'btn btn-warning']);?>
        <?php echo  Html::submitButton('Guardar', ['class' => 'btn btn-success']); ?>
    </div>
    <?php
    ActiveForm::end();
    ?>
</div>
