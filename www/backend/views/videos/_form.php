<?php

use dosamigos\ckeditor\CKEditor;
use kartik\widgets\Spinner;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Videos */
/* @var $form yii\widgets\ActiveForm */
$isEditable = false;
$this->registerJs("CKEDITOR.plugins.addExternal('sourcearea', '/sourcearea/plugin.js', '');");
$this->registerJs("CKEDITOR.plugins.addExternal('sourcedialog', '/sourcedialog/plugin.js', '');");
$this->registerJsFile("/js/backend_video.js", ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<div id="main-form" style="display: none">
    <?php
    $form = ActiveForm::begin();
    echo $form->field($model, 'page_id')->dropDownList(
        $pages,
        ['prompt' => 'Seleccione la página...'],
        ['options' =>
            [
                $model->id => ['selected' => true]
            ]
        ]
    )->label('Página relacionada');
    echo $form->field($model, 'title')->textInput(['readonly'=>$isEditable, 'disabled' => $isEditable]);
    echo $form->field($model, 'slug')->textInput(['readonly'=>true]);
    echo $form->field($model, 'extract')->textInput();
    echo  $form->field($model, 'content')
        ->widget(CKEditor::className(), [
            'options' => ['rows' => 6],
            'preset' => 'custom',
            'clientOptions' => [
                'extraPlugins' => 'sourcearea',
            ]
        ]);
    echo $form->field($model, 'url_video')->textInput();
    ?>
    <div class="form-group">
        <?php echo  Html::a('Cancelar', ['index'], ['class' => 'btn btn-warning']);?>
        <?php echo  Html::submitButton('Guardar', ['class' => 'btn btn-success']); ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<div id="spinner">
    <?php echo Spinner::widget(['preset' => 'large', 'align' => 'center']); ?>
</div>
