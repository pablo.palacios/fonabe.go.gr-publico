<?php
$this->title = 'Nuevo vídeo';
$this->params['breadcrumbs'][] = ['label' => 'Video', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="well">
    <?php echo  $this->render('_form', [
        'model' => $model,'pages' => $pages
    ]) ?>
</div>
