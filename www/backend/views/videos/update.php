<?php
$this->title = "Edición: {$model->title}";
$this->params['breadcrumbs'][] = ['label' => 'Vídeo', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="well">
    <?php
    echo $this->render('_form', [
        'model' => $model, 'pages' => $pages
    ])
    ?>
</div>
