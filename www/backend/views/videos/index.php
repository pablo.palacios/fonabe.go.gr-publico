<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\grid\ActionColumn;

$this->title = 'Videos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="well">
    <p>
        <?php echo Html::a('Cargar un vídeo nuevo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsive' => true,
        'hover' => true,
        'pjax' => true,
        'columns' => [
            [
                'label' => 'Página relacionada',
                'attribute' => 'page',
                'value' => function($model) {
                    if (!empty($model->page)) {
                        return $model->page->category->title . ' | ' . $model->page->title;
                    }
                }
            ],
            'title',
            'slug',
            'url_video',
            [
                'class' => ActionColumn::className(),
                'template' => '{update}{delete}',
                'buttons' => [
                    'delete' => function($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], [
                                    'class' => '',
                                    'data' => [
                                        'title' => 'Confirmación',
                                        'confirm' => 'Está seguro, ¿Desea eliminar el evento seleccionado?.',
                                        'method' => 'post',
                                    ],
                        ]);
                    }
                ]
            ]
        ],
    ]);
    ?>
</div>
