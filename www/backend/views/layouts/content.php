<?php

use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;
$settings = Yii::$app->settings;
/** @var TYPE_NAME $content */
?>
<div class="content-wrapper">
    <section class="content-header">
        <?php
        if (isset($this->blocks['content-header'])) {
            echo Html::tag('h1', $this->blocks['content-header']);
        } else {
                if ($this->title !== null) {
                    echo Html::tag('h1', Html::encode($this->title));
                } else {
                    echo Inflector::camel2words(
                        Inflector::id2camel($this->context->module->id)
                    );
                    echo Html::tag('h1', ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '');
                 }
        }

        echo Breadcrumbs::widget(
            [
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]
        ) ?>
    </section>

    <section class="content">
        <?php echo  Alert::widget() ?>
        <?php echo  $content ?>
    </section>
</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
    </div>
    <strong><?php echo $settings->get('main', 'site_main_title');?>
</footer>
<aside class="control-sidebar control-sidebar-dark">
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
        <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane" id="control-sidebar-settings-tab">
        </div>
    </div>
</aside>
<div class='control-sidebar-bg'></div>