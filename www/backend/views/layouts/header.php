<?php
use yii\helpers\Html;
$settings = Yii::$app->settings;
?>

<header class="main-header">
    <?php echo  Html::a('<span class="logo-mini">FN</span><span class="logo-lg"> <img src="'. $settings->get('main', 'frontend_url') . '/uploads/logo.png" style="width: 115px;"></span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
            </ul>
        </div>
    </nav>
</header>
