<?php
use dmstr\widgets\Menu;

$settings = Yii::$app->settings;
$isGuest = Yii::$app->user->isGuest;
$rol = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());

if(!$isGuest){
    $userRole =  array_keys( Yii::$app-> authManager->getRolesByUser( Yii::$app->user->getId() ) )[0];
}

$userRole_adm = Yii::$app->authManager->getAssignment('SUPERADMIN', Yii::$app->user->getId());
$has_general_config =  false;
$has_user_admin =  false;
$has_static_pages =  false;
$has_events =  false;
$has_news =  false;
$has_documents =  false;
$has_videos =  false;
$has_history =  false;
$has_slider =  false;
$has_category =  false;
$has_queries =  false;
$has_complaints =  false;
$has_account_profile =  false;
if (isset($userRole)) {
        $isSuperAdmin = ($userRole === 'SUPERADMIN');
    switch ($userRole) {
        case 'SUPERADMIN':
            $has_general_config =  true;
            $has_user_admin =  true;
            $has_static_pages =  true;
            $has_events =  true;
            $has_news =  true;
            $has_documents =  true;
            $has_videos =  true;
            $has_history =  true;
            $has_slider =  true;
            $has_category =  true;
            $has_queries =  true;
            $has_complaints =  true;
            $has_account_profile =  true;
            break;
        case 'ADMIN':
            $has_user_admin =  true;
            $has_static_pages =  true;
            $has_events =  true;
            $has_news =  true;
            $has_documents =  true;
            $has_videos =  true;
            $has_history =  true;
            $has_slider =  true;
            $has_category =  true;
            $has_account_profile =  true;
            break;
        case 'GESTOR_SECCIONES':
            $has_static_pages =  true;
            $has_events =  true;
            $has_news =  true;
            $has_documents =  true;
            $has_videos =  true;
            $has_history =  true;
            $has_queries =  true;
            $has_complaints =  true;
            $has_account_profile =  true;
            break;
        case 'GESTOR_CONTENIDOS':
            $has_static_pages =  true;
            $has_news =  true;
            $has_documents =  true;
            $has_videos =  true;
            $has_account_profile =  true;
            break;
    }
}
?>
<aside class="main-sidebar">
    <section class="sidebar">
        <hr>
        <?php
       echo Menu::widget([
                    'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                    'items' => [
                        [
                            'label' => 'MENU',
                            'options' => ['class' => 'header'],
                            'visible' => !Yii::$app->user->isGuest
                        ],
                        [
                            'label' => 'Inicio',
                            'icon' => 'home',
                              'url' => '/',
                        ],
                        [
                            'label' => 'Generales',
                            'icon' => 'share',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Configuraciones generales', 'icon' => 'file-code-o', 'url' => ['/manage-settings'], 'visible' => $has_general_config],
                                ['label' => 'Administración de usuarios', 'icon' => 'file-code-o', 'url' => ['/user/admin'], 'visible' => $has_user_admin],
                            ],
                            'visible' => !Yii::$app->user->isGuest
                        ],
                        [
                            'label' => 'Sitio',
                            'icon' => 'share',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Páginas estáticas', 'icon' => 'file-code-o', 'url' => ['/static-pages'], 'visible' => $has_static_pages],
                                ['label' => 'Páginas Información', 'icon' => 'file-code-o', 'url' => ['/info'], 'visible' => $has_static_pages],
                                ['label' => 'Eventos', 'icon' => 'file-code-o', 'url' => ['/events'], 'visible' => $has_events],
                                ['label' => 'Noticias', 'icon' => 'file-code-o', 'url' => ['/news'], 'visible' => $has_news],
                                ['label' => 'Documentos', 'icon' => 'file-code-o', 'url' => ['/files'], 'visible' => $has_documents],
                                ['label' => 'Videos', 'icon' => 'file-code-o', 'url' => ['/videos'], 'visible' => $has_videos],
                                ['label' => 'Historia', 'icon' => 'file-code-o', 'url' => ['/history'], 'visible' => $has_history],
                                ['label' => 'Imágenes Slider', 'icon' => 'file-code-o', 'url' => ['/slider-images'], 'visible' => $has_slider],
                                ['label' => 'Categoría', 'icon' => 'file-code-o', 'url' => ['/category'], 'visible' => $has_category],
                                ['label' => 'Consultas', 'icon' => 'file-code-o', 'url' => ['/contact'], 'visible' => $has_queries],
                                ['label' => 'Denuncias', 'icon' => 'file-code-o', 'url' => ['/complaints'], 'visible' => $has_complaints],
                            ],
                            'visible' => !Yii::$app->user->isGuest
                        ],
                        [
                            'label' => 'Usuario',
                            'icon' => 'share',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Perfil - Cuenta', 'icon' => 'file-code-o', 'url' => ['/user/settings/profile'], 'visible' => $has_account_profile],
                            ],
                            'visible' => !Yii::$app->user->isGuest
                        ],
                        ['label' => 'Ingresar', 'url' => ['/user/login'], 'visible' => Yii::$app->user->isGuest],
                        ['label' => 'Salir', 'url' => ['/site/logout'], 'visible' => !Yii::$app->user->isGuest],
                    ]
                ]) ?>
    </section>
</aside>