/**
 * Override del dialogo de confirmación de yii
 *
 * @param mensaje a mostrar
 * @param okCallback ejecutado con el mensaje en verdadero
 * @param cancelCallback callback ejecutado en cancelación
 */
yii.confirm = function (message, okCallback, cancelCallback) {
    swal({
        title: 'Confirmación',
        text: message,
        type: 'warning',
        icon: "warning",
        showCancelButton: true,
        closeOnConfirm: true,
        allowOutsideClick: true
    }, okCallback);
};