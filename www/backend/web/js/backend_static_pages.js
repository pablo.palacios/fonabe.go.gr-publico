jQuery(function ($) {
    $(document).ready(function () {
        $('#staticpages-title').bind('keypress keyup blur', function () {
            $('#staticpages-slug').val(_.kebabCase($('#staticpages-title').val()));
        });
        $('#main-form').show();
        $('#spinner').hide();
    });
});
