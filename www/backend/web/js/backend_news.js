jQuery(function ($) {
    $(document).ready(function () {
        $('#news-title').bind('keypress keyup blur', function () {
            $('#news-slug').val(_.kebabCase($('#news-title').val()));
        });
        $('#main-form').show();
        $('#spinner').hide();

        $('#gt-uploaderImg').click(function () {
            $('#submit-btn').hide();
        });
    });
});
