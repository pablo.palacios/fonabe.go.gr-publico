jQuery(function ($) {
    $(document).ready(function () {
        $('#videos-title').bind('keypress keyup blur', function () {
            $('#videos-slug').val(_.kebabCase($('#videos-title').val()));
        });
        $('#main-form').show();
        $('#spinner').hide();
    });
});
