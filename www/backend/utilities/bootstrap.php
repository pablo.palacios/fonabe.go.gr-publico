<?php

namespace backend\utilities;

use dektrium\user\models\User;
use Yii;
use yii\base\BootstrapInterface;

class bootstrap implements BootstrapInterface {

    private $db;

    public function __construct() {
        $this->db = Yii::$app->db;
    }

    public function bootstrap($app) {
        $users = User::find()->all();
        $admins = [];
        foreach ($users as $value) {
            $id = $value['id'];
            $userRole_array = Yii::$app->authManager->getRolesByUser($id);
            if (isset($userRole_array) && sizeof($userRole_array) > 0) {
                $userRole = array_keys($userRole_array)[0];
                if ($userRole == 'ADMIN' || $userRole == 'SUPERADMIN') {
                    array_push($admins, $value['username']);
                }
            }
        }

        Yii::$app->getModule('user')->admins = $admins;
        Yii::$app->getModule('rbac')->admins = $admins;
    }

}
