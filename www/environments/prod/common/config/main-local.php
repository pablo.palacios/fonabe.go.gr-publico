<?php
$params = require(__DIR__ . '/params-local.php'); //configuración en params-local.php
return [
    'components' => [
        'db' => $params['db_params'],
        'mailer' =>$params['mail_params'],
    ],
];
