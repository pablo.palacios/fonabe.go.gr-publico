<?php

return [
    'db_params' => [
        'class' => 'yii\db\Connection',
        'dsn' => 'sqlsrv:Server=FNB-SQL02\TENORIO;Database=FONABE_PWEB', //Dirección de IP externa para WFH
        'username' => 'USRFonabe',
        'password' => '123F0n4b3',
        'charset' => 'utf8',
        'tablePrefix' => 'fn_',
    ],
    'mail_params' => [
        'class' => 'yii\swiftmailer\Mailer',
        'viewPath' => '@common/mail',
        'transport' => [
            'class' => 'Swift_SmtpTransport',
            'host' => 'mail.fonabe.go.cr',
            'username' => 'FONABE\contactos',
            'password' => 'Hola1234',
            'port' => '25',
            'encryption' => 'tls',
            'streamOptions' => [
                'ssl' => [
                    'allow_self_signed' => true,
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                ],
            ],
        ],
        'useFileTransport' => false,
    ]
];
