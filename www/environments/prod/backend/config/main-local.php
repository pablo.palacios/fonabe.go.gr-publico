<?php

$params = require(__DIR__ . '/params-local.php'); //configuración en params-local.php
$config = [
    'components' => [
        'request' => [
            'enableCsrfValidation' => false,
            'cookieValidationKey' => $params['cookieValidationKey'],
        ],
        'recaptcha' => [
            'class' => 'alexeevdv\recaptcha\Recaptcha',
            'siteKey' => $params['reCaptcha_siteKey'],
            'secret' => $params['reCaptcha_secret'],
        ],
    ],
];

return $config;
