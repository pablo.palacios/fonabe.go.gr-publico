<?php

$params = require(__DIR__ . '/params-local.php'); //configuración en params-local.php
$config = [
    'components' => [
        'request' => [
            'cookieValidationKey' => $params['cookieValidationKey'],
        ],
        'recaptcha' => [
            'class' => 'alexeevdv\recaptcha\Recaptcha',
            'siteKey' => $params['reCaptcha_siteKey'],
            'secret' => $params['reCaptcha_secret'],
        ],
    ],
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
//    $config['bootstrap'][] = 'debug';
//    $config['modules']['debug'] = [
//        'class' => 'yii\debug\Module',
//    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
