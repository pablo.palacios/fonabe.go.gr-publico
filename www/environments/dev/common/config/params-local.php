<?php

return [
    'db_params' => [
        'class' => 'yii\db\Connection',
        'dsn' => 'sqlsrv:Server=KEILORIGD-PC\IGD;Database=FONABEDB', //Dirección de IP externa para WFH
        'username' => 'USRFonabe',
        'password' => '123F0n4b3',
        'charset' => 'utf8',
        'tablePrefix' => 'fn_',
    ],
    'mail_params' => [
        'class' => 'yii\swiftmailer\Mailer',
        'viewPath' => '@common/mail',
        'transport' => [
            'class' => 'Swift_SmtpTransport',
            'host' => 'smtp.mailtrap.io',
            'username' => '15c274dbb74761',
            'password' => 'fd51703ee17884',
            'port' => '2525',
            'encryption' => 'tls',
        ],
        'useFileTransport' => false,
    ]
];
