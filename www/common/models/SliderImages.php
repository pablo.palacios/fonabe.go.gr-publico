<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%slider_images}}".
 *
 * @property int $id Id Slide
 * @property string $image_url Url imagen del slide
 * @property string $image_link Link del slide
 * @property string $slide_text Texto del slide
 */
class SliderImages extends \yii\db\ActiveRecord {

    public static function tableName() {
        return '{{%slider_images}}';
    }

    public function rules() {
        return [
            [['image_link', 'slide_text'], 'required'],
            [['image_url'], 'required', 'on' => 'create'],
            [['image_url', 'image_link', 'slide_text'], 'string'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id Slide',
            'image_url' => 'Url imagen del slide',
            'image_link' => 'Link del slide',
            'slide_text' => 'Texto del slide',
        ];
    }

    //función para obtener los sliders principales, siempre es un número predeterminado
    public static function getSliders() {
        return self::find()
                        ->limit(Yii::$app->params['cantidadSliders'])
                        ->all();
    }

}
