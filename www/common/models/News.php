<?php

namespace common\models;

use Yii;
use \yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%news}}".
 *
 * @property int $id Id de noticia
 * @property string $title Título de la noticia
 * @property string $slug Slug para URL
 * @property string $extract Resumen
 * @property string $content Contenido
 * @property int $created_at Fecha de creación
 * @property int $updated_at Fecha de ultima actualización
 * @property string $author_name Nombre del autor
 * @property string $url_imagen
 * @property string $published
 */
class News extends ActiveRecord {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%news}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title', 'slug', 'published'], 'required'],
            [['title', 'slug'], 'unique'],
            [['extract'], 'string', 'max' => 550],
            [['title', 'slug', 'extract', 'content', 'author_name', 'url_imagen'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['published'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'Id de noticia',
            'title' => 'Título de la noticia',
            'slug' => 'Slug para URL',
            'extract' => 'Resumen',
            'content' => 'Contenido',
            'created_at' => 'Fecha de creación',
            'updated_at' => 'Fecha de última actualización',
            'author_name' => 'Autor',
            'url_imagen' => 'Url Imagen',
            'published' => 'Publicado',
        ];
    }

    public static function getNews() {
        return self::find()
                        ->where(['published' => true])
                        ->orderBy(['created_at' => SORT_DESC])
                        ->limit(2)
                        ->all();
    }

}
