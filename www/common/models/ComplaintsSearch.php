<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Complaints;

class ComplaintsSearch extends Complaints
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['first_name', 'last_name', 'identification_card', 'phone', 'address', 'email', 'fax', 'place_of_work', 'details', 'reported_name_01', 'reported_name_02', 'reported_name_03', 'reported_location_01', 'reported_location_02', 'reported_location_03', 'evidence_01', 'evidence_02', 'evidence_03', 'evidence_location_01', 'evidence_location_02', 'evidence_location_03', 'witness_01', 'witness_02', 'witness_03', 'witness_location_01', 'witness_location_02', 'witness_location_03'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Complaints::find();
        $query->orderBy(['created_at'=>SORT_ASC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'identification_card', $this->identification_card])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'fax', $this->fax])
            ->andFilterWhere(['like', 'place_of_work', $this->place_of_work])
            ->andFilterWhere(['like', 'details', $this->details])
            ->andFilterWhere(['like', 'reported_name_01', $this->reported_name_01])
            ->andFilterWhere(['like', 'reported_name_02', $this->reported_name_02])
            ->andFilterWhere(['like', 'reported_name_03', $this->reported_name_03])
            ->andFilterWhere(['like', 'reported_location_01', $this->reported_location_01])
            ->andFilterWhere(['like', 'reported_location_02', $this->reported_location_02])
            ->andFilterWhere(['like', 'reported_location_03', $this->reported_location_03])
            ->andFilterWhere(['like', 'evidence_01', $this->evidence_01])
            ->andFilterWhere(['like', 'evidence_02', $this->evidence_02])
            ->andFilterWhere(['like', 'evidence_03', $this->evidence_03])
            ->andFilterWhere(['like', 'evidence_location_01', $this->evidence_location_01])
            ->andFilterWhere(['like', 'evidence_location_02', $this->evidence_location_02])
            ->andFilterWhere(['like', 'evidence_location_03', $this->evidence_location_03])
            ->andFilterWhere(['like', 'witness_01', $this->witness_01])
            ->andFilterWhere(['like', 'witness_02', $this->witness_02])
            ->andFilterWhere(['like', 'witness_03', $this->witness_03])
            ->andFilterWhere(['like', 'witness_location_01', $this->witness_location_01])
            ->andFilterWhere(['like', 'witness_location_02', $this->witness_location_02])
            ->andFilterWhere(['like', 'witness_location_03', $this->witness_location_03]);

        return $dataProvider;
    }
}
