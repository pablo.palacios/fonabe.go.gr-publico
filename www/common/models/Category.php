<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%category}}".
 *
 * @property int $id
 * @property string $title Título categoría
 * @property string $slug Slug para el URL
 *
 * @property StaticPages[] $staticPages
 */
class Category extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'slug'], 'string'],
            [['title', 'slug'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Título categoría',
            'slug' => 'Slug para el URL',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStaticPages()
    {
        return $this->hasMany(StaticPages::className(), ['category_id' => 'id']);
    }
}
