<?php

namespace common\models;

use Yii;
use yii\data\Pagination;

/**
 * This is the model class for table "{{%info}}".
 *
 * @property int $id Id
 * @property int $page_id Ide de la página relacionada
 * @property string $title Título
 * @property string $content Contenido
 * @property string $excerpt Resumen
 * @property StaticPages $page
 */
class Info extends \yii\db\ActiveRecord {

  const ID_TIPO_BECA = 5; //Id para tipo de beca

  public static function tableName() {
    return '{{%info}}';
  }

  public function rules() {
    return [
        [['page_id'], 'integer'],
        [['title', 'content', 'excerpt'], 'string'],
        [['title', 'excerpt', 'page_id'], 'required'],
        [['content'], 'string', 'max' => 5000],
        [['excerpt'], 'string', 'max' => 500],
        [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => StaticPages::className(), 'targetAttribute' => ['page_id' => 'id']],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
        'id' => 'Id',
        'page_id' => 'Página relacionada',
        'title' => 'Título',
        'content' => 'Contenido',
        'excerpt' => 'Resumen',
    ];
  }

//función para obtener todas las páginas de información de tipos de beca (depdrop)
  public static function getAllInfoPages() {
    return self::find()
                    ->select(['id', 'title as name'])
                    ->where(['page_id' => self::ID_TIPO_BECA])
                    ->indexBy('id')
                    ->asArray()
                    ->all();
  }

  public function getPage() {
    return $this->hasOne(StaticPages::className(), ['id' => 'page_id']);
  }

  public function getInfoById($id) {
    $query = Info::find();
    $query->where(['page_id' => $id]);
    $countQuery = clone $query;
    $pages = new Pagination([
        'totalCount' => $countQuery->count(),
    ]);
    $masterDetailDocuments = $query
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
    return [$masterDetailDocuments, $pages];
  }

  /*
   * Función para obtener los archivos de una sección secundaria (tipos de beca)
   */

  public static function getFilesById($id) {
    $query = Files::find();
    $query->where(['page_information_id' => $id]);
    $countQuery = clone $query;
    $pages = new Pagination([
        'totalCount' => $countQuery->count(),
        'pageSize' => Yii::$app->params['paginationBackendDefault']
    ]);
    $masterDetailDocuments = $query
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
    return [$masterDetailDocuments, $pages];
  }

}
