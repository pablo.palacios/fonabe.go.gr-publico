<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\data\Pagination;

/**
 * This is the model class for table "{{%videos}}".
 * @property int $id Id de video
 * @property string $title Título del video
 * @property string $slug Slug para URL
 * @property string $extract Resúmen
 * @property string $content Contenido
 * @property string $author_name Nombre del autor
 * @property string $url_video
 * @property int $published Publicado
 * @property int $created_at Fecha de creación
 * @property int $updated_at Fecha actualización
 * @property int $page_id Página relacionada
 */
class Videos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%videos}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'slug', 'url_video'], 'required'],
            [['title', 'slug', 'extract', 'content', 'author_name', 'url_video'], 'string'],
            [['published', 'created_at', 'updated_at', 'page_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id de video',
            'title' => 'Título del video',
            'slug' => 'Slug para URL',
            'extract' => 'Resúmen',
            'content' => 'Contenido',
            'author_name' => 'Nombre del autor',
            'url_video' => 'Código Youtube del video',
            'published' => 'Publicado',
            'created_at' => 'Fecha de creación',
            'updated_at' => 'Fecha actualización',
            'page_id' => 'Página relacionada',
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(StaticPages::className(), ['id' => 'page_id']);
    }

    public function getAllVideos($has_md_video, $id)
    {
        if($has_md_video){
            $query = Videos::find();
            $query->where(['page_id'=> $id]);
            $query->orderBy(['created_at'=>SORT_DESC]);
            $countQuery = clone $query;
            $pages = new Pagination([
                'totalCount' => $countQuery->count(),
                'pageSize' => 5
            ]);
            $masterDetailVideos = $query
                ->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
            return [$masterDetailVideos, $pages];
        }
        return [];
    }
}
