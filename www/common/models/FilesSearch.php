<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Files;

class FilesSearch extends Files {

    public $page;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'page_id', 'hit_counter', 'download_counter', 'created_at', 'updated_at', 'page_id'], 'integer'],
            [['title', 'description', 'file_url', 'page'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Files::find();
        $query->joinWith(['page']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['page'] = [
            'asc' => ['fn_static_pages.title' => SORT_ASC],
            'desc' => ['fn_static_pages.title' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'page_id' => $this->page_id,
            'hit_counter' => $this->hit_counter,
            'download_counter' => $this->download_counter,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'fn_files.title', $this->title])
                ->andFilterWhere(['like', 'description', $this->description])
                ->andFilterWhere(['like', 'file_url', $this->file_url])
                ->andFilterWhere(['like', 'fn_static_pages.title', $this->page]);

        return $dataProvider;
    }

}
