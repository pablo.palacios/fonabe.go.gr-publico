<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property int $id Id usuario
 * @property string $username Nombre de Usuario
 * @property string $email Correo del Usuario
 * @property string $password_hash Hash de contraseña
 * @property string $auth_key Llave de atorización
 * @property int $confirmed_at Fecha de confirmacion
 * @property string $unconfirmed_email Correo electronico no confirma
 * @property int $blocked_at Fecha de bloqueo
 * @property int $created_at Fecha de creación
 * @property int $updated_at
 * @property string $registration_ip No IP del registro
 * @property int $last_login_at Fecha de últimop registro
 * @property int $flags Banderas
 *
 * @property Account[] $accounts
 * @property Profile $profile
 * @property SocialAccount[] $socialAccounts
 * @property Token[] $tokens
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'email', 'password_hash', 'auth_key', 'created_at', 'updated_at'], 'required'],
            [['username', 'email', 'password_hash', 'auth_key', 'unconfirmed_email', 'registration_ip'], 'string'],
            [['confirmed_at', 'blocked_at', 'created_at', 'updated_at', 'last_login_at', 'flags'], 'integer'],
            [['email'], 'unique'],
            [['username'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id usuario',
            'username' => 'Nombre de Usuario',
            'email' => 'Correo del Usuario',
            'password_hash' => 'Hash de contraseña',
            'auth_key' => 'Llave de atorización',
            'confirmed_at' => 'Fecha de electrónico',
            'unconfirmed_email' => 'Correo electrónico no confirma',
            'blocked_at' => 'Fecha de bloqueo',
            'created_at' => 'Fecha de creación',
            'updated_at' => 'Updated At',
            'registration_ip' => 'No IP del registro',
            'last_login_at' => 'Fecha de últimop registro',
            'flags' => 'Banderas',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccounts()
    {
        return $this->hasMany(Account::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocialAccounts()
    {
        return $this->hasMany(SocialAccount::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTokens()
    {
        return $this->hasMany(Token::className(), ['user_id' => 'id']);
    }
}
