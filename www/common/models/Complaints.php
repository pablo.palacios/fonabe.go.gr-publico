<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%complaints}}".
 *
 * @property int $id Id de las quejas
 * @property string $first_name Nombre
 * @property string $last_name Apellido
 * @property string $identification_card Número de identificación
 * @property string $phone Teléfono
 * @property string $address Dirección
 * @property string $email Correo electrónico
 * @property string $fax Número de Fax
 * @property string $place_of_work Lugar de trabajo
 * @property string $details Detalles
 * @property string $reported_name_01 Nombre
 * @property string $reported_name_02 Nombre
 * @property string $reported_name_03 Nombre
 * @property string $reported_location_01 Ubicación
 * @property string $reported_location_02 Ubicación
 * @property string $reported_location_03 Ubicación
 * @property string $evidence_01 Evidencia
 * @property string $evidence_02 Evidencia
 * @property string $evidence_03 Evidencia
 * @property string $evidence_location_01 Ubicación
 * @property string $evidence_location_02 Ubicación
 * @property string $evidence_location_03 Ubicación
 * @property string $witness_01 Testigo
 * @property string $witness_02 Testigo
 * @property string $witness_03 Testigo
 * @property string $witness_location_01 Ubicación
 * @property string $witness_location_02 Ubicación
 * @property string $witness_location_03 Ubicación
 * @property int $created_at Fecha de creación
 * @property int $updated_at Fecha actualización
 */
class Complaints extends \yii\db\ActiveRecord {

    public $reCaptcha;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%complaints}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['first_name', 'last_name', 'identification_card', 'phone', 'address', 'email', 'place_of_work',
            'details', 'reported_name_01', 'reported_location_01', 'evidence_01', 'evidence_location_01',
            'witness_01', 'witness_location_01'], 'required'],
            [['first_name', 'last_name', 'identification_card', 'phone', 'address', 'email', 'details', 'place_of_work', 'fax',
            'reported_name_01', 'reported_location_01', 'evidence_01', 'evidence_location_01', 'witness_01', 'witness_location_01',
            'reported_name_02', 'reported_location_02', 'evidence_02', 'evidence_02', 'evidence_location_02', 'witness_02', 'witness_location_02',
            'reported_name_03', 'reported_location_03', 'evidence_03', 'evidence_03', 'evidence_location_03', 'witness_03', 'witness_location_03',], 'safe'],
            [['details'], 'string', 'max' => 2500],
            [['details'], 'string', 'max' => 2500],
            [['created_at', 'updated_at', 'identification_card', 'fax'], 'integer'],
            [['email'], 'email'],
            [['phone',], 'integer', 'message' => 'El número no cumple con el formato, valide que sólo se ingresen números'],
            [['phone'], 'string', 'min' => 8, 'max' => 8,
                'tooShort' => 'El número telefónico no debe ser menor a 8 dígitos',
                'tooLong' => 'El número telefónico no debe ser mayor a 8 dígitos',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'Id de las quejas',
            'first_name' => 'Nombre',
            'last_name' => 'Apellidos',
            'identification_card' => 'Cédula / Pasaporte',
            'phone' => 'Teléfono',
            'address' => 'Dirección para Notificaciones',
            'email' => 'Correo electrónico',
            'fax' => 'Número de Fax',
            'place_of_work' => 'Lugar de Trabajo',
            'details' => 'Detalle de los Hechos',
            'reported_name_01' => 'Nombre',
            'reported_name_02' => 'Nombre',
            'reported_name_03' => 'Nombre',
            'reported_location_01' => 'Ubicación',
            'reported_location_02' => 'Ubicación',
            'reported_location_03' => 'Ubicación',
            'evidence_01' => 'Pruebas Aportadas',
            'evidence_02' => 'Pruebas Aportadas',
            'evidence_03' => 'Pruebas Aportadas',
            'evidence_location_01' => 'Ubicación',
            'evidence_location_02' => 'Ubicación',
            'evidence_location_03' => 'Ubicación',
            'witness_01' => 'Testigo',
            'witness_02' => 'Testigo',
            'witness_03' => 'Testigo',
            'witness_location_01' => 'Testigos: Lugar de Accesos de Testigos',
            'witness_location_02' => 'Testigos: Lugar de Accesos de Testigos',
            'witness_location_03' => 'Testigos: Lugar de Accesos de Testigos',
            'created_at' => 'Fecha de creación',
            'updated_at' => 'Fecha actualización',
            'reCaptcha' => 'Para continuar, debe afirmar que no es un robot, dándole click al recuadro',
        ];
    }

}
