<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Videos;

class VideosSearch extends Videos {

    public $page;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'published', 'created_at', 'updated_at', 'page_id', 'page_id'], 'integer'],
            [['title', 'slug', 'extract', 'content', 'author_name', 'url_video', 'page'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Videos::find();
        $query->joinWith(['page']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['page'] = [
            'asc' => ['fn_static_pages.title' => SORT_ASC],
            'desc' => ['fn_static_pages.title' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'page_id' => $this->page_id,
            'published' => $this->published,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
                ->andFilterWhere(['like', 'slug', $this->slug])
                ->andFilterWhere(['like', 'extract', $this->extract])
                ->andFilterWhere(['like', 'content', $this->content])
                ->andFilterWhere(['like', 'author_name', $this->author_name])
                ->andFilterWhere(['like', 'url_video', $this->url_video])
                ->andFilterWhere(['like', 'fn_static_pages.title', $this->page]);

        return $dataProvider;
    }

}
