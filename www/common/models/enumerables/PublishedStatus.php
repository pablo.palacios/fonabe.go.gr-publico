<?php

namespace common\models\enumerables;

use yii2mod\enum\helpers\BaseEnum;

/**
 * Class PublishedStatus
 *
 */
class PublishedStatus extends BaseEnum
{
    const ACTIVE = 1;
    const INACTIVE = 0;

    public static $messageCategory = 'yii2mod.settings';

    public static $list = [
        self::ACTIVE => 'Publicado',
        self::INACTIVE => 'No Publicado',
    ];
}
