<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%events}}".
 *
 * @property int $id Id Eventos
 * @property string $title Título del evento
 * @property string $description Descripción del evento
 * @property int $updated_at Fecha actualización
 * @property int $created_at Fecha creación
 * @property string $start_dt Hora y fecha de inicio
 * @property string $end_dt Hora y fecha de inicio
 * @property string $date Hora y fecha de inicio
 * @property string $time Hora y fecha de inicio
 * @property string $hours Hora y fecha de inicio
 */
class Events extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%events}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description'], 'string'],
            [['title'], 'string', 'max' => 550],
            [['description'], 'string', 'max' => 2000],
            [['time'], 'string', 'max' => 32],
            [['title','date'], 'required'],
            [['updated_at', 'created_at', 'hours'], 'integer'],
            [['start_dt', 'end_dt', 'date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id Eventos',
            'title' => 'Título del evento',
            'description' => 'Descripción del evento',
            'updated_at' => 'Fecha actualización',
            'created_at' => 'Fecha creación',
            'start_dt' => 'Hora y fecha de inicio',
            'end_dt' => 'Hora y fecha de fin',
            'date' => 'Fecha del evento',
            'time' => 'Hora de inicio del evento',
            'hours' => 'Duración del evento',
        ];
    }
}
