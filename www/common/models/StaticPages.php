<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%static_pages}}".
 *
 * @property int $id Id
 * @property string $slug Slug para URL limpia
 * @property string $title Título de la página
 * @property string $excerpt Resúmen del contenido
 * @property string $content Contenido de la página
 * @property int $created_at Fecha de creación
 * @property int $updated_at Fecha de actualización
 * @property int $category_id Id Categoría
 * @property int $editable Es editable?
 * @property string $icon Icono de menú
 * @property string $has_md_documents
 * @property string $has_md_video
 * @property string $has_md_info
 *
 * @property Category $category
 */
class StaticPages extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%static_pages}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slug', 'title'], 'required'],
            [['excerpt'], 'string', 'max' => 550],
            [['slug', 'title', 'excerpt', 'content', 'icon'], 'string'],
            [['created_at', 'updated_at', 'category_id', 'editable'], 'integer'],
            [['has_md_documents','has_md_video','has_md_info'], 'safe'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'slug' => 'Slug para URL limpia',
            'title' => 'Título de la página',
            'excerpt' => 'Resúmen del contenido',
            'content' => 'Contenido de la página',
            'created_at' => 'Fecha de creación',
            'updated_at' => 'Fecha de actualización',
            'category_id' => 'Id Categoría',
            'editable' => 'Es editable?',
            'icon' => 'Icono de menú',
            'has_md_documents' => 'Tiene maestro detalle documentos',
            'has_md_videos' => 'Tiene maestro detalle videos',
            'has_md_información' => 'Tiene maestro detalle información',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPagesForMenu($id)
    {
        return $this->find()->where(['category_id'=>$id])->orderBy(['order'=>SORT_ASC])->all();
    }
}
