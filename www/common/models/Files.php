<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\data\Pagination;

/**
 * This is the model class for table "{{%files}}".
 *
 * @property int $id Id documento
 * @property int $page_id Id página
 * @property string $title Título documento
 * @property string $description Descripción documento
 * @property string $file_url Url documento
 * @property int $hit_counter Contador hits
 * @property int $download_counter Contador downloads
 * @property int $created_at Fecha de creación
 * @property int $updated_at Fecha actualización
 * @property string $file_date Fecha del documento
 * @property int $page_information_id Documentos asignados a páginas secundarias
 *
 * @property StaticPages $page
 */
class Files extends ActiveRecord {

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        TimestampBehavior::className(),
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName() {
    return '{{%files}}';
  }

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
        [['title', 'file_url'], 'string', 'max' => 500],
        [['title'], 'unique'],
        [['description'], 'string', 'max' => 2500],
        [['page_id', 'hit_counter', 'download_counter', 'created_at', 'updated_at'], 'integer'],
        [['title', 'description', 'file_url'], 'string'],
        [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => StaticPages::className(), 'targetAttribute' => ['page_id' => 'id']],
        [['file_date', 'page_information_id'], 'safe'],
        [['title', 'file_url', 'file_date', 'page_id'], 'required'],
        [['page_information_id'], 'required', 'on' => 'pagina_secundaria', 'message' => 'Página secundaria es requerida'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
        'page_id' => 'Id página',
        'title' => 'Título del documento',
        'description' => 'Descripción del documento',
        'file_url' => 'Nombre del archivo',
        'hit_counter' => 'Contador hits',
        'download_counter' => 'Contador downloads',
        'created_at' => 'Fecha de creación',
        'updated_at' => 'Fecha actualización',
        'file_date' => 'Fecha del documento',
        'page_information_id' => 'Página secundaria (únicamente activo para tipos de beca)'
    ];
  }

  public function getPage() {
    return $this->hasOne(StaticPages::className(), ['id' => 'page_id']);
  }

  public function getAllFiles($has_md_documents, $id) {
    if ($has_md_documents) {
      $query = Files::find();
      $query->where(['page_id' => $id]);
      $query->andWhere(['>=', 'file_date', date('Y-m-d', strtotime('-3 years'))]);
      $query->orderBy(['file_date' => SORT_DESC]);
      $countQuery = clone $query;
      $pages = new Pagination([
          'totalCount' => $countQuery->count(),
          'pageSize' => Yii::$app->params['paginationBackendDefault']
      ]);
      $masterDetailDocuments = $query
              ->offset($pages->offset)
              ->limit($pages->limit)
              ->all();
      return [$masterDetailDocuments, $pages];
    }
    return [];
  }

  /*
   * Función para obtener los archivos de acuerdo a una página de información relacionada
   */

  public static function getInfoFiles($page_id) {
    return self::find()
                    ->where(['page_information_id' => $page_id])
                    ->all();
  }

  public static function getFiles() {
    return self::find()
                    ->orderBy(['hit_counter' => SORT_DESC])
                    ->limit(10)
                    ->all();
  }

}
