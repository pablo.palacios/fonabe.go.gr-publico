<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%history}}".
 *
 * @property int $id Id del Histórico
 * @property string $year Año
 * @property string $title Título
 * @property string $content Contenido
 */
class History extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%history}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'content'], 'string'],
            [['year'], 'unique'],
            [['year'], 'integer', 'max' => 2025, 'min' => 1980],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id del Histórico',
            'year' => 'Año',
            'title' => 'Título',
            'content' => 'Contenido',
        ];
    }
}
