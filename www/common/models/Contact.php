<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%contact}}".
 * @property int $id Id de contacto
 * @property string $first_name Nombre
 * @property string $last_name Apellido
 * @property string $identification Identificación
 * @property string $phone Teléfono
 * @property string $email Correo electrónico
 * @property string $subject Tema
 * @property string $detail Detalles
 * @property int $created_at Fecha de creación
 * @property int $updated_at Fecha actualización
 */
class Contact extends ActiveRecord {

    public $recaptcha;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%contact}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        $settings = Yii::$app->settings;
        return [
            [['first_name', 'last_name', 'identification', 'email', 'subject', 'detail'], 'required'],
            [['first_name', 'last_name', 'identification', 'phone', 'email', 'subject', 'detail'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['email'], 'email'],
            [['phone', 'identification'], 'integer', 'message' => 'Debe ingresar solamente números, sin guiones ni caracteres'],
            [['phone'], 'string', 'min' => 8, 'max' => 8,
                'tooShort' => 'El número telefónico no debe ser menor a 8 dígitos',
                'tooLong' => 'El número telefónico no debe ser mayor a 8 dígitos',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'Id de contacto',
            'first_name' => 'Nombre',
            'last_name' => 'Apellidos',
            'identification' => 'Cédula / Pasaporte',
            'phone' => 'Teléfono',
            'email' => 'Correo electrónico',
            'subject' => 'Tema',
            'detail' => 'Detalle de la Consulta o Sugerencia',
            'created_at' => 'Fecha de creación',
            'updated_at' => 'Fecha actualización',
            'recaptcha' => 'Para continuar, debe afirmar que no es un robot, dándole click al recuadro',
        ];
    }

}
