<?php
use yii\helpers\Html;

?>

<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    Se ha recibido Consulta/Sugerencia al portal
</p>
<hr>
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    Con la siguiente información: </p>
    <table style="width:100%" border="1px solid black">
        <tr>
            <th border="1px solid black">Nombre</th>
            <th border="1px solid black">Apellidos</th>
            <th border="1px solid black">Cédula / Pasaporte</th>
        </tr>
        <tr>
            <td border="1px solid black"><?php echo $model->first_name?></td>
            <td border="1px solid black"><?php echo $model->last_name?></td>
            <td border="1px solid black"><?php echo $model->identification ?></td>
        </tr>
        <tr>
            <th>Teléfono</th>
            <th>Correo Electrónico</th>
            <th>Tema</th>
        </tr>
        <tr>
            <td border="1px solid black"><?php echo $model->phone ?></td>
            <td border="1px solid black"><?php echo $model->email ?></td>
            <td border="1px solid black"><?php echo $model->subject ?></td>
        </tr>
        <tr>
            <th colspan = "3" border="1px solid black">Detalle de la Consulta o Sugerencia</th>
        </tr>
        <tr>
            <td colspan = "3"><?php echo $model->detail ?></td>
        </tr>
    </table>

