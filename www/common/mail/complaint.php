<?php
use yii\helpers\Html;

?>

<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    Se ha recibido Consulta/Sugerencia al portal
</p>
<hr>
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    Con la siguiente información: </p>
    <table style="width:100%" border="1px solid black">
        <tr>
            <th border="1px solid black">Nombre</th>
            <th border="1px solid black">Apellidos</th>
            <th border="1px solid black">Cédula / Pasaporte</th>
        </tr>
        <tr>
            <td border="1px solid black"><?php echo $model->first_name?></td>
            <td border="1px solid black"><?php echo $model->last_name?></td>
            <td border="1px solid black"><?php echo $model->identification_card ?></td>
        </tr>
        <tr>
            <th>Teléfono</th>
            <th>Dirección para Notificaciones</th>
            <th>Correo Electrónico</th>
        </tr>
        <tr>
            <td border="1px solid black"><?php echo $model->phone ?></td>
            <td border="1px solid black"><?php echo $model->address ?></td>
            <td border="1px solid black"><?php echo $model->email ?></td>
        </tr>
        <tr>
            <th>Fax</th>
            <th colspan = "2">Lugar de Trabajo</th>
        </tr>
        <tr>
            <td border="1px solid black"><?php echo $model->fax ?></td>
            <td border="1px solid black" colspan = "2"><?php echo $model->place_of_work ?></td>
        </tr>
        <tr>
            <th colspan = "3" border="1px solid black">Detalle de la Consulta o Sugerencia</th>
        </tr>
        <tr>
            <td colspan = "3"><?php echo $model->details ?></td>
        </tr>
        <tr>
            <th colspan = "3">Personas Denunciadas</th>
        </tr>
        <tr>
            <th>Nombre</th>
            <th>Ubicación</th>
            <th>Pruebas Aportadas</th>
        </tr>
        <tr>
            <td border="1px solid black"><?php echo $model->reported_name_01 ?></td>
            <td border="1px solid black"><?php echo $model->reported_location_01 ?></td>
            <td border="1px solid black"><?php echo $model->evidence_01 ?></td>
        </tr>
        <td border="1px solid black"><?php echo $model->reported_name_02 ?></td>
        <td border="1px solid black"><?php echo $model->reported_location_02 ?></td>
        <td border="1px solid black"><?php echo $model->evidence_02 ?></td>
        <tr>
            <td border="1px solid black"><?php echo $model->reported_name_03 ?></td>
            <td border="1px solid black"><?php echo $model->reported_location_03 ?></td>
            <td border="1px solid black"><?php echo $model->evidence_03 ?></td>
        </tr>
        <tr>
            <th>Lugar de Acceso a Pruebas</th>
            <th>Testigos</th>
            <th>Lugar de Acceso a Testigos</th>
        </tr>
        <tr>
            <td border="1px solid black"><?php echo $model->evidence_location_01 ?></td>
            <td border="1px solid black"><?php echo $model->witness_01 ?></td>
            <td border="1px solid black"><?php echo $model->witness_location_01 ?></td>
        </tr>
        <td border="1px solid black"><?php echo $model->evidence_location_02 ?></td>
        <td border="1px solid black"><?php echo $model->witness_02 ?></td>
        <td border="1px solid black"><?php echo $model->witness_location_02 ?></td>
        <tr>
            <td border="1px solid black"><?php echo $model->evidence_location_03 ?></td>
            <td border="1px solid black"><?php echo $model->witness_03 ?></td>
            <td border="1px solid black"><?php echo $model->witness_location_03 ?></td>
        </tr>
    </table>