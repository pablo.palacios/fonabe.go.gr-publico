<?php
$params = require(__DIR__ . '/params-local.php');
return [
    'id' => 'fonabe',
    'name' => 'Fonabe',
    'language' => 'es',
    'timeZone' => 'America/Costa_Rica',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'db' => $params['db_params'],
        'mailer' =>$params['mail_params'],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'settings' => [
            'class' => 'yii2mod\settings\components\Settings',
        ],
        'i18n' => [
            'translations' => [
                'yii2mod.settings' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@yii2mod/settings/messages',
                ],
            ],
        ],
    ],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableUnconfirmedLogin' => false,
            'confirmWithin' => 86400,
            'cost' => 12,
            'enableRegistration' => false,
            'enableGeneratingPassword' => true,
            'enableAccountDelete' => false,
            'rememberFor' => 1209600,
            'recoverWithin' => 21600,
            'admins' => []
        ],
        'rbac' => 'dektrium\rbac\RbacWebModule',
        'settings' => [
            'class' => 'yii2mod\settings\Module',
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ]
    ],
];
