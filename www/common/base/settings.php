<?php

namespace app\base;

use Yii;
use yii\base\BootstrapInterface;
use yii\base\Exception;

/**
 * Settings sirve para la inicialización de configuraciones en modelo programático
 *
 * @package app\base
 */
class settings implements BootstrapInterface {

    private $db;

    public function __construct() {
        $this->db = Yii::$app->db;
    }

    /**
     * Bootstrap general
     * @param \yii\base\Application $app
     */
    public function bootstrap($app) {



    }

}