<?php

use yii\helpers\Html;
use frontend\assets\AppAsset;
use common\widgets\Alert;

$settings = Yii::$app->settings;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
    <head>
        <meta charset="<?php echo Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php echo Html::csrfMetaTags() ?>
        <title><?php echo Html::encode($this->title) ?> | Fonabe</title>
        <?php
        include_once('../../common/components/Google/analyticstracking.php');
        $this->head()
        ?>

    </head>
    <body class="front">

        <?php
        $this->beginBody();
        echo $this->render('../partials/_main_header.php');
        echo Alert::widget();
        echo $content;
        echo $this->render('../partials/_main_footer.php');
        $this->endBody();
        echo $settings->get('page', 'footer_code');
        echo $this->render('../partials/_debug_warn.php');
        ?>
    </body>
</html>
<?php
$this->endPage();
