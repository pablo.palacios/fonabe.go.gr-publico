<?php

use yii\helpers\Html;
use frontend\assets\AppAsset;

$settings = Yii::$app->settings;
AppAsset::register($this);
$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
    <head>
        <meta charset="<?php echo Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php echo Html::csrfMetaTags() ?>
        <title><?php echo Html::encode($this->title) ?> | Fonabe</title>
        <!--Etiqueta para permitir navegación dentro de la página de FONABE-->
        <base href="" target="_parent">
        <?php
        $this->head()
        ?>

    </head>
    <body class="front">

        <?php
        $this->beginBody();
        echo $this->render('/partials/_main_header.php');
        $this->endBody();
        ?>
    </body>
</html>
<?php
$this->endPage();
