<?php

use frontend\assets\AppAsset;

$settings = Yii::$app->settings;
AppAsset::register($this);
$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
    <head>
        <!--Etiqueta para permitir navegación dentro de la página de FONABE-->
        <base href="" target="_parent">
        <?php
        $this->head();
        ?>
    </head>
    <?php
    $this->beginBody();
    echo $this->render('/partials/_main_footer.php');
    $this->endBody();
    ?>
</html>
<?php
$this->endPage();
