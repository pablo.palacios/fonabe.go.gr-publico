<?php

use \yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use common\models\Info;
use yii\helpers\Url;

$this->title = $info->title;
$this->params['breadcrumbs'][] = $info->title;
?>
<div class="container">
    <?php echo Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]) ?>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="blog-preview-wrap">
                <div class="blog-section">
                    <?php echo Html::tag('h2', $info->title); ?>
                </div>
                <div class="blog-content">
                    <?php echo $info->excerpt ?>
                    <br>
                    <?php
                    echo $info->content;
                    $files = Info::getFilesById($info->id);
                    echo (!empty($files)) ? $this->render('/static-page/partials/_sp_files', [
                                'model' => $files[0],
                                'pages' => $files[1]
                            ]) : '';
                    /*
                     * NOTA: Actualmente sólo se tiene un tipo de página secundaria (tipos de beca),
                     * por lo que siempre el botón de "Volver" regresa la ruta /centro-educativo/tipos-de-becas
                     * En caso de que esta progra cambie en la parte backend, se debe
                     * cambiar esta redirección.
                     */
                    echo Html::a('Volver', Url::to('/centro-educativo/tipos-de-becas'), ['class' => 'btn btn-primary']);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

