<?php

use yii\widgets\LinkPager;
use yii\helpers\Html;

$formatter = \Yii::$app->formatter;
?>
<div class="row">
    <div class="col-md-12">
        <div class="downloads-page">
            <?php foreach ($model as $i => $document) { ?>
                <div>
                    <a href="<?php echo '/ver/' . $document->id ?>" target="<?php echo (strpos($document->file_url, 'pdf') === false ? '_blank' : '_self' ) ?>">
                        <h4>
                            <i class="icon-g-descarga"></i>
                            <?php echo $document->title ?> 
                        </h4>
                    </a>
                    <?php echo Html::tag('p', $document->description); ?>
                </div>
            <?php } ?>
        </div>
        <nav class="pagination-all" aria-label="Page navigation">
            <?php echo LinkPager::widget(['pagination' => $pages,]); ?>
        </nav>
    </div>
</div>
