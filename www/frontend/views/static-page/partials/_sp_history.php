<?php

use yii\web\JqueryAsset;
use yii\helpers\Html;

$this->registerCssFile('/css/slick.css');
$this->registerCssFile('/css/slick-theme.css');
$this->registerCssFile('/css/timeline.css');
$this->registerJsFile('/js/timeline.js', ['depends' => [JqueryAsset::className()]]);
$this->registerJsFile('/js/frontend_timeline.js', ['depends' => [JqueryAsset::className()]]);
$formatter = \Yii::$app->formatter;
?>
<div id="timeline">
    <div class="container-fluid">
        <div class="row main-title">
            <div class="col-md-12">
                <div id="dates">
                    <?php foreach ($model as $fact) { ?>
                        <div>
                            <?php
                            echo Html::a($fact->year, '#' . $fact->year);
                            ?>

                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="#" id="prev"></a>
                <a href="#" id="next"></a>
                <div id="issues">
                    <?php foreach ($model as $fact) { ?>
                        <div id="<?php echo $fact->year ?>">
                            <?php
                            echo Html::tag('h1', $fact->title);
                            echo $fact->content;
                            ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
