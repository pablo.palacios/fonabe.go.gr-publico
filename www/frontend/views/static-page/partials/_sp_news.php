<?php
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
$formatter = \Yii::$app->formatter;
Yii::$app->formatter->locale = 'es-Es';
?>
<?php Pjax::begin(); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php foreach($model as $news): ?>
                <?php
                    $epoch = $news->created_at;
                    $dt = new DateTime("@$epoch");
                    $print_dt =  $dt->format('Y-m-d');
                ?>
                <div class="blog-preview-wrap">
                    <div class="blog-section">
                        <p> <?php echo $dt->format('d')?> <span><?php echo $formatter->asDate($print_dt, 'MMM')?></span></p>
                        <h2><a href="/noticia/<?php echo $news->slug?>"> <?php echo $news->title?> </a></h2>
                    </div>
                    <div class="blog-content">
                        <div class="imgBlog"><img src="<?php echo '/img-uploads/'.$news->url_imagen?>"></div>
                        <p><?php echo $news->extract?></p>
                        <a href="/noticia/<?php echo $news->slug?>" class=" btn btn-primary">Ver más</a>
                    </div>
                </div>
            <?php endforeach; ?>
            <nav class="pagination-all" aria-label="Page navigation">
                <?php echo  LinkPager::widget(['pagination' => $pages,]); ?>
            </nav>
        </div>
    </div>
</div>
<?php Pjax::end(); ?>
