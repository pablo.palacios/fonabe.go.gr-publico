<?php

use alexeevdv\recaptcha\RecaptchaWidget;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\JqueryAsset;
use yii2mod\alert\Alert;

$settings = Yii::$app->settings;
$province = [];
echo Alert::widget();


$this->registerJsFile("https://www.google.com/recaptcha/api.js");
$form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL, 'action' => '/buscar/estado-solicitud']);
$this->registerJsFile("/js/frontend_request_status.js", ['depends' => [JqueryAsset::className()]]);
echo $form->field($model, 'isUniversity')->hiddenInput(['value' => true])->label(false);
?>
<div class="row">
    <h1>Consulta del Estado de Solicitud</h1>
    <p>Consulte el estado de solicitud de beca ingresando los siguientes datos del estudiante: </p>
    <div class="form-standard">
        <div class="col col-md-4 col-md-offset-1">
            <?php
            echo $form->field($model, 'idType')
                    ->dropDownList($model->getDropDownList('idType'), ['prompt' => 'Seleccione el tipo de consulta']);
            ?>
        </div>
        <div class="col col-md-4 col-md-offset-1">
            <?php echo Html::activeLabel($model, 'id', ['class' => 'gt-idl', 'style' => 'display:none']); ?>
            <?php echo $form->field($model, 'id')->textInput(['maxlength' => true, 'class' => 'gt-id', 'style' => 'display:none'])->label(false) ?>

            <?php echo Html::activeLabel($model, 'first_name', ['class' => 'gt-namel', 'style' => 'display:none']); ?>
            <?php echo $form->field($model, 'first_name')->textInput(['maxlength' => true, 'class' => 'gt-name', 'style' => 'display:none'])->label(false) ?>

            <?php echo Html::activeLabel($model, 'last_name_1', ['class' => 'gt-namel', 'style' => 'display:none']); ?>
            <?php echo $form->field($model, 'last_name_1')->textInput(['maxlength' => true, 'class' => 'gt-name', 'style' => 'display:none'])->label(false) ?>

            <?php echo Html::activeLabel($model, 'last_name_2', ['class' => 'gt-namel', 'style' => 'display:none']); ?>
            <?php echo $form->field($model, 'last_name_2')->textInput(['maxlength' => true, 'class' => 'gt-name', 'style' => 'display:none'])->label(false) ?>
        </div>
        <div class="col col-md-12">
            <div class="col col-md-4 col-md-offset-4">
                <?php
                echo RecaptchaWidget::widget([
                    "name" => 'recaptcha',
                ]);
                ?>
                <br>
                <?php echo Html::submitButton('CONSULTAR', ['class' => 'btn btn-success', 'id' => 'do-submit']) ?>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

