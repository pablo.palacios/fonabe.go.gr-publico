<?php

use yii\helpers\Html;

if ($page->has_md_info) {
    ?>
    <div class="row">
        <div class="col-md-12 main-content">
            <div class="downloads-page">
                <?php foreach ($model as $i => $info) { ?>
                    <div>
                        <a href="/mas-información/<?php echo $info->id ?>">
                            <?php echo Html::tag('h4', $info->title); ?>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php
}
