<?php
/* @var $this yii\web\View */
/* @var $model frontend\models\SearchEducationCenterForm */
/* @var $form yii\widgets\ActiveForm */

use alexeevdv\recaptcha\RecaptchaWidget;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii2mod\alert\Alert;
$settings = Yii::$app->settings;
$province = [];
echo Alert::widget();
$this->registerJsFile("https://www.google.com/recaptcha/api.js");
$form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL, 'action'=>'/buscar/tarjeta-prepago']);
echo $form->field($model, 'isUniversity')->hiddenInput(['value' => true])->label(false);
?>
<div class="row">
    <div class="form-standard">
        <div class="col col col-md-4 col-md-offset-4"">
            <?php echo $form->field($model, 'id')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col col-md-12">
            <div class="col col-md-4 col-md-offset-4">
                <?php echo  RecaptchaWidget::widget([
                    "name" => 'recaptcha',
                ]); ?>
                <br>
                <?php echo Html::submitButton('CONSULTAR', ['class' => 'btn btn-success','id' => 'do-submit']) ?>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
