<?php
/* @var $this yii\web\View */
/* @var $model common\models\Complaints */
/* @var $form yii\widgets\ActiveForm */

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use alexeevdv\recaptcha\RecaptchaWidget;
use yii2mod\alert\Alert;

$settings = Yii::$app->settings;
echo Alert::widget();

$this->registerJsFile("https://www.google.com/recaptcha/api.js");
$form = ActiveForm::begin([
            'type' => ActiveForm::TYPE_VERTICAL,
            'action' => '/complaint/save',
            'enableAjaxValidation' => false
        ]);
?>
<div class="row">
    <div class="col-sm-12">
        <h1>Formulario Denuncias</h1>
        <p>Ingrese su Denuncia completando la siguiente información</p>
    </div>
</div>
<div class="row">
    <div class="col-sm-6"><?php echo $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?></div>
    <div class="col-sm-6"><?php echo $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?></div>
</div>
<div class="row">
    <div class="col-sm-6">
        <?php echo $form->field($model, 'identification_card')->textInput(['maxlength' => true])
                ->label('Cédula / Pasaporte (Digitar sin guiones)');
        ?>
    </div>
    <div class="col-sm-6">
        <?php
        echo $form->field($model, 'phone')
                ->textInput(['maxlength' => 8])
                ->label('Teléfono (Digitar sin guiones)');
        ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-6"><?php echo $form->field($model, 'address')->textInput(['maxlength' => true]) ?></div>
    <div class="col-sm-6"><?php echo $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
</div>

<div class="row">
    <div class="col-sm-6"><?php echo $form->field($model, 'fax')->textInput(['maxlength' => true]) ?></div>
    <div class="col-sm-6"><?php echo $form->field($model, 'place_of_work')->textInput(['maxlength' => true]) ?></div>
</div>

<div class="row">
    <div class="col-sm-12">
        <hr>
        <h2>Información sobre la Denuncia</h2></div>
</div>
<div class="row">
    <div class="col-sm-12"><?php echo $form->field($model, 'details')->textarea(['rows' => 6]) ?></div>
</div>
<div class="row">
    <div class="col-sm-12">
        <hr>
        <h2>Personas Denunciadas</h2></div>
</div>

<div class="row">
    <div class="col-sm-6"><?php echo $form->field($model, 'reported_name_01')->textInput(['maxlength' => true]) ?></div>
    <div class="col-sm-6"><?php echo $form->field($model, 'reported_location_01')->textInput(['maxlength' => true]) ?></div>
    <div class="col-sm-6"><?php echo $form->field($model, 'reported_name_02')->textInput(['maxlength' => true])->label(false) ?></div>
    <div class="col-sm-6"><?php echo $form->field($model, 'reported_location_02')->textInput(['maxlength' => true])->label(false) ?></div>
    <div class="col-sm-6"><?php echo $form->field($model, 'reported_name_03')->textInput(['maxlength' => true])->label(false) ?></div>
    <div class="col-sm-6"><?php echo $form->field($model, 'reported_location_03')->textInput(['maxlength' => true])->label(false) ?></div>
</div>
<div class="row">
    <div class="col-sm-6"><?php echo $form->field($model, 'evidence_01')->textInput(['maxlength' => true]) ?></div>
    <div class="col-sm-6"><?php echo $form->field($model, 'evidence_location_01')->textInput(['maxlength' => true]) ?></div>
    <div class="col-sm-6"><?php echo $form->field($model, 'evidence_02')->textInput(['maxlength' => true])->label(false) ?></div>
    <div class="col-sm-6"><?php echo $form->field($model, 'evidence_location_02')->textInput(['maxlength' => true])->label(false) ?>
    </div> <div class="col-sm-6"><?php echo $form->field($model, 'evidence_03')->textInput(['maxlength' => true])->label(false) ?></div>
    <div class="col-sm-6"><?php echo $form->field($model, 'evidence_location_03')->textInput(['maxlength' => true])->label(false) ?></div>
</div>
<div class="row">
    <div class="col-sm-6"><?php echo $form->field($model, 'witness_01')->textInput(['maxlength' => true]) ?></div>
    <div class="col-sm-6"><?php echo $form->field($model, 'witness_location_01')->textInput(['maxlength' => true]) ?></div>
    <div class="col-sm-6"><?php echo $form->field($model, 'witness_02')->textInput(['maxlength' => true])->label(false) ?></div>
    <div class="col-sm-6"><?php echo $form->field($model, 'witness_location_02')->textInput(['maxlength' => true])->label(false) ?></div>
    <div class="col-sm-6"><?php echo $form->field($model, 'witness_03')->textInput(['maxlength' => true])->label(false) ?></div>
    <div class="col-sm-6"><?php echo $form->field($model, 'witness_location_03')->textInput(['maxlength' => true])->label(false) ?></div>
</div>
<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <?php
        echo RecaptchaWidget::widget([
            "name" => 'recaptcha',
        ]);
        ?>
    </div>
    <div class="col-md-4"></div>
</div>
<div class="row">
    <div class="col-md-5"></div>
    <div class="col-md-2"><br><?php echo Html::submitButton('ENVIAR', ['class' => 'btn btn-success', 'id' => 'do-submit']) ?></div>
    <div class="col-md-5"></div>
</div>
<?php ActiveForm::end(); ?>