<?php
/* @var $this yii\web\View */
/* @var $model common\models\Contact */
/* @var $form yii\widgets\ActiveForm */

use alexeevdv\recaptcha\RecaptchaWidget;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii2mod\alert\Alert;

$settings = Yii::$app->settings;
echo Alert::widget();

$this->registerJsFile("https://www.google.com/recaptcha/api.js");
$form = ActiveForm::begin([
            'type' => ActiveForm::TYPE_VERTICAL, 'action' => '/contact/save',
            'enableAjaxValidation' => false
        ]);
?>
<h1>Formulario de Consultas o Sugerencias</h1>
<p>Ingrese su Consulta o Sugerencia completando la siguiente información</p>
<div class="row">
    <div class="col-sm-6"><?php echo $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?></div>
    <div class="col-sm-6"><?php echo $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?></div>
</div>
<div class="row">
    <div class="col-sm-6">
        <?php
        echo $form->field($model, 'identification')->textInput(['maxlength' => true])
                ->label('Cédula / Pasaporte (Digitar sin guiones)');
        ?>
    </div>
    <div class="col-sm-6">
        <?php
        echo $form->field($model, 'phone')->textInput(['maxlength' => 8])
                ->label('Teléfono (Digitar sin guiones)');
        ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-6"><?php echo $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
    <div class="col-sm-6"><?php echo $form->field($model, 'subject')->textInput(['maxlength' => true]) ?></div>
</div>
<div class="row">
    <div class="col-sm-12"><?php echo $form->field($model, 'detail')->textarea(['rows' => 6]) ?></div>
</div>
<div class="row">
    <div class="col-sm-4"></div>
    <div class="col-sm-4">
        <?php
        echo RecaptchaWidget::widget([
            "name" => 'recaptcha',
        ]);
        ?>
    </div>
    <div class="col-sm-4 " ></div>
</div>        <div class="row">
    <div class="col-sm-5"></div>
    <div class="col-sm-2">
        <br>
<?php echo Html::submitButton('ENVIAR', ['class' => 'btn btn-success', 'id' => 'do-submit']) ?>
    </div>
    <div class="col-sm-5 " ></div>
</div>
<?php ActiveForm::end(); ?>

