<?php
use common\models\Category;
$categories = Category::find()
    ->where("NOT slug = 'buscar'")
    ->all();
?>
<h3>Páginas</h3>
<ul style="column-count: 3;">
    <?php foreach($categories as $category): ?>
        <li><?php echo $category->title?></li>
        <ul>
            <?php
            foreach($model as $page):?>
                <?php if($category->id == $page->category_id):?>
                <li><a href="<?php echo "/{$category->slug}/{$page->slug}"?>"><?php echo $page->title?></a></li>
                <?php endif;?>
            <?php  endforeach; ?>

        </ul>
    <?php endforeach; ?>
</ul>