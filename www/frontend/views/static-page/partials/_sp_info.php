<?php

use yii\widgets\LinkPager;
use yii\helpers\Html;
?>
<div class="row">
    <div class="col-md-12">
        <div class="informacion-content">
            <?php
            foreach ($model as $i => $info) {

                echo Html::tag('span', $info->title);
                echo $info->excerpt;

                if (strlen($info->content) >= 1) {
                    ?>
                    <div class="voffset4"></div>
                    <a href="/mas-información/<?php echo $info->id ?>"  class=" btn btn-primary">Ver más</a>
                    <?php
                }
                ?>
                <div class="voffset4"></div>
            <?php } ?>
        </div>
        <nav class="pagination-all" aria-label="Page navigation">
            <?php echo LinkPager::widget(['pagination' => $pages,]); ?>
        </nav>
    </div>
</div>
