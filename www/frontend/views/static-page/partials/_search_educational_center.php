<?php
/* @var $this yii\web\View */
/* @var $model frontend\models\SearchEducationCenterForm */
/* @var $form yii\widgets\ActiveForm */

use alexeevdv\recaptcha\RecaptchaWidget;
use frontend\models\FonabeWs;
use frontend\models\SearchEducationCenterForm;
use kartik\depdrop\DepDrop;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii2mod\alert\Alert;

$settings = Yii::$app->settings;
$province = [];
echo Alert::widget();
$this->registerJsFile("https://www.google.com/recaptcha/api.js");
$form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL, 'action'=>'/buscar/centro-educativo']);
echo $form->field($model, 'isUniversity')->hiddenInput(['value' => false])->label(false);
$ws = new FonabeWs();
?>
<div class="row">
    <div class="col-md-12 main-content">
        <form id="">
            <div class="form-standard">
                <div class="row">
                    <div class="col col-md-4 col-md-offset-1">
                        <?php echo $form->field($model, 'educational_center_name')->textInput(['maxlength' => true,'placeholder' => 'Nombre del Centro Educativo',]); ?>
                        <?php echo $form->field($model, 'province')->dropDownList($ws->getProvincesList(), ['prompt' => 'Seleccione la Provincia','id'=>'province-id']); ?>
                        <?php echo $form->field($model, 'district')->widget(DepDrop::classname(), [
                            'options' => ['id'=>'district-id'],
                            'pluginOptions'=>[
                                'depends'=>['canton-id'],
                                'initialize' => true,
                                'loadingText' => 'Cargando ...',
                                'placeholder' => 'Seleccione el Distrito',
                                'url' => Url::to(['/buscar/distrito'])
                            ]
                        ]);?>
                        <?php echo $form->field($model, 'regional_address_name')->textInput(['maxlength' => true,'placeholder' => 'Nombre de la Dirección Regional',]) ?>
                    </div>
                    <div class="col col-md-4 col-md-offset-1">
                        <?php echo $form->field($model, 'unique_code')->textInput(['maxlength' => true,'placeholder' => 'Código Único del Centro Educativo',]); ?>
                        <?php echo $form->field($model, 'canton')->widget(DepDrop::classname(), [
                            'options' => ['id'=>'canton-id'],
                            'pluginOptions'=>[
                                'depends'=>['province-id'],
                                'placeholder' => 'Seleccione el Cantón',
                                'initialize' => true,
                                'loadingText' => 'Cargando ...',
                                'url' => Url::to(['/buscar/canton'])
                            ]
                        ]);?>




                        <?php echo $form->field($model, 'circuit')->textInput(['maxlength' => true,'placeholder' => 'Nombre del Circuito',]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-md-4 col-md-offset-4">
                        <?php echo  RecaptchaWidget::widget([
                            "name" => 'recaptcha',
                        ]); ?>
                        <br>
                        <?php echo Html::submitButton('CONSULTAR', ['class' => 'btn btn-success','id' => 'do-submit']) ?>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php ActiveForm::end(); ?>