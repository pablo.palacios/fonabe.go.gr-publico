<?php

use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\helpers\Html;
?>
<div class="container">
    <?php
    Pjax::begin();
    foreach ($model as $video) {
        ?>
        <div class= "multimedia-content">
            <div class="col-md-6">
                <iframe width="100%" height="320" src="<?php echo Yii::$app->params['url_youtube'] . $video->url_video ?>" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="col-md-6">
                <?php
                echo Html:: tag('h2', $video->title);
                echo Html:: tag('p', $video->content);
                ?>
            </div>
        </div>
    <?php } ?>
    <div class="row">
        <nav class="pagination-all" aria-label="Page navigation">
            <?php echo LinkPager::widget(['pagination' => $pages,]); ?>
        </nav>
    </div>
</div>
<?php
Pjax::end();
