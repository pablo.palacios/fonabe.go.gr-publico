<?php
/* @var $this yii\web\View */
/* @var $model common\models\Events */
/* @var $pages [] */
/** @var $day  string */
/** @var $month  string */

/** @var $year string */
use edofre\fullcalendar\Fullcalendar;
use yii\helpers\Url;
use yii\web\JqueryAsset;
use yii\web\JsExpression;
?>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <?php
            $setDate = $year . '-' . $month . '-' . $day;
            echo Fullcalendar::widget([
                'options' => [
                    'id' => 'calendar',
                    'language' => 'es',
                ],
                'clientOptions' => [
                    'weekNumbers' => false,
                    'selectable' => false,
                    'defaultView' => 'month',
                    'defaultDate' => $setDate,
                    'eventClick' => new JsExpression(" function(calEvent, jsEvent, view) {
                                        $('#gt-dd').html(function() {
                                          return ''+ moment(calEvent.start).format('DD')   + '<span>'+ _.upperCase(moment(calEvent.start).format('MMM'))   +'</span>';
                                        });
                                        $('#gt-title').html(function() {
                                          return ''+ calEvent.title + '<br><br><a href=\"/evento/'+calEvent.id+'/'+calEvent.start+'\">más...</a>';
                                        });
                                    } "),
                    'dayClick' => new JsExpression(" function(date, jsEvent, view) {
                                        $('#gt-container').html(function() {
                                          return '<div class=\"blog-section\">Cargando ...</div>';
                                        });
                                        var startDate = date.format('YYYY-MM-DD');
                                        var endDate = date.format('YYYY-MM-DD');
                                        var cache = new Date().getTime();
                                        $.getJSON(
                                            '/events/jsoncalendar?start='+startDate+'&end='+endDate+'&_='+cache,
                                            function(data) {
                                                var str = '';
                                                _.forEach(data, function(value) {


                                                   var newHtml = '' +
                                                   '<div class=\"blog-section\"><p>'+
                                                    moment.utc(value.start).format('DD') +
                                                    '<span>'+
                                                    _.upperCase(moment(value.start).format('MMM')) +
                                                    '</span></p>'+
                                                     '<p>'+value.title+'<br><a href=\"/evento/'+value.id+'/'+value.start+'\">más...</a></p></div>';
                                                  str = str + newHtml;
                                                });

                                                if (data.length === 0 ){
                                                     str = '<div class=\"blog-section\"><p>'+
                                                     date.format('DD') +
                                                     '<span>'+
                                                     _.upperCase(date.format('MMM')) +
                                                     '</span></p>' +
                                                     '<p>No existen eventos en el día seleccionado</p></div>';
                                                }

                                                $('#gt-container').html(function() {
                                                  return str;
                                                });
                                            });
                                    } "),
                    'eventRender' => new JsExpression(" function(event, element) {
                                        var eventStart = moment(event.start);
                                        var eventEnd = event._end === null ? eventStart : moment(event.end);
                                        var diffInDays = eventEnd.diff(eventStart, 'days');
                                        $(\"td[data-date='\" + eventStart.format('YYYY-MM-DD') + \"']\").css('background-color','#0C5FA9');
                                        for(var i = 1; i < diffInDays; i++) {
                                            eventStart.add(1,'day');
                                            $(\"td[data-date='\" + eventStart.format('YYYY-MM-DD') + \"']\").css('background-color','#0C5FA9');
                                        }
                                    } "),
                ],
                'events' => Url::to(['/events/jsoncalendar']),
            ]);
            ?>
        </div>
        <div class="col-md-4 sidebar-calendario blog-preview-wrap" style="overflow: auto; height: 500px;"
             id="gt-container">
            <div class="blog-section">
                Haga click en el día marcado para ver los eventos y sus detalles.
            </div>
        </div>
    </div>
</div>
<br>