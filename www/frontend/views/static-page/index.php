<?php

use yii\widgets\Breadcrumbs;

$this->title = $page->title;

if ($category->slug != 'pagina' && $category->slug != 'noticias') {
  $this->params['breadcrumbs'][] = ['label' => $page->category->title, 'url' => ''];
}
$this->params['breadcrumbs'][] = $page->title;

echo $this->render('/partials/_main_submenu.php', [
    'category' => $category,
    'page' => $page,
]);
?>
<div class="container">
  <?php
  echo Breadcrumbs::widget([
      'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
  ]);
  ?>
</div>
<div class="container">
  <?php echo $page->content ?>
</div>
<div class="container">
  <?php
  echo ($masterDetailControl) ? $this->render($renderView, [
              'model' => $masterDetailModel,
              'pages' => $pages,
              'page' => $page,
              'day' => $day,
              'month' => $month,
              'year' => $year,
          ]) : '';
  /*
   * Valida la configuración de BD (si muestra o no documentos) y también la variable
   * $document que es seteada en el controller para poder definir secciones específicas
   * que a pesar de tener administración de archivos en BD no se requieran que muestren
   * los archivos.
   */
  if ($page->has_md_documents && $masterDetailDocuments !== '' && $document) {
    echo ($page->has_md_documents) ? $this->render('partials/_sp_files', [
                'model' => $masterDetailDocuments,
                'pages' => $pages
            ]) : '';
  }
  if ($page->has_md_video && $masterDetailVideos !== '') {
    echo ($page->has_md_video) ? $this->render('partials/_sp_videos', [
                'model' => $masterDetailVideos,
                'pages' => $pages
            ]) : '';
  }
  ?>
</div>
