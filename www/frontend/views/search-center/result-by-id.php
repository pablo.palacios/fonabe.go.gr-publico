<?php

use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = 'Centros Educativo';

$this->registerCss(<<<CSS
    a#w0-togdata-page > i {
        display: none;
    }
    a#w0-togdata-all > i {
        display: none;
    }  
    button#w2 > i {
        display: none;
    }
    a.export-csv > i {
        display: none;
    }
    
    a.export-pdf > i {
        display: none;
    }
CSS
);
?>
<div class="container">
    <br><br><?php echo Html::a('< Volver', '/buscar/centro-educativo', ['class' => 'btn btn-primary']); ?><br><br>
    <h2>Listado Beneficiarios de Centro Educativo</h2>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?PHP

            echo GridView::widget([
                'dataProvider' => $provider,
                'responsive' => true,
                'hover' => true,
                'pjax' => false,

                'export'=>[
                    'fontAwesome'=>true,
                    'showConfirmAlert'=>false,
                    'target'=>GridView::TARGET_BLANK
                ],

                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'CODIGO_UNICO',
                        'label' => 'Código Único'
                    ],
                    [
                        'attribute' => 'INSTITUCION',
                        'label' => 'Institución',
                    ],
                    [
                        'attribute' => 'CEDULA',
                        'label' => 'Cédula',
                    ],
                    [
                        'attribute' => 'APELLIDO1',
                        'label' => 'Primer Apellido',
                    ],
                    [
                        'attribute' => 'APELLIDO2',
                        'label' => 'Segundo Apellido',
                    ],
                    'NOMBRE',
                    [
                        'attribute' => 'DESC_PROYECTO',
                        'label' => 'Proyecto',
                    ],
                    'ESTADO',
                    [
                        'attribute' => 'MOTIVO_SUSPENSION',
                        'label' => 'Motivo Suspensión',
                    ],
                    'ESTADO_NO_POSTULANTE',
                ],
                'panel'=>[
                    'type'=>'primary',
                    'heading'=>'',
                    'icons' => false,
                ],
                'exportConfig' =>  [
                    GridView::PDF=>[
                        'label' => 'Exportar a PDF',
                        'icon' => '',
                        'iconOptions' => '',
                        'showHeader' => 'Fonabe',
                        'showPageSummary' => false,
                        'showFooter' => false,
                        'showCaption' => false,
                        'filename' => 'fonabe_centros_educativo_'.date('d-M-Y'),
                        'alertMsg' => false,
                        'config' => [
                            'marginTop' => 10,
                            'marginBottom' => 10,
                            'methods' => [
                                'SetHeader' => [
                                    ['odd' => '', 'even' => '']
                                ],
                                'SetFooter' => [
                                    ['odd' => '', 'even' => '']
                                ],
                            ],
                            'options' => [
                                'title' => '',
                                'subject' => '',
                                'keywords' => ''
                            ],
                        ],
                    ],
                    GridView::CSV => [
                        'label' => 'Exportar a CSV',
                        'filename' => 'fonabe_centros_educativos_'.date('d-M-Y'),
                    ],
                ],
            ])
            ?>
        </div>
    </div>
</div>
