<?php
/* @var $month array */
/* @var $cantones array */
/* @var $year array */
/* @var $this \yii\web\View */
/* @var $model \frontend\models\SearchEducationCenterForm */
/* @var $pages array */

use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = 'Buscar Centros Educativos';
$this->registerCss(<<<CSS
    a#w0-togdata-all > i {
        display: none;
    }
    a#w0-togdata-page > i {
        display: none;
    }   
    button#w2 > i {
        display: none;
    }
    a.export-csv > i {
        display: none;
    }
    
    a.export-pdf > i {
        display: none;
    }
CSS
);
?>
<div class="container">



    <br><br><?php echo Html::a('Buscar nuevamente', '/centro-educativo/centros-educativos', ['class' => 'btn btn-primary']); ?><br><br>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?PHP
            echo GridView::widget([
                'dataProvider' => $provider,
                'autoXlFormat'=>true,
                'export'=>[
                    'fontAwesome'=>true,
                    'showConfirmAlert'=>false,
                    'target'=>GridView::TARGET_BLANK
                ],

                'responsive' => true,
                'hover' => true,
                'pjax' => false,
                'columns' => [
                    [
                        'attribute' => 'CODIGO_UNICO',
                        'label' => 'Código Único'
                    ],
                    [
                        'attribute' => 'DIRECCION_REGIONAL',
                        'label' => 'Dirección Regional',
                    ],
                    'CIRCUITO',
                    [
                        'attribute' => 'INSTITUCION',
                        'label' => 'Institución',
                    ],
                    'DIRECTOR',
                    [
                        'attribute' => 'CANTIDAD_BECAS',
                        'label' => 'Cant. Becas',
                    ],
                    [
                        'attribute' => 'CODIGO_UNICO',
                        'filter' => false,
                        'format' => 'html',
                        'label' => 'Más Detalles',
                        'value' => function($data) {
                            return Html::a('Ver beneficiarios', ['/ver/centro-educativo/' . $data['CODIGO_UNICO']], ['class' => 'btn btn-warning']);
                        },
                    ],
                ],
                'panel'=>[
                    'type'=>'primary',
                    'heading'=>''
                ],
                'exportConfig' =>  [
                    GridView::PDF=>[
                        'label' => 'Exportar a PDF',
                        'icon' => '',
                        'iconOptions' => '',
                        'showHeader' => 'Fonabe',
                        'showPageSummary' => false,
                        'showFooter' => false,
                        'showCaption' => false,
                        'filename' => 'fonabe_centros_educativos_'.date('d-M-Y'),
                        'alertMsg' => false,
                        'config' => [
                            'marginTop' => 10,
                            'marginBottom' => 10,
                            'methods' => [
                                'SetHeader' => [
                                    ['odd' => '', 'even' => '']
                                ],
                                'SetFooter' => [
                                    ['odd' => '', 'even' => '']
                                ],
                            ],
                            'options' => [
                                'title' => '',
                                'subject' => '',
                                'keywords' => ''
                            ],
                        ],
                    ],
                    GridView::CSV => [
                        'label' => 'Exportar a CSV',
                        'filename' => 'fonabe_centros_educativos_'.date('d-M-Y'),
                    ],
                ],
            ])
            ?>
        </div>
    </div>
</div>
