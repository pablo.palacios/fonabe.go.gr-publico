<?php

use kartik\grid\GridView;

$this->title = 'Buscar por Tarjeta Prepago';
?>
<div class="container">
    <?php
    echo $this->render('/static-page/partials/_search_prepaid_card', [
        'model' => $model,
        'pages' => $pages,
        'month' => $month,
        'year' => $year,
    ]);
    ?>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php
            echo GridView::widget([
                'dataProvider' => $provider,
                'responsive' => true,
                'hover' => true,
                'columns' => [
                    [
                        'attribute' => 'CEDULA',
                        'label' => 'Cédula'
                    ],
                    [
                        'attribute' => 'NOMBRE',
                        'label' => 'Beneficiario'
                    ],
                    [
                        'attribute' => 'CENTRO_EDUCATIVO',
                        'label' => 'Centro Educativo'
                    ],
                    [
                        'attribute' => 'NOMBRE_OFICINA',
                        'label' => 'Oficina del BNCR'
                    ],
                    [
                        'attribute' => 'AÑO',
                        'label' => 'Año'
                    ],
                ]
            ]);
            ?>
        </div>
    </div>
</div>

