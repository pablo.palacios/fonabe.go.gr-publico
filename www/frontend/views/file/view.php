<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii2assets\pdfjs\PdfJs;

/* @var $this yii\web\View */
/* @var $file common\models\Files */
/* @var $pages [] */
/* @var $page common\models\StaticPages */

$this->registerJsFile("/js/pdf_viewer.js");
$this->params['breadcrumbs'][] = ['label' => $page->category->title, 'url' => '/'.$page->category->slug . '/'. $page->slug];
$this->params['breadcrumbs'][] = ['label' => $page->title, 'url' => Yii::$app->request->referrer];
$this->params['breadcrumbs'][] = $file->title;
?>
<div class="container">
    <?php echo Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],])?>
</div>
<div class="container">
    <h2><?php echo $file->title?></h2>
    <p><?php echo $file->description?></p>
    <br>
    <?php
    if(strrpos($file->file_url, 'pdf')){
        echo PdfJs::widget([
            'url'=> Url::base().$path,
            'buttons'=>[
                'presentationMode' => false,
                'openFile' => false,
                'print' => false,
                'download' => false,
                'viewBookmark' => false,
                'secondaryToolbarToggle' => false
            ]
        ]);
    }
    ?>
    <a href="/descargar/<?php echo $file->id?>" target="_blank" class="btn btn-primary">DESCARGAR</a>
</div>


