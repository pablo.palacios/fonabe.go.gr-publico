<?php

use common\models\SliderImages;
use yii\web\JqueryAsset;
use yii\bootstrap\Html;

$this->registerJsFile('/js/frontend_slider.js', ['depends' => [JqueryAsset::className()]]);
$slide_images = SliderImages::getSliders();

$settings = Yii::$app->settings;
?>
<div class="container-fluid">
    <div class="row">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <?php foreach ($slide_images as $i => $banner) { ?>
                    <li data-target="#myCarousel" data-slide-to="<?php echo $i + 1 ?>" <?php echo($i + 1 == 1) ? 'class="active"' : '' ?>></li>
                <?php } ?>
            </ol>
            <div class="carousel-inner" role="listbox">
                <?php foreach ($slide_images as $i => $banner) { ?>
                    <div <?php echo($i + 1 == 1) ? 'class="item active"' : 'class="item"' ?>> <img class="first-slide" src="<?php echo '/img-uploads/' . $banner->image_url ?>" alt="<?php echo $banner->slide_text ?>">
                        <div class="container">
                            <div class="carousel-caption">
                                <?php
                                echo Html::tag('h1', Html::a($banner->slide_text, $banner->image_link));
                                ?>

                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="sr-only">Anterior</span> </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            </span> <span class="sr-only">Siguiente</span> </a>
</div>
</div>
</div>

