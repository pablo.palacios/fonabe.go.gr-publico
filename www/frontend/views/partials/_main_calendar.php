<?php
use kartik\widgets\Spinner;
$this->registerCssFile("https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css");
$this->registerJsFile("https://cdn.polyfill.io/v2/polyfill.min.js");
$this->registerJsFile("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js");
$this->registerJsFile("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/locale/es.js");
$this->registerJsFile("https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js");
$this->registerJsFile("https://cdn.jsdelivr.net/npm/vue");
$this->registerJsFile("https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js");
$this->registerJsFile("https://unpkg.com/vue-swal");
$this->registerJsFile("/js/fonabe_calendar.js");
?>
<nav class="pagination-home" aria-label="Page navigation" id="app" >
    <div class="row" v-cloak>
        <a class="page-link-left" aria-label="Previous" @click="prev()" style="cursor: pointer;"><span class="sr-only">Previo</span> </a>
        <div class="col col-md-3">
            <h2 v-cloak>{{ calendarDate }}</h2>
            <a v-bind:href="'/fonabe/calendario/' + year + '/' + month" class=" btn btn-primary" >Ver calendario</a> </div>
        <div class="col col-md-9 gt-cal" v-show="!hide">
            <ul class="pagination">
                <li class="page-item" v-for="event in events" v-bind:class="{ active: event.active }">
                    <a v-if="event.active" @click="alertMe(event)" class="page-link">{{event.day}}</a>
                    <a v-if="!event.active" class="page-link">{{event.day}}</a>
                </li>
            </ul>
        </div>
        <div class="col col-md-9 gt-cal" v-show="hide">
            <?php echo Spinner::widget(['preset' => 'medium', 'align' => 'center', 'color' => 'grey']); ?>
        </div>
        <a class="page-link-right" aria-label="Next"  @click="next()" style="cursor: pointer;"><span class="sr-only">Siguiente</span> </a>
    </div>
</nav>
