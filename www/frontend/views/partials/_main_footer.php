<?php

$settings = Yii::$app->settings;
?>

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <a href="<?php echo $settings->get('main', 'facebook_url')?>" class="btn btn-primary"><i class="icon-g-facebook"></i></a>
                <a href="<?php echo $settings->get('main', 'twitter_url')?>" class="btn btn-primary"><i class="icon-g-twitter"></i></a>
            </div>
            <div class="col-md-10 ">
                <nav class="nav-bar-footer text-right">
                    <a href="/principal/mapa-del-sitio">Mapa del sitio</a>
                    <a href="/principal/privacidad">Privacidad</a>
                    <a href="/principal/sitios-de-interes">Sitios de interés</a>
                    <a href="/principal/preguntas-frecuentes">Preguntas frecuentes</a>
                    <a href="<?php echo $settings->get('main', 'correo_institucional_url');?>">Correo institucional</a>
                    <a href="<?php echo $settings->get('main', 'admin_url');?>/user/login">Inicio de sesión</a>
                </nav>
            </div>
        </div>
    </div>
</footer>
