<?php
use \common\models\StaticPages;
$s_pages = new StaticPages();
$pages= $s_pages->getPagesForMenu($category->id)
?>
<div class="container-fluid">
    <?php if($page->slug != 'noticias' && $category->slug != 'principal' && $category->slug != 'pagina' ){?>
    <div class="row main-nav-bar">
        <div class="carousel-icons-menu">
            <?php foreach ($pages as $pag) {
                if($category->id === $pag->category_id) {
                ?>
                <div class="carousel-menu-item col-md-2 active">
                    <a href="<?php echo "/{$pag->category->slug}/{$pag->slug}"?>" class="icons-main-menu btn btn-primary">
                        <i class="<?php echo $pag->icon?>"></i>
                        <span><?php echo $pag->title?></span>
                    </a>
                </div>
            <?php }
                } ?>
        </div>
        <?php } ?>
    </div>
    <div class="row main-title">
        <h1><?php echo $page->title?></h1>
    </div>
</div>



