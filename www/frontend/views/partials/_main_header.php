<?php

use frontend\models\SearchForm;

$searchModel = new SearchForm();
$settings = Yii::$app->settings;
$current_uri = $_SERVER['REQUEST_URI'];
?><header id="header">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <a href="/"><img src="/uploads/logo.png" class="logo" alt="Logo Fonabe" width="213" height="80"></a>
            </div>
            <div class="col-md-6 text-right">
                <a href="#" class="btn btn-primary" id="decreaseFont">A-</a>
                <a href="#" class="btn btn-primary" id="increaseFont">A+</a>
                <div class="btn-search">
                    <form id="w2"
                          method="get"
                          action="/buscar">
                        <input id="searchform-q"
                               placeholder="Buscar"
                               class="search-header"
                               type="text"
                               name="SearchForm[q]"
                               aria-required="true"
                               aria-invalid="true"
                               >
                    </form>
                </div>
            </div>
            <div class="col-md-6 text-right">
                <div class="contact-icon">
                    <a href="/contactenos/contacto"><i class="icon-g-telefono"></i></a>
                    <span class="separator"></span>
                    <a href="/fonabe/organigrama"><i class="icon-g-mapa-sitio"></i></a>
                    <span class="separator"></span>
                    <a href="/principal/preguntas-frecuentes"><i class="icon-g-pregunta"></i></a>
                </div>
                <div class="social-icon">
                    <a href="<?php echo $settings->get('main', 'facebook_url') ?>" class="btn btn-primary"><i class="icon-g-facebook"></i></a>
                    <a href="<?php echo $settings->get('main', 'twitter_url') ?>" class="btn btn-primary"><i class="icon-g-twitter"></i></a>
                </div>
            </div>
        </div>
    </div>

    <nav class="main-nav-bar navbar navbar-inverse navbar-static-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Menu</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav principal-menu">
                    <li  class="<?php echo(($current_uri === '/') ? 'active' : '') ?>"><a href="/">Inicio</a></li>
                    <li  class="<?php echo((strpos($current_uri, 'centro-educativo')) ? 'active' : '') ?>">
                        <a href="/centro-educativo/informacion">Centro Educativo</a>
                    </li>
                    <li class="<?php echo((strpos($current_uri, 'universidad')) ? 'active' : '') ?>">
                        <a href="/universidad/becas-universitarias">Universidad</a>
                    </li>
                    <li class="<?php echo((strpos($current_uri, 'educacion-abierta')) ? 'active' : '') ?>">
                        <a href="/educacion-abierta/becas-universitarias">Educación abierta</a>
                    </li>
                    <li class="<?php echo((strpos($current_uri, 'noticias')) ? 'active' : '') ?>">
                        <a href="/noticias/noticias">Noticias</a>
                    </li>
                    <li class="<?php echo((strpos($current_uri, 'transparencia')) ? 'active' : '') ?>">
                        <a href="/transparencia/normativa">Transparencia</a>
                    </li>
                    <li class="<?php echo((strpos($current_uri, 'fonabe')) ? 'active' : '') ?>">
                        <a href="/fonabe/junta-directiva">FONABE</a>
                    </li>
                    <li class="<?php echo((strpos($current_uri, 'contactenos')) ? 'active' : '') ?>">
                        <a href="/contactenos/contacto">Contáctenos</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>