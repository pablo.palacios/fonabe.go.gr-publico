<?php
$settings = Yii::$app->settings;
?>
<div class="icons-home-wrap">
    <div class="container">
        <div class="col col-md-3">
            <h2>Datos Abiertos</h2>
        </div>
        <?php
        if($settings->get('data', 'link_label_1') !== '#OCULTO'){
        ?>
        <div class="col col-md-3">
            <a href="<?php echo $settings->get('data', 'link_url_1')?>" class="icons-home-bottom btn btn-primary"><i class="icon-g-trofeo"></i>
                <span><?php echo $settings->get('data', 'link_label_1')?></span>
            </a>
        </div>
        <?php
        }
        if($settings->get('data', 'link_label_2') !== '#OCULTO'){
        ?>
        <div class="col col-md-3">
            <a href="<?php echo $settings->get('data', 'link_url_2')?>" class="icons-home-bottom btn btn-primary"><i class="icon-g-financiero"></i>
                <span><?php echo $settings->get('data', 'link_label_2')?></span>
            </a>
        </div>
            <?php
        }

        if($settings->get('data', 'link_label_3') !== '#OCULTO'){
            ?>
        <div class="col col-md-3"><a href="<?php echo $settings->get('data', 'link_url_3')?>" class="icons-home-bottom btn btn-primary"><i class="icon-g-demografia"></i>
                <span><?php echo $settings->get('data', 'link_label_3')?></span></a></div>
        <?php
         }
        ?>
    </div>
</div>