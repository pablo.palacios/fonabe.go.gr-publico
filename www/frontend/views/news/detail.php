<?php

use \yii\helpers\Html;
use yii\widgets\Breadcrumbs;

$formatter = \Yii::$app->formatter;
Yii::$app->formatter->locale = 'es-Es';
$epoch = $news->created_at;
$dt = new DateTime("@$epoch");
$print_dt = $dt->format('Y-m-d');
$this->title = $news->title;

$this->registerJsFile("/js/pdf_viewer.js");
$this->params['breadcrumbs'][] = ['label' => 'Noticias', 'url' => Yii::$app->request->referrer];
$this->params['breadcrumbs'][] = $news->title;
?>
<div class="container">
    <?php echo Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]) ?>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="blog-preview-wrap">
                <div class="blog-section">
                    <p> <?php echo $dt->format('d') ?> <span><?php echo $formatter->asDate($print_dt, 'MMM') ?></span></p>
                    <h2><?php echo $news->title ?></h2>
                </div>
                <div class="blog-content">
                    <div class="imgBlog"><img src="<?php echo'/img-uploads/' . $news->url_imagen ?>"></div>
                        <?php echo $news->content ?>
                        <?php echo Html::a('< Volver', Yii::$app->request->referrer, ['class' => 'btn btn-primary']); ?>
                </div>
            </div>
        </div>
    </div>
</div>