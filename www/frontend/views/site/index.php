<?php

use yii\helpers\Html;
use yii\web\JqueryAsset;

$settings = Yii::$app->settings;
$this->title = $settings->get('main', 'site_main_title');
$this->registerJsFile("/js/frontend_main_index.js", ['depends' => [JqueryAsset::className()]]);
echo $this->render('../partials/_main_slider.php');
?>
<div class="container">
    <div class="row  home-template">
        <?php foreach ($news as $the_news) { ?>
            <div class="col col-md-4">
                <div class="title-tumbnail">
                    <?php echo Html::tag('p', $the_news->title); ?>
                    <img class="img-noticia-home" src="<?php echo '/img-uploads/' . $the_news->url_imagen; ?>">
                </div>
                <div class="content-tumbnail">
                    <?php echo Html::tag('p', $the_news->extract, ['class' => 'truncate_me']); ?>
                </div>
                <?php echo Html::a('Leer más', ['noticia/' . $the_news->slug], ['class' => 'btn btn-primary']) ?>
            </div>
        <?php } ?>
        <div class="col col-md-3 ">
            <ul>
                <li style="padding-left: 12px;">
                    <a href="<?php echo $settings->get('index', 'urlLink0') ?>" class="">
                        <i class="ico-btn"></i>
                        <span style="margin-top: 11px;"><?php echo $settings->get('index', 'link0_label') ?></span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $settings->get('index', 'urlLink1') ?>" class="">
                        <i class="icon-g-tipo"></i>
                        <span><?php echo $settings->get('index', 'link1_label') ?></span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $settings->get('index', 'urlLink2') ?>" class="">
                        <i class="icon-g-tarjeta-prepago"></i>
                        <span><?php echo $settings->get('index', 'link2_label') ?></span>
                    </a>
                </li>
                <li><a href="<?php echo $settings->get('index', 'urlLink3') ?>" class="">
                        <i class="icon-g-reloj"></i>
                        <span><?php echo $settings->get('index', 'link3_label') ?></span>
                    </a>
                </li>
            </ul>
            <div class="content-nav">
                <h4>Documentos más vistos</h4>
                <?php foreach ($files as $file) { ?>
                    <a href="<?php echo '/ver/' . $file->id ?>" class=""><?php echo $file->title ?></a>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php echo $this->render('../partials/_main_calendar.php'); ?>
</div>
<?php echo $this->render('../partials/_main_open_data.php'); ?>