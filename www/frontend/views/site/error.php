<?php

use yii\helpers\Html;

$readable_name = '';

switch ($name) {
    case 'Not Found (#404)':
        $readable_name = '404  - No encontrado';
        break;
    case '':
        $readable_name = 'Error no determinado';
        break;
    default:
        $readable_name = 'Hemos encontrado un problema';
}
$this->title = $readable_name;
?>

<div class="container">
    <?php echo Html::tag('h1', $readable_name); ?>
    <div class="alert alert-danger">
        <?php echo nl2br(Html::encode($message)) ?>
    </div>
    <div class="error-actions">
        <?php
        //si es un usuario que no ha navegado regresa al home, si no a la última página donde estuvo
        $ruta = is_null(Yii::$app->request->referrer) ? '/' : Yii::$app->request->referrer;
        echo Html::a(Yii::t('app', ' Regresar'), $ruta, [
            'class' => 'btn btn-primary btn-lg',
        ]);
        ?>

    </div>
</div>
