<?php
use \yii\helpers\Html;
use yii\widgets\Breadcrumbs;

$this->title = $event->title;
$this->params['breadcrumbs'][] = 'EVENTO: '. $event->title;
?>


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2><?php echo $event->title ?></h2>
            <hr>
            <p><b>Fecha: </b><?php echo $event->date ?></p>
            <p><b>Hora de inicio: </b><?php echo $event->time ?></p>
            <p><b>Duración: </b><?php echo $event->hours ?> horas</p>

            <p><?php echo $event->description ?></p>
        </div>
    </div>
</div>
<div class="container">
    <br>
    <?php echo Html::a( '< Volver', Yii::$app->request->referrer, ['class'=> 'btn btn-primary']); ?>
</div>