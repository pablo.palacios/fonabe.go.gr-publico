<?php

use kartik\grid\GridView;

$this->title = 'Buscar Centros Educativos';
?>
<div class="container">
    <?php echo $this->render('../static-page/partials/_search_request_status', ["model" => $model, 'pages' => $pages, 'month' => $month, 'year' => $year,]) ?>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php
            echo GridView::widget([
                'dataProvider' => $provider,
                'responsive' => true,
                'hover' => true,
                'columns' => [
                    [
                        'attribute' => 'CEDULA',
                        'label' => 'Cédula'
                    ],
                    'NOMBRE_COMPLETO',
                    [
                        'attribute' => 'DESC_PROYECTO',
                        'label' => 'Proyecto'
                    ],
                    'RIGE',
                    'VENCE',
                    [
                        'attribute' => 'FECHA_APROBACION',
                        'label' => 'Fecha Aprobación',
                        'format' => ['date']
                    ],
                    'ESTADO',
                    [
                        'attribute' => 'MOTIVO_ESTADO',
                        'label' => 'Motivo Estado',
                    ],
                    'NOMBRE_CENTRO_EDUCATIVO',
                    [
                        'attribute' => 'AÑO',
                        'label' => 'Año'
                    ],
                ]
            ]);
            ?>
            <br>
        </div>
    </div>
</div>

