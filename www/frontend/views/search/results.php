<?php
/* @var $this yii\web\View */
/* @var $searchModel \frontend\models\SearchForm */
/** @var $news  \common\models\News */
/** @var $newsPagination  \yii\data\Pagination */
/** @var $files  \common\models\News */
/** @var $filesPagination  \yii\data\Pagination */
/** @var $static_pages \common\models\StaticPages */

/** @var $pagesPagination  \yii\data\Pagination */

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = 'Búsqueda';
$this->registerJsFile("https://cdnjs.cloudflare.com/ajax/libs/mark.js/8.11.1/jquery.mark.min.js", ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile("/js/frontend_search.js", ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<br>
<div class="container">
    <div class="row">

        <div class="col-md-8 gt-src-input">
            <?php $form = ActiveForm::begin([
                'type' => ActiveForm::TYPE_INLINE,
                'method' => 'get',
                'action' => Url::to(['/buscar']),
            ]);
            echo $form->field($searchModel, 'q', [
                'addon' => [
                    'append' => [
                        'content' => Html::submitButton('BUSCAR', ['class' => 'btn btn-warn']),
                        'asButton' => true
                    ]
                ]
            ]);
            ?>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="gt-search">Buscar en: </label>
                <label class="gt-search"><input name="pag" type="checkbox" id="cbox1"  value="true" <?php echo ($searchModel->includePages)?'checked':''?> > Páginas</label>
                <label class="gt-search"><input name="doc" type="checkbox" id="cbox2"  value="true" <?php echo ($searchModel->includeFiles)?'checked':''?>> Documentos</label>
                <label class="gt-search"><input name="nws" type="checkbox" id="cbox3"  value="true" <?php echo ($searchModel->includeNews)?'checked':''?>> Noticias</label>
            </div>
            <div class="row">
                <label class="gt-search">Buscar palabras: </label>
                <label class="gt-search"><input name="type" type="radio" id="rd1" value="true"<?php echo ($searchModel->type)?'checked':''?>>consecutivas</label>
                <label class="gt-search"><input name="type" type="radio" id="rd2" value="false" <?php echo (!$searchModel->type)?'checked':''?>>sueltas</label>
            </div>
        </div>
    </div>

    <!-- STATIC_PAGES -->
    <?php if (sizeof($static_pages) !== 0) { ?>
        <div class="row">
            <h2>Páginas</h2>
            <div class="col-md-12">
                <?php foreach ($static_pages as $key => $page) {
                    $p_title = $page['title'];
                    $p_slug = $page['slug'];
                    $c_title = $page['cat_title'];
                    $c_slug = $page['cat_slug'];
                    $p_description = mb_strimwidth($page['excerpt'], 0, 255, "...");
                    ?>
                    <h3 class="context"><a href="<?php echo "/{$c_slug}/{$p_slug}"  ?>"><?php echo $c_title . ' / ' . $p_title ?></a></h3>
                    <p class="context"><?php echo $p_description ?></p>
                    <p class="context"><a href="<?php echo "/{$c_slug}/{$p_slug}"  ?>">Ver más...</a></p>
                    <hr>
                <?php }
                echo LinkPager::widget([
                    'pagination' => $pagesPagination,
                ]);
                ?>
            </div>
        </div>
    <?php } ?>

    <!-- NEWS -->
    <?php if (sizeof($news) !== 0) { ?>
        <div class="row">
            <h2>NOTICIAS</h2>
            <div class="col-md-12">
                <?php foreach ($news as $key => $nws) {
                    $n_title = $nws['title'];
                    $n_slug = $nws['slug'];
                    $n_description = mb_strimwidth($nws['extract'], 0, 255, "...");
                    ?>
                    <h3 class="context"><a href="<?php echo "/noticia/{$n_slug}"?>"><?php echo $n_title ?></a></h3>
                    <p class="context"><?php echo $n_description ?></p>
                    <p class="context"><a href="<?php echo "/noticia/{$n_slug}"?>">Ver más...</a></p>
                    <hr>
                <?php }
                echo LinkPager::widget([
                    'pagination' => $newsPagination,
                ]);
                ?>
            </div>
        </div>
    <?php } ?>

    <!-- FILES -->
    <?php if (sizeof($files) !== 0) { ?>
        <div class="row">
            <h2>DOCUMENTOS</h2>
            <div class="col-md-12">
                <?php foreach ($files as $key => $file) {
                    $f_title = $file['title'];
                    $f_id = $file['id'];
                    $f_description = mb_strimwidth($file['description'], 0, 255, "...");
                    ?>
                    <h3 class="context"><a href="<?php echo "/ver/{$f_id}"?>"><?php echo $f_title ?></a></h3>
                    <p class="context"><?php echo $f_description ?></p>
                    <p class="context"><a href="<?php echo "/ver/{$f_id}"?>">Ver más...</a></p>
                    <hr>
                <?php }
                echo LinkPager::widget([
                    'pagination' => $filesPagination,
                ]);
                ?>
            </div>
        </div>
    <?php } ?>
    <?php
    ActiveForm::end(); ?>

</div>