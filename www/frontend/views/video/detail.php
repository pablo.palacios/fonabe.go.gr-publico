<?php
use \yii\helpers\Html;
use yii\widgets\Breadcrumbs;

$formatter = \Yii::$app->formatter;
Yii::$app->formatter->locale = 'es-Es';
$this->title = $video->title;

$this->registerJsFile("/js/pdf_viewer.js");
$this->params['breadcrumbs'][] = ['label' => 'Videos', 'url' => Yii::$app->request->referrer];
$this->params['breadcrumbs'][] = $video->title;
?>
<div class="container">
    <?php echo Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],])?>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
                <div class="blog-preview-wrap">
                    <div class="blog-section">
                        <h2><?php echo $video->title?></h2>
                    </div>
                    <div class="blog-content">
                        <div class="video">
                            <iframe  src="https://www.youtube.com/embed/<?php echo $video->url_video?>?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                        <?php echo $video->content?>
                        <?php echo Html::a( '< Volver', Yii::$app->request->referrer, ['class'=> 'btn btn-primary']); ?>
                    </div>
                </div>
        </div>
    </div>
</div>