<?php

use kartik\mpdf\Pdf;

$params = array_merge(
        require __DIR__ . '/../../common/config/params.php', require __DIR__ . '/../../common/config/params-local.php', require __DIR__ . '/params.php', require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'es',
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'assetManager' => [
            'appendTimestamp' => true,
        ],
        'user' => [
            'as frontend' => 'dektrium\user\filters\FrontendFilter',
        ],
        'session' => [
            'name' => 'sesval_ePmWC4qsVtjd',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                'ver/centro-educativo/<unicod>' => 'search-center/search-center-by-id',
                'ver/centro-educativo' => 'search-center/search-center-by-id',
                'noticia/<news_slug>' => 'news/index',
                'video/<video_slug>' => 'video/index',
                'descargar/<file_id>' => 'file/download',
                'ver/<file_id>' => 'file/view',
                'evento/<id>' => 'events/index',
                'evento/<id>/<epoch>' => 'events/index',
                'mas-información/<id>' => 'info/index',
                'api/get-events/<month>/<year>' => 'events/get-events',
                'contact/save' => 'contact/save',
                'complaint/save' => 'complaints/save',
                'buscar/centro-educativo' => 'search-center/search',
                'buscar/canton' => 'search-center/canton',
                'buscar/distrito' => 'search-center/district',
                'buscar/estado-solicitud' => 'search-request-status/search',
                'buscar/tarjeta-prepago' => 'search-prepaid-card/search',
                'buscar' => 'search/q',
                'buscar/q' => 'search/q',
                'settings/default/index' => '/error',
                'settings/default' => '/error',
                'settings' => '/error',
                'site/header' => 'site/header',
                'site/footer' => 'site/footer',
                'events/jsoncalendar' => 'events/jsoncalendar',
                '<category_slug>/<page_slug>/<year>/<month>/<day>' => 'static-page/index',
                '<category_slug>/<page_slug>/<year>/<month>' => 'static-page/index',
                '<category_slug>/<page_slug>' => 'static-page/index',
            ],
        ],
        'settings' => [
            'class' => 'yii2mod\settings\components\Settings',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'pdf' => [
            'class' => Pdf::classname(),
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'options' => ['title' => 'FONABE'],
        ]
    ],
    'modules' => [
        'settings' => [
            'class' => 'yii2mod\settings\Module',
        ],
        'pdfjs' => [
            'class' => '\yii2assets\pdfjs\Module',
        ],
    ],
    'params' => $params,
];
