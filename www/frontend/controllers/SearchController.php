<?php

namespace frontend\controllers;

use Yii;
use frontend\models\SearchForm;
use yii\data\Pagination;

class SearchController extends \yii\web\Controller {

    public $defaultAction = 'q';

    public function actionQ() {
        $settings = Yii::$app->settings;
        $searchModel = new SearchForm();
        $request = Yii::$app->request;
        $get = $request->get();
        $news = [];
        $files = [];
        $static_pages = [];
        $newsPagination = new Pagination();
        $filesPagination = new Pagination();
        $pagesPagination = new Pagination();
        $pageSize = $settings->get('main', 'pagination');

        //Configuramos según los controles
        if (!isset($get['pag']) && !isset($get['doc']) && !isset($get['nws'])) {
            $searchModel->includePages = true;
            $searchModel->includeFiles = true;
            $searchModel->includeNews = true;
        } else {
            $searchModel->includePages = (isset($get['pag'])) ? true : false;
            $searchModel->includeFiles = (isset($get['doc'])) ? true : false;
            $searchModel->includeNews = (isset($get['nws'])) ? true : false;
        }
        $searchModel->type = (isset($get['type']) && $get['type'] == 'true') ? true : false;

        if ($request->isGet && $searchModel->load($get)) {
            //STATIC PAGES
            $queryP = $searchModel->getPages($searchModel->includePages, $searchModel, $pageSize);
            if ($queryP) {
                $static_pages = $queryP[0];
                $pagesPagination = $queryP[1];
            }
            //NOTICIAS
            $queryN = $searchModel->getNews($searchModel->includeNews, $searchModel, $pageSize);
            if ($queryN) {
                $news = $queryN[0];
                $newsPagination = $queryN[1];
            }
            //FILES
            $queryF = $searchModel->getFiles($searchModel->includeFiles, $searchModel, $pageSize);
            if ($queryF) {
                $files = $queryF[0];
                $filesPagination = $queryF[1];
            }
            return $this->render('results', [
                        'searchModel' => $searchModel,
                        'news' => $news,
                        'newsPagination' => $newsPagination,
                        'files' => $files,
                        'filesPagination' => $filesPagination,
                        'static_pages' => $static_pages,
                        'pagesPagination' => $pagesPagination,
            ]);
        }
        return $this->render('results', [
                    'searchModel' => $searchModel,
                    'news' => $news,
                    'newsPagination' => $newsPagination,
                    'files' => $files,
                    'filesPagination' => $filesPagination,
                    'static_pages' => $static_pages,
                    'pagesPagination' => $pagesPagination,
        ]);
    }

}
