<?php
namespace frontend\controllers;


use common\models\Files;
use common\models\StaticPages;
use Exception;
use Yii;
use common\models\News;
use yii\web\Controller;
use yii\web\HttpException;

/**
 * Site controller
 */
class FileController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionView()
    {
        $request = Yii::$app->request;
        $get = $request->get();
        $file = Files::find()->where(['id'=>$get['file_id']])->one();
        $page = StaticPages::find()->where(['id'=>$file->page_id])->one();
        $path = '/file-uploads/'. $file->file_url;
        if (!$request->isAjax && $request->isGet) {
            $file->hit_counter++;
            $file->save(false);
        }
        return $this->render('view', [
            'path'=>$path,
            'file' => $file,
            'page' => $page,
        ]);
    }


    public function actionDownload()
    {
        $settings = Yii::$app->settings;
        $request = Yii::$app->request;
        $get = $request->get();
        $get = $request->get();
        $file = Files::find()->where(['id'=>$get['file_id']])->one();
        if (!$request->isAjax && $request->isGet) {
            $file->download_counter++;
            $file->save(false);
        }
        $file_dl = Yii::getAlias('@frontend') . '\web\file-uploads\\' . $file->file_url;

        //$file_dl2 = $settings->get('main', 'frontend_url') . '/file-uploads/' . $file->file_url;

        try{
            return Yii::$app->response->sendFile($file_dl);
        } catch (Exception $e) {
            throw new HttpException(404, 'No se ha encontrado el documento que busca');
        }

    }
}
