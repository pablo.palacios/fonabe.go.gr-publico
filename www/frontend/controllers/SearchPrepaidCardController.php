<?php
namespace frontend\controllers;

use frontend\models\FonabeWs;
use frontend\models\SearchPrepaidCardForm;
use Yii;
use yii\data\ArrayDataProvider;
use yii\web\Controller;
use yii\web\HttpException;

class SearchPrepaidCardController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function actionSearch()
    {
        $request = Yii::$app->request;
        $post = $request->post();
        $model = new SearchPrepaidCardForm();
        $ws = new FonabeWs();
        if ($model->load($post) && $request->isPost){

            $rsPrepaid = $ws->getPrepaidCard($model->id);
            $provider = new ArrayDataProvider([
                'allModels' => $rsPrepaid,
            ]);

        }
        return $this->render('result', [
            'pages' => [],
            'month' => [],
            'year' => [],
            'model' => $model,
            'provider' => $provider]);
    }
}