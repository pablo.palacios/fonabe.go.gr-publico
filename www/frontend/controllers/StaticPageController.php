<?php

namespace frontend\controllers;

use common\models\Category;
use common\models\Complaints;
use common\models\Contact;
use common\models\Files;
use common\models\History;
use common\models\Info;
use common\models\News;
use common\models\Videos;
use common\models\StaticPages;
use frontend\models\SearchEducationCenterForm;
use frontend\models\SearchPrepaidCardForm;
use frontend\models\SearchRequestStatusForm;
use yii\web\HttpException;
use yii\data\Pagination;
use Yii;

class StaticPageController extends \yii\web\Controller {

  /**
   * Usamos la acción para mostrar las páginas estáticas
   * @return string
   * @throws HttpException
   * @throws \yii\base\InvalidConfigException
   */
  public function actionIndex() {
    $settings = Yii::$app->settings;

    $request = Yii::$app->request;
    $get = $request->get();
    $catSlug = $get['category_slug'];
    $pagSlug = $get['page_slug'];
    $category = Category::find()->where(['slug' => $catSlug])->one();
    if ($category == null) {
      throw new HttpException(404, 'Lá página que esta buscando no fue encontrada');
    }
    $page = StaticPages::find()->where(['slug' => $pagSlug, 'category_id' => $category->id,])->one();
    if ($page == null) {
      throw new HttpException(404, 'Lá página que esta buscando no fue encontrada');
    }
    $renderView = '';
    $masterDetailModel = '';
    $masterDetailDocuments = '';
    $masterDetailVideos = '';
    $masterDetailControl = false;
    $pages = '';
    $day = '';
    $month = '';
    $year = '';
    $document = TRUE;
    switch ($category->slug . '/' . $page->slug) {
      case 'noticias/noticias':
        $query = News::find();
        $query->where(['published' => true]);
        $query->orderBy(['created_at' => SORT_DESC]);
        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => $settings->get('main', 'pagination')
        ]);
        $masterDetailModel = $query
                ->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
        $masterDetailControl = true;
        $renderView = 'partials/_sp_news.php';
        break;
      case 'principal/mapa-del-sitio':
        $masterDetailModel = StaticPages::find()
                ->all();
        $masterDetailControl = true;
        $renderView = 'partials/_sp_sitemap.php';
        break;
      case 'fonabe/calendario':
        $day = (isset($get['day']) ? $get['day'] : Yii::$app->formatter->asDate('now', 'dd'));
        $month = (isset($get['month']) ? $get['month'] : Yii::$app->formatter->asDate('now', 'MM'));
        $year = (isset($get['year']) ? $get['year'] : Yii::$app->formatter->asDate('now', 'yyyy'));
        $masterDetailModel = [];
        $masterDetailControl = true;
        $renderView = 'partials/_sp_calendar.php';
        break;
      case 'contactenos/consultas':
        $masterDetailModel = new Contact();
        $masterDetailControl = true;
        $renderView = 'partials/_sp_contact.php';
        break;
      case 'contactenos/denuncias':
        $masterDetailModel = new Complaints();
        $masterDetailControl = true;
        $renderView = 'partials/_sp_complain.php';
        break;
      case 'centro-educativo/centros-educativos':
        $session = Yii::$app->session;
        $session->remove('searchSession');
        $masterDetailModel = new SearchEducationCenterForm();
        $masterDetailControl = true;
        $renderView = 'partials/_search_educational_center.php';
        break;
      case 'centro-educativo/estado-solicitud':
        $masterDetailModel = new SearchRequestStatusForm();
        $masterDetailControl = true;
        $renderView = 'partials/_search_request_status.php';
        break;
      case 'educacion-abierta/estado-de-solicitd':
        $masterDetailModel = new SearchRequestStatusForm();
        $masterDetailControl = true;
        $renderView = 'partials/_search_request_status.php';
        break;
      case 'universidad/estado-solicitud':
        $masterDetailModel = new SearchRequestStatusForm();
        $masterDetailControl = true;
        $renderView = 'partials/_search_request_status.php';
        break;
      case 'pagina/tarjetas-prepago':
        $masterDetailModel = new SearchPrepaidCardForm();
        $masterDetailControl = true;
        $renderView = 'partials/_search_prepaid_card.php';
        break;
      case 'fonabe/historia':
        $masterDetailModel = History::find()->orderBy(['year' => SORT_DESC])->all();
        $masterDetailControl = true;
        $renderView = 'partials/_sp_history.php';
        break;
      case 'centro-educativo/informacion':
        $info = new Info();
        $rs = $info->getInfoById($page->id);
        $pages = $rs[1];
        $masterDetailModel = $rs[0];
        $masterDetailControl = true;
        $renderView = 'partials/_sp_info.php';
        break;
      case 'centro-educativo/tipos-de-becas':
        $info = new Info();
        $rs = $info->getInfoById($page->id);
        $pages = $rs[1];
        $masterDetailModel = $rs[0];
        $masterDetailControl = true;
        $renderView = 'partials/_sp_info_tb.php';
        $document = FALSE;
        break;
      default:

        $masterDetailControl = false;
    }
    $fls = new Files();
    $f = $fls->getAllFiles($page->has_md_documents, $page->id);
    if ($f) {
      $pages = $f[1];
      $masterDetailDocuments = $f[0];
    }
    $vid = new Videos();
    $v = $vid->getAllVideos($page->has_md_video, $page->id);
    if ($v) {
      $pages = $v[1];
      $masterDetailVideos = $v[0];
    }




    if ($page && $category) {
      return $this->render('index', [
                  'page' => $page,
                  'category' => $category,
                  'masterDetailControl' => $masterDetailControl,
                  'renderView' => $renderView,
                  'masterDetailModel' => $masterDetailModel,
                  'pages' => $pages,
                  'masterDetailDocuments' => $masterDetailDocuments,
                  'masterDetailVideos' => $masterDetailVideos,
                  'day' => $day,
                  'month' => $month,
                  'year' => $year,
                  'document' => $document
      ]);
    } else {
      throw new HttpException(404, 'Lá página que esta buscando no fue encontrada');
    }
  }

}
