<?php

namespace frontend\controllers;

use common\models\Contact;
use Yii;
use yii\web\Controller;

/**
 * Site controller
 */
class ContactController extends Controller {

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Guardando datos
     *
     * @return void
     */
    public function actionSave() {
        $settings = Yii::$app->settings;

        $model = new Contact();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // ENVIO EMAIL
            $view = 'contact';
            $mail = Yii::$app->mailer
                    ->compose(['html' => $view], [
                'model' => $model,
            ]);
            $mail->setFrom($settings->get('mail', 'email'));
            $mail->setTo($settings->get('mail', 'email'));
            $mail->setSubject('Solicitud de contacto');

            if ($mail->send()) {
                Yii::$app->session->setFlash('success', 'Gracias por enviarnos tu mensaje');
                Yii::$app->response->redirect(array('contactenos/consultas'));
            } else {
                Yii::error('No se pudo enviar el correo electrónico, intente más tarde');
                Yii::$app->session->setFlash('error', 'Se produjo un error al enviar tu mensaje, intenta más tarde');
            }
        } else {
            Yii::$app->session->setFlash('error', 'Se produjo un error al enviar tu mensaje, intenta más tarde');
            Yii::$app->response->redirect(array('contactenos/consultas'));
        }
    }

}
