<?php

namespace frontend\controllers;

use common\models\Files;
use Yii;
use yii\web\Controller;
use common\models\News;

class SiteController extends Controller {

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? '6LfSZkwUAAAAAFYoOWFox0axTu4FoQh9AY7PaNl6' : null,
            ],
        ];
    }

    /**
     * Muestra página inicial.
     * @return mixed
     */
    public function actionIndex() {
        $news = News::getNews();

        $files = Files::getFiles();

        return $this->render('index', [
                    'news' => $news,
                    'files' => $files,
        ]);
    }

    public function actionHeader() {
        $this->layout = 'header';
        return $this->render('header');
    }

    public function actionFooter() {
        $this->layout = 'footer';
        return $this->render('footer');
    }

}
