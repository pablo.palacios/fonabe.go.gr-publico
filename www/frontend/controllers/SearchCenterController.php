<?php

namespace frontend\controllers;

use Yii;
use frontend\models\FonabeWs;
use frontend\models\SearchEducationCenterForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\Response;


class SearchCenterController extends Controller
{
    public $defaultAction = 'search';

    /**
     * Búsqueda por ID
     * @return string
     * @throws HttpException
     */
    public function actionSearchCenterById()
    {
        $settings = Yii::$app->settings;
        $request = Yii::$app->request;
        $get = $request->get();
        if(isset($get['unicod']) && $get['unicod'] !== null){
            $unicod = $get['unicod'];
            $fnbWS = new FonabeWs();
            $beneficiaries = $fnbWS->getBeneficiariesCE($unicod, 3600);

            if(isset($get['_tog1149016d']) && $get['_tog1149016d'] === 'all'){
                $paging_size = false;
            } else {
                $paging_size = $settings->get('main', 'pagination');
            }
            $provider = new ArrayDataProvider([
                'allModels' => $beneficiaries,
                'pagination' => [
                    'pageSize' => $paging_size,
                ],
                'sort' => [
                    'attributes' => ['INSTITUCION','APELLIDO1','APELLIDO2','NOMBRE'],
                ],
            ]);
            $rows = $provider->getModels();
            if(sizeof($beneficiaries) <= 0){
                throw new HttpException(404, 'Lá página que esta buscando no fue encontrada');
            } else {
                return $this->render('result-by-id', [
                    'provider' => $provider
                ]);
            }
        } else {
            throw new HttpException(404, 'Lá página que esta buscando no fue encontrada');
        }



    }

    /**
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionSearch()
    {
        $settings = Yii::$app->settings;
        $session = Yii::$app->session;
        $request = Yii::$app->request;
        $post = $request->post();
        $get = $request->get();
        $model = new SearchEducationCenterForm();
        $fnbWS = new FonabeWs();
        $searchSession = $session->get('searchSession');
        $timestamp = Yii::$app->formatter->asTimestamp('now');
        if ($request->isPost && $model->load($post) && $searchSession === null){
            //set timestap para cache
            $session->set('searchSession', $timestamp);
            $searchSession = $session->get('searchSession');
            $provincias = [];
            $cantones =[];
            $distritos = [];
            $centros = [];
            //Procesamos ids
            if(isset($model->province)){
                $tmp_list = $fnbWS->getProvincesList();
                foreach ($tmp_list as $key => $value){
                    if($key === (integer)$model->province){
                        $selectedProvince = $value;
                        break;
                    }
                }
            }
            if(isset($model->canton)){
                $tmp_list = $fnbWS->getCantones((integer)$model->province);
                foreach ($tmp_list as $key => $value){
                    if($key === (integer)$model->canton){
                        $selectedCanton = $value;
                        break;
                    }
                }
            }
            if(isset($model->district)){
                $tmp_list = $fnbWS->getDistritos((integer)$model->canton);
                foreach ($tmp_list as $key => $value){
                    if($key === (integer)$model->district){
                        $selectedDistrict = $value;
                        break;
                    }
                }
            }
            //Consultamos WS con data + timeout y id de cache
            $centros = $fnbWS->getCentroDeEstudios(
                isset($model->unique_code)?$model->unique_code:'',
                isset($model->educational_center_name)?$model->educational_center_name:'',
                isset($selectedProvince)?$selectedProvince:'',
                isset($selectedCanton)?$selectedCanton:'',
                isset($selectedDistrict)?$selectedDistrict:'',
                isset($model->regional_address_name)?$model->regional_address_name:'',
                isset($model->circuit)?$model->circuit:'',
                3600, $searchSession);
            $provider = new ArrayDataProvider([
                'allModels' => $centros,
                'pagination' => [
                    'pageSize' => $settings->get('main', 'pagination'),
                ],
                'sort' => [
                    'attributes' => ['INSTITUCION', 'DIRECTOR', 'CANTIDAD_BECAS'],
                ],
            ]);
            $rows = $provider->getModels();
            $data = ['centros'=>$centros];
            return $this->render('result', [
                'pages' => [],
                'month' => [],
                'year' => [],
                'data' => $data,
                'provider'=>$provider,
                'model' => $model
            ]);
        }

        if ( $searchSession !== null  && Yii::$app->cache->get('getCC_'.$searchSession)){
            $centros = $fnbWS->getCentroDeEstudiosSession($searchSession);


            //_tog1149016d

            if(isset($get['_tog1149016d']) && $get['_tog1149016d'] === 'all'){
                $paging_size = false;
            } else {
                $paging_size = $settings->get('main', 'pagination');
            }

            $provider = new ArrayDataProvider([
                'allModels' => $centros,
                'pagination' => [
                    'pageSize' => $paging_size,
                ],
                'sort' => [
                    'attributes' => ['INSTITUCION', 'DIRECTOR', 'CANTIDAD_BECAS'],
                ],
            ]);
            $rows = $provider->getModels();
            $data = ['centros'=>$centros];
            return $this->render('result', [
                'pages' => [],
                'month' => [],
                'year' => [],
                'data' => $data,
                'provider'=>$provider,
                'model' => $model
            ]);
        }
        $session->remove('searchSession');
        Yii::$app->response->redirect(array('centro-educativo/centros-educativos'));
    }

    public function actionCanton()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $post = $request->post();
        $out = [];
        if (isset($post['depdrop_parents']) && $post['depdrop_parents'][0] !== '') {
            $parents = $post['depdrop_parents'];
            $ws = new FonabeWs();
            if ($parents != null) {
                $prov_id = $parents[0];
                $temp_out = $ws->getCantones($prov_id, 3600);
                foreach ($temp_out as $key => $value) {
                    $out[] = ['id'=> $key,'name'=> $value,];
                }
                return ['output'=>$out, 'selected'=>''];
            }
        }
        return ['output'=>'', 'selected'=>''];
    }
    public function actionDistrict()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $post = $request->post();
        $out = [];
        if (isset($post['depdrop_parents']) && $post['depdrop_parents'][0] !== '' && $post['depdrop_parents'][0] !== 'Cargando ...') {
            $parents = $post['depdrop_parents'];
            $ws = new FonabeWs();
            if ($parents != null) {
                $cant_id = $parents[0];
                $temp_out = $ws->getDistritos($cant_id, 3600);
                foreach ($temp_out as $key => $value) {
                    $out[] = ['id'=> $key,'name'=> $value,];
                }
                return ['output'=>$out, 'selected'=>''];
            }
        }
        return ['output'=>'', 'selected'=>''];
    }

}
