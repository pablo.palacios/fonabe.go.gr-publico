<?php
namespace frontend\controllers;


use Yii;
use common\models\News;
use yii\web\Controller;
use yii\web\HttpException;

/**
 * Site controller
 */
class NewsController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Muestra el detalle de las noticias.
     *
     * @return mixed
     * @throws HttpException
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        $get = $request->get();

        $news = News::find()->where(['slug'=>$get['news_slug']])->one();
        if($news){
            return $this->render('detail', [
                'news' => $news,
            ]);
        } else {
            throw new HttpException(404, 'Lá página que esta buscando no fue encontrada');
        }
    }
}
