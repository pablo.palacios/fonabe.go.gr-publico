<?php

namespace frontend\controllers;

use common\models\Complaints;
use Yii;
use yii\web\Controller;
use yii\web\HttpException;

/**
 * Site controller
 */
class ComplaintsController extends Controller {

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionSave() {
        $request = Yii::$app->request;
        $post = $request->post();
        $settings = Yii::$app->settings;
        $model = new Complaints();

        if ($model->load($post) && $model->save()) {
            $view = 'complaint';

            $mail = Yii::$app->mailer
                    ->compose(['html' => $view], [
                'model' => $model,
            ]);

            $mail->setFrom($settings->get('mail', 'email-denuncias'));
            $mail->setTo($settings->get('mail', 'email-denuncias'));
            $mail->setSubject('Ha recibido una denuncia desde su página web.');

            if ($mail->send()) {
                Yii::$app->session->setFlash('success', 'Gracias por enviarnos tu denuncia');
                Yii::$app->response->redirect(array('contactenos/denuncias'));
            } else {
                Yii::error('No se pudo enviar el correo electrónico, intente más tarde');
                Yii::$app->session->setFlash('error', 'Se produjo un error al enviar tu mensaje, intenta más tarde');
            }
        } else {
            Yii::$app->session->setFlash('error', 'Se produjo un error al enviar tu denuncia, intenta más tarde');
            Yii::$app->response->redirect(array('contactenos/denuncias'));
        }
    }

}
