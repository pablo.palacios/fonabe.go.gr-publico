<?php
namespace frontend\controllers;


use common\models\Videos;
use Yii;
use common\models\News;
use yii\web\Controller;
use yii\web\HttpException;

/**
 * Site controller
 */
class VideoController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Muestra el detalle de las noticias.
     *
     * @return mixed
     * @throws HttpException
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        $get = $request->get();

        $video = Videos::find()->where(['slug'=>$get['video_slug']])->one();
        if($video){
            return $this->render('detail', [
                'video' => $video,
            ]);
        } else {
            throw new HttpException(404, 'Lá página que esta buscando no fue encontrada');
        }
    }
}
