<?php

namespace frontend\controllers;

use common\models\Events;
use common\models\Info;
use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\Response;

class InfoController extends Controller {

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex() {
        $request = Yii::$app->request;
        $get = $request->get();
        if (isset($get['id'])) {
            $info = Info::findOne($get['id']);
            if (!empty($info)) {
                return $this->render('detail', ['info' => $info]);
            } else {
                throw new HttpException(404, 'No se encontró la información que busca');
            }
        } else {
            throw new HttpException(404, 'No se encontró el evento que busca');
        }
    }

    /**
     * Retorna eventos por mes.
     *
     * @return mixed
     * @throws HttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionGetEvents() {
        $formatter = \Yii::$app->formatter;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $get = $request->get();
        if ($request->isGet) {
            $days_in_month = cal_days_in_month(CAL_GREGORIAN, $get['month'], $get['year']);
            $lower = $get['year'] . '-' . $get['month'] . '-01';
            $upper = $get['year'] . '-' . $get['month'] . '-' . $days_in_month;
            $events = Events::find()
                    ->where(['between', 'date', $lower, $upper])
                    ->all();
            $events_array = [];
            for ($i = 1; $i <= $days_in_month; $i++) {
                if (sizeof($events) !== 0) {
                    foreach ($events as $ev) {
                        $day_is = $formatter->asDate($ev->date, 'dd');
                        $month_is = $formatter->asDate($ev->date, 'MM');
                        $year_is = $formatter->asDate($ev->date, 'yyyy');
                        if ((int) $day_is === (int) $i && $month_is === $get['month'] && $year_is === $get['year']) {
                            $ev = [
                                'day' => $i,
                                'active' => true,
                                'id' => $ev->id,
                                'title' => $ev->title,
                                'description' => $ev->description,
                                'date' => $ev->date,
                            ];
                            break;
                        } else {
                            $ev = ['day' => $i, 'active' => false, 'id' => null];
                        }
                    }
                    array_push($events_array, $ev);
                } else {
                    $no_ev = ['day' => $i, 'active' => false, 'id' => null];
                    array_push($events_array, $no_ev);
                }
            }
            return ['date' => $get['month'] . '/' . $get['year'], 'daysInMonth' => $days_in_month, 'events' => $events_array];
        } else {
            throw new HttpException(404, 'El EP solo recibe peticiones GET');
        }
    }

}
