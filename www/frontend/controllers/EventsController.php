<?php
namespace frontend\controllers;

use common\models\Events;
use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\Response;
use edofre\fullcalendar\models\Event;
class EventsController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $request = Yii::$app->request;
        $get = $request->get();
        if(isset($get['id'])){
            $event = Events::findOne($get['id']);
            if($event){
                return $this->render('detail', ['event'=>$event]);
            } else {
                throw new HttpException(404, 'No se encontró el evento que busca');
            }
        } else {
            throw new HttpException(404, 'No se encontró el evento que busca');
        };

    }



    /**
     * Retorna eventos por mes.
     *
     * @return mixed
     * @throws HttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionGetEvents()
    {
        $formatter = \Yii::$app->formatter;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $get = $request->get();
        if ($request->isGet)  {
            $days_in_month = cal_days_in_month(CAL_GREGORIAN, $get['month'], $get['year']);
            $lower = $get['year'].'-'.$get['month'].'-01';
            $upper = $get['year'].'-'.$get['month'].'-'.$days_in_month;
            $events = Events::find()
                ->where(['between', 'date', $lower, $upper ])
                ->all();
            $events_array = [];
            for ($i = 1; $i <= $days_in_month; $i++) {
                if(sizeof($events) !== 0){
                    foreach ($events as $ev) {
                        $day_is =  $formatter->asDate($ev->date, 'dd');
                        $month_is =  $formatter->asDate($ev->date, 'MM');
                        $year_is =  $formatter->asDate($ev->date, 'yyyy');
                        if( (int)$day_is === (int)$i && $month_is === $get['month'] && $year_is === $get['year']) {
                            $ev = [
                                'day'=>$i,
                                'active'=>true,
                                'id'=>$ev->id,
                                'title'=>$ev->title,
                                'description'=>$ev->description,
                                'date'=>$ev->date,
                            ];
                            break;
                        } else {
                            $ev = ['day'=>$i, 'active'=>false, 'id'=>null];
                        }
                    }
                    array_push($events_array, $ev);
                } else {
                    $no_ev = ['day'=>$i, 'active'=>false, 'id'=>null];
                    array_push($events_array, $no_ev);
                }
            }
            return ['date'=>$get['month'].'/'.$get['year'], 'daysInMonth'=>$days_in_month, 'events'=>$events_array];
        } else {
            throw new HttpException(404, 'El EP solo recibe peticiones GET');
        }
    }

    /**
     * Consulta de eventos para fullCalendar
     * @param null $start
     * @param null $end
     * @param null $_
     * @return array
     */
    public function actionJsoncalendar($start=NULL,$end=NULL,$_=NULL){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $month_events = Events::find()
            ->where(['between', 'date', $start, $end ])
            ->orderBy(['date'=>SORT_ASC])
            ->all();
        $events = [];
        foreach ($month_events AS $day_event){
            $Event = new Event();
            $Event->id = $day_event->id;
            $Event->title = $day_event->title;
            $Event->start = date('Y-m-d\TH:i:s\Z',strtotime($day_event->date.' '.$day_event->time));
            $Event->end = date('Y-m-d\TH:i:s\Z', strtotime('+'.(int)$day_event->hours.' hours', strtotime($day_event->date.' '.$day_event->time)));
            $Event->overlap = false;
            $Event->editable = false;
            $events[] = $Event;
        }
        return $events;
    }
}
