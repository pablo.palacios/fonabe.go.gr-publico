<?php
namespace frontend\controllers;

use frontend\models\FonabeWs;
use frontend\models\SearchRequestStatusForm;
use Yii;
use yii\data\ArrayDataProvider;
use yii\web\Controller;
use yii\web\HttpException;

class SearchRequestStatusController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function actionSearch()
    {
        $request = Yii::$app->request;
        $post = $request->post();
        $model = new SearchRequestStatusForm();
        $ws = new FonabeWs();
        if ($model->load($post) && $request->isPost){

            $rsStatus = $ws->getRequestStatus(
                $model->id,
                $model->first_name,
                $model->last_name_1,
                $model->last_name_2
            );
            $provider = new ArrayDataProvider([
                'allModels' => $rsStatus,
            ]);
        } else {
            $provider = new ArrayDataProvider([
                'allModels' => [],
            ]);
        }
        return $this->render('result', [
            'pages' => [],
            'month' => [],
            'year' => [],
            'model' => $model,
            'provider' => $provider,
        ]);
    }
}