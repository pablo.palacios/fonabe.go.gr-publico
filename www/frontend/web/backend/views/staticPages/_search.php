<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\StaticPagesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="static-pages-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'SLUG') ?>

    <?= $form->field($model, 'TITLE') ?>

    <?= $form->field($model, 'EXCERPT') ?>

    <?= $form->field($model, 'CONTENT') ?>

    <?php // echo $form->field($model, 'CREATION_DATE') ?>

    <?php // echo $form->field($model, 'LAST_UPDATE_DATE') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
