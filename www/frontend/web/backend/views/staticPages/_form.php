<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\StaticPages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="static-pages-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'SLUG')->textInput() ?>

    <?= $form->field($model, 'TITLE')->textInput() ?>

    <?= $form->field($model, 'EXCERPT')->textInput() ?>

    <?= $form->field($model, 'CONTENT')->textInput() ?>

    <?= $form->field($model, 'CREATION_DATE')->textInput() ?>

    <?= $form->field($model, 'LAST_UPDATE_DATE')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
