moment.locale('es');

var app = new Vue({
    el: '#app',
    data: {
        currentDate: moment().format("YYYY-MM-DD"),
        calendarDate:  moment().format("MMMM YYYY"),
        month: moment().format("MM"),
        year: moment().format("YYYY"),
        numberOfDays: moment().daysInMonth(),
        hide: true,
        events:[]
    },
    methods: {
        prev: function () {
            this.hide = true;
            this.currentDate = moment(this.currentDate).subtract(1, 'M').format("YYYY-MM-DD");
            this.month = moment(this.currentDate).format("MM");
            this.year = moment(this.currentDate).format("YYYY");
            this.calendarDate =  moment(this.currentDate).format("MMMM YYYY");
            this.numberOfDays = moment(this.currentDate).daysInMonth();
            axios.get('/api/get-events/'+this.month+'/'+this.year)
                .then(function (rsp) {
                    this.events = rsp.data.events;
                    this.hide = false;
                }.bind(this))
                .catch(function (err) {
                    console.error('err::',err);
                });
        },
        next: function () {
            this.hide = true;
            this.currentDate = moment(this.currentDate).add(1, 'M').format("YYYY-MM-DD");
            this.month = moment(this.currentDate).format("MM");
            this.year = moment(this.currentDate).format("YYYY");
            this.calendarDate =  moment(this.currentDate).format("MMMM YYYY");
            this.numberOfDays = moment(this.currentDate).daysInMonth();
            axios.get('/api/get-events/'+this.month+'/'+this.year)
                .then(function (rsp) {
                    this.events = rsp.data.events;
                    this.hide = false;
                }.bind(this))
                .catch(function (err) {
                    console.error('err::',err);
                });
        },
        firstCall: function () {
            this.hide = true;
            axios.get('/api/get-events/'+this.month+'/'+this.year)
                .then(function (rsp) {
                    this.events = rsp.data.events;
                    this.hide = false;
                }.bind(this))
                .catch(function (err) {
                    console.error('err::',err);
                });
        },
        alertMe: function(event) {
            var eventTitle = event.title;
            var eventTxt = 'Detalle: '+ event.description + ', Fecha del evento: ' + event.date;
            this.$swal("Evento", "Tema: "+ event.title + "<br>"  );
            this.$swal({
                html:true,
                title:eventTitle,
                text:eventTxt,
                closeOnClickOutside: true,
                closeOnEsc: false
            });
        }
    },
    created: function(){
        this.firstCall();
    }
});



