jQuery(function ($) {
    $('.truncate_me').text(function( index ) {
        return  _.truncate($(this).text(), {
            'length': 350,
            'separator': '...'
        });
    });
});
