jQuery(function ($) {
    console.log('slider-ready');
    $('.carousel-icons-menu').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 2000
    });
});
