jQuery(function ($) {
    $('.gt-name').val('');
    $('.gt-id').val('');
    $('#searchrequeststatusform-idtype').val('');
    $('#searchrequeststatusform-idtype').change(function(){
        var val = $('#searchrequeststatusform-idtype').val();
        if( val === 'Cédula') {
            $('.gt-id').show().val('');
            $('.gt-idl').show();
            $('.gt-id').siblings().show();
            $('.gt-name').hide().val('');
            $('.gt-namel').hide().val('');
            $('.gt-name').siblings().hide();
            $('#w1').yiiActiveForm('remove', 'searchrequeststatusform-id');
            $('#w1').yiiActiveForm('remove', 'searchrequeststatusform-first_name');
            $('#w1').yiiActiveForm('remove', 'searchrequeststatusform-last_name_1');
            $('#w1').yiiActiveForm('remove', 'searchrequeststatusform-last_name_2');
            $('#w1').yiiActiveForm('add', {
                id: 'searchrequeststatusform-id',
                name: "id",
                container: '.field-searchrequeststatusform-id',
                input: "#searchrequeststatusform-id",
                validate: function(attribute, value, messages, deferred, $form) {
                    yii.validation.required(value, messages, {
                        message: 'La cédula es requerido'
                    });
                }
            });
        } else {
            $('.gt-id').hide().val('');
            $('.gt-idl').hide();
            $('.gt-id').siblings().hide();
            $('.gt-name').show().val('');
            $('.gt-namel').show();
            $('.gt-name').siblings().show();
            $('#w1').yiiActiveForm('remove', 'searchrequeststatusform-id');
            $('#w1').yiiActiveForm('remove', 'searchrequeststatusform-first_name');
            $('#w1').yiiActiveForm('remove', 'searchrequeststatusform-last_name_1');
            $('#w1').yiiActiveForm('remove', 'searchrequeststatusform-last_name_2');
            $('#w1').yiiActiveForm('add', {
                id: 'searchrequeststatusform-first_name',
                name: 'first_name',
                container: '.field-searchrequeststatusform-first_name',
                input: '#searchrequeststatusform-first_name',
                validate: function(attribute, value, messages, deferred, $form) {
                    yii.validation.required(value, messages, {
                        message: 'El nombre es requerido'
                    });
                }
            });
            $('#w1').yiiActiveForm('add', {
                id: 'searchrequeststatusform-last_name_1',
                name: 'last_name_1',
                container: '.field-searchrequeststatusform-last_name_1',
                input: '#searchrequeststatusform-last_name_1',
                validate: function(attribute, value, messages, deferred, $form) {
                    yii.validation.required(value, messages, {
                        message: 'El apellido es requerido'
                    });
                }
            });
            $('#w1').yiiActiveForm('add', {
                id: 'searchrequeststatusform-last_name_2',
                name: 'last_name_2',
                container: '.field-searchrequeststatusform-last_name_2',
                input: '#searchrequeststatusform-last_name_2',
                validate: function(attribute, value, messages, deferred, $form) {
                    yii.validation.required(value, messages, {
                        message: 'El segundo apellido es requerido'
                    });
                }
            });
        }
    });
});
