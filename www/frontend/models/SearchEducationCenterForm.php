<?php
namespace frontend\models;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class SearchEducationCenterForm extends Model
{
    public $unique_code;
    public $educational_center_name;
    public $province;
    public $canton;
    public $district;
    public $regional_address_name;
    public $circuit;
    public $isUniversity;
    public $reCaptcha;

    public function rules()
    {
        return [
            [['unique_code','educational_center_name','province','canton','district','regional_address_name','circuit'], 'string'],
            [['isUniversity'], 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'unique_code' => 'Código Único',
            'educational_center_name' => 'Nombre Centro Educativo',
            'province' => 'Provincia',
            'canton' => 'Cantón',
            'district' => 'Distrito',
            'regional_address_name' => 'Nombre Dirección Regional',
            'circuit' => 'Circuito',
            'reCaptcha' => 'Para continuar, debe afirmar que no es un robot, dándole click al recuadro',
        ];
    }

    /**
     * Buscar el nombre de la lista en settings
     * @param $settingsListName - del grupo csv_list
     * @return array
     */
    public function getDropDownList($settingsListName)
    {
        $settings = Yii::$app->settings;
        $array  = explode(",", $settings->get('forms', $settingsListName));
        foreach ($array as $value) {
            $ret_array[$value] = $value;
        }
        return $ret_array;
    }



}