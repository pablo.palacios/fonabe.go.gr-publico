<?php
namespace frontend\models;
use Yii;
use yii\base\Model;

class SearchPrepaidCardForm extends Model
{

    public $id;
    public $isUniversity;
    public $reCaptcha;

    public function rules()
    {
        return [
            [['id'], 'string'],
            [['id'], 'required'],
            [['isUniversity'], 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'Cédula (Digitar sin Guiones)',
            'isUniversity' => 'Es universidad',
            'reCaptcha' => 'Para continuar, debe afirmar que no es un robot, dándole click al recuadro',
        ];
    }
}