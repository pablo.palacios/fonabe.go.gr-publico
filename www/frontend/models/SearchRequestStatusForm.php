<?php
namespace frontend\models;
use Yii;
use yii\base\Model;

class SearchRequestStatusForm extends Model
{

    public $id;
    public $idType;
    public $first_name;
    public $last_name_1;
    public $last_name_2;
    public $isUniversity;
    public $reCaptcha;

    public function rules()
    {
        return [
            [['id'], 'number'],
            [['idType','first_name', 'last_name_1', 'last_name_2'], 'string'],
            [['idType'], 'required'],
            [['isUniversity'], 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'Cédula (digitar sin guiones)',
            'idType' => 'Consultar por',
            'first_name' => 'Nombre',
            'last_name_1' => 'Apellido 1',
            'last_name_2' => 'Apellido 2',
            'isUniversity' => 'es universidad?',
            'reCaptcha' => 'Para continuar, debe afirmar que no es un robot, dándole click al recuadro',
        ];
    }

    /**
     * Buscar el nombre de la lista en settings
     * @param $settingsListName - del grupo csv_list
     * @return array
     */
    public function getDropDownList($settingsListName)
    {
        $settings = Yii::$app->settings;
        $array  = explode(",", $settings->get('forms', $settingsListName));
        foreach ($array as $value) {
            $ret_array[$value] = $value;
        }
        return $ret_array;
    }
}