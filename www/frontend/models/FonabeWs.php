<?php

namespace frontend\models;

use DCarbone\SoapPlus\SoapClientPlus;
use DOMDocument;
use SimpleXMLElement;
use SoapClient;
use Yii;
use yii\base\ErrorException;
use \yii\web\HttpException;
use yii\base\Model;
use yii\data\Pagination;
use yii\db\Query;
use common\models\Files;
use common\models\News;

/**
 * Class FonabeWs, Consultas de WSDL FONABE
 *
 * @package frontend\models
 */
class FonabeWs extends Model {

    public static $fonabeWSDL = '';

    /**
     *
     */
    public function init() {
        parent::init();
        self::$fonabeWSDL = Yii::$app->params['wsdl_url'];
    }

    /**
     * Obtiene estado por cédula
     *
     * @param $id, índice a buscar
     * @param int $cache_timeout, timeout en milisegundos aplicado al cache
     * @return array|mixed
     * @throws HttpException
     */
    public function getRequestStatus($id, $first_name, $last_name_1, $last_name_2, $cache_timeout = 3600) {
        $cid = 'getRequestStatus' . $id . $first_name . $last_name_1 . $last_name_2;
        $cid = str_replace(' ', '', $cid);
        if (!Yii::$app->cache->get($cid)) {
            try {
                $soapClient = new SoapClientPlus(self::$fonabeWSDL);
            } catch (ErrorException $e) {
                Yii::error(['Error: ' => $e->getMessage()]);
                throw new HttpException(502, 'No se puede realizar la conexión a cantones: ' . $e->getMessage());
            }
            try {
                $params = ['consulta_Estado_Solicitud' => [
                        'cedula' => $id,
                        'nombre' => $first_name,
                        'apellido1' => $last_name_1,
                        'apellido2' => $last_name_2
                ]];
                $response = $soapClient->consulta_Estado_Solicitud($params);
                $str = $response->Consulta_Estado_SolicitudResult->any;
                $expString = explode("</xs:schema>", $str);
                $newxml = simplexml_load_string($expString[1]);
                $response_array = [];
                if (sizeof($newxml) > 0) {
                    foreach ($newxml->DocumentElement->Datos as $field) {
                        $response_array[] = (array) $field;
                    }
                }
            } catch (ErrorException $e) {
                Yii::error(['Error: ' => $e->getMessage()]);
                throw new HttpException(500, 'Se produjo un error en la petición WSDL : ' . $e->getMessage());
            } catch (\Exception $e) {
                Yii::error(['Error: ' => $e->getMessage()]);
                throw new HttpException(502, 'Error de conexión, Gateway no disponible : ' . $e->getMessage());
            }
            Yii::$app->cache->set($cid, $response_array, $cache_timeout);
            $beneficiaries = $response_array;
        } else {
            $beneficiaries = Yii::$app->cache->get($cid);
        }
        return $beneficiaries;
    }

    /**
     * Obtiene Consulta_Estado_Beneficiario
     *
     * @param $id, índice a buscar
     * @param int $cache_timeout, timeout en milisegundos aplicado al cache
     * @return array|mixed
     * @throws HttpException
     */
    public function getBeneficiaryState($id, $cache_timeout = 3600) {
        if (!Yii::$app->cache->get('getBeneficiaryState' . $id)) {
            try {
                $soapClient = new SoapClientPlus(self::$fonabeWSDL);
            } catch (ErrorException $e) {
                Yii::error(['Error: ' => $e->getMessage()]);
                throw new HttpException(502, 'No se puede realizar la conexión: ');
            }
            try {
                $params = ['consulta_Estado_Beneficiario' => ['cedula' => $id]];
                $response = $soapClient->consulta_Estado_Beneficiario($params);
                $str = $response->Consulta_Estado_BeneficiarioResult->any;
                $expString = explode("</xs:schema>", $str);
                $newxml = simplexml_load_string($expString[1]);
                $response_array = [];
                foreach ($newxml->DocumentElement->Datos as $field) {
                    $response_array[] = (array) $field;
                }
            } catch (ErrorException $e) {
                Yii::error(['Error: ' => $e->getMessage()]);
                throw new HttpException(500, 'Se produjo un error en la petición: ');
            } catch (\Exception $e) {
                Yii::error(['Error: ' => $e->getMessage()]);
                throw new HttpException(502, 'Error de conexión,intente más tarde ');
            }
            Yii::$app->cache->set('getBeneficiaryState' . $id, $response_array, $cache_timeout);
            $beneficiaries = $response_array;
        } else {
            $beneficiaries = Yii::$app->cache->get('getBeneficiaryState' . $id);
        }
        return $beneficiaries;
    }

    /**
     * Obtiene Consulta_Tarjeta_Prepago
     *
     * @param $id, índice a buscar
     * @param int $cache_timeout, timeout en milisegundos aplicado al cache
     * @return array|mixed
     * @throws HttpException
     */
    public function getPrepaidCard($id, $cache_timeout = 3600) {
        if (!Yii::$app->cache->get('getPrepaidCard' . $id)) {
            try {
                $soapClient = new SoapClientPlus(self::$fonabeWSDL);
            } catch (ErrorException $e) {
                Yii::error(['Error: ' => $e->getMessage()]);
                throw new HttpException(502, 'No se puede realizar la conexión: ' . $e->getMessage());
            }
            try {
                $params = ['consulta_Tarjeta_Prepago' => ['cedula' => $id]];
                $response = $soapClient->consulta_Tarjeta_Prepago($params);
                $str = $response->Consulta_Tarjeta_PrepagoResult->any;
                $expString = explode("</xs:schema>", $str);
                $newxml = simplexml_load_string($expString[1]);
                $response_array = [];
                foreach ($newxml->DocumentElement->Datos as $field) {
                    $response_array[] = (array) $field;
                }
            } catch (ErrorException $e) {
                Yii::error(['Error: ' => $e->getMessage()]);
                throw new HttpException(500, 'Tarjeta no encontrada o en proceso de activación.');
            } catch (\Exception $e) {
                Yii::error(['Error: ' => $e->getMessage()]);
                throw new HttpException(502, 'Error de conexión,intente más tarde');
            }
            Yii::$app->cache->set('getPrepaidCard' . $id, $response_array, $cache_timeout);
            $beneficiaries = $response_array;
        } else {
            $beneficiaries = Yii::$app->cache->get('getPrepaidCard' . $id);
        }
        return $beneficiaries;
    }

    /**
     * Obtiene Consulta_Transaccion_Bancaria
     *
     * @param $id, índice a buscar
     * @param int $cache_timeout, timeout en milisegundos aplicado al cache
     * @return array|mixed
     * @throws HttpException
     */
    public function getBankTransaction($id, $cache_timeout = 3600) {
        if (!Yii::$app->cache->get('getBankTransaction' . $id)) {
            try {
                $soapClient = new SoapClientPlus(self::$fonabeWSDL);
            } catch (ErrorException $e) {
                Yii::error(['Error: ' => $e->getMessage()]);
                throw new HttpException(502, 'No se puede realizar la conexión a cantones: ' . $e->getMessage());
            }
            try {
                $params = ['consulta_Transaccion_Bancaria' => ['cedula' => $id]];
                $response = $soapClient->consulta_Transaccion_Bancaria($params);
                $str = $response->Consulta_Transaccion_BancariaResult->any;
                $expString = explode("</xs:schema>", $str);
                $newxml = simplexml_load_string($expString[1]);
                $response_array = [];
                foreach ($newxml->DocumentElement->Datos as $field) {
                    $response_array[] = (array) $field;
                }
            } catch (ErrorException $e) {
                Yii::error(['Error: ' => $e->getMessage()]);
                throw new HttpException(500, 'Se produjo un error en la petición WSDL : ' . $e->getMessage());
            } catch (\Exception $e) {
                Yii::error(['Error: ' => $e->getMessage()]);
                throw new HttpException(502, 'Error de conexión, Gateway no disponible : ' . $e->getMessage());
            }
            Yii::$app->cache->set('getBankTransaction' . $id, $response_array, $cache_timeout);
            $beneficiaries = $response_array;
        } else {
            $beneficiaries = Yii::$app->cache->get('getBankTransaction' . $id);
        }
        return $beneficiaries;
    }

    /**
     * Obtiene beneficiarios
     *
     * @param $id, índice a buscar
     * @param int $cache_timeout, timeout en milisegundos aplicado al cache
     * @return array|mixed
     * @throws HttpException
     */
    public function getBeneficiariesCE($id, $cache_timeout = 3600) {
        if (!Yii::$app->cache->get('getBeneficiariesCE' . $id)) {
            try {
                $soapClient = new SoapClientPlus(self::$fonabeWSDL);
            } catch (ErrorException $e) {
                Yii::error(['Error: ' => $e->getMessage()]);
                throw new HttpException(502, 'No se puede realizar la conexión a cantones: ' . $e->getMessage());
            }
            try {
                $params = ['consulta_Beneficiarios_Centro_Educativo' => ['unicod' => $id]];
                $response = $soapClient->consulta_Beneficiarios_Centro_Educativo($params);
                $str = $response->Consulta_Beneficiarios_Centro_EducativoResult->any;
                $expString = explode("</xs:schema>", $str);
                $newxml = simplexml_load_string($expString[1]);
                $response_array = [];
                foreach ($newxml->DocumentElement->Datos as $field) {
                    $response_array[] = (array) $field;
                }
            } catch (ErrorException $e) {
                Yii::error(['Error: ' => $e->getMessage()]);
                throw new HttpException(500, 'Se produjo un error en la petición WSDL : ' . $e->getMessage());
            } catch (\Exception $e) {
                Yii::error(['Error: ' => $e->getMessage()]);
                throw new HttpException(502, 'Error de conexión, Gateway no disponible : ' . $e->getMessage());
            }
            Yii::$app->cache->set('getBeneficiariesCE' . $id, $response_array, $cache_timeout);
            $beneficiaries = $response_array;
        } else {
            $beneficiaries = Yii::$app->cache->get('getBeneficiariesCE' . $id);
        }
        return $beneficiaries;
    }

    /**
     * Lista  de provincias
     *
     * @return array
     */
    public function getProvincesList() {
        return [
            '1' => 'SAN JOSÉ',
            '2' => 'ALAJUELA',
            '3' => 'CARTAGO',
            '4' => 'HEREDIA',
            '5' => 'GUANACASTE',
            '6' => 'PUNTARENAS',
            '7' => 'LIMÓN',];
    }

    /**
     * Obtiene cantones
     *
     * @param $id, indice a buscar
     * @param int $cache_timeout, timeout en milisegundos aplicado al cache
     * @return array|mixed
     * @throws HttpException
     */
    public function getCantones($id, $cache_timeout = 3600) {
        if (!Yii::$app->cache->get('getCantones_' . $id)) {
            try {
                $soapClient = new SoapClientPlus(self::$fonabeWSDL);
            } catch (ErrorException $e) {
                Yii::error(['Error: ' => $e->getMessage()]);
                throw new HttpException(502, 'No se puede realizar la conexión a cantones: ' . $e->getMessage());
            }
            try {
                $params = ['consulta_Cantones' => ['idProvincia' => $id]];
                $response = $soapClient->consulta_Cantones($params);
                $str = $response->Consulta_CantonesResult->any;
                $expString = explode("</xs:schema>", $str);
                $newxml = simplexml_load_string($expString[1]);
                $response_array = [];
                foreach ($newxml->DocumentElement->Datos as $field) {
                    $response_array[(string) $field->ID_CANTON] = (string) $field->DETALLE_CANTON;
                }
            } catch (ErrorException $e) {
                Yii::error(['Error: ' => $e->getMessage()]);
                throw new HttpException(500, 'Se produjo un error en la petición WSDL : ' . $e->getMessage());
            } catch (\Exception $e) {
                Yii::error(['Error: ' => $e->getMessage()]);
                throw new HttpException(502, 'Error de conexión, Gateway no disponible');
            }
            Yii::$app->cache->set('getCantones_' . $id, $response_array, $cache_timeout);
            $cantones = $response_array;
        } else {
            $cantones = Yii::$app->cache->get('getCantones_' . $id);
        }
        return $cantones;
    }

    /**
     * Obtiene distritos
     *
     * @param $id, índice a buscar
     * @param int $cache_timeout, timeout en milisegundos aplicado al cache
     * @return array|mixed
     * @throws HttpException
     */
    public function getDistritos($id, $cache_timeout = 3600) {
        if (!Yii::$app->cache->get('getDistritos_' . $id)) {
            try {
                $soapClient = new SoapClientPlus(self::$fonabeWSDL);
            } catch (ErrorException $e) {
                Yii::error(['Error: ' => $e->getMessage()]);
                throw new HttpException(502, 'No se puede realizar la conexión a distritos: ' . $e->getMessage());
            }
            try {
                $params = ['consulta_Distritos' => ['idCanton' => $id]];
                $response = $soapClient->consulta_Distritos($params);
                $strX = $response->Consulta_DistritosResult->any;
                $expString = explode("</xs:schema>", $strX);
                $newxml = simplexml_load_string($expString[1]);
                $response_array = [];
                foreach ($newxml->DocumentElement->Datos as $field) {
                    $response_array[(string) $field->ID_DISTRITO] = (string) $field->DETALLE_DISTRITO;
                }
            } catch (ErrorException $e) {
                Yii::error(['Error: ' => $e->getMessage()]);
                throw new HttpException(500, 'Se produjo un error en la petición WSDL : ' . $e->getMessage());
            } catch (\Exception $e) {
                Yii::error(['Error: ' => $e->getMessage()]);
                throw new HttpException(502, 'Error de conexión, Gateway no disponible');
            }
            Yii::$app->cache->set('getDistritos_' . $id, $response_array, $cache_timeout);
            $distritos = $response_array;
        } else {
            $distritos = Yii::$app->cache->get('getDistritos_' . $id);
        }
        return $distritos;
    }

    /**
     *  Consulta de Centros Educativos
     * @param     $unicod
     * @param     $centro_Educativo
     * @param     $detalle_Provincia
     * @param     $detalle_Canton
     * @param     $detalle_Distrito
     * @param     $regional
     * @param     $circuito_Escolar
     * @param int $cache_timeout
     * @return array|mixed
     * @throws HttpException
     */
    public function getCentroDeEstudios($unicod = '', $centro_Educativo = '', $detalle_Provincia = '', $detalle_Canton = '', $detalle_Distrito = '', $regional = '', $circuito_Escolar = '', $cache_timeout = 3600, $cacheId) {
        if (!Yii::$app->cache->get('getCC_' . $cacheId)) {
            try {
                $soapClient = new SoapClientPlus(self::$fonabeWSDL);
            } catch (ErrorException $e) {
                Yii::error(['Error: ' => $e->getMessage()]);
                throw new HttpException(502, 'No se puede realizar la conexión a cantones: ' . $e->getMessage());
            }
            try {
                $params = [
                    'Consulta_Centros_Educativos' => [
                        'unicod' => $unicod,
                        'Centro_Educativo' => $centro_Educativo,
                        'Detalle_Provincia' => $detalle_Provincia,
                        'Detalle_Canton' => $detalle_Canton,
                        'Detalle_Distrito' => $detalle_Distrito,
                        'Regional' => $regional,
                        'Circuito_Escolar' => $circuito_Escolar,
                    ]
                ];
                $response = $soapClient->consulta_Centros_Educativos($params);
                $str = $response->Consulta_Centros_EducativosResult->any;
                $expString = explode("</xs:schema>", $str);
                $newxml = simplexml_load_string($expString[1]);
                $response_array = [];
                if (sizeof($newxml) >= 1) {
                    foreach ($newxml->DocumentElement->Datos as $field) {
                        $response_array[(string) $field->CODIGO_UNICO] = [
                            'CODIGO_UNICO' => (string) $field->CODIGO_UNICO,
                            'CODIGO_DIRECCION_REGIONAL' => (string) $field->CODIGO_DIRECCION_REGIONAL,
                            'DIRECCION_REGIONAL' => (string) $field->DIRECCION_REGIONAL,
                            'CIRCUITO' => (string) $field->CIRCUITO,
                            'INSTITUCION' => (string) $field->INSTITUCION,
                            'DIRECTOR' => (string) $field->DIRECTOR,
                            'PROVINCIA' => (string) $field->PROVINCIA,
                            'CANTON' => (string) $field->CANTON,
                            'DISTRITO' => (string) $field->DISTRITO,
                            'CANTIDAD_BECAS' => (string) $field->CANTIDAD_BECAS,
                        ];
                    }
                }
            } catch (ErrorException $e) {
                Yii::error(['Error: ' => $e->getMessage()]);
                throw new HttpException(500, 'Se produjo un error en la petición WSDL : ' . $e->getMessage());
            } catch (\Exception $e) {
                Yii::error(['Error: ' => $e->getMessage()]);
                throw new HttpException(502, 'Error de conexión, Gateway no disponible : ' . $e->getMessage());
            }
            Yii::$app->cache->set('getCC_' . $cacheId, $response_array, $cache_timeout);
            $centros = $response_array;
        } else {
            $centros = Yii::$app->cache->get('getCC_' . $cacheId);
        }
        return $centros;
    }

    public function getCentroDeEstudiosSession($cacheId) {
        return Yii::$app->cache->get('getCC_' . $cacheId);
    }

}
