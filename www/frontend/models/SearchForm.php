<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\db\Query;
use common\models\Files;
use common\models\News;



class SearchForm extends Model
{
    public $q;
    public $includePages;
    public $includeFiles;
    public $includeNews;
    public $type;
    public $reCaptcha;

    public function rules()
    {
        return [
            [['q'], 'string'],
            [['includePages','includeFiles','includeNews','type',], 'integer'],
            [['q','includePages','includeFiles','includeNews','type',], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'q' => 'Buscar...',
            'includePages' => 'Páginas',
            'includeFiles' => 'Documentos',
            'includeNews' => 'Noticias',
            'type' => 'Tipo de búsqueda',
            'reCaptcha' => 'Para continuar, debe afirmar que no es un robot, dándole click al recuadro',
        ];
    }

    public function getPages($doSearch, $model, $pageSize)
    {
        if($doSearch){
            $pagesQry = new Query();
            $pagesQry->from('fn_static_pages');
            $pagesQry->select('fn_category.title as cat_title, fn_static_pages.title, fn_static_pages.slug, fn_static_pages.excerpt, fn_category.slug as cat_slug, fn_static_pages.slug');
            $pagesQry->leftJoin('fn_category', 'fn_static_pages.category_id = fn_category.id');
            if(!$model->type){
                $pagesQry->where("FREETEXT ((fn_static_pages.title, fn_static_pages.excerpt, fn_static_pages.content), '{$model->q}')");
            } else {
                $pagesQry->where("CONTAINS ((fn_static_pages.title, fn_static_pages.excerpt, fn_static_pages.content), '\"{$model->q}\"')");
            }
            $countPagesQuery = clone $pagesQry;
            $pagesPagination = new Pagination(['totalCount' => $countPagesQuery->count()]);
            return [$pagesQry->offset($pagesPagination->offset)->limit($pageSize)->all(), $pagesPagination];
        }
        return [];
    }

    public function getNews($doSearch, $model, $pageSize)
    {
        if($doSearch){
            $newsQry = News::find();
            if(!$model->type){
                $newsQry->where("FREETEXT ((content, title, extract), '{$model->q}')");
            } else {
                $newsQry->where("CONTAINS ((content, title, extract), '\"{$model->q}\"')");
            }
            $countNewsQuery = clone $newsQry;
            $newsPagination = new Pagination(['totalCount' => $countNewsQuery->count()]);
            return [$newsQry->offset($newsPagination->offset)->limit($pageSize)->all(), $newsPagination];
        }
        return [];
    }

    public function getFiles($doSearch, $model, $pageSize)
    {
        if($doSearch){
            $filesQry = Files::find();
            if(!$model->type){
                $filesQry->where("FREETEXT ((title, description), '{$model->q}')");
            } else {
                $filesQry->where("CONTAINS ((title, description), '\"{$model->q}\"')");
            }
            $countFilesQuery = clone $filesQry;
            $filesPagination = new Pagination(['totalCount' => $countFilesQuery->count()]);
            $files = $filesQry->offset($filesPagination->offset)->limit($pageSize)->all();
            return [$filesQry->offset($filesPagination->offset)->limit($pageSize)->all(), $filesPagination];
        }
        return [];
    }

}