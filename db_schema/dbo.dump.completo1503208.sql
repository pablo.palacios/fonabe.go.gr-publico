/*
Navicat SQL Server Data Transfer

Source Server         : Fonabe
Source Server Version : 110000
Source Host           : 192.168.100.112:1433
Source Database       : fonabe
Source Schema         : dbo

Target Server Type    : SQL Server
Target Server Version : 110000
File Encoding         : 65001

Date: 2018-03-15 11:50:25
*/


-- ----------------------------
-- Table structure for fn_account
-- ----------------------------
DROP TABLE [dbo].[fn_account]
GO
CREATE TABLE [dbo].[fn_account] (
[id] int NOT NULL IDENTITY(1,1) ,
[user_id] int NULL DEFAULT NULL ,
[provider] nvarchar(255) NOT NULL ,
[client_id] nvarchar(255) NOT NULL ,
[properties] nvarchar(MAX) NULL DEFAULT NULL 
)


GO

-- ----------------------------
-- Records of fn_account
-- ----------------------------
SET IDENTITY_INSERT [dbo].[fn_account] ON
GO
SET IDENTITY_INSERT [dbo].[fn_account] OFF
GO

-- ----------------------------
-- Table structure for fn_auth_assignment
-- ----------------------------
DROP TABLE [dbo].[fn_auth_assignment]
GO
CREATE TABLE [dbo].[fn_auth_assignment] (
[item_name] nvarchar(64) NOT NULL ,
[user_id] nvarchar(64) NOT NULL ,
[created_at] int NULL 
)


GO

-- ----------------------------
-- Records of fn_auth_assignment
-- ----------------------------
INSERT INTO [dbo].[fn_auth_assignment] ([item_name], [user_id], [created_at]) VALUES (N'ADMIN', N'12', N'1520784717')
GO
GO
INSERT INTO [dbo].[fn_auth_assignment] ([item_name], [user_id], [created_at]) VALUES (N'SUPERADMIN', N'13', N'1520784349')
GO
GO

-- ----------------------------
-- Table structure for fn_auth_item
-- ----------------------------
DROP TABLE [dbo].[fn_auth_item]
GO
CREATE TABLE [dbo].[fn_auth_item] (
[name] nvarchar(64) NOT NULL ,
[type] smallint NOT NULL ,
[description] varchar(MAX) NULL DEFAULT NULL ,
[rule_name] nvarchar(64) NULL DEFAULT NULL ,
[created_at] int NULL DEFAULT NULL ,
[updated_at] int NULL DEFAULT NULL ,
[data] nvarchar(MAX) NULL DEFAULT NULL 
)


GO

-- ----------------------------
-- Records of fn_auth_item
-- ----------------------------
INSERT INTO [dbo].[fn_auth_item] ([name], [type], [description], [rule_name], [created_at], [updated_at], [data]) VALUES (N'ADMIN', N'1', N'', null, N'1519917471', N'1519917484', null)
GO
GO
INSERT INTO [dbo].[fn_auth_item] ([name], [type], [description], [rule_name], [created_at], [updated_at], [data]) VALUES (N'manageCategory', N'2', N'Gestión de  categorías', null, N'1520965935', N'1520966007', null)
GO
GO
INSERT INTO [dbo].[fn_auth_item] ([name], [type], [description], [rule_name], [created_at], [updated_at], [data]) VALUES (N'manageStaticPages', N'2', N'Gestión de Páginas Estáticas', null, N'1520965990', N'1520965990', null)
GO
GO
INSERT INTO [dbo].[fn_auth_item] ([name], [type], [description], [rule_name], [created_at], [updated_at], [data]) VALUES (N'SUPERADMIN', N'1', N'Acceso a toda la administración', null, N'1519917448', N'1519917448', null)
GO
GO

-- ----------------------------
-- Table structure for fn_auth_item_child
-- ----------------------------
DROP TABLE [dbo].[fn_auth_item_child]
GO
CREATE TABLE [dbo].[fn_auth_item_child] (
[parent] nvarchar(64) NOT NULL ,
[child] nvarchar(64) NOT NULL 
)


GO

-- ----------------------------
-- Records of fn_auth_item_child
-- ----------------------------

-- ----------------------------
-- Table structure for fn_auth_rule
-- ----------------------------
DROP TABLE [dbo].[fn_auth_rule]
GO
CREATE TABLE [dbo].[fn_auth_rule] (
[name] nvarchar(64) NOT NULL ,
[created_at] int NULL DEFAULT NULL ,
[updated_at] int NULL DEFAULT NULL ,
[data] nvarchar(MAX) NULL DEFAULT NULL 
)


GO

-- ----------------------------
-- Records of fn_auth_rule
-- ----------------------------

-- ----------------------------
-- Table structure for fn_category
-- ----------------------------
DROP TABLE [dbo].[fn_category]
GO
CREATE TABLE [dbo].[fn_category] (
[id] int NOT NULL IDENTITY(1,1) ,
[title] varchar(255) NULL ,
[slug] varchar(255) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[fn_category]', RESEED, 9)
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_category', 
'COLUMN', N'title')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Título categoría'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_category'
, @level2type = 'COLUMN', @level2name = N'title'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Título categoría'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_category'
, @level2type = 'COLUMN', @level2name = N'title'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_category', 
'COLUMN', N'slug')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Slug para el URL'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_category'
, @level2type = 'COLUMN', @level2name = N'slug'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Slug para el URL'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_category'
, @level2type = 'COLUMN', @level2name = N'slug'
GO

-- ----------------------------
-- Records of fn_category
-- ----------------------------
SET IDENTITY_INSERT [dbo].[fn_category] ON
GO
INSERT INTO [dbo].[fn_category] ([id], [title], [slug]) VALUES (N'1', N'Centro Educativo', N'centro-educativo')
GO
GO
INSERT INTO [dbo].[fn_category] ([id], [title], [slug]) VALUES (N'2', N'Universidad', N'universidad')
GO
GO
INSERT INTO [dbo].[fn_category] ([id], [title], [slug]) VALUES (N'3', N'Educación abierta', N'educacion-abierta')
GO
GO
INSERT INTO [dbo].[fn_category] ([id], [title], [slug]) VALUES (N'4', N'Noticias', N'noticias')
GO
GO
INSERT INTO [dbo].[fn_category] ([id], [title], [slug]) VALUES (N'5', N'Transparencia', N'transparencia')
GO
GO
INSERT INTO [dbo].[fn_category] ([id], [title], [slug]) VALUES (N'6', N'FONABE', N'fonabe')
GO
GO
INSERT INTO [dbo].[fn_category] ([id], [title], [slug]) VALUES (N'7', N'Contáctenos', N'contactenos')
GO
GO
INSERT INTO [dbo].[fn_category] ([id], [title], [slug]) VALUES (N'8', N'Principal', N'principal')
GO
GO
INSERT INTO [dbo].[fn_category] ([id], [title], [slug]) VALUES (N'9', N'Otros', N'pagina')
GO
GO
SET IDENTITY_INSERT [dbo].[fn_category] OFF
GO

-- ----------------------------
-- Table structure for fn_complaints
-- ----------------------------
DROP TABLE [dbo].[fn_complaints]
GO
CREATE TABLE [dbo].[fn_complaints] (
[id] int NOT NULL IDENTITY(1,1) ,
[first_name] varchar(255) NOT NULL ,
[last_name] varchar(255) NOT NULL ,
[identification_card] varchar(40) NOT NULL ,
[phone] varchar(50) NOT NULL ,
[address] varchar(500) NOT NULL ,
[email] varchar(150) NOT NULL ,
[fax] varchar(150) NULL ,
[place_of_work] varchar(500) NULL ,
[details] varchar(2500) NOT NULL ,
[reported_name_01] varchar(500) NOT NULL ,
[reported_name_02] varchar(500) NULL ,
[reported_name_03] varchar(500) NULL ,
[reported_location_01] varchar(500) NOT NULL ,
[reported_location_02] varchar(500) NULL ,
[reported_location_03] varchar(500) NULL ,
[evidence_01] varchar(2000) NOT NULL ,
[evidence_02] varchar(2000) NULL ,
[evidence_03] varchar(2000) NULL ,
[evidence_location_01] varchar(500) NOT NULL ,
[evidence_location_02] varchar(500) NULL ,
[evidence_location_03] varchar(500) NULL ,
[witness_01] varchar(500) NOT NULL ,
[witness_02] varchar(500) NULL ,
[witness_03] varchar(500) NULL ,
[witness_location_01] varchar(500) NOT NULL ,
[witness_location_02] varchar(500) NULL ,
[witness_location_03] varchar(500) NULL ,
[created_at] int NULL ,
[updated_at] int NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[fn_complaints]', RESEED, 6)
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Id de las quejas'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Id de las quejas'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'id'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'first_name')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Nombre'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'first_name'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'first_name'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'last_name')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Apellido'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'last_name'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Apellido'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'last_name'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'identification_card')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Número de identificación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'identification_card'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Número de identificación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'identification_card'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'phone')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Teléfono'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'phone'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Teléfono'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'phone'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'address')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Dirección'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'address'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Dirección'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'address'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'email')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Correo electrónico'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'email'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Correo electrónico'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'email'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'fax')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Número de Fax'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'fax'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Número de Fax'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'fax'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'place_of_work')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Lugar de trabajo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'place_of_work'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Lugar de trabajo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'place_of_work'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'details')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Detalles'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'details'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Detalles'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'details'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'reported_name_01')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Nombre'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'reported_name_01'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'reported_name_01'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'reported_name_02')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Nombre'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'reported_name_02'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'reported_name_02'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'reported_name_03')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Nombre'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'reported_name_03'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'reported_name_03'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'reported_location_01')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'reported_location_01'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'reported_location_01'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'reported_location_02')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'reported_location_02'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'reported_location_02'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'reported_location_03')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'reported_location_03'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'reported_location_03'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'evidence_01')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Evidencia'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'evidence_01'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Evidencia'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'evidence_01'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'evidence_02')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Evidencia'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'evidence_02'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Evidencia'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'evidence_02'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'evidence_03')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Evidencia'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'evidence_03'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Evidencia'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'evidence_03'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'evidence_location_01')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'evidence_location_01'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'evidence_location_01'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'evidence_location_02')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'evidence_location_02'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'evidence_location_02'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'evidence_location_03')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'evidence_location_03'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'evidence_location_03'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'witness_01')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Testigo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'witness_01'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Testigo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'witness_01'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'witness_02')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Testigo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'witness_02'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Testigo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'witness_02'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'witness_03')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Testigo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'witness_03'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Testigo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'witness_03'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'witness_location_01')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'witness_location_01'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'witness_location_01'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'witness_location_02')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'witness_location_02'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'witness_location_02'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'witness_location_03')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'witness_location_03'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'witness_location_03'
GO

-- ----------------------------
-- Records of fn_complaints
-- ----------------------------
SET IDENTITY_INSERT [dbo].[fn_complaints] ON
GO
INSERT INTO [dbo].[fn_complaints] ([id], [first_name], [last_name], [identification_card], [phone], [address], [email], [fax], [place_of_work], [details], [reported_name_01], [reported_name_02], [reported_name_03], [reported_location_01], [reported_location_02], [reported_location_03], [evidence_01], [evidence_02], [evidence_03], [evidence_location_01], [evidence_location_02], [evidence_location_03], [witness_01], [witness_02], [witness_03], [witness_location_01], [witness_location_02], [witness_location_03], [created_at], [updated_at]) VALUES (N'2', N'asdfasdf', N'asdfasdf', N'145234234', N'23141234', N'fasdfasdf', N'asdf@sdf.com', null, null, N'Quisque velit nisi, pretium ut lacinia in, elementum id enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Cras ultricies ligula sed magna dictum porta. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Sed porttitor lectus nibh. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis lorem ut libero malesuada feugiat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vivamus magna justo, lacinia eget consectetur ', N'435435', null, null, N'sdfg', null, null, N'sdfgsdfg', null, null, N'sdfgsdfg', null, null, N'sdfgsdfg', null, null, N'sdfgsdfg', null, null, N'1520952826', N'1520952826')
GO
GO
INSERT INTO [dbo].[fn_complaints] ([id], [first_name], [last_name], [identification_card], [phone], [address], [email], [fax], [place_of_work], [details], [reported_name_01], [reported_name_02], [reported_name_03], [reported_location_01], [reported_location_02], [reported_location_03], [evidence_01], [evidence_02], [evidence_03], [evidence_location_01], [evidence_location_02], [evidence_location_03], [witness_01], [witness_02], [witness_03], [witness_location_01], [witness_location_02], [witness_location_03], [created_at], [updated_at]) VALUES (N'3', N'asdfasdf', N'asdfasdf', N'145234234', N'23141234', N'fasdfasdf', N'asdf@sdf.com', null, null, N'Quisque velit nisi, pretium ut lacinia in, elementum id enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Cras ultricies ligula sed magna dictum porta. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Sed porttitor lectus nibh. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis lorem ut libero malesuada feugiat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vivamus magna justo, lacinia eget consectetur ', N'435435', null, null, N'sdfg', null, null, N'sdfgsdfg', null, null, N'sdfgsdfg', null, null, N'sdfgsdfg', null, null, N'sdfgsdfg', null, null, N'1520952827', N'1520952827')
GO
GO
INSERT INTO [dbo].[fn_complaints] ([id], [first_name], [last_name], [identification_card], [phone], [address], [email], [fax], [place_of_work], [details], [reported_name_01], [reported_name_02], [reported_name_03], [reported_location_01], [reported_location_02], [reported_location_03], [evidence_01], [evidence_02], [evidence_03], [evidence_location_01], [evidence_location_02], [evidence_location_03], [witness_01], [witness_02], [witness_03], [witness_location_01], [witness_location_02], [witness_location_03], [created_at], [updated_at]) VALUES (N'4', N'Pablo', N'Palacios', N'12312 3 123 12 ', N'3345423145', N'ZXCXZ ZXCZXC', N'mp@moplin.com', null, null, N'Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Quisque velit nisi, pretium ut lacinia in, elementum id enim.

Sed porttitor lectus nibh. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Nulla quis lorem ut libero malesuada feugiat.

Nulla porttitor accumsan tincidunt. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eget tortor risus.', N'gfd sdfgfgds dfg', null, null, N'sd fgsdfgsdfg', null, null, N'sdf gsdfg', null, null, N'sdfgsdfg', null, null, N'sd fgdsfg', null, null, N'sd fgsdfg ', null, null, N'1520953041', N'1520953041')
GO
GO
INSERT INTO [dbo].[fn_complaints] ([id], [first_name], [last_name], [identification_card], [phone], [address], [email], [fax], [place_of_work], [details], [reported_name_01], [reported_name_02], [reported_name_03], [reported_location_01], [reported_location_02], [reported_location_03], [evidence_01], [evidence_02], [evidence_03], [evidence_location_01], [evidence_location_02], [evidence_location_03], [witness_01], [witness_02], [witness_03], [witness_location_01], [witness_location_02], [witness_location_03], [created_at], [updated_at]) VALUES (N'5', N'waxa', N'ww', N'34543534', N'34543543543', N'345345434 553 45 xdfgdfg sdfg', N'pp@moplin.com', null, null, N'sCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Sed porttitor lectus nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Donec rutrum congue leo eget malesuada.

Donec rutrum congue leo eget malesuada. Curabitur aliquet quam id dui posuere blandit. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Vivamus suscipit tortor eget felis porttitor volutpat. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus.

Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Proin eget tortor risus. Nulla quis lorem ut libero malesuada feugiat. Curabitur aliquet quam id dui posuere blandit. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.', N'gfdhgfdh', null, null, N'dfghdfgh', null, null, N'dfghdfgh', null, null, N'dfghdfg', null, null, N'dfghdfgh', null, null, N'dfghdfgh', null, null, N'1520953267', N'1520953267')
GO
GO
INSERT INTO [dbo].[fn_complaints] ([id], [first_name], [last_name], [identification_card], [phone], [address], [email], [fax], [place_of_work], [details], [reported_name_01], [reported_name_02], [reported_name_03], [reported_location_01], [reported_location_02], [reported_location_03], [evidence_01], [evidence_02], [evidence_03], [evidence_location_01], [evidence_location_02], [evidence_location_03], [witness_01], [witness_02], [witness_03], [witness_location_01], [witness_location_02], [witness_location_03], [created_at], [updated_at]) VALUES (N'6', N'waxa-2', N'ww', N'34543534', N'34543543543', N'345345434 553 45 xdfgdfg sdfg', N'pp@moplin.com', null, null, N'sCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Sed porttitor lectus nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Donec rutrum congue leo eget malesuada.

Donec rutrum congue leo eget malesuada. Curabitur aliquet quam id dui posuere blandit. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Vivamus suscipit tortor eget felis porttitor volutpat. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus.

Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Proin eget tortor risus. Nulla quis lorem ut libero malesuada feugiat. Curabitur aliquet quam id dui posuere blandit. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.', N'gfdhgfdh', null, null, N'dfghdfgh', null, null, N'dfghdfgh', null, null, N'dfghdfg', null, null, N'dfghdfgh', null, null, N'dfghdfgh', null, null, N'1520953382', N'1520953382')
GO
GO
SET IDENTITY_INSERT [dbo].[fn_complaints] OFF
GO

-- ----------------------------
-- Table structure for fn_contact
-- ----------------------------
DROP TABLE [dbo].[fn_contact]
GO
CREATE TABLE [dbo].[fn_contact] (
[id] int NOT NULL IDENTITY(1,1) ,
[first_name] varchar(255) NOT NULL ,
[last_name] varchar(255) NOT NULL ,
[identification] varchar(255) NOT NULL ,
[phone] varchar(255) NULL ,
[email] varchar(255) NOT NULL ,
[subject] varchar(500) NOT NULL ,
[detail] varchar(2500) NOT NULL ,
[created_at] int NULL ,
[updated_at] int NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[fn_contact]', RESEED, 3)
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_contact', 
'COLUMN', N'id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Id de contacto'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Id de contacto'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'id'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_contact', 
'COLUMN', N'first_name')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Nombre'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'first_name'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'first_name'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_contact', 
'COLUMN', N'last_name')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Apellido'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'last_name'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Apellido'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'last_name'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_contact', 
'COLUMN', N'identification')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Identificación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'identification'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Identificación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'identification'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_contact', 
'COLUMN', N'phone')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Teléfono'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'phone'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Teléfono'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'phone'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_contact', 
'COLUMN', N'email')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Correo electrónico'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'email'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Correo electrónico'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'email'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_contact', 
'COLUMN', N'subject')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Tema'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'subject'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Tema'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'subject'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_contact', 
'COLUMN', N'detail')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Detalles'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'detail'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Detalles'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'detail'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_contact', 
'COLUMN', N'created_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'created_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'created_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_contact', 
'COLUMN', N'updated_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha de actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'updated_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'updated_at'
GO

-- ----------------------------
-- Records of fn_contact
-- ----------------------------
SET IDENTITY_INSERT [dbo].[fn_contact] ON
GO
INSERT INTO [dbo].[fn_contact] ([id], [first_name], [last_name], [identification], [phone], [email], [subject], [detail], [created_at], [updated_at]) VALUES (N'1', N'Pablo', N'Palacios', N'2342343242', N'2523452345', N'pp@moplin.com', N'test', N'test test', N'1520951158', N'1520951158')
GO
GO
INSERT INTO [dbo].[fn_contact] ([id], [first_name], [last_name], [identification], [phone], [email], [subject], [detail], [created_at], [updated_at]) VALUES (N'2', N'Perico', N'Palotes', N'2312321312312', N'2423423432', N'pp@moplin.com', N'wtertwerterw', N'sdfg sdfgsdfgsdfgdsfg dsfg', N'1520994847', N'1520994847')
GO
GO
INSERT INTO [dbo].[fn_contact] ([id], [first_name], [last_name], [identification], [phone], [email], [subject], [detail], [created_at], [updated_at]) VALUES (N'3', N'perrito', N'negro', N'234234324', N'34534534534', N'pp@moplin.com', N'5345dfgdfg ', N'dfggdfs ifdsigjdsfijgifdj gijsdigjdsf k', N'1520994932', N'1520994932')
GO
GO
SET IDENTITY_INSERT [dbo].[fn_contact] OFF
GO

-- ----------------------------
-- Table structure for fn_events
-- ----------------------------
DROP TABLE [dbo].[fn_events]
GO
CREATE TABLE [dbo].[fn_events] (
[id] int NOT NULL IDENTITY(1,1) ,
[title] varchar(500) NOT NULL ,
[description] varchar(2000) NOT NULL ,
[updated_at] int NULL ,
[created_at] int NULL ,
[end_dt] datetime2(7) NOT NULL ,
[start_dt] datetime2(7) NOT NULL ,
[hours] int NULL DEFAULT ((1)) ,
[date] date NULL ,
[time] varchar(35) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[fn_events]', RESEED, 16)
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_events', 
'COLUMN', N'id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Id Eventos'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Id Eventos'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'id'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_events', 
'COLUMN', N'title')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Título del evento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'title'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Título del evento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'title'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_events', 
'COLUMN', N'description')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Descripción del evento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'description'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Descripción del evento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'description'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_events', 
'COLUMN', N'updated_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'updated_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'updated_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_events', 
'COLUMN', N'created_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'created_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'created_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_events', 
'COLUMN', N'end_dt')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Hora y fecha de fin'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'end_dt'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Hora y fecha de fin'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'end_dt'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_events', 
'COLUMN', N'start_dt')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Hora y fecha de inicio'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'start_dt'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Hora y fecha de inicio'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'start_dt'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_events', 
'COLUMN', N'hours')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Duración del evento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'hours'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Duración del evento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'hours'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_events', 
'COLUMN', N'date')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha del evento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'date'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha del evento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'date'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_events', 
'COLUMN', N'time')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Hora de inicio del evento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'time'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Hora de inicio del evento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'time'
GO

-- ----------------------------
-- Records of fn_events
-- ----------------------------
SET IDENTITY_INSERT [dbo].[fn_events] ON
GO
INSERT INTO [dbo].[fn_events] ([id], [title], [description], [updated_at], [created_at], [end_dt], [start_dt], [hours], [date], [time]) VALUES (N'2', N'TEST2', N'testing 2', null, null, N'2031-05-10 18:00:00.0000000', N'2031-05-10 15:00:00.0000000', N'3', N'2017-11-25', N'15:00')
GO
GO
INSERT INTO [dbo].[fn_events] ([id], [title], [description], [updated_at], [created_at], [end_dt], [start_dt], [hours], [date], [time]) VALUES (N'9', N'sdfsdfds', N'sdfdsfdsf', null, null, N'2015-09-08 21:30:00.0000000', N'2015-09-08 19:30:00.0000000', N'2', N'2018-03-10', N'19:30')
GO
GO
INSERT INTO [dbo].[fn_events] ([id], [title], [description], [updated_at], [created_at], [end_dt], [start_dt], [hours], [date], [time]) VALUES (N'11', N'perr', N'totote', null, null, N'2018-03-06 12:30:00.0000000', N'2018-03-06 09:30:00.0000000', N'3', N'2018-03-06', N'09:30')
GO
GO
INSERT INTO [dbo].[fn_events] ([id], [title], [description], [updated_at], [created_at], [end_dt], [start_dt], [hours], [date], [time]) VALUES (N'13', N'Esta es una prueba completa del calendario', N'Los detalles del evento son varios', null, null, N'2033-09-08 22:00:00.0000000', N'2033-09-08 18:00:00.0000000', N'4', N'2018-03-28', N'18:00')
GO
GO
INSERT INTO [dbo].[fn_events] ([id], [title], [description], [updated_at], [created_at], [end_dt], [start_dt], [hours], [date], [time]) VALUES (N'14', N'LOS LOCOS ADAMS', N'Juas Juas', null, null, N'2036-09-07 21:30:00.0000000', N'2036-09-07 15:30:00.0000000', N'6', N'2018-03-31', N'15:30')
GO
GO
INSERT INTO [dbo].[fn_events] ([id], [title], [description], [updated_at], [created_at], [end_dt], [start_dt], [hours], [date], [time]) VALUES (N'15', N'Este es el evento de Abril', N'Planeamos dar muchas becas...', null, null, N'2035-10-09 20:00:00.0000000', N'2035-10-09 12:00:00.0000000', N'8', N'2018-04-30', N'12:00')
GO
GO
INSERT INTO [dbo].[fn_events] ([id], [title], [description], [updated_at], [created_at], [end_dt], [start_dt], [hours], [date], [time]) VALUES (N'16', N'PRUEBA', N'PRUEBA', null, null, N'2031-09-08 17:00:00.0000000', N'2031-09-08 13:00:00.0000000', N'4', N'2018-03-26', N'13:00')
GO
GO
SET IDENTITY_INSERT [dbo].[fn_events] OFF
GO

-- ----------------------------
-- Table structure for fn_files
-- ----------------------------
DROP TABLE [dbo].[fn_files]
GO
CREATE TABLE [dbo].[fn_files] (
[page_id] int NULL ,
[title] varchar(500) NULL ,
[description] varchar(2500) NULL ,
[file_url] varchar(500) NULL ,
[hit_counter] int NULL ,
[download_counter] int NULL ,
[created_at] int NULL ,
[updated_at] int NULL ,
[id] int NOT NULL IDENTITY(1,1) ,
[file_date] date NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[fn_files]', RESEED, 21)
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_files', 
'COLUMN', N'page_id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Id página'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'page_id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Id página'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'page_id'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_files', 
'COLUMN', N'title')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Título documento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'title'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Título documento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'title'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_files', 
'COLUMN', N'description')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Descripción documento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'description'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Descripción documento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'description'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_files', 
'COLUMN', N'file_url')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Url documento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'file_url'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Url documento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'file_url'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_files', 
'COLUMN', N'hit_counter')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Contador hits'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'hit_counter'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Contador hits'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'hit_counter'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_files', 
'COLUMN', N'download_counter')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Contador downloads'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'download_counter'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Contador downloads'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'download_counter'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_files', 
'COLUMN', N'created_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'FEcha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'created_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'FEcha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'created_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_files', 
'COLUMN', N'updated_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'updated_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'updated_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_files', 
'COLUMN', N'id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Id documento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Id documento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'id'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_files', 
'COLUMN', N'file_date')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha del documento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'file_date'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha del documento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'file_date'
GO

-- ----------------------------
-- Records of fn_files
-- ----------------------------
SET IDENTITY_INSERT [dbo].[fn_files] ON
GO
INSERT INTO [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (N'25', N'Documento 1', N'Este es el documento de la grande Pa', N'5aa5d7c2802a70.48570643.docx', N'9', null, N'1520535500', N'1520969530', N'1', N'2018-03-07')
GO
GO
INSERT INTO [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (N'26', N'Documento 2', N'ddd', N'5aa5d7dff3a8c1.36128436.pdf', N'7', null, N'1520535500', N'1520861233', N'2', N'2018-03-11')
GO
GO
INSERT INTO [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (N'29', N'Documento 3', N'Documento3Documento3Documento3', N'5aa5d7dff3a8c1.36128436.pdf', N'3', N'3', N'1520535500', N'1520861007', N'3', N'2018-03-07')
GO
GO
INSERT INTO [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (N'29', N'Documento 4', N'd 4', N'5aa5d7dff3a8c1.36128436.pdf', N'2', null, N'1520535500', N'1520606587', N'4', N'2010-03-01')
GO
GO
INSERT INTO [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (N'25', N'D1', N'D1', N'5aa5d7dff3a8c1.36128436.pdf', N'6', null, N'1520543419', N'1520969867', N'5', N'2018-03-09')
GO
GO
INSERT INTO [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (N'32', N'Inf 1428ccdcdcd', N'Wobba lobba dub dub', N'5aa5d7c2802a70.48570643.docx', null, N'2', N'1520547373', N'1520797684', N'6', N'2018-03-11')
GO
GO
INSERT INTO [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (N'29', N'Inf 2', N'Inf 2', N'5aa5d7dff3a8c1.36128436.pdf', null, null, N'1520547702', N'1520607018', N'7', N'2018-03-07')
GO
GO
INSERT INTO [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (N'29', N'Inf 3', N'Inf 3', N'5aa5d7dff3a8c1.36128436.pdf', null, null, N'1520547723', N'1520607037', N'8', N'2018-03-05')
GO
GO
INSERT INTO [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (N'29', N'Informe 4 de lo que sea', N'un monton de bla bla bla...', N'5aa5d7dff3a8c1.36128436.pdf', N'26', N'6', N'1520547745', N'1520969422', N'9', N'2018-03-01')
GO
GO
INSERT INTO [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (N'29', N'Informe 5', N'Inf 5', N'5aa5d7dff3a8c1.36128436.pdf', null, N'1', N'1520547770', N'1520860755', N'10', N'2018-02-01')
GO
GO
INSERT INTO [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (N'29', N'Informe 6', N'Inf 6', N'5aa5d7dff3a8c1.36128436.pdf', N'1', null, N'1520547791', N'1520946550', N'11', N'2018-02-01')
GO
GO
INSERT INTO [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (N'29', N'EL P DOCUMENTO', N'asdasdaf', N'5aa5d7dff3a8c1.36128436.pdf', null, null, N'1520606897', N'1520606976', N'12', N'2015-03-08')
GO
GO
INSERT INTO [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (N'25', N'Test doc', N'prueba', N'5aa5d7dff3a8c1.36128436.pdf', N'3', N'4', N'1520608104', N'1520969851', N'13', N'2018-03-09')
GO
GO
INSERT INTO [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (N'25', N'test', N'test', N'5aa5d7dff3a8c1.36128436.pdf', N'2', null, N'1520794467', N'1520969874', N'14', N'2017-10-10')
GO
GO
INSERT INTO [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (N'33', N'Estadisctica', N'asdasdasdsad', N'5aa5d7dff3a8c1.36128436.pdf', null, N'2', N'1520797100', N'1520797282', N'15', N'2018-03-15')
GO
GO
INSERT INTO [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (N'26', N'Perico', N'Teting', N'5aa5d7c2802a70.48570643.docx', N'3', N'2', N'1520797394', N'1520946569', N'16', N'2018-03-09')
GO
GO
INSERT INTO [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (N'27', N'sdfg', N'sdfg', N'5aa5d7c2802a70.48570643.docx', null, null, N'1520798563', N'1520798563', N'17', N'2018-03-08')
GO
GO
INSERT INTO [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (N'26', N'zxcvcxzv', N'sdfgsd', N'5aa5d7c2802a70.48570643.docx', null, null, N'1520798749', N'1520798749', N'18', N'2018-03-08')
GO
GO
INSERT INTO [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (N'30', N'Presupuesto', N'Presupuesto Presupuesto', N'5aa5d7c2802a70.48570643.docx', null, null, N'1520818013', N'1520818013', N'19', N'2018-03-04')
GO
GO
INSERT INTO [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (N'30', N'Nuevo', N'nnn n n n n n ', N'5aa5d7dff3a8c1.36128436.pdf', null, null, N'1520818144', N'1520818144', N'20', N'2018-03-08')
GO
GO
INSERT INTO [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (N'34', N'Titulo', N'Test', N'5aa82a935d1a74.24825587.docx', null, null, N'1520970387', N'1520970387', N'21', N'2018-04-12')
GO
GO
SET IDENTITY_INSERT [dbo].[fn_files] OFF
GO

-- ----------------------------
-- Table structure for fn_news
-- ----------------------------
DROP TABLE [dbo].[fn_news]
GO
CREATE TABLE [dbo].[fn_news] (
[id] int NOT NULL IDENTITY(1,1) ,
[title] varchar(500) NOT NULL ,
[slug] varchar(500) NOT NULL ,
[extract] varchar(550) NULL ,
[content] nvarchar(MAX) NULL ,
[author_name] varchar(255) NULL ,
[url_imagen] varchar(500) NULL ,
[updated_at] int NULL ,
[created_at] int NULL ,
[published] bit NOT NULL DEFAULT ((1)) 
)


GO
DBCC CHECKIDENT(N'[dbo].[fn_news]', RESEED, 11)
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_news', 
'COLUMN', N'id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Id de noticia'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Id de noticia'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'id'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_news', 
'COLUMN', N'title')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Título de la noticia'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'title'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Título de la noticia'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'title'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_news', 
'COLUMN', N'slug')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Slug para URL'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'slug'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Slug para URL'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'slug'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_news', 
'COLUMN', N'extract')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Resúmen'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'extract'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Resúmen'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'extract'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_news', 
'COLUMN', N'content')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Contenido'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'content'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Contenido'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'content'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_news', 
'COLUMN', N'author_name')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Nombre del autor'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'author_name'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre del autor'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'author_name'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_news', 
'COLUMN', N'updated_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha de ultima actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'updated_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de ultima actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'updated_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_news', 
'COLUMN', N'created_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'created_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'created_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_news', 
'COLUMN', N'published')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Publicado'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'published'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Publicado'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'published'
GO

-- ----------------------------
-- Records of fn_news
-- ----------------------------
SET IDENTITY_INSERT [dbo].[fn_news] ON
GO
INSERT INTO [dbo].[fn_news] ([id], [title], [slug], [extract], [content], [author_name], [url_imagen], [updated_at], [created_at], [published]) VALUES (N'3', N'Esta es la noticiota bien waxa test', N'esta-es-la-noticiota-bien-waxa-test', N'Pellentesque in ipsum id orci porta dapibus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus.', N'<p>Pellentesque in ipsum id orci porta dapibus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Donec rutrum congue leo eget malesuada.</p>

<p>Donec sollicitudin molestie malesuada. Pellentesque in ipsum id orci porta dapibus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Pellentesque in ipsum id orci porta dapibus. Nulla quis lorem ut libero malesuada feugiat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Nulla porttitor accumsan tincidunt. Vivamus suscipit tortor eget felis porttitor volutpat.</p>
', N'ppr', N'5aa5d46e506c50.38034636.jpg', N'1520817262', N'1520185071', N'1')
GO
GO
INSERT INTO [dbo].[fn_news] ([id], [title], [slug], [extract], [content], [author_name], [url_imagen], [updated_at], [created_at], [published]) VALUES (N'4', N'Otra-noticiota Muy bien', N'otra-noticiota-muy-bien', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Proin eget tortor risus. Donec rutrum congue leo eget malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae', N'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Proin eget tortor risus. Donec rutrum congue leo eget malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus.</p>

<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec rutrum congue leo eget malesuada. Vivamus suscipit tortor eget felis porttitor volutpat. Vivamus suscipit tortor eget felis porttitor volutpat. Donec rutrum congue leo eget malesuada. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Sed porttitor lectus nibh. Nulla quis lorem ut libero malesuada feugiat.</p>
', N'ppr', N'5aa86bd26508c6.85010726.jpg', N'1520987090', N'1544469147', N'1')
GO
GO
INSERT INTO [dbo].[fn_news] ([id], [title], [slug], [extract], [content], [author_name], [url_imagen], [updated_at], [created_at], [published]) VALUES (N'5', N'Noticia', N'noticia', N'Noticia', N'<p>Noticia</p>
', N'Noticia', N'5aa86bc04d8418.74870424.jpg', N'1520987072', N'1520185761', N'1')
GO
GO
INSERT INTO [dbo].[fn_news] ([id], [title], [slug], [extract], [content], [author_name], [url_imagen], [updated_at], [created_at], [published]) VALUES (N'6', N'Noticia 2', N'noticia-2', N'Noticia', N'<p>Noticia</p>
', N'Noticia', N'5aa5d4988c9c57.38191525.jpg', N'1520817304', N'1512414747', N'1')
GO
GO
INSERT INTO [dbo].[fn_news] ([id], [title], [slug], [extract], [content], [author_name], [url_imagen], [updated_at], [created_at], [published]) VALUES (N'7', N'sdasda', N'sdasda', N'Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Pellentesque in ipsum id orci porta dapibus. Nulla porttitor accumsan tinci.', N'<p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Pellentesque in ipsum id orci porta dapibus. Nulla porttitor accumsan tincidunt. Nulla porttitor accumsan tincidunt. Curabitur aliquet quam id dui posuere blandit. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur aliquet quam id dui posuere blandit. Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.</p>

<p>Curabitur aliquet quam id dui posuere blandit. Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquet quam id dui posuere blandit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vivamus suscipit tortor eget felis porttitor volutpat. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Donec sollicitudin molestie malesuada.</p>
', N'dsasd', N'5aa5d47bd8c0a2.23725812.jpg', N'1520817275', N'1513019547', N'1')
GO
GO
INSERT INTO [dbo].[fn_news] ([id], [title], [slug], [extract], [content], [author_name], [url_imagen], [updated_at], [created_at], [published]) VALUES (N'8', N'513245rasd asd gf', N'513245-rasd-asd-gf', N'Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. ', N'<p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Pellentesque in ipsum id orci porta dapibus. Nulla porttitor accumsan tincidunt. Nulla porttitor accumsan tincidunt. Curabitur aliquet quam id dui posuere blandit. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur aliquet quam id dui posuere blandit. Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.</p>

<p>Curabitur aliquet quam id dui posuere blandit. Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquet quam id dui posuere blandit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vivamus suscipit tortor eget felis porttitor volutpat. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Donec sollicitudin molestie malesuada.</p>
', N'q3we', N'5aa5d48a90bd24.69383634.jpg', N'1520817290', N'1512414747', N'1')
GO
GO
INSERT INTO [dbo].[fn_news] ([id], [title], [slug], [extract], [content], [author_name], [url_imagen], [updated_at], [created_at], [published]) VALUES (N'9', N'La noticia del año', N'la-noticia-del-ano', N'Donec sollicitudin molestie malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Nulla quis lorem ut libero malesuada feugiat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Donec sollicitudin molestie malesuada. ', N'<p>Donec sollicitudin molestie malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Nulla quis lorem ut libero malesuada feugiat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Donec sollicitudin molestie malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porttitor accumsan tincidunt. Nulla porttitor accumsan tincidunt. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.</p>

<p>Cras ultricies ligula sed magna dictum porta. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Pellentesque in ipsum id orci porta dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Proin eget tortor risus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Donec rutrum congue leo eget malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur aliquet quam id dui posuere blandit.</p>

<p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Pellentesque in ipsum id orci porta dapibus. Vivamus suscipit tortor eget felis porttitor volutpat. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Sed porttitor lectus nibh. Nulla quis lorem ut libero malesuada feugiat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Sed porttitor lectus nibh.</p>

<p>Proin eget tortor risus. Vivamus suscipit tortor eget felis porttitor volutpat. Proin eget tortor risus. Nulla porttitor accumsan tincidunt. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus suscipit tortor eget felis porttitor volutpat.</p>

<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur aliquet quam id dui posuere blandit. Proin eget tortor risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec sollicitudin molestie malesuada. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Donec sollicitudin molestie malesuada.</p>

<p>Donec sollicitudin molestie malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Nulla quis lorem ut libero malesuada feugiat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Donec sollicitudin molestie malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porttitor accumsan tincidunt. Nulla porttitor accumsan tincidunt. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.</p>

<p>Cras ultricies ligula sed magna dictum porta. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Pellentesque in ipsum id orci porta dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Proin eget tortor risus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Donec rutrum congue leo eget malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur aliquet quam id dui posuere blandit.</p>

<p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Pellentesque in ipsum id orci porta dapibus. Vivamus suscipit tortor eget felis porttitor volutpat. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Sed porttitor lectus nibh. Nulla quis lorem ut libero malesuada feugiat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Sed porttitor lectus nibh.</p>

<p>Proin eget tortor risus. Vivamus suscipit tortor eget felis porttitor volutpat. Proin eget tortor risus. Nulla porttitor accumsan tincidunt. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus suscipit tortor eget felis porttitor volutpat.</p>

<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur aliquet quam id dui posuere blandit. Proin eget tortor risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec sollicitudin molestie malesuada. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Donec sollicitudin molestie malesuada.</p>

<p>Donec sollicitudin molestie malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Nulla quis lorem ut libero malesuada feugiat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Donec sollicitudin molestie malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porttitor accumsan tincidunt. Nulla porttitor accumsan tincidunt. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.</p>

<p>Cras ultricies ligula sed magna dictum porta. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Pellentesque in ipsum id orci porta dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Proin eget tortor risus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Donec rutrum congue leo eget malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur aliquet quam id dui posuere blandit.</p>

<p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Pellentesque in ipsum id orci porta dapibus. Vivamus suscipit tortor eget felis porttitor volutpat. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Sed porttitor lectus nibh. Nulla quis lorem ut libero malesuada feugiat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Sed porttitor lectus nibh.</p>

<p>Proin eget tortor risus. Vivamus suscipit tortor eget felis porttitor volutpat. Proin eget tortor risus. Nulla porttitor accumsan tincidunt. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus suscipit tortor eget felis porttitor volutpat.</p>

<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur aliquet quam id dui posuere blandit. Proin eget tortor risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec sollicitudin molestie malesuada. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Donec sollicitudin molestie malesuada.</p>

<p>Donec sollicitudin molestie malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Nulla quis lorem ut libero malesuada feugiat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Donec sollicitudin molestie malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porttitor accumsan tincidunt. Nulla porttitor accumsan tincidunt. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.</p>

<p>Cras ultricies ligula sed magna dictum porta. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Pellentesque in ipsum id orci porta dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Proin eget tortor risus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Donec rutrum congue leo eget malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur aliquet quam id dui posuere blandit.</p>

<p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Pellentesque in ipsum id orci porta dapibus. Vivamus suscipit tortor eget felis porttitor volutpat. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Sed porttitor lectus nibh. Nulla quis lorem ut libero malesuada feugiat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Sed porttitor lectus nibh.</p>

<p>Proin eget tortor risus. Vivamus suscipit tortor eget felis porttitor volutpat. Proin eget tortor risus. Nulla porttitor accumsan tincidunt. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus suscipit tortor eget felis porttitor volutpat.</p>

<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur aliquet quam id dui posuere blandit. Proin eget tortor risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec sollicitudin molestie malesuada. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Donec sollicitudin molestie malesuada.</p>

<p>Donec sollicitudin molestie malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Nulla quis lorem ut libero malesuada feugiat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Donec sollicitudin molestie malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porttitor accumsan tincidunt. Nulla porttitor accumsan tincidunt. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.</p>

<p>Cras ultricies ligula sed magna dictum porta. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Pellentesque in ipsum id orci porta dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Proin eget tortor risus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Donec rutrum congue leo eget malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur aliquet quam id dui posuere blandit.</p>

<p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Pellentesque in ipsum id orci porta dapibus. Vivamus suscipit tortor eget felis porttitor volutpat. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Sed porttitor lectus nibh. Nulla quis lorem ut libero malesuada feugiat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Sed porttitor lectus nibh.</p>

<p>Proin eget tortor risus. Vivamus suscipit tortor eget felis porttitor volutpat. Proin eget tortor risus. Nulla porttitor accumsan tincidunt. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus suscipit tortor eget felis porttitor volutpat.</p>

<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur aliquet quam id dui posuere blandit. Proin eget tortor risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec sollicitudin molestie malesuada. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Donec sollicitudin molestie malesuada.</p>

<p>Donec sollicitudin molestie malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Nulla quis lorem ut libero malesuada feugiat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Donec sollicitudin molestie malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porttitor accumsan tincidunt. Nulla porttitor accumsan tincidunt. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.</p>

<p>Cras ultricies ligula sed magna dictum porta. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Pellentesque in ipsum id orci porta dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Proin eget tortor risus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Donec rutrum congue leo eget malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur aliquet quam id dui posuere blandit.</p>

<p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Pellentesque in ipsum id orci porta dapibus. Vivamus suscipit tortor eget felis porttitor volutpat. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Sed porttitor lectus nibh. Nulla quis lorem ut libero malesuada feugiat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Sed porttitor lectus nibh.</p>

<p>Proin eget tortor risus. Vivamus suscipit tortor eget felis porttitor volutpat. Proin eget tortor risus. Nulla porttitor accumsan tincidunt. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus suscipit tortor eget felis porttitor volutpat.</p>

<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur aliquet quam id dui posuere blandit. Proin eget tortor risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec sollicitudin molestie malesuada. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Donec sollicitudin molestie malesuada.</p>

<p>Donec sollicitudin molestie malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Nulla quis lorem ut libero malesuada feugiat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Donec sollicitudin molestie malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porttitor accumsan tincidunt. Nulla porttitor accumsan tincidunt. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.</p>

<p>Cras ultricies ligula sed magna dictum porta. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Pellentesque in ipsum id orci porta dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Proin eget tortor risus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Donec rutrum congue leo eget malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur aliquet quam id dui posuere blandit.</p>

<p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Pellentesque in ipsum id orci porta dapibus. Vivamus suscipit tortor eget felis porttitor volutpat. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Sed porttitor lectus nibh. Nulla quis lorem ut libero malesuada feugiat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Sed porttitor lectus nibh.</p>

<p>Proin eget tortor risus. Vivamus suscipit tortor eget felis porttitor volutpat. Proin eget tortor risus. Nulla porttitor accumsan tincidunt. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus suscipit tortor eget felis porttitor volutpat.</p>

<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur aliquet quam id dui posuere blandit. Proin eget tortor risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec sollicitudin molestie malesuada. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Donec sollicitudin molestie malesuada.</p>

<p>Donec sollicitudin molestie malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Nulla quis lorem ut libero malesuada feugiat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Donec sollicitudin molestie malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porttitor accumsan tincidunt. Nulla porttitor accumsan tincidunt. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.</p>

<p>Cras ultricies ligula sed magna dictum porta. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Pellentesque in ipsum id orci porta dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Proin eget tortor risus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Donec rutrum congue leo eget malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur aliquet quam id dui posuere blandit.</p>

<p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Pellentesque in ipsum id orci porta dapibus. Vivamus suscipit tortor eget felis porttitor volutpat. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Sed porttitor lectus nibh. Nulla quis lorem ut libero malesuada feugiat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Sed porttitor lectus nibh.</p>

<p>Proin eget tortor risus. Vivamus suscipit tortor eget felis porttitor volutpat. Proin eget tortor risus. Nulla porttitor accumsan tincidunt. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus suscipit tortor eget felis porttitor volutpat.</p>

<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur aliquet quam id dui posuere blandit. Proin eget tortor risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec sollicitudin molestie malesuada. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Donec sollicitudin molestie malesuada.</p>

<p>Donec sollicitudin molestie malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Nulla quis lorem ut libero malesuada feugiat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Donec sollicitudin molestie malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porttitor accumsan tincidunt. Nulla porttitor accumsan tincidunt. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.</p>

<p>Cras ultricies ligula sed magna dictum porta. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Pellentesque in ipsum id orci porta dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Proin eget tortor risus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Donec rutrum congue leo eget malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur aliquet quam id dui posuere blandit.</p>

<p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Pellentesque in ipsum id orci porta dapibus. Vivamus suscipit tortor eget felis porttitor volutpat. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Sed porttitor lectus nibh. Nulla quis lorem ut libero malesuada feugiat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Sed porttitor lectus nibh.</p>

<p>Proin eget tortor risus. Vivamus suscipit tortor eget felis porttitor volutpat. Proin eget tortor risus. Nulla porttitor accumsan tincidunt. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus suscipit tortor eget felis porttitor volutpat.</p>

<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur aliquet quam id dui posuere blandit. Proin eget tortor risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec sollicitudin molestie malesuada. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Donec sollicitudin molestie malesuada.</p>

<p>Donec sollicitudin molestie malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Nulla quis lorem ut libero malesuada feugiat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Donec sollicitudin molestie malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porttitor accumsan tincidunt. Nulla porttitor accumsan tincidunt. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.</p>

<p>Cras ultricies ligula sed magna dictum porta. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Pellentesque in ipsum id orci porta dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Proin eget tortor risus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Donec rutrum congue leo eget malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur aliquet quam id dui posuere blandit.</p>

<p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Pellentesque in ipsum id orci porta dapibus. Vivamus suscipit tortor eget felis porttitor volutpat. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Sed porttitor lectus nibh. Nulla quis lorem ut libero malesuada feugiat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Sed porttitor lectus nibh.</p>

<p>Proin eget tortor risus. Vivamus suscipit tortor eget felis porttitor volutpat. Proin eget tortor risus. Nulla porttitor accumsan tincidunt. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus suscipit tortor eget felis porttitor volutpat.</p>

<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur aliquet quam id dui posuere blandit. Proin eget tortor risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec sollicitudin molestie malesuada. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Donec sollicitudin molestie malesuada.</p>

<p>Donec sollicitudin molestie malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Nulla quis lorem ut libero malesuada feugiat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Donec sollicitudin molestie malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porttitor accumsan tincidunt. Nulla porttitor accumsan tincidunt. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.</p>

<p>Cras ultricies ligula sed magna dictum porta. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Pellentesque in ipsum id orci porta dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Proin eget tortor risus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Donec rutrum congue leo eget malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur aliquet quam id dui posuere blandit.</p>

<p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Pellentesque in ipsum id orci porta dapibus. Vivamus suscipit tortor eget felis porttitor volutpat. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Sed porttitor lectus nibh. Nulla quis lorem ut libero malesuada feugiat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Sed porttitor lectus nibh.</p>

<p>Proin eget tortor risus. Vivamus suscipit tortor eget felis porttitor volutpat. Proin eget tortor risus. Nulla porttitor accumsan tincidunt. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus suscipit tortor eget felis porttitor volutpat.</p>

<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur aliquet quam id dui posuere blandit. Proin eget tortor risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec sollicitudin molestie malesuada. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Donec sollicitudin molestie malesuada.</p>

<p>Donec sollicitudin molestie malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Nulla quis lorem ut libero malesuada feugiat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Donec sollicitudin molestie malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porttitor accumsan tincidunt. Nulla porttitor accumsan tincidunt. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.</p>

<p>Cras ultricies ligula sed magna dictum porta. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Pellentesque in ipsum id orci porta dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Proin eget tortor risus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Donec rutrum congue leo eget malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur aliquet quam id dui posuere blandit.</p>

<p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Pellentesque in ipsum id orci porta dapibus. Vivamus suscipit tortor eget felis porttitor volutpat. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Sed porttitor lectus nibh. Nulla quis lorem ut libero malesuada feugiat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Sed porttitor lectus nibh.</p>

<p>Proin eget tortor risus. Vivamus suscipit tortor eget felis porttitor volutpat. Proin eget tortor risus. Nulla porttitor accumsan tincidunt. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus suscipit tortor eget felis porttitor volutpat.</p>

<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur aliquet quam id dui posuere blandit. Proin eget tortor risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec sollicitudin molestie malesuada. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Donec sollicitudin molestie malesuada.</p>

<p>Donec sollicitudin molestie malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Nulla quis lorem ut libero malesuada feugiat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Donec sollicitudin molestie malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porttitor accumsan tincidunt. Nulla porttitor accumsan tincidunt. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.</p>

<p>Cras ultricies ligula sed magna dictum porta. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Pellentesque in ipsum id orci porta dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Proin eget tortor risus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Donec rutrum congue leo eget malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur aliquet quam id dui posuere blandit.</p>

<p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Pellentesque in ipsum id orci porta dapibus. Vivamus suscipit tortor eget felis porttitor volutpat. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Sed porttitor lectus nibh. Nulla quis lorem ut libero malesuada feugiat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Sed porttitor lectus nibh.</p>

<p>Proin eget tortor risus. Vivamus suscipit tortor eget felis porttitor volutpat. Proin eget tortor risus. Nulla porttitor accumsan tincidunt. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus suscipit tortor eget felis porttitor volutpat.</p>

<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur aliquet quam id dui posuere blandit. Proin eget tortor risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec sollicitudin molestie malesuada. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Donec sollicitudin molestie malesuada.</p>

<p>Donec sollicitudin molestie malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Nulla quis lorem ut libero malesuada feugiat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Donec sollicitudin molestie malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porttitor accumsan tincidunt. Nulla porttitor accumsan tincidunt. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.</p>

<p>Cras ultricies ligula sed magna dictum porta. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Pellentesque in ipsum id orci porta dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Proin eget tortor risus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Donec rutrum congue leo eget malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur aliquet quam id dui posuere blandit.</p>

<p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Pellentesque in ipsum id orci porta dapibus. Vivamus suscipit tortor eget felis porttitor volutpat. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Sed porttitor lectus nibh. Nulla quis lorem ut libero malesuada feugiat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Sed porttitor lectus nibh.</p>

<p>Proin eget tortor risus. Vivamus suscipit tortor eget felis porttitor volutpat. Proin eget tortor risus. Nulla porttitor accumsan tincidunt. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus suscipit tortor eget felis porttitor volutpat.</p>

<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur aliquet quam id dui posuere blandit. Proin eget tortor risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec sollicitudin molestie malesuada. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Donec sollicitudin molestie malesuada.</p>

<p>Donec sollicitudin molestie malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Nulla quis lorem ut libero malesuada feugiat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Donec sollicitudin molestie malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porttitor accumsan tincidunt. Nulla porttitor accumsan tincidunt. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.</p>

<p>Cras ultricies ligula sed magna dictum porta. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Proin eget tortor risus. Pellentesque in ipsum id orci porta dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Proin eget tortor risus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Donec rutrum congue leo eget malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur aliquet quam id dui posuere blandit.</p>

<p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Pellentesque in ipsum id orci porta dapibus. Vivamus suscipit tortor eget felis porttitor volutpat. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Sed porttitor lectus nibh. Nulla quis lorem ut libero malesuada feugiat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Sed porttitor lectus nibh.</p>

<p>Proin eget tortor risus. Vivamus suscipit tortor eget felis porttitor volutpat. Proin eget tortor risus. Nulla porttitor accumsan tincidunt. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus suscipit tortor eget felis porttitor volutpat.</p>

<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur aliquet quam id dui posuere blandit. Proin eget tortor risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec sollicitudin molestie malesuada. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Donec sollicitudin molestie malesuada.</p>
', N'fds', N'5aa5d448bd70d9.88538204.jpg', N'1520817233', N'1520187231', N'1')
GO
GO
INSERT INTO [dbo].[fn_news] ([id], [title], [slug], [extract], [content], [author_name], [url_imagen], [updated_at], [created_at], [published]) VALUES (N'10', N'La nocticia 2', N'la-nocticia-2', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Proin eget tortor risus. Donec rutrum congue leo eget malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae', N'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Proin eget tortor risus. Donec rutrum congue leo eget malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae&nbsp;</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Proin eget tortor risus. Donec rutrum congue leo eget malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae</p>

<p>&nbsp;</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Proin eget tortor risus. Donec rutrum congue leo eget malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Proin eget tortor risus. Donec rutrum congue leo eget malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae</p>
', N'sdfs', N'5aa7fb89d2d501.79555889.png', N'1520958345', N'1520814010', N'1')
GO
GO
INSERT INTO [dbo].[fn_news] ([id], [title], [slug], [extract], [content], [author_name], [url_imagen], [updated_at], [created_at], [published]) VALUES (N'11', N'Pinche noticia', N'pinche-noticia', N'Pinche noticia', N'<p>Pinche noticia</p>
', N'Pinche noticia', N'5aa5d725327209.25289352.jpg', N'1520817957', N'1520817957', N'0')
GO
GO
SET IDENTITY_INSERT [dbo].[fn_news] OFF
GO

-- ----------------------------
-- Table structure for fn_profile
-- ----------------------------
DROP TABLE [dbo].[fn_profile]
GO
CREATE TABLE [dbo].[fn_profile] (
[user_id] int NOT NULL ,
[name] nvarchar(255) NULL DEFAULT NULL ,
[public_email] nvarchar(255) NULL DEFAULT NULL ,
[gravatar_email] nvarchar(255) NULL DEFAULT NULL ,
[gravatar_id] nvarchar(32) NULL DEFAULT NULL ,
[location] nvarchar(255) NULL DEFAULT NULL ,
[website] nvarchar(255) NULL DEFAULT NULL ,
[bio] nvarchar(MAX) NULL DEFAULT NULL ,
[timezone] nvarchar(40) NULL 
)


GO

-- ----------------------------
-- Records of fn_profile
-- ----------------------------
INSERT INTO [dbo].[fn_profile] ([user_id], [name], [public_email], [gravatar_email], [gravatar_id], [location], [website], [bio], [timezone]) VALUES (N'12', null, null, null, null, null, null, null, null)
GO
GO
INSERT INTO [dbo].[fn_profile] ([user_id], [name], [public_email], [gravatar_email], [gravatar_id], [location], [website], [bio], [timezone]) VALUES (N'13', N'Admin', N'info@fonabe.go.cr', N'', N'd41d8cd98f00b204e9800998ecf8427e', N'CR', N'http://www.fonabe.go.cr', N'', N'America/Costa_Rica')
GO
GO

-- ----------------------------
-- Table structure for fn_setting
-- ----------------------------
DROP TABLE [dbo].[fn_setting]
GO
CREATE TABLE [dbo].[fn_setting] (
[id] int NOT NULL IDENTITY(1,1) ,
[type] nvarchar(10) NOT NULL ,
[section] nvarchar(255) NOT NULL ,
[key] nvarchar(255) NOT NULL ,
[value] nvarchar(MAX) NOT NULL ,
[status] smallint NOT NULL DEFAULT ((1)) ,
[description] nvarchar(255) NULL DEFAULT NULL ,
[created_at] int NOT NULL ,
[updated_at] int NOT NULL 
)


GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_setting', 
'COLUMN', N'id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Id Settings'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Id Settings'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'id'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_setting', 
'COLUMN', N'type')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Tipo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'type'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Tipo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'type'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_setting', 
'COLUMN', N'section')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Seccion'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'section'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Seccion'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'section'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_setting', 
'COLUMN', N'key')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Llave elemento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'key'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Llave elemento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'key'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_setting', 
'COLUMN', N'value')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Valor elemento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'value'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Valor elemento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'value'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_setting', 
'COLUMN', N'status')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Estado'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'status'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Estado'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'status'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_setting', 
'COLUMN', N'description')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Descripción'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'description'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Descripción'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'description'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_setting', 
'COLUMN', N'created_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'created_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'created_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_setting', 
'COLUMN', N'updated_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'updated_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'updated_at'
GO

-- ----------------------------
-- Records of fn_setting
-- ----------------------------
SET IDENTITY_INSERT [dbo].[fn_setting] ON
GO
INSERT INTO [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (N'1', N'string', N'main', N'admin_url', N'http://admin.igd.local', N'1', N'Url general de Administración', N'1519994056', N'1520371184')
GO
GO
INSERT INTO [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (N'2', N'string', N'main', N'correo_institucional_url', N'https://mail.fonabe.go.cr/owa/auth/logon.aspx?replaceCurrent=1&url=https%3a%2f%2fmail.fonabe.go.cr%2fowa', N'1', N'Url de correo institucional', N'1519994378', N'1520371184')
GO
GO
INSERT INTO [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (N'3', N'string', N'main', N'site_main_title', N'Fondo Nacional de Becas', N'1', N'titulo general del sitio', N'1519997798', N'1519997798')
GO
GO
INSERT INTO [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (N'4', N'string', N'main', N'facebook_url', N'https://www.facebook.com/fonabe', N'1', N'Url de facebook', N'1520106143', N'1520106143')
GO
GO
INSERT INTO [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (N'5', N'string', N'main', N'twitter_url', N'https://twitter.com/fonabe', N'1', N'Url de Twitter', N'1520106182', N'1520106182')
GO
GO
INSERT INTO [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (N'6', N'string', N'main', N'copy_string', N'Derechos reservados © FONABE 2018', N'1', N'Texto Derechos Reservados', N'1520106345', N'1520373431')
GO
GO
INSERT INTO [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (N'7', N'string', N'index', N'url_link1', N'#fonabe', N'1', N'Url del link 1 en página principal', N'1520284713', N'1520970693')
GO
GO
INSERT INTO [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (N'8', N'string', N'index', N'url_link2', N'#tarjetas_prepago', N'1', N'Url del link 2 en página principal', N'1520288683', N'1520365377')
GO
GO
INSERT INTO [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (N'9', N'string', N'index', N'url_link3', N'#reservar_cita', N'1', N'Url del link 3 en página principal', N'1520288712', N'1520365379')
GO
GO
INSERT INTO [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (N'10', N'string', N'main', N'frontend_url', N'http://igd.local', N'1', N'URL del frontend', N'1520789093', N'1520789302')
GO
GO
INSERT INTO [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (N'11', N'string', N'data', N'link_url_1', N'#', N'1', N'Link 1 de Datos abiertos', N'1520790296', N'1520790296')
GO
GO
INSERT INTO [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (N'12', N'string', N'data', N'link_url_2', N'#', N'1', N'Link Datos abiertos No 2', N'1520790328', N'1520790328')
GO
GO
INSERT INTO [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (N'13', N'string', N'data', N'link_url_3', N'#', N'1', N'', N'1520790344', N'1520790344')
GO
GO
INSERT INTO [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (N'14', N'string', N'data', N'link_label_1', N'Logros', N'1', N'', N'1520790382', N'1520790382')
GO
GO
INSERT INTO [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (N'15', N'string', N'data', N'link_label_2', N'Financiero', N'1', N'', N'1520790400', N'1520790400')
GO
GO
INSERT INTO [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (N'16', N'string', N'data', N'link_label_3', N'Demografía', N'1', N'', N'1520790425', N'1520790425')
GO
GO
INSERT INTO [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (N'17', N'string', N'index', N'link_url_1', N'#', N'1', null, N'1520791175', N'1520791175')
GO
GO
INSERT INTO [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (N'18', N'string', N'index', N'link_url_2', N'#', N'1', null, N'1520791175', N'1520791175')
GO
GO
INSERT INTO [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (N'19', N'string', N'index', N'link_url_3', N'#', N'1', null, N'1520791175', N'1520791175')
GO
GO
INSERT INTO [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (N'20', N'string', N'index', N'link_label_1', N'Logros', N'1', null, N'1520791175', N'1520791279')
GO
GO
INSERT INTO [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (N'21', N'string', N'index', N'link_label_2', N'Financiero', N'1', null, N'1520791175', N'1520791279')
GO
GO
INSERT INTO [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (N'22', N'string', N'index', N'link_label_3', N'Demografía', N'1', null, N'1520791175', N'1520791279')
GO
GO
INSERT INTO [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (N'23', N'string', N'mail', N'email', N'consultas@fonabe.go.cr', N'1', N'Correo institucional para envío de solicitudes y mensajes', N'1520958636', N'1520958792')
GO
GO
INSERT INTO [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (N'1002', N'string', N'index', N'link1_label', N'FONABE INFORMA', N'1', N'Label 1 para página inicial', N'1520365135', N'1520970693')
GO
GO
INSERT INTO [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (N'1003', N'string', N'index', N'link2_label', N'Tarjetas prepago', N'1', N'Label 2 para página inicial', N'1520365165', N'1520365253')
GO
GO
INSERT INTO [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (N'1004', N'string', N'index', N'link3_label', N'Reservar cita', N'1', N'Label 3 para página inicial', N'1520365250', N'1520365250')
GO
GO
SET IDENTITY_INSERT [dbo].[fn_setting] OFF
GO

-- ----------------------------
-- Table structure for fn_slider_images
-- ----------------------------
DROP TABLE [dbo].[fn_slider_images]
GO
CREATE TABLE [dbo].[fn_slider_images] (
[id] int NOT NULL IDENTITY(1,1) ,
[image_url] varchar(500) NOT NULL ,
[image_link] varchar(500) NOT NULL ,
[slide_text] varchar(500) NOT NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[fn_slider_images]', RESEED, 5)
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_slider_images', 
'COLUMN', N'id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Id Slide'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_slider_images'
, @level2type = 'COLUMN', @level2name = N'id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Id Slide'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_slider_images'
, @level2type = 'COLUMN', @level2name = N'id'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_slider_images', 
'COLUMN', N'image_url')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Url imagen del slide'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_slider_images'
, @level2type = 'COLUMN', @level2name = N'image_url'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Url imagen del slide'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_slider_images'
, @level2type = 'COLUMN', @level2name = N'image_url'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_slider_images', 
'COLUMN', N'image_link')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Link del slide'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_slider_images'
, @level2type = 'COLUMN', @level2name = N'image_link'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Link del slide'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_slider_images'
, @level2type = 'COLUMN', @level2name = N'image_link'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_slider_images', 
'COLUMN', N'slide_text')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Texto del slide'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_slider_images'
, @level2type = 'COLUMN', @level2name = N'slide_text'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Texto del slide'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_slider_images'
, @level2type = 'COLUMN', @level2name = N'slide_text'
GO

-- ----------------------------
-- Records of fn_slider_images
-- ----------------------------
SET IDENTITY_INSERT [dbo].[fn_slider_images] ON
GO
INSERT INTO [dbo].[fn_slider_images] ([id], [image_url], [image_link], [slide_text]) VALUES (N'1', N'5aa833b8422c55.23012784.jpg', N'#', N'IMG 1')
GO
GO
INSERT INTO [dbo].[fn_slider_images] ([id], [image_url], [image_link], [slide_text]) VALUES (N'2', N'5aa833c3503a55.48098869.jpg', N'#', N'IMG 2')
GO
GO
INSERT INTO [dbo].[fn_slider_images] ([id], [image_url], [image_link], [slide_text]) VALUES (N'3', N'5aa833ce46a429.10000733.jpg', N'#', N'IMG 3')
GO
GO
INSERT INTO [dbo].[fn_slider_images] ([id], [image_url], [image_link], [slide_text]) VALUES (N'4', N'5aa833dcc11304.89871847.jpg', N'#', N'IMG 4')
GO
GO
INSERT INTO [dbo].[fn_slider_images] ([id], [image_url], [image_link], [slide_text]) VALUES (N'5', N'5aa833e6e9e654.53845801.jpg', N'#', N'IMG 5')
GO
GO
SET IDENTITY_INSERT [dbo].[fn_slider_images] OFF
GO

-- ----------------------------
-- Table structure for fn_social_account
-- ----------------------------
DROP TABLE [dbo].[fn_social_account]
GO
CREATE TABLE [dbo].[fn_social_account] (
[id] int NOT NULL ,
[user_id] int NULL ,
[provider] nvarchar(255) NULL ,
[client_id] nvarchar(255) NULL ,
[data] nvarchar(MAX) NULL ,
[code] nvarchar(32) NULL ,
[created_at] int NULL ,
[email] nvarchar(255) NULL ,
[username] nvarchar(255) NULL 
)


GO

-- ----------------------------
-- Records of fn_social_account
-- ----------------------------

-- ----------------------------
-- Table structure for fn_static_pages
-- ----------------------------
DROP TABLE [dbo].[fn_static_pages]
GO
CREATE TABLE [dbo].[fn_static_pages] (
[id] int NOT NULL IDENTITY(1,1) ,
[slug] varchar(255) NOT NULL ,
[title] varchar(255) NOT NULL ,
[excerpt] varchar(500) NULL ,
[content] nvarchar(MAX) NULL ,
[created_at] int NULL ,
[updated_at] int NULL ,
[category_id] int NULL ,
[editable] bit NULL DEFAULT ((1)) ,
[icon] varchar(32) NULL ,
[order] int NOT NULL DEFAULT ((0)) ,
[has_md_documents] bit NOT NULL DEFAULT ((0)) ,
[has_md_video] bit NOT NULL DEFAULT ((0)) ,
[has_md_info] bit NOT NULL DEFAULT ((0)) 
)


GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Id'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Id'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'id'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'slug')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Slug para URL limpia'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'slug'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Slug para URL limpia'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'slug'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'title')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Título de la página'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'title'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Título de la página'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'title'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'excerpt')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Resúmen del contenido'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'excerpt'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Resúmen del contenido'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'excerpt'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'content')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Contenido de la página'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'content'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Contenido de la página'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'content'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'created_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'created_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'created_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'updated_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha de actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'updated_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'updated_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'category_id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Id Categoría'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'category_id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Id Categoría'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'category_id'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'editable')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Es editable?'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'editable'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Es editable?'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'editable'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'icon')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Icono de menú'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'icon'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Icono de menú'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'icon'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'order')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Orden de las páginas'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'order'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Orden de las páginas'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'order'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'has_md_documents')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Tiene maestro/detalle de documentos?'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'has_md_documents'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Tiene maestro/detalle de documentos?'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'has_md_documents'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'has_md_video')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Tienes maestro/detalle de videos?'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'has_md_video'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Tienes maestro/detalle de videos?'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'has_md_video'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'has_md_info')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Tienes maestro/detalle de información?'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'has_md_info'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Tienes maestro/detalle de información?'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'has_md_info'
GO

-- ----------------------------
-- Records of fn_static_pages
-- ----------------------------
SET IDENTITY_INSERT [dbo].[fn_static_pages] ON
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'1', N'centros-educativos', N'Centros Educativos', N'', N'<h2>TITULO</h2>

<p>ipsum&nbsp;&nbsp;<strong>Quisque </strong>velit nisi, <s>pretium ut lacinia</s> in, elementum id enim. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Pellentesque in ipsum id orci porta dapibus. Nulla porttitor accumsan tincidunt. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.</p>
', N'1519918469', N'1519918469', N'1', N'0', N'icon-g-centros', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'2', N'convocatorias', N'Convocatorias', N'', N'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ultrices vitae orci eget sagittis. Quisque rhoncus sodales diam ut ullamcorper. Suspendisse a sagittis lacus. Fusce dictum in enim vel faucibus. Curabitur ultrices nulla quis hendrerit congue. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis id tincidunt erat, quis sodales eros. Nulla eu finibus nunc, quis dapibus tellus.</p>
', N'1519918469', N'1519918469', N'1', N'0', N'icon-g-calendario-fechas', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'3', N'tramites', N'Trámites', N'', N'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ultrices vitae orci eget sagittis. Quisque rhoncus sodales diam ut ullamcorper. Suspendisse a sagittis lacus. Fusce dictum in enim vel faucibus. Curabitur ultrices nulla quis hendrerit congue. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis id tincidunt erat, quis sodales eros. Nulla eu finibus nunc, quis dapibus tellus.</p>
', N'1519918469', N'1519918469', N'1', N'0', N'icon-g-transaccion', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'4', N'informacion', N'Información', N'', N'<p>asd</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec faucibus accumsan efficitur. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean dui ex, gravida eget vestibulum eu, mattis eget elit. Pellentesque id venenatis metus, nec euismod orci. Suspendisse elementum augue sit amet eleifend tristique. Vestibulum venenatis turpis finibus fringilla elementum. Morbi ex justo, laoreet in purus quis, eleifend vehicula erat. Etiam vel turpis at tortor viverra imperdiet eu nec leo. Donec tincidunt interdum ipsum at elementum. Etiam porta nisl sagittis velit cursus fermentum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent quam libero, aliquet quis sapien vitae, elementum scelerisque lacus.</p>

<h2>Test</h2>

<p>Nulla convallis maximus enim, nec convallis lorem porta vitae. Quisque tincidunt ex sed magna viverra, eu suscipit ipsum gravida. Vestibulum ac ipsum vestibulum, iaculis purus nec, auctor arcu. Cras sodales sodales magna ac pretium. Donec nec bibendum elit. In a arcu massa. Vestibulum volutpat urna id arcu lacinia consectetur. In risus quam, mattis in faucibus at, sodales eget mi.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'1', N'0', N'icon-g-informacion', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'5', N'tipos-de-becas', N'Tipos de becas', N'', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit.<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec faucibus accumsan efficitur. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean dui ex, gravida eget vestibulum eu, mattis eget elit. Pellentesque id venenatis metus, nec euismod orci. Suspendisse elementum augue sit amet eleifend tristique. Vestibulum venenatis turpis finibus fringilla elementum. Morbi ex justo, laoreet in purus quis, eleifend vehicula erat. Etiam vel turpis at tortor viverra imperdiet eu nec leo. Donec tincidunt interdum ipsum at elementum. Etiam porta nisl sagittis velit cursus fermentum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent quam libero, aliquet quis sapien vitae, elementum scelerisque lacus.</p>

<h2>Test</h2>

<p>Nulla convallis maximus enim, nec convallis lorem porta vitae. Quisque tincidunt ex sed magna viverra, eu suscipit ipsum gravida. Vestibulum ac ipsum vestibulum, iaculis purus nec, auctor arcu. Cras sodales sodales magna ac pretium. Donec nec bibendum elit. In a arcu massa. Vestibulum volutpat urna id arcu lacinia consectetur. In risus quam, mattis in faucibus at, sodales eget mi.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'1', N'0', N'icon-g-tipo', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'6', N'circulares', N'Circulares', N'', N'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'1', N'0', N'icon-g-estado-financiero', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'7', N'tutoriales', N'Tutoriales', N'', N'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'1', N'0', N'icon-g-videos', N'0', N'0', N'1', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'8', N'mapa-del-sitio', N'Mapa del sitio', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris lobortis, sem in posuere scelerisque, turpis nisl sollicitudin nisi, eget venenatis ex enim in orci. Nulla quis dictum ligula. Duis luctus arcu vitae finibus rutrum. ', N'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris lobortis, sem in posuere scelerisque, turpis nisl sollicitudin nisi, eget venenatis ex enim in orci. Nulla quis dictum ligula. Duis luctus arcu vitae finibus rutrum. Curabitur at turpis non enim tempor lacinia. Ut in augue at ipsum laoreet molestie. Morbi suscipit elementum velit. Praesent nibh sem, tristique sit amet vulputate eget, facilisis nec ex. Cras congue consectetur justo vitae convallis. Maecenas varius fringilla nisl ut hendrerit. Nam tincidunt erat sed est pretium dictum.</p>
', N'1519918469', N'1519918469', N'8', N'0', N'icon-g-centros', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'9', N'privacidad', N'Privacidad', N'', N'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ultrices vitae orci eget sagittis. Quisque rhoncus sodales diam ut ullamcorper. Suspendisse a sagittis lacus. Fusce dictum in enim vel faucibus. Curabitur ultrices nulla quis hendrerit congue. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis id tincidunt erat, quis sodales eros. Nulla eu finibus nunc, quis dapibus tellus.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'8', N'0', N'icon-g-centros', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'10', N'sitios-de-interes', N'Sitios de Interes', N'', N'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ultrices vitae orci eget sagittis. Quisque rhoncus sodales diam ut ullamcorper. Suspendisse a sagittis lacus. Fusce dictum in enim vel faucibus. Curabitur ultrices nulla quis hendrerit congue. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis id tincidunt erat, quis sodales eros. Nulla eu finibus nunc, quis dapibus tellus.</p>

<p>Morbi ut sagittis massa, eu tristique augue. Donec faucibus nulla vitae urna imperdiet tincidunt. Nullam tellus enim, mattis quis lobortis eu, vestibulum id augue. Nulla interdum tempus ullamcorper. Praesent vel aliquet urna. Nunc elementum quam ac egestas blandit. Suspendisse potenti. Aliquam quis eleifend libero. Donec molestie nisi et lobortis consequat.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'8', N'0', N'icon-g-centros', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'11', N'preguntas-frecuentes', N'Preguntas frecuentes', N'', N'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ultrices vitae orci eget sagittis. Quisque rhoncus sodales diam ut ullamcorper. Suspendisse a sagittis lacus. Fusce dictum in enim vel faucibus. Curabitur ultrices nulla quis hendrerit congue. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis id tincidunt erat, quis sodales eros. Nulla eu finibus nunc, quis dapibus tellus.</p>

<p>Morbi ut sagittis massa, eu tristique augue. Donec faucibus nulla vitae urna imperdiet tincidunt. Nullam tellus enim, mattis quis lobortis eu, vestibulum id augue. Nulla interdum tempus ullamcorper. Praesent vel aliquet urna. Nunc elementum quam ac egestas blandit. Suspendisse potenti. Aliquam quis eleifend libero. Donec molestie nisi et lobortis consequat.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'8', N'0', N'icon-g-centros', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'12', N'circulares', N'Circulares', N'circulares', N'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ultrices vitae orci eget sagittis. Quisque rhoncus sodales diam ut ullamcorper. Suspendisse a sagittis lacus. Fusce dictum in enim vel faucibus. Curabitur ultrices nulla quis hendrerit congue. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis id tincidunt erat, quis sodales eros. Nulla eu finibus nunc, quis dapibus tellus.</p>

<p>Morbi ut sagittis massa, eu tristique augue. Donec faucibus nulla vitae urna imperdiet tincidunt. Nullam tellus enim, mattis quis lobortis eu, vestibulum id augue. Nulla interdum tempus ullamcorper. Praesent vel aliquet urna. Nunc elementum quam ac egestas blandit. Suspendisse potenti. Aliquam quis eleifend libero. Donec molestie nisi et lobortis consequat.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ultrices vitae orci eget sagittis. Quisque rhoncus sodales diam ut ullamcorper. Suspendisse a sagittis lacus. Fusce dictum in enim vel faucibus. Curabitur ultrices nulla quis hendrerit congue. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis id tincidunt erat, quis sodales eros. Nulla eu finibus nunc, quis dapibus tellus.</p>

<p>Morbi ut sagittis massa, eu tristique augue. Donec faucibus nulla vitae urna imperdiet tincidunt. Nullam tellus enim, mattis quis lobortis eu, vestibulum id augue. Nulla interdum tempus ullamcorper. Praesent vel aliquet urna. Nunc elementum quam ac egestas blandit. Suspendisse potenti. Aliquam quis eleifend libero. Donec molestie nisi et lobortis consequat.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'2', N'0', N'icon-g-centros', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'13', N'tutoriales', N'Tutoriales', N'tutoriales', N'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'2', N'0', N'icon-g-centros', N'0', N'0', N'1', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'14', N'tramites', N'Tramites ', N'tramites', N'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'2', N'0', N'icon-g-centros', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'15', N'becas-universitarias', N'Becas Universitarias', N'Becas Universitarias', N'Becas Universitarias <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'2', N'0', N'icon-g-centros', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'16', N'solicitud-de-beca', N'Solicitud de Beca', N'Solicitud de Beca', N'Solicitud de Beca <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'2', N'0', N'icon-g-centros', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'17', N'estado-solicitud', N'Estado Solicitud', N'Estado Solicitud', N'Estado Solicitud <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'2', N'0', N'icon-g-centros', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'18', N'circulares', N'Circulares', N'Circulares', N'Circulares <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'3', N'0', N'icon-g-centros', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'19', N'tutoriales', N'Tutoriales', N'Tutoriales', N'Tutoriales <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'3', N'0', N'icon-g-centros', N'0', N'0', N'1', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'20', N'tramites', N'Tramites', N'Tramites', N'Tramites <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'3', N'0', N'icon-g-centros', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'21', N'becas-universitarias', N'Becas Universitarias', N'Becas Universitarias', N'Becas Universitarias <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'3', N'0', N'icon-g-centros', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'22', N'solicitud-de-beca', N'Solicitud de beca', N'Solicitud de beca', N'Solicitud de beca <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'3', N'0', N'icon-g-centros', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'23', N'estado-de-solicitd', N'Estado de solicitd', N'Estado de solicitd', N'Estado de solicitd <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'3', N'0', N'icon-g-centros', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'24', N'noticias', N'Noticias', N'Noticias del Fondo Nacional de Becas', N'', N'1519918469', N'1519918469', N'4', N'0', N'icon-g-centros', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'25', N'normativa', N'Normativa', N'Normativa', N'Normativa <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'5', N'0', N'icon-g-centros', N'0', N'1', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'26', N'gestion-de-rrhh', N'Gestión de RRHH', N'Gestión de RRHH', N'Gestión de RRHH <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'5', N'0', N'icon-g-centros', N'0', N'1', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'27', N'contraloria-de-servicios', N'Contraloria de servicios', N'Contraloria de servicios', N'Contraloria de servicios <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'5', N'0', N'icon-g-centros', N'0', N'1', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'28', N'contratacion-administrativa', N'Contratación administrativa', N'Contratación administrativa', N'Contratación administrativa <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'5', N'0', N'icon-g-centros', N'0', N'1', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'29', N'informes', N'Informes', N'Informes', N'Informes <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'5', N'0', N'icon-g-centros', N'0', N'1', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'30', N'presupuestos', N'Presupuestos', N'Presupuestos', N'Presupuestos <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'5', N'0', N'icon-g-centros', N'0', N'1', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'31', N'estados-financieros', N'Estados Financieros', N'Estados Financieros', N'Estados Financieros <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'5', N'0', N'icon-g-centros', N'0', N'1', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'32', N'liquidaciones', N'Liquidaciones', N'Liquidaciones', N'Liquidaciones<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'5', N'0', N'icon-g-centros', N'0', N'1', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'33', N'estadisticas', N'Estadisticas', N'Estadisticas', N'Estadisticas <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'5', N'0', N'icon-g-centros', N'0', N'1', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'34', N'archivo-institucional', N'Archivo Institucional', N'Archivo Institucional', N'Archivo Institucional <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'5', N'0', N'icon-g-centros', N'0', N'1', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'35', N'junta-directiva', N'Junta Directiva', N'Junta Directiva', N'Junta Directiva <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'6', N'0', N'icon-g-centros', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'36', N'calendario', N'Calendario', N'Calendario', N'', N'1519918469', N'1519918469', N'6', N'0', N'icon-g-centros', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'37', N'multimedia', N'Multimedia', N'Multimedia', N'Multimedia <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'6', N'0', N'icon-g-centros', N'0', N'0', N'1', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'38', N'historia', N'Historia', N'Historia', N'Historia<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'6', N'0', N'icon-g-centros', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'39', N'gente-comprometida', N'Gente Comprometida', N'Gente Comprometida', N'Gente Comprometida<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'6', N'0', N'icon-g-centros', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'40', N'mision-vision', N'Mision/Visión', N'Mision/Visión', N'Mision/Visión<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'6', N'0', N'icon-g-centros', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'41', N'organigrama', N'Organigrama', N'Organigrama', N'<p><img alt="Organigrama" src="http://igd.local/images/organigrama.png" style="height:496px; width:874px" /></p>
', N'1519918469', N'1519918469', N'6', N'0', N'icon-g-centros', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'42', N'contacto', N'Contacto', N'Contacto', N'      <p>Para mayor información, comuníquese con nosotros por los siguientes medios:</p>
      <h6>Teléfono: <span>2521-6322</span></h6>
      <h6>Fax: <span>Fax: 2221-5448</span></h6>
      <h6>Localización <span>De Acueductos y Alcantarillados, Paseo de los Estudiantes, 200mts Este y 50mts Sur edificio Azúl, Av 10 y 12, Calle 13.</span></h6>
      <h6>Correo institucional: <span>consultas@fonabe.go.cr</span></h6>
      <h6>Horario de Atención: <span>De lunes a Viernes, 7:00 am a 3:00 pm, Jornada Continua.</span></h6>
      <a href="/pagina/direcciones-funcionarios"  class="btn btn-primary">Direcciones Electrónicas Funcionarios</a> 
', N'1519918469', N'1519918469', N'7', N'0', N'icon-g-centros', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'43', N'consultas', N'Consultas', N'Consultas', N'Consultas<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur commodo lorem arcu, sed euismod ante rutrum et. Duis ligula orci, mattis id arcu vel, viverra imperdiet felis. Aenean vitae odio quis massa volutpat feugiat at id ante. Pellentesque ac ullamcorper ex. Integer blandit, arcu in lobortis imperdiet, felis felis fermentum elit, sed fermentum nibh magna et nibh. Aliquam gravida porta pretium. Vestibulum egestas diam massa. Proin tincidunt odio non pharetra bibendum. Suspendisse auctor mauris nec iaculis scelerisque. Cras nec placerat metus. Vivamus pellentesque nisi vitae congue accumsan. Suspendisse ullamcorper lacus purus. Nullam a efficitur nisi. Mauris iaculis at erat cursus ullamcorper. Nullam in imperdiet augue, non volutpat ante. Cras tempor sit amet diam nec pharetra.</p>

<p>Sed malesuada aliquam justo at imperdiet. Quisque ut ornare sapien, sed vestibulum dui. Sed erat nisi, pretium at posuere eget, dignissim vitae eros. Sed eget pellentesque nulla. Vestibulum rhoncus, enim ac finibus semper, lacus metus sodales ligula, et volutpat tortor justo quis lorem. Vestibulum lacinia nec erat eu ullamcorper. Integer blandit sagittis tellus in molestie. Vestibulum mi metus, ultrices at mi eget, tristique iaculis dui.</p>', N'1519918469', N'1519918469', N'7', N'0', N'icon-g-centros', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'44', N'denuncias', N'Denuncias', N'Denuncias', N'<p>Quisque velit nisi, pretium ut lacinia in, elementum id enim. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Pellentesque in ipsum id orci porta dapibus. Nulla porttitor accumsan tincidunt. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.</p>
', N'1519918469', N'1519918469', N'7', N'0', N'icon-g-centros', N'0', N'0', N'0', N'0')
GO
GO
INSERT INTO [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (N'45', N'direcciones-funcionarios', N'Direcciones Electrónicas Funcionarios', N'direcciones electrónicas', N'<div style="column-count: 3;">
<p><strong><em>Secretaria</em></strong></p>

<p>Zeneida Mej&iacute;as,&nbsp;<a href="mailto:zmejias@fonabe.go.cr">zmejias@fonabe.go.cr</a></p>

<p><strong><em>Auditor&iacute;a Interna</em></strong></p>

<p>Roxana Rodr&iacute;guez.&nbsp;<a href="mailto:rrodriguez@fonabe.go.cr">rrodriguez@fonabe.go.cr</a></p>

<p><strong><em>Asistente de Auditor&iacute;a Interna</em></strong></p>

<p>Helin Piedra.&nbsp;<a href="mailto:hpiedra@fonabe.go.cr">hpiedra@fonabe.go.cr</a></p>

<p><strong>Direcci&oacute;n&nbsp;Ejecutiva</strong></p>

<p><strong><em>Director Ejecutivo</em></strong></p>

<p>Mauricio Donato,&nbsp;<a href="mailto:mdonato@fonabe.go.cr">mdonato@fonabe.go.cr</a></p>

<p><strong><em>Secretaria</em></strong></p>

<p>Adriana Navarro,&nbsp;<a href="mailto:anavarro@fonabe.go.cr">anavarro@fonabe.go.cr</a></p>

<p><strong><em>Asesor&iacute;a Legal</em></strong></p>

<p>Ana Mar&iacute;a Rivera,&nbsp;<a href="mailto:arivera@fonabe.go.cr">arivera@fonabe.go.cr</a></p>

<p><strong><em>Asistente de Asesor&iacute;a Legal</em></strong></p>

<p>Mar&iacute;a Fernanda Barquero,&nbsp;<a href="mailto:mbarquero@fonabe.go.cr">mbarquero@fonabe.go.cr</a></p>

<p>Natalia Castro,&nbsp;<a href="mailto:ncastro@fonabe.go.cr">ncastro@fonabe.go.cr</a></p>

<p><strong><em>Contralor&iacute;a de Servicios</em></strong></p>

<p>Orlando Miranda,&nbsp;<a href="mailto:omiranda@fonabe.go.cr">omiranda@fonabe.go.cr</a></p>

<p><strong><em>Jefe de&nbsp;&nbsp;Planificaci&oacute;n</em></strong>&nbsp;</p>

<p>Laura Mena,&nbsp;<a href="mailto:lmena@fonabe.go.cr">lmena@fonabe.go.cr</a></p>

<p><strong><em>Asistente de&nbsp;Planificaci&oacute;n</em></strong>&nbsp;</p>

<p>Gabriela Barahona,&nbsp;<a href="mailto:gbarahona@fonabe.go.cr">gbarahona@fonabe.go.cr</a></p>

<p>Francisco Vindas,&nbsp;<a href="mailto:fvindasc@fonabe.go.cr">fvindasc@fonabe.go.cr</a></p>

<p>Sof&iacute;a Bola&ntilde;os,&nbsp;<a href="mailto:sbolanos@fonabe.go.cr">sbolanos@fonabe.go.cr</a></p>

<p><strong>Direcci&oacute;n&nbsp;de Gesti&oacute;n de Becas</strong></p>

<p>Vanessa Ram&iacute;rez,&nbsp;<a href="mailto:vramirez@fonabe.go.cr">vramirez@fonabe.go.cr</a></p>

<p>Merl&iacute;n Mena,&nbsp;<a href="mailto:mmena@fonabe.go.cr">mmena@fonabe.go.cr</a></p>

<p>Adriana Le&oacute;n Sorio,&nbsp;<a href="mailto:aleon@fonabe.go.cr">aleon@fonabe.go.cr</a></p>

<p><em><strong>Jefe de Atenci&oacute;n al Becario</strong></em></p>

<p>Jansen Coto,&nbsp;<a href="mailto:jcoto@fonabe.go.cr">jcoto@fonabe.go.cr</a></p>

<p><strong><em>Asistentes de Atenci&oacute;n al Becario</em></strong></p>

<p>Gabriela Garc&iacute;a,&nbsp;<a href="mailto:ggarcia@fonabe.go.cr">ggarcia@fonabe.go.cr</a></p>

<p>Miriam Sequeira,&nbsp;<a href="mailto:msequeira@fonabe.go.cr">msequeira@fonabe.go.cr</a></p>

<p>Oscar Sanabria,&nbsp;<a href="mailto:osanabria@fonabe.go.cr">osanabria@fonabe.go.cr</a></p>

<p>Jacqueline Brenes&nbsp;<a href="mailto:jbrenes@fonabe.go.cr">jbrenes@fonabe.go.cr</a></p>

<p>Laura Rodr&iacute;guez,&nbsp;<a href="mailto:lrodriguezg@fonabe.go.cr">lrodriguezg@fonabe.go.cr</a></p>

<p>Eli&eacute;cer Aguilera,&nbsp;<a href="mailto:eaguilera@fonabe.go.cr">eaguilera@fonabe.go.cr</a></p>

<p>Diana Jim&eacute;nez<a href="mailto:djimenez@fonabe.go.cr">djimenez@fonabe.go.cr</a></p>

<p>Isis S&aacute;enz,&nbsp;<a href="mailto:isaenz@fonabe.go.cr">isaenz@fonabe.go.cr</a></p>

<p>Marilyn Alfaro,&nbsp;<a href="mailto:malfaro@fonabe.go.cr.go.cr">malfaro@fonabe.go.cr</a></p>

<p><strong><em>Jefe de Asignaci&oacute;n de Becas</em></strong></p>

<p>Nalda Hay L&oacute;pez,&nbsp;<a href="mailto:%20nhay@fonabe.go.cr">nhay@fonabe.go.cr</a></p>

<p>&nbsp;</p>

<p><strong><em>Equipo de Trabajo Social&nbsp;</em></strong></p>

<p>Adriana Arce,&nbsp;<a href="mailto:adarce@fonabe.go.cr">adarce@fonabe.go.cr</a></p>

<p>Andrea L&oacute;pez,&nbsp;<a href="mailto:alopez@fonabe.go.cr">alopez@fonabe.go.cr</a></p>

<p>Mar&iacute;a Fernanda Retana,&nbsp;<a href="mailto:mretana@fonabe.go.cr">mretana@fonabe.go.cr</a></p>

<p>Carmen Arias&nbsp;<a href="mailto:carias@fonabe.go.cr">carias@fonabe.go.cr</a></p>

<p>Andrea Ch&aacute;vez&nbsp;<a href="mailto:achavez@fonabe.go.cr">achavez@fonabe.go.cr</a></p>

<p>Karen Fern&aacute;ndez&nbsp;<a href="mailto:kfernandez@fonabe.go.cr">kfernandez@fonabe.go.cr</a></p>

<p>Yamileth Guardado&nbsp;<a href="mailto:yguradado@fonabe.go.cr">yguradado@fonabe.go.cr</a></p>

<p>Rossy M&eacute;ndez&nbsp;<a href="mailto:rmendez@fonabe.go.cr">rmendez@fonabe.go.cr</a></p>

<p><strong><em>Asistentes de Asignaci&oacute;n de Becas</em></strong></p>

<p>Grace Zamora,&nbsp;<a href="mailto:gzamora@fonabe.go.cr">gzamora@fonabe.go.cr</a></p>

<p>Maricel Acu&ntilde;a,&nbsp;<a href="mailto:macuna@fonabe.go.cr">macuna@fonabe.go.cr</a></p>

<p>Eugenia Cruz,&nbsp;<a href="mailto:ecruz@fonabe.go.cr">ecruz@fonabe.go.cr</a></p>

<p><strong>Direcci&oacute;n&nbsp;Administrativa - Financiera</strong></p>

<p><strong><em>Director Administrativo Financiero</em></strong></p>

<p>Lucrecia Rodr&iacute;guez,&nbsp;<a href="mailto:lrodriguez@fonabe.go.cr">lrodriguez@fonabe.go.cr</a></p>

<p><strong><em>Jefe de Contabilidad y Tesorer&iacute;a</em></strong></p>

<p>Adriana Velasco,&nbsp;<a href="mailto:avelasco@fonabe.go.cr">avelasco@fonabe.go.cr</a></p>

<p><strong><em>Analista&nbsp;Financiera</em></strong></p>

<p>Andrea G&oacute;mez,&nbsp;<a href="mailto:agomez@fonabe.go.cr">agomez@fonabe.go.cr</a></p>

<p><strong><em>Tesorer&iacute;a, Contabilidad y Presupuesto</em></strong></p>

<p>Mariela Hern&aacute;ndez Sol&iacute;s,&nbsp;<a href="mailto:mhernandez@fonabe.go.cr">mhernandez@fonabe.go.cr</a>&nbsp;</p>

<p>Giselle Dur&aacute;n Oviedo,&nbsp;<a href="mailto:gduran@fonabe.go.cr">gduran@fonabe.go.cr</a>&nbsp;</p>

<p>Ana Karina Fallas,&nbsp;<a href="mailto:afallas@fonabe.go.cr">afallas@fonabe.go.cr</a>&nbsp;</p>

<p><strong><em>Jefe de la Proveedur&iacute;a Institucional y Servicios Generales&nbsp;</em></strong></p>

<p>Luis Barrantes,&nbsp;<a href="mailto:lbarrantes@fonabe.go.cr">lbarrantes@fonabe.go.cr</a>&nbsp;</p>

<p><em><strong>Analista de Contrataciones</strong></em></p>

<p>Olman Bonilla,&nbsp;<a href="mailto:obonilla@fonabe.go.cr">obonilla@fonabe.go.cr</a></p>

<p><em><strong>Auxiliar de Almacenamiento y Distribuci&oacute;n</strong></em></p>

<p>Marta Gonz&aacute;lez,&nbsp;<a href="mailto:mgonzalez@fonabe.go.cr">mgonzalez@fonabe.go.cr</a></p>

<p><em><strong>Servicios Generales&nbsp;</strong></em></p>

<p>V&iacute;ctor Quesada ,&nbsp;<a href="mailto:vquesada@fonabe.go.cr">vquesada@fonabe.go.cr</a></p>

<p><em><strong>Mensajero&nbsp;</strong></em></p>

<p>Edwin Arauz,&nbsp;<a href="mailto:earauz@fonabe.go.cr">earauz@fonabe.go.cr</a></p>

<p><em><strong>Miscel&aacute;nea&nbsp;</strong></em></p>

<p>Sof&iacute;a V&iacute;quez,&nbsp;<a href="mailto:sviquez@fonabe.go.cr">sviquez@fonabe.go.cr</a></p>

<p><strong><em>Jefatura de Gesti&oacute;n Institucional de Recursos Humanos</em></strong></p>

<p>Karla Cubero,&nbsp;<a href="mailto:kcubero@fonabe.go.cr">kcubero@fonabe.go.cr</a></p>

<p><strong><em>Asistentes de Recursos Humanos</em></strong></p>

<p>Francine Cardoso,&nbsp;<a href="mailto:fcardoso@fonabe.go.cr">fcardoso@fonabe.go.cr</a></p>

<p>Irene Morales,&nbsp;<a href="mailto:imorales@fonabe.go.cr">imorales@fonabe.go.cr</a></p>

<p><strong><em>Archivo Institucional</em></strong></p>

<p>Ivania Vindas,&nbsp;<a href="mailto:ivindas@fonabe.go.cr">ivindas@fonabe.go.cr</a></p>

<p><strong>Direcci&oacute;n&nbsp;Desarrollo Tecnol&oacute;gico</strong></p>

<p>Ivannia Badilla,&nbsp;<a href="mailto:ibadilla@fonabe.go.cr">ibadilla@fonabe.go.cr</a></p>

<p><strong><em><em>Jefe de Desarrollo Tecnol&oacute;gico</em></em></strong></p>

<p>Laura Z&uacute;&ntilde;iga,&nbsp;<a href="mailto:lzunigam@fonabe.go.cr">lzunigam@fonabe.go.cr</a></p>

<p><strong><em>Administradores&nbsp;Desarrollo Tecnol&oacute;gico&nbsp;</em></strong></p>

<p>El&iacute;as Mora,&nbsp;<a href="mailto:emora@fonabe.go.cr">emora@fonabe.go.cr</a></p></div>
', N'1519918469', N'1519918469', N'9', N'0', N'icon-g-centros', N'0', N'0', N'0', N'0')
GO
GO
SET IDENTITY_INSERT [dbo].[fn_static_pages] OFF
GO

-- ----------------------------
-- Table structure for fn_token
-- ----------------------------
DROP TABLE [dbo].[fn_token]
GO
CREATE TABLE [dbo].[fn_token] (
[user_id] int NOT NULL ,
[code] nvarchar(32) NOT NULL ,
[created_at] int NULL ,
[type] int NOT NULL 
)


GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_token', 
NULL, NULL)) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Tabla de gestion de tokens de autenticación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_token'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Tabla de gestion de tokens de autenticación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_token'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_token', 
'COLUMN', N'user_id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Id del usuario'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_token'
, @level2type = 'COLUMN', @level2name = N'user_id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Id del usuario'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_token'
, @level2type = 'COLUMN', @level2name = N'user_id'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_token', 
'COLUMN', N'code')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Código'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_token'
, @level2type = 'COLUMN', @level2name = N'code'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Código'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_token'
, @level2type = 'COLUMN', @level2name = N'code'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_token', 
'COLUMN', N'created_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_token'
, @level2type = 'COLUMN', @level2name = N'created_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_token'
, @level2type = 'COLUMN', @level2name = N'created_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_token', 
'COLUMN', N'type')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Tipo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_token'
, @level2type = 'COLUMN', @level2name = N'type'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Tipo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_token'
, @level2type = 'COLUMN', @level2name = N'type'
GO

-- ----------------------------
-- Records of fn_token
-- ----------------------------

-- ----------------------------
-- Table structure for fn_user
-- ----------------------------
DROP TABLE [dbo].[fn_user]
GO
CREATE TABLE [dbo].[fn_user] (
[id] int NOT NULL IDENTITY(1,1) ,
[username] nvarchar(255) NOT NULL ,
[email] nvarchar(255) NOT NULL ,
[password_hash] nvarchar(60) NOT NULL ,
[auth_key] nvarchar(32) NOT NULL ,
[confirmed_at] int NULL DEFAULT NULL ,
[unconfirmed_email] nvarchar(255) NULL DEFAULT NULL ,
[blocked_at] int NULL DEFAULT NULL ,
[created_at] int NOT NULL ,
[updated_at] int NOT NULL ,
[registration_ip] nvarchar(45) NULL ,
[last_login_at] int NULL ,
[flags] int NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[fn_user]', RESEED, 11)
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_user', 
NULL, NULL)) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Tabla de usuarios del sistema'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Tabla de usuarios del sistema'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_user', 
'COLUMN', N'id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Id usuario'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Id usuario'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'id'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_user', 
'COLUMN', N'username')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Nombre de Usuario'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'username'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre de Usuario'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'username'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_user', 
'COLUMN', N'email')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Correo del Usuario'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'email'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Correo del Usuario'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'email'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_user', 
'COLUMN', N'password_hash')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Hash de contraseña'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'password_hash'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Hash de contraseña'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'password_hash'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_user', 
'COLUMN', N'auth_key')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Llave de atorización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'auth_key'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Llave de atorización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'auth_key'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_user', 
'COLUMN', N'confirmed_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha de confirmacion'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'confirmed_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de confirmacion'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'confirmed_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_user', 
'COLUMN', N'unconfirmed_email')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Correo electronico no confirmado'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'unconfirmed_email'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Correo electronico no confirmado'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'unconfirmed_email'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_user', 
'COLUMN', N'blocked_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha de bloqueo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'blocked_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de bloqueo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'blocked_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_user', 
'COLUMN', N'created_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'FEcha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'created_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'FEcha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'created_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_user', 
'COLUMN', N'registration_ip')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'No IP del registro'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'registration_ip'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'No IP del registro'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'registration_ip'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_user', 
'COLUMN', N'last_login_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha de últimop registro'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'last_login_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de últimop registro'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'last_login_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_user', 
'COLUMN', N'flags')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Banderas'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'flags'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Banderas'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'flags'
GO

-- ----------------------------
-- Records of fn_user
-- ----------------------------
SET IDENTITY_INSERT [dbo].[fn_user] ON
GO
INSERT INTO [dbo].[fn_user] ([id], [username], [email], [password_hash], [auth_key], [confirmed_at], [unconfirmed_email], [blocked_at], [created_at], [updated_at], [registration_ip], [last_login_at], [flags]) VALUES (N'12', N'fonabe', N'admin@moplin.com', N'$2y$12$NNt1IdiizYLutoLYEu3I4.4NqrP6wEyE3RaYKDM0A3CDmzBCKQQRG', N'5r4FrdwQZueQ6PiitbxoDQEpD9Knyj-V', N'1520784697', null, null, N'1520784697', N'1520784697', N'127.0.0.1', N'1520784948', null)
GO
GO
INSERT INTO [dbo].[fn_user] ([id], [username], [email], [password_hash], [auth_key], [confirmed_at], [unconfirmed_email], [blocked_at], [created_at], [updated_at], [registration_ip], [last_login_at], [flags]) VALUES (N'13', N'admin', N'pp@moplin.com', N'$2y$10$eAhK6CwcgOKpcI8vHDeEG.FFt1kYdIPd4ztr07tFuSJ4Y9Z3IUCO.', N'1BoJx992Sxmrm-3TsTFpP4DYol3g0Gyi', N'1519843968', null, null, N'1519843944', N'1519843944', N'127.0.0.1', N'1520896677', null)
GO
GO
SET IDENTITY_INSERT [dbo].[fn_user] OFF
GO

-- ----------------------------
-- Table structure for fn_videos
-- ----------------------------
DROP TABLE [dbo].[fn_videos]
GO
CREATE TABLE [dbo].[fn_videos] (
[id] int NOT NULL IDENTITY(1,1) ,
[title] varchar(500) NOT NULL ,
[slug] varchar(500) NOT NULL ,
[extract] varchar(550) NULL ,
[content] nvarchar(MAX) NULL ,
[author_name] varchar(255) NULL ,
[url_video] varchar(500) NULL ,
[published] bit NOT NULL DEFAULT ((1)) ,
[created_at] int NULL ,
[updated_at] int NULL ,
[page_id] int NOT NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[fn_videos]', RESEED, 12)
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_videos', 
'COLUMN', N'id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Id de video'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Id de video'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'id'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_videos', 
'COLUMN', N'title')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Título del video'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'title'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Título del video'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'title'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_videos', 
'COLUMN', N'slug')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Slug para URL'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'slug'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Slug para URL'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'slug'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_videos', 
'COLUMN', N'extract')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Resúmen'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'extract'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Resúmen'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'extract'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_videos', 
'COLUMN', N'content')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Contenido'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'content'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Contenido'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'content'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_videos', 
'COLUMN', N'author_name')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Nombre del autor'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'author_name'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre del autor'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'author_name'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_videos', 
'COLUMN', N'published')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Publicado'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'published'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Publicado'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'published'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_videos', 
'COLUMN', N'created_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'created_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'created_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_videos', 
'COLUMN', N'updated_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'updated_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'updated_at'
GO

-- ----------------------------
-- Records of fn_videos
-- ----------------------------
SET IDENTITY_INSERT [dbo].[fn_videos] ON
GO
INSERT INTO [dbo].[fn_videos] ([id], [title], [slug], [extract], [content], [author_name], [url_video], [published], [created_at], [updated_at], [page_id]) VALUES (N'4', N'Kombucha X', N'kombucha-x', N'Kombucha X', N'<h1>Kombucha X</h1>
', null, N'8YktZtBBjps', N'1', N'1520822088', N'1520822129', N'13')
GO
GO
INSERT INTO [dbo].[fn_videos] ([id], [title], [slug], [extract], [content], [author_name], [url_video], [published], [created_at], [updated_at], [page_id]) VALUES (N'5', N'Dogs', N'dogs', N'BlxCjjClDnY', N'<p>BlxCjjClDnY</p>
', null, N'BlxCjjClDnY', N'1', N'1520823018', N'1520823018', N'7')
GO
GO
INSERT INTO [dbo].[fn_videos] ([id], [title], [slug], [extract], [content], [author_name], [url_video], [published], [created_at], [updated_at], [page_id]) VALUES (N'6', N'czau9QSX3fo', N'czau-9-qsx-3-fo', N'czau9QSX3fo', N'<p>czau9QSX3fo</p>
', null, N'czau9QSX3fo', N'1', N'1520858502', N'1520858502', N'7')
GO
GO
INSERT INTO [dbo].[fn_videos] ([id], [title], [slug], [extract], [content], [author_name], [url_video], [published], [created_at], [updated_at], [page_id]) VALUES (N'7', N'tCXGJQYZ9JA', N't-cxgjqyz-9-ja', N'tCXGJQYZ9JA', N'<p>tCXGJQYZ9JA</p>
', null, N'tCXGJQYZ9JA', N'1', N'1520858526', N'1520858526', N'7')
GO
GO
INSERT INTO [dbo].[fn_videos] ([id], [title], [slug], [extract], [content], [author_name], [url_video], [published], [created_at], [updated_at], [page_id]) VALUES (N'8', N'gQkI4hvqPCs', N'g-qk-i-4-hvq-p-cs', N'gQkI4hvqPCs', N'<p>gQkI4hvqPCs</p>
', null, N'gQkI4hvqPCs', N'1', N'1520858547', N'1520858547', N'7')
GO
GO
INSERT INTO [dbo].[fn_videos] ([id], [title], [slug], [extract], [content], [author_name], [url_video], [published], [created_at], [updated_at], [page_id]) VALUES (N'9', N'IYOarUVEQf0', N'iy-oar-uve-qf-0', N'IYOarUVEQf0', N'<p>IYOarUVEQf0</p>
', null, N'IYOarUVEQf0', N'1', N'1520858588', N'1520858588', N'7')
GO
GO
INSERT INTO [dbo].[fn_videos] ([id], [title], [slug], [extract], [content], [author_name], [url_video], [published], [created_at], [updated_at], [page_id]) VALUES (N'10', N'W7jkygJ_QNo', N'w-7-jkyg-j-q-no', N'W7jkygJ_QNo', N'<p>W7jkygJ_QNo</p>
', null, N'W7jkygJ_QNo', N'1', N'1520858615', N'1520858615', N'7')
GO
GO
INSERT INTO [dbo].[fn_videos] ([id], [title], [slug], [extract], [content], [author_name], [url_video], [published], [created_at], [updated_at], [page_id]) VALUES (N'11', N'aWJ7SmOVUO8', N'a-wj-7-sm-ovuo-8', N'aWJ7SmOVUO8', N'<p>aWJ7SmOVUO8</p>
', null, N'aWJ7SmOVUO8', N'1', N'1520858684', N'1520858684', N'7')
GO
GO
INSERT INTO [dbo].[fn_videos] ([id], [title], [slug], [extract], [content], [author_name], [url_video], [published], [created_at], [updated_at], [page_id]) VALUES (N'12', N'PRUEBA V', N'prueba-v', N'prueba v', N'<p>Prueba</p>
', null, N'HE9nLWFZ6ac', N'1', N'1520970549', N'1520970549', N'37')
GO
GO
SET IDENTITY_INSERT [dbo].[fn_videos] OFF
GO

-- ----------------------------
-- Indexes structure for table fn_account
-- ----------------------------
CREATE UNIQUE INDEX [account_unique] ON [dbo].[fn_account]
([provider] ASC, [client_id] ASC) 
WITH (IGNORE_DUP_KEY = ON)
GO

-- ----------------------------
-- Primary Key structure for table fn_account
-- ----------------------------
ALTER TABLE [dbo].[fn_account] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table fn_auth_assignment
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_auth_assignment
-- ----------------------------
ALTER TABLE [dbo].[fn_auth_assignment] ADD PRIMARY KEY ([item_name], [user_id])
GO

-- ----------------------------
-- Indexes structure for table fn_auth_item
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_auth_item
-- ----------------------------
ALTER TABLE [dbo].[fn_auth_item] ADD PRIMARY KEY ([name])
GO

-- ----------------------------
-- Indexes structure for table fn_auth_item_child
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_auth_item_child
-- ----------------------------
ALTER TABLE [dbo].[fn_auth_item_child] ADD PRIMARY KEY ([parent], [child])
GO

-- ----------------------------
-- Indexes structure for table fn_auth_rule
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_auth_rule
-- ----------------------------
ALTER TABLE [dbo].[fn_auth_rule] ADD PRIMARY KEY ([name])
GO

-- ----------------------------
-- Indexes structure for table fn_category
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_category
-- ----------------------------
ALTER TABLE [dbo].[fn_category] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table fn_complaints
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_complaints
-- ----------------------------
ALTER TABLE [dbo].[fn_complaints] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table fn_contact
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_contact
-- ----------------------------
ALTER TABLE [dbo].[fn_contact] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table fn_events
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_events
-- ----------------------------
ALTER TABLE [dbo].[fn_events] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table fn_files
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_files
-- ----------------------------
ALTER TABLE [dbo].[fn_files] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table fn_news
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_news
-- ----------------------------
ALTER TABLE [dbo].[fn_news] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table fn_profile
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_profile
-- ----------------------------
ALTER TABLE [dbo].[fn_profile] ADD PRIMARY KEY ([user_id])
GO

-- ----------------------------
-- Indexes structure for table fn_setting
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_setting
-- ----------------------------
ALTER TABLE [dbo].[fn_setting] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table fn_slider_images
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_slider_images
-- ----------------------------
ALTER TABLE [dbo].[fn_slider_images] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table fn_social_account
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_social_account
-- ----------------------------
ALTER TABLE [dbo].[fn_social_account] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table fn_static_pages
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_static_pages
-- ----------------------------
ALTER TABLE [dbo].[fn_static_pages] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table fn_token
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_token
-- ----------------------------
ALTER TABLE [dbo].[fn_token] ADD PRIMARY KEY ([user_id], [code], [type])
GO

-- ----------------------------
-- Indexes structure for table fn_user
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_user
-- ----------------------------
ALTER TABLE [dbo].[fn_user] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Uniques structure for table fn_user
-- ----------------------------
ALTER TABLE [dbo].[fn_user] ADD UNIQUE ([email] ASC)
GO
ALTER TABLE [dbo].[fn_user] ADD UNIQUE ([username] ASC)
GO

-- ----------------------------
-- Indexes structure for table fn_videos
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_videos
-- ----------------------------
ALTER TABLE [dbo].[fn_videos] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[fn_account]
-- ----------------------------
ALTER TABLE [dbo].[fn_account] ADD FOREIGN KEY ([user_id]) REFERENCES [dbo].[fn_user] ([id]) ON DELETE CASCADE ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[fn_auth_assignment]
-- ----------------------------
ALTER TABLE [dbo].[fn_auth_assignment] ADD FOREIGN KEY ([item_name]) REFERENCES [dbo].[fn_auth_item] ([name]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[fn_auth_item]
-- ----------------------------
ALTER TABLE [dbo].[fn_auth_item] ADD FOREIGN KEY ([rule_name]) REFERENCES [dbo].[fn_auth_rule] ([name]) ON DELETE SET NULL ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[fn_auth_item_child]
-- ----------------------------
ALTER TABLE [dbo].[fn_auth_item_child] ADD FOREIGN KEY ([child]) REFERENCES [dbo].[fn_auth_item] ([name]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[fn_auth_item_child] ADD FOREIGN KEY ([parent]) REFERENCES [dbo].[fn_auth_item] ([name]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[fn_files]
-- ----------------------------
ALTER TABLE [dbo].[fn_files] ADD FOREIGN KEY ([page_id]) REFERENCES [dbo].[fn_static_pages] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[fn_profile]
-- ----------------------------
ALTER TABLE [dbo].[fn_profile] ADD FOREIGN KEY ([user_id]) REFERENCES [dbo].[fn_user] ([id]) ON DELETE CASCADE ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[fn_social_account]
-- ----------------------------
ALTER TABLE [dbo].[fn_social_account] ADD FOREIGN KEY ([user_id]) REFERENCES [dbo].[fn_user] ([id]) ON DELETE CASCADE ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[fn_static_pages]
-- ----------------------------
ALTER TABLE [dbo].[fn_static_pages] ADD FOREIGN KEY ([category_id]) REFERENCES [dbo].[fn_category] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[fn_token]
-- ----------------------------
ALTER TABLE [dbo].[fn_token] ADD FOREIGN KEY ([user_id]) REFERENCES [dbo].[fn_user] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[fn_videos]
-- ----------------------------
ALTER TABLE [dbo].[fn_videos] ADD FOREIGN KEY ([page_id]) REFERENCES [dbo].[fn_static_pages] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
