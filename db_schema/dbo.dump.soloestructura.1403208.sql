/*
Navicat SQL Server Data Transfer

Source Server         : Fonabe
Source Server Version : 110000
Source Host           : 192.168.100.112:1433
Source Database       : fonabe
Source Schema         : dbo

Target Server Type    : SQL Server
Target Server Version : 110000
File Encoding         : 65001

Date: 2018-03-14 07:27:03
*/


-- ----------------------------
-- Table structure for fn_account
-- ----------------------------
DROP TABLE [dbo].[fn_account]
GO
CREATE TABLE [dbo].[fn_account] (
[id] int NOT NULL IDENTITY(1,1) ,
[user_id] int NULL DEFAULT NULL ,
[provider] nvarchar(255) NOT NULL ,
[client_id] nvarchar(255) NOT NULL ,
[properties] nvarchar(MAX) NULL DEFAULT NULL 
)


GO

-- ----------------------------
-- Table structure for fn_auth_assignment
-- ----------------------------
DROP TABLE [dbo].[fn_auth_assignment]
GO
CREATE TABLE [dbo].[fn_auth_assignment] (
[item_name] nvarchar(64) NOT NULL ,
[user_id] nvarchar(64) NOT NULL ,
[created_at] int NULL 
)


GO

-- ----------------------------
-- Table structure for fn_auth_item
-- ----------------------------
DROP TABLE [dbo].[fn_auth_item]
GO
CREATE TABLE [dbo].[fn_auth_item] (
[name] nvarchar(64) NOT NULL ,
[type] smallint NOT NULL ,
[description] varchar(MAX) NULL DEFAULT NULL ,
[rule_name] nvarchar(64) NULL DEFAULT NULL ,
[created_at] int NULL DEFAULT NULL ,
[updated_at] int NULL DEFAULT NULL ,
[data] nvarchar(MAX) NULL DEFAULT NULL 
)


GO

-- ----------------------------
-- Table structure for fn_auth_item_child
-- ----------------------------
DROP TABLE [dbo].[fn_auth_item_child]
GO
CREATE TABLE [dbo].[fn_auth_item_child] (
[parent] nvarchar(64) NOT NULL ,
[child] nvarchar(64) NOT NULL 
)


GO

-- ----------------------------
-- Table structure for fn_auth_rule
-- ----------------------------
DROP TABLE [dbo].[fn_auth_rule]
GO
CREATE TABLE [dbo].[fn_auth_rule] (
[name] nvarchar(64) NOT NULL ,
[created_at] int NULL DEFAULT NULL ,
[updated_at] int NULL DEFAULT NULL ,
[data] nvarchar(MAX) NULL DEFAULT NULL 
)


GO

-- ----------------------------
-- Table structure for fn_category
-- ----------------------------
DROP TABLE [dbo].[fn_category]
GO
CREATE TABLE [dbo].[fn_category] (
[id] int NOT NULL IDENTITY(1,1) ,
[title] varchar(255) NULL ,
[slug] varchar(255) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[fn_category]', RESEED, 9)
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_category', 
'COLUMN', N'title')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Título categoría'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_category'
, @level2type = 'COLUMN', @level2name = N'title'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Título categoría'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_category'
, @level2type = 'COLUMN', @level2name = N'title'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_category', 
'COLUMN', N'slug')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Slug para el URL'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_category'
, @level2type = 'COLUMN', @level2name = N'slug'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Slug para el URL'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_category'
, @level2type = 'COLUMN', @level2name = N'slug'
GO

-- ----------------------------
-- Table structure for fn_complaints
-- ----------------------------
DROP TABLE [dbo].[fn_complaints]
GO
CREATE TABLE [dbo].[fn_complaints] (
[id] int NOT NULL IDENTITY(1,1) ,
[first_name] varchar(255) NOT NULL ,
[last_name] varchar(255) NOT NULL ,
[identification_card] varchar(40) NOT NULL ,
[phone] varchar(50) NOT NULL ,
[address] varchar(500) NOT NULL ,
[email] varchar(150) NOT NULL ,
[fax] varchar(150) NULL ,
[place_of_work] varchar(500) NULL ,
[details] varchar(2500) NOT NULL ,
[reported_name_01] varchar(500) NOT NULL ,
[reported_name_02] varchar(500) NULL ,
[reported_name_03] varchar(500) NULL ,
[reported_location_01] varchar(500) NOT NULL ,
[reported_location_02] varchar(500) NULL ,
[reported_location_03] varchar(500) NULL ,
[evidence_01] varchar(2000) NOT NULL ,
[evidence_02] varchar(2000) NULL ,
[evidence_03] varchar(2000) NULL ,
[evidence_location_01] varchar(500) NOT NULL ,
[evidence_location_02] varchar(500) NULL ,
[evidence_location_03] varchar(500) NULL ,
[witness_01] varchar(500) NOT NULL ,
[witness_02] varchar(500) NULL ,
[witness_03] varchar(500) NULL ,
[witness_location_01] varchar(500) NOT NULL ,
[witness_location_02] varchar(500) NULL ,
[witness_location_03] varchar(500) NULL ,
[created_at] int NULL ,
[updated_at] int NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[fn_complaints]', RESEED, 6)
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Id de las quejas'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Id de las quejas'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'id'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'first_name')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Nombre'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'first_name'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'first_name'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'last_name')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Apellido'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'last_name'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Apellido'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'last_name'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'identification_card')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Número de identificación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'identification_card'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Número de identificación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'identification_card'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'phone')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Teléfono'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'phone'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Teléfono'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'phone'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'address')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Dirección'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'address'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Dirección'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'address'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'email')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Correo electrónico'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'email'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Correo electrónico'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'email'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'fax')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Número de Fax'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'fax'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Número de Fax'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'fax'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'place_of_work')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Lugar de trabajo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'place_of_work'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Lugar de trabajo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'place_of_work'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'details')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Detalles'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'details'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Detalles'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'details'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'reported_name_01')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Nombre'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'reported_name_01'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'reported_name_01'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'reported_name_02')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Nombre'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'reported_name_02'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'reported_name_02'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'reported_name_03')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Nombre'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'reported_name_03'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'reported_name_03'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'reported_location_01')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'reported_location_01'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'reported_location_01'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'reported_location_02')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'reported_location_02'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'reported_location_02'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'reported_location_03')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'reported_location_03'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'reported_location_03'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'evidence_01')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Evidencia'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'evidence_01'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Evidencia'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'evidence_01'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'evidence_02')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Evidencia'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'evidence_02'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Evidencia'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'evidence_02'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'evidence_03')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Evidencia'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'evidence_03'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Evidencia'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'evidence_03'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'evidence_location_01')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'evidence_location_01'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'evidence_location_01'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'evidence_location_02')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'evidence_location_02'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'evidence_location_02'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'evidence_location_03')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'evidence_location_03'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'evidence_location_03'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'witness_01')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Testigo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'witness_01'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Testigo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'witness_01'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'witness_02')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Testigo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'witness_02'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Testigo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'witness_02'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'witness_03')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Testigo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'witness_03'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Testigo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'witness_03'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'witness_location_01')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'witness_location_01'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'witness_location_01'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'witness_location_02')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'witness_location_02'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'witness_location_02'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_complaints', 
'COLUMN', N'witness_location_03')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'witness_location_03'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Ubicación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_complaints'
, @level2type = 'COLUMN', @level2name = N'witness_location_03'
GO

-- ----------------------------
-- Table structure for fn_contact
-- ----------------------------
DROP TABLE [dbo].[fn_contact]
GO
CREATE TABLE [dbo].[fn_contact] (
[id] int NOT NULL IDENTITY(1,1) ,
[first_name] varchar(255) NOT NULL ,
[last_name] varchar(255) NOT NULL ,
[identification] varchar(255) NOT NULL ,
[phone] varchar(255) NULL ,
[email] varchar(255) NOT NULL ,
[subject] varchar(500) NOT NULL ,
[detail] varchar(2500) NOT NULL ,
[created_at] int NULL ,
[updated_at] int NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[fn_contact]', RESEED, 3)
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_contact', 
'COLUMN', N'id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Id de contacto'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Id de contacto'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'id'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_contact', 
'COLUMN', N'first_name')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Nombre'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'first_name'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'first_name'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_contact', 
'COLUMN', N'last_name')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Apellido'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'last_name'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Apellido'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'last_name'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_contact', 
'COLUMN', N'identification')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Identificación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'identification'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Identificación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'identification'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_contact', 
'COLUMN', N'phone')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Teléfono'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'phone'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Teléfono'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'phone'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_contact', 
'COLUMN', N'email')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Correo electrónico'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'email'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Correo electrónico'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'email'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_contact', 
'COLUMN', N'subject')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Tema'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'subject'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Tema'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'subject'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_contact', 
'COLUMN', N'detail')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Detalles'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'detail'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Detalles'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'detail'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_contact', 
'COLUMN', N'created_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'created_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'created_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_contact', 
'COLUMN', N'updated_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha de actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'updated_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_contact'
, @level2type = 'COLUMN', @level2name = N'updated_at'
GO

-- ----------------------------
-- Table structure for fn_events
-- ----------------------------
DROP TABLE [dbo].[fn_events]
GO
CREATE TABLE [dbo].[fn_events] (
[id] int NOT NULL IDENTITY(1,1) ,
[title] varchar(500) NOT NULL ,
[description] varchar(2000) NOT NULL ,
[updated_at] int NULL ,
[created_at] int NULL ,
[end_dt] datetime2(7) NOT NULL ,
[start_dt] datetime2(7) NOT NULL ,
[hours] int NULL DEFAULT ((1)) ,
[date] date NULL ,
[time] varchar(35) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[fn_events]', RESEED, 16)
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_events', 
'COLUMN', N'id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Id Eventos'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Id Eventos'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'id'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_events', 
'COLUMN', N'title')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Título del evento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'title'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Título del evento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'title'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_events', 
'COLUMN', N'description')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Descripción del evento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'description'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Descripción del evento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'description'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_events', 
'COLUMN', N'updated_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'updated_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'updated_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_events', 
'COLUMN', N'created_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'created_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'created_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_events', 
'COLUMN', N'end_dt')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Hora y fecha de fin'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'end_dt'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Hora y fecha de fin'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'end_dt'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_events', 
'COLUMN', N'start_dt')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Hora y fecha de inicio'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'start_dt'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Hora y fecha de inicio'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'start_dt'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_events', 
'COLUMN', N'hours')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Duración del evento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'hours'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Duración del evento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'hours'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_events', 
'COLUMN', N'date')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha del evento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'date'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha del evento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'date'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_events', 
'COLUMN', N'time')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Hora de inicio del evento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'time'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Hora de inicio del evento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_events'
, @level2type = 'COLUMN', @level2name = N'time'
GO

-- ----------------------------
-- Table structure for fn_files
-- ----------------------------
DROP TABLE [dbo].[fn_files]
GO
CREATE TABLE [dbo].[fn_files] (
[page_id] int NULL ,
[title] varchar(500) NULL ,
[description] varchar(2500) NULL ,
[file_url] varchar(500) NULL ,
[hit_counter] int NULL ,
[download_counter] int NULL ,
[created_at] int NULL ,
[updated_at] int NULL ,
[id] int NOT NULL IDENTITY(1,1) ,
[file_date] date NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[fn_files]', RESEED, 21)
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_files', 
'COLUMN', N'page_id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Id página'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'page_id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Id página'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'page_id'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_files', 
'COLUMN', N'title')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Título documento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'title'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Título documento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'title'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_files', 
'COLUMN', N'description')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Descripción documento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'description'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Descripción documento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'description'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_files', 
'COLUMN', N'file_url')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Url documento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'file_url'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Url documento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'file_url'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_files', 
'COLUMN', N'hit_counter')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Contador hits'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'hit_counter'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Contador hits'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'hit_counter'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_files', 
'COLUMN', N'download_counter')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Contador downloads'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'download_counter'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Contador downloads'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'download_counter'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_files', 
'COLUMN', N'created_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'FEcha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'created_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'FEcha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'created_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_files', 
'COLUMN', N'updated_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'updated_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'updated_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_files', 
'COLUMN', N'id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Id documento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Id documento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'id'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_files', 
'COLUMN', N'file_date')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha del documento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'file_date'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha del documento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_files'
, @level2type = 'COLUMN', @level2name = N'file_date'
GO

-- ----------------------------
-- Table structure for fn_news
-- ----------------------------
DROP TABLE [dbo].[fn_news]
GO
CREATE TABLE [dbo].[fn_news] (
[id] int NOT NULL IDENTITY(1,1) ,
[title] varchar(500) NOT NULL ,
[slug] varchar(500) NOT NULL ,
[extract] varchar(550) NULL ,
[content] nvarchar(MAX) NULL ,
[author_name] varchar(255) NULL ,
[url_imagen] varchar(500) NULL ,
[updated_at] int NULL ,
[created_at] int NULL ,
[published] bit NOT NULL DEFAULT ((1)) 
)


GO
DBCC CHECKIDENT(N'[dbo].[fn_news]', RESEED, 11)
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_news', 
'COLUMN', N'id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Id de noticia'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Id de noticia'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'id'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_news', 
'COLUMN', N'title')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Título de la noticia'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'title'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Título de la noticia'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'title'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_news', 
'COLUMN', N'slug')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Slug para URL'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'slug'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Slug para URL'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'slug'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_news', 
'COLUMN', N'extract')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Resúmen'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'extract'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Resúmen'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'extract'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_news', 
'COLUMN', N'content')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Contenido'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'content'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Contenido'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'content'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_news', 
'COLUMN', N'author_name')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Nombre del autor'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'author_name'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre del autor'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'author_name'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_news', 
'COLUMN', N'updated_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha de ultima actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'updated_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de ultima actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'updated_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_news', 
'COLUMN', N'created_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'created_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'created_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_news', 
'COLUMN', N'published')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Publicado'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'published'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Publicado'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_news'
, @level2type = 'COLUMN', @level2name = N'published'
GO

-- ----------------------------
-- Table structure for fn_profile
-- ----------------------------
DROP TABLE [dbo].[fn_profile]
GO
CREATE TABLE [dbo].[fn_profile] (
[user_id] int NOT NULL ,
[name] nvarchar(255) NULL DEFAULT NULL ,
[public_email] nvarchar(255) NULL DEFAULT NULL ,
[gravatar_email] nvarchar(255) NULL DEFAULT NULL ,
[gravatar_id] nvarchar(32) NULL DEFAULT NULL ,
[location] nvarchar(255) NULL DEFAULT NULL ,
[website] nvarchar(255) NULL DEFAULT NULL ,
[bio] nvarchar(MAX) NULL DEFAULT NULL ,
[timezone] nvarchar(40) NULL 
)


GO

-- ----------------------------
-- Table structure for fn_setting
-- ----------------------------
DROP TABLE [dbo].[fn_setting]
GO
CREATE TABLE [dbo].[fn_setting] (
[id] int NOT NULL IDENTITY(1,1) ,
[type] nvarchar(10) NOT NULL ,
[section] nvarchar(255) NOT NULL ,
[key] nvarchar(255) NOT NULL ,
[value] nvarchar(MAX) NOT NULL ,
[status] smallint NOT NULL DEFAULT ((1)) ,
[description] nvarchar(255) NULL DEFAULT NULL ,
[created_at] int NOT NULL ,
[updated_at] int NOT NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[fn_setting]', RESEED, 23)
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_setting', 
'COLUMN', N'id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Id Settings'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Id Settings'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'id'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_setting', 
'COLUMN', N'type')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Tipo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'type'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Tipo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'type'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_setting', 
'COLUMN', N'section')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Seccion'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'section'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Seccion'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'section'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_setting', 
'COLUMN', N'key')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Llave elemento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'key'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Llave elemento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'key'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_setting', 
'COLUMN', N'value')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Valor elemento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'value'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Valor elemento'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'value'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_setting', 
'COLUMN', N'status')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Estado'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'status'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Estado'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'status'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_setting', 
'COLUMN', N'description')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Descripción'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'description'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Descripción'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'description'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_setting', 
'COLUMN', N'created_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'created_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'created_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_setting', 
'COLUMN', N'updated_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'updated_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_setting'
, @level2type = 'COLUMN', @level2name = N'updated_at'
GO

-- ----------------------------
-- Table structure for fn_slider_images
-- ----------------------------
DROP TABLE [dbo].[fn_slider_images]
GO
CREATE TABLE [dbo].[fn_slider_images] (
[id] int NOT NULL IDENTITY(1,1) ,
[image_url] varchar(500) NOT NULL ,
[image_link] varchar(500) NOT NULL ,
[slide_text] varchar(500) NOT NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[fn_slider_images]', RESEED, 5)
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_slider_images', 
'COLUMN', N'id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Id Slide'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_slider_images'
, @level2type = 'COLUMN', @level2name = N'id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Id Slide'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_slider_images'
, @level2type = 'COLUMN', @level2name = N'id'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_slider_images', 
'COLUMN', N'image_url')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Url imagen del slide'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_slider_images'
, @level2type = 'COLUMN', @level2name = N'image_url'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Url imagen del slide'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_slider_images'
, @level2type = 'COLUMN', @level2name = N'image_url'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_slider_images', 
'COLUMN', N'image_link')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Link del slide'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_slider_images'
, @level2type = 'COLUMN', @level2name = N'image_link'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Link del slide'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_slider_images'
, @level2type = 'COLUMN', @level2name = N'image_link'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_slider_images', 
'COLUMN', N'slide_text')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Texto del slide'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_slider_images'
, @level2type = 'COLUMN', @level2name = N'slide_text'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Texto del slide'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_slider_images'
, @level2type = 'COLUMN', @level2name = N'slide_text'
GO

-- ----------------------------
-- Table structure for fn_social_account
-- ----------------------------
DROP TABLE [dbo].[fn_social_account]
GO
CREATE TABLE [dbo].[fn_social_account] (
[id] int NOT NULL ,
[user_id] int NULL ,
[provider] nvarchar(255) NULL ,
[client_id] nvarchar(255) NULL ,
[data] nvarchar(MAX) NULL ,
[code] nvarchar(32) NULL ,
[created_at] int NULL ,
[email] nvarchar(255) NULL ,
[username] nvarchar(255) NULL 
)


GO

-- ----------------------------
-- Table structure for fn_static_pages
-- ----------------------------
DROP TABLE [dbo].[fn_static_pages]
GO
CREATE TABLE [dbo].[fn_static_pages] (
[id] int NOT NULL IDENTITY(1,1) ,
[slug] varchar(255) NOT NULL ,
[title] varchar(255) NOT NULL ,
[excerpt] varchar(500) NULL ,
[content] nvarchar(MAX) NULL ,
[created_at] int NULL ,
[updated_at] int NULL ,
[category_id] int NULL ,
[editable] bit NULL DEFAULT ((1)) ,
[icon] varchar(32) NULL ,
[order] int NOT NULL DEFAULT ((0)) ,
[has_md_documents] bit NOT NULL DEFAULT ((0)) ,
[has_md_video] bit NOT NULL DEFAULT ((0)) ,
[has_md_info] bit NOT NULL DEFAULT ((0)) 
)


GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Id'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Id'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'id'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'slug')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Slug para URL limpia'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'slug'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Slug para URL limpia'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'slug'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'title')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Título de la página'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'title'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Título de la página'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'title'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'excerpt')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Resúmen del contenido'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'excerpt'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Resúmen del contenido'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'excerpt'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'content')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Contenido de la página'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'content'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Contenido de la página'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'content'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'created_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'created_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'created_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'updated_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha de actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'updated_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'updated_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'category_id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Id Categoría'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'category_id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Id Categoría'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'category_id'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'editable')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Es editable?'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'editable'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Es editable?'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'editable'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'icon')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Icono de menú'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'icon'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Icono de menú'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'icon'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'order')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Orden de las páginas'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'order'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Orden de las páginas'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'order'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'has_md_documents')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Tiene maestro/detalle de documentos?'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'has_md_documents'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Tiene maestro/detalle de documentos?'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'has_md_documents'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'has_md_video')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Tienes maestro/detalle de videos?'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'has_md_video'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Tienes maestro/detalle de videos?'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'has_md_video'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_static_pages', 
'COLUMN', N'has_md_info')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Tienes maestro/detalle de información?'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'has_md_info'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Tienes maestro/detalle de información?'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_static_pages'
, @level2type = 'COLUMN', @level2name = N'has_md_info'
GO

-- ----------------------------
-- Table structure for fn_token
-- ----------------------------
DROP TABLE [dbo].[fn_token]
GO
CREATE TABLE [dbo].[fn_token] (
[user_id] int NOT NULL ,
[code] nvarchar(32) NOT NULL ,
[created_at] int NULL ,
[type] int NOT NULL 
)


GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_token', 
NULL, NULL)) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Tabla de gestion de tokens de autenticación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_token'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Tabla de gestion de tokens de autenticación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_token'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_token', 
'COLUMN', N'user_id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Id del usuario'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_token'
, @level2type = 'COLUMN', @level2name = N'user_id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Id del usuario'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_token'
, @level2type = 'COLUMN', @level2name = N'user_id'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_token', 
'COLUMN', N'code')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Código'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_token'
, @level2type = 'COLUMN', @level2name = N'code'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Código'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_token'
, @level2type = 'COLUMN', @level2name = N'code'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_token', 
'COLUMN', N'created_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_token'
, @level2type = 'COLUMN', @level2name = N'created_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_token'
, @level2type = 'COLUMN', @level2name = N'created_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_token', 
'COLUMN', N'type')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Tipo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_token'
, @level2type = 'COLUMN', @level2name = N'type'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Tipo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_token'
, @level2type = 'COLUMN', @level2name = N'type'
GO

-- ----------------------------
-- Table structure for fn_user
-- ----------------------------
DROP TABLE [dbo].[fn_user]
GO
CREATE TABLE [dbo].[fn_user] (
[id] int NOT NULL IDENTITY(1,1) ,
[username] nvarchar(255) NOT NULL ,
[email] nvarchar(255) NOT NULL ,
[password_hash] nvarchar(60) NOT NULL ,
[auth_key] nvarchar(32) NOT NULL ,
[confirmed_at] int NULL DEFAULT NULL ,
[unconfirmed_email] nvarchar(255) NULL DEFAULT NULL ,
[blocked_at] int NULL DEFAULT NULL ,
[created_at] int NOT NULL ,
[updated_at] int NOT NULL ,
[registration_ip] nvarchar(45) NULL ,
[last_login_at] int NULL ,
[flags] int NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[fn_user]', RESEED, 11)
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_user', 
NULL, NULL)) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Tabla de usuarios del sistema'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Tabla de usuarios del sistema'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_user', 
'COLUMN', N'id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Id usuario'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Id usuario'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'id'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_user', 
'COLUMN', N'username')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Nombre de Usuario'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'username'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre de Usuario'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'username'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_user', 
'COLUMN', N'email')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Correo del Usuario'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'email'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Correo del Usuario'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'email'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_user', 
'COLUMN', N'password_hash')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Hash de contraseña'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'password_hash'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Hash de contraseña'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'password_hash'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_user', 
'COLUMN', N'auth_key')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Llave de atorización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'auth_key'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Llave de atorización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'auth_key'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_user', 
'COLUMN', N'confirmed_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha de confirmacion'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'confirmed_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de confirmacion'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'confirmed_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_user', 
'COLUMN', N'unconfirmed_email')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Correo electronico no confirmado'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'unconfirmed_email'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Correo electronico no confirmado'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'unconfirmed_email'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_user', 
'COLUMN', N'blocked_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha de bloqueo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'blocked_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de bloqueo'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'blocked_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_user', 
'COLUMN', N'created_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'FEcha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'created_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'FEcha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'created_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_user', 
'COLUMN', N'registration_ip')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'No IP del registro'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'registration_ip'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'No IP del registro'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'registration_ip'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_user', 
'COLUMN', N'last_login_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha de últimop registro'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'last_login_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de últimop registro'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'last_login_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_user', 
'COLUMN', N'flags')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Banderas'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'flags'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Banderas'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_user'
, @level2type = 'COLUMN', @level2name = N'flags'
GO

-- ----------------------------
-- Table structure for fn_videos
-- ----------------------------
DROP TABLE [dbo].[fn_videos]
GO
CREATE TABLE [dbo].[fn_videos] (
[id] int NOT NULL IDENTITY(1,1) ,
[title] varchar(500) NOT NULL ,
[slug] varchar(500) NOT NULL ,
[extract] varchar(550) NULL ,
[content] nvarchar(MAX) NULL ,
[author_name] varchar(255) NULL ,
[url_video] varchar(500) NULL ,
[published] bit NOT NULL DEFAULT ((1)) ,
[created_at] int NULL ,
[updated_at] int NULL ,
[page_id] int NOT NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[fn_videos]', RESEED, 12)
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_videos', 
'COLUMN', N'id')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Id de video'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'id'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Id de video'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'id'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_videos', 
'COLUMN', N'title')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Título del video'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'title'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Título del video'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'title'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_videos', 
'COLUMN', N'slug')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Slug para URL'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'slug'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Slug para URL'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'slug'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_videos', 
'COLUMN', N'extract')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Resúmen'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'extract'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Resúmen'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'extract'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_videos', 
'COLUMN', N'content')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Contenido'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'content'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Contenido'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'content'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_videos', 
'COLUMN', N'author_name')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Nombre del autor'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'author_name'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre del autor'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'author_name'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_videos', 
'COLUMN', N'published')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Publicado'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'published'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Publicado'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'published'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_videos', 
'COLUMN', N'created_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'created_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de creación'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'created_at'
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'fn_videos', 
'COLUMN', N'updated_at')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Fecha actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'updated_at'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha actualización'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'fn_videos'
, @level2type = 'COLUMN', @level2name = N'updated_at'
GO

-- ----------------------------
-- Table structure for ft_test
-- ----------------------------
DROP TABLE [dbo].[ft_test]
GO
CREATE TABLE [dbo].[ft_test] (
[id] int NOT NULL ,
[text] varchar(255) NULL 
)


GO

-- ----------------------------
-- Indexes structure for table fn_account
-- ----------------------------
CREATE UNIQUE INDEX [account_unique] ON [dbo].[fn_account]
([provider] ASC, [client_id] ASC) 
WITH (IGNORE_DUP_KEY = ON)
GO

-- ----------------------------
-- Primary Key structure for table fn_account
-- ----------------------------
ALTER TABLE [dbo].[fn_account] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table fn_auth_assignment
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_auth_assignment
-- ----------------------------
ALTER TABLE [dbo].[fn_auth_assignment] ADD PRIMARY KEY ([item_name], [user_id])
GO

-- ----------------------------
-- Indexes structure for table fn_auth_item
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_auth_item
-- ----------------------------
ALTER TABLE [dbo].[fn_auth_item] ADD PRIMARY KEY ([name])
GO

-- ----------------------------
-- Indexes structure for table fn_auth_item_child
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_auth_item_child
-- ----------------------------
ALTER TABLE [dbo].[fn_auth_item_child] ADD PRIMARY KEY ([parent], [child])
GO

-- ----------------------------
-- Indexes structure for table fn_auth_rule
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_auth_rule
-- ----------------------------
ALTER TABLE [dbo].[fn_auth_rule] ADD PRIMARY KEY ([name])
GO

-- ----------------------------
-- Indexes structure for table fn_category
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_category
-- ----------------------------
ALTER TABLE [dbo].[fn_category] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table fn_complaints
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_complaints
-- ----------------------------
ALTER TABLE [dbo].[fn_complaints] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table fn_contact
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_contact
-- ----------------------------
ALTER TABLE [dbo].[fn_contact] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table fn_events
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_events
-- ----------------------------
ALTER TABLE [dbo].[fn_events] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table fn_files
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_files
-- ----------------------------
ALTER TABLE [dbo].[fn_files] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table fn_news
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_news
-- ----------------------------
ALTER TABLE [dbo].[fn_news] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table fn_profile
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_profile
-- ----------------------------
ALTER TABLE [dbo].[fn_profile] ADD PRIMARY KEY ([user_id])
GO

-- ----------------------------
-- Indexes structure for table fn_setting
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_setting
-- ----------------------------
ALTER TABLE [dbo].[fn_setting] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table fn_slider_images
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_slider_images
-- ----------------------------
ALTER TABLE [dbo].[fn_slider_images] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table fn_social_account
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_social_account
-- ----------------------------
ALTER TABLE [dbo].[fn_social_account] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table fn_static_pages
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_static_pages
-- ----------------------------
ALTER TABLE [dbo].[fn_static_pages] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table fn_token
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_token
-- ----------------------------
ALTER TABLE [dbo].[fn_token] ADD PRIMARY KEY ([user_id], [code], [type])
GO

-- ----------------------------
-- Indexes structure for table fn_user
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_user
-- ----------------------------
ALTER TABLE [dbo].[fn_user] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Uniques structure for table fn_user
-- ----------------------------
ALTER TABLE [dbo].[fn_user] ADD UNIQUE ([email] ASC)
GO
ALTER TABLE [dbo].[fn_user] ADD UNIQUE ([username] ASC)
GO

-- ----------------------------
-- Indexes structure for table fn_videos
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table fn_videos
-- ----------------------------
ALTER TABLE [dbo].[fn_videos] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table ft_test
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table ft_test
-- ----------------------------
ALTER TABLE [dbo].[ft_test] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[fn_account]
-- ----------------------------
ALTER TABLE [dbo].[fn_account] ADD FOREIGN KEY ([user_id]) REFERENCES [dbo].[fn_user] ([id]) ON DELETE CASCADE ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[fn_auth_assignment]
-- ----------------------------
ALTER TABLE [dbo].[fn_auth_assignment] ADD FOREIGN KEY ([item_name]) REFERENCES [dbo].[fn_auth_item] ([name]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[fn_auth_item]
-- ----------------------------
ALTER TABLE [dbo].[fn_auth_item] ADD FOREIGN KEY ([rule_name]) REFERENCES [dbo].[fn_auth_rule] ([name]) ON DELETE SET NULL ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[fn_auth_item_child]
-- ----------------------------
ALTER TABLE [dbo].[fn_auth_item_child] ADD FOREIGN KEY ([child]) REFERENCES [dbo].[fn_auth_item] ([name]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[fn_auth_item_child] ADD FOREIGN KEY ([parent]) REFERENCES [dbo].[fn_auth_item] ([name]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[fn_files]
-- ----------------------------
ALTER TABLE [dbo].[fn_files] ADD FOREIGN KEY ([page_id]) REFERENCES [dbo].[fn_static_pages] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[fn_profile]
-- ----------------------------
ALTER TABLE [dbo].[fn_profile] ADD FOREIGN KEY ([user_id]) REFERENCES [dbo].[fn_user] ([id]) ON DELETE CASCADE ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[fn_social_account]
-- ----------------------------
ALTER TABLE [dbo].[fn_social_account] ADD FOREIGN KEY ([user_id]) REFERENCES [dbo].[fn_user] ([id]) ON DELETE CASCADE ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[fn_static_pages]
-- ----------------------------
ALTER TABLE [dbo].[fn_static_pages] ADD FOREIGN KEY ([category_id]) REFERENCES [dbo].[fn_category] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[fn_token]
-- ----------------------------
ALTER TABLE [dbo].[fn_token] ADD FOREIGN KEY ([user_id]) REFERENCES [dbo].[fn_user] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[fn_videos]
-- ----------------------------
ALTER TABLE [dbo].[fn_videos] ADD FOREIGN KEY ([page_id]) REFERENCES [dbo].[fn_static_pages] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
