USE [FONABEDB]
GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_videos', @level2type=N'COLUMN',@level2name=N'updated_at'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_videos', @level2type=N'COLUMN',@level2name=N'created_at'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_videos', @level2type=N'COLUMN',@level2name=N'published'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_videos', @level2type=N'COLUMN',@level2name=N'author_name'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_videos', @level2type=N'COLUMN',@level2name=N'content'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_videos', @level2type=N'COLUMN',@level2name=N'extract'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_videos', @level2type=N'COLUMN',@level2name=N'slug'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_videos', @level2type=N'COLUMN',@level2name=N'title'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_videos', @level2type=N'COLUMN',@level2name=N'id'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_user'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_user', @level2type=N'COLUMN',@level2name=N'flags'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_user', @level2type=N'COLUMN',@level2name=N'last_login_at'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_user', @level2type=N'COLUMN',@level2name=N'registration_ip'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_user', @level2type=N'COLUMN',@level2name=N'created_at'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_user', @level2type=N'COLUMN',@level2name=N'blocked_at'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_user', @level2type=N'COLUMN',@level2name=N'unconfirmed_email'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_user', @level2type=N'COLUMN',@level2name=N'confirmed_at'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_user', @level2type=N'COLUMN',@level2name=N'auth_key'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_user', @level2type=N'COLUMN',@level2name=N'password_hash'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_user', @level2type=N'COLUMN',@level2name=N'email'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_user', @level2type=N'COLUMN',@level2name=N'username'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_user', @level2type=N'COLUMN',@level2name=N'id'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_token'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_token', @level2type=N'COLUMN',@level2name=N'type'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_token', @level2type=N'COLUMN',@level2name=N'created_at'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_token', @level2type=N'COLUMN',@level2name=N'code'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_token', @level2type=N'COLUMN',@level2name=N'user_id'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'has_md_info'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'has_md_video'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'has_md_documents'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'order'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'icon'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'editable'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'category_id'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'updated_at'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'created_at'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'content'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'excerpt'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'title'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'slug'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'id'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_slider_images', @level2type=N'COLUMN',@level2name=N'slide_text'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_slider_images', @level2type=N'COLUMN',@level2name=N'image_link'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_slider_images', @level2type=N'COLUMN',@level2name=N'image_url'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_slider_images', @level2type=N'COLUMN',@level2name=N'id'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_setting', @level2type=N'COLUMN',@level2name=N'updated_at'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_setting', @level2type=N'COLUMN',@level2name=N'created_at'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_setting', @level2type=N'COLUMN',@level2name=N'description'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_setting', @level2type=N'COLUMN',@level2name=N'status'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_setting', @level2type=N'COLUMN',@level2name=N'value'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_setting', @level2type=N'COLUMN',@level2name=N'key'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_setting', @level2type=N'COLUMN',@level2name=N'section'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_setting', @level2type=N'COLUMN',@level2name=N'type'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_setting', @level2type=N'COLUMN',@level2name=N'id'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_news', @level2type=N'COLUMN',@level2name=N'published'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_news', @level2type=N'COLUMN',@level2name=N'created_at'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_news', @level2type=N'COLUMN',@level2name=N'updated_at'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_news', @level2type=N'COLUMN',@level2name=N'author_name'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_news', @level2type=N'COLUMN',@level2name=N'content'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_news', @level2type=N'COLUMN',@level2name=N'extract'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_news', @level2type=N'COLUMN',@level2name=N'slug'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_news', @level2type=N'COLUMN',@level2name=N'title'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_news', @level2type=N'COLUMN',@level2name=N'id'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_info', @level2type=N'COLUMN',@level2name=N'excerpt'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_info', @level2type=N'COLUMN',@level2name=N'content'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_info', @level2type=N'COLUMN',@level2name=N'title'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_info', @level2type=N'COLUMN',@level2name=N'page_id'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_info', @level2type=N'COLUMN',@level2name=N'id'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_history', @level2type=N'COLUMN',@level2name=N'content'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_history', @level2type=N'COLUMN',@level2name=N'title'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_history', @level2type=N'COLUMN',@level2name=N'year'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_history', @level2type=N'COLUMN',@level2name=N'id'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_files', @level2type=N'COLUMN',@level2name=N'file_date'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_files', @level2type=N'COLUMN',@level2name=N'id'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_files', @level2type=N'COLUMN',@level2name=N'updated_at'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_files', @level2type=N'COLUMN',@level2name=N'created_at'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_files', @level2type=N'COLUMN',@level2name=N'download_counter'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_files', @level2type=N'COLUMN',@level2name=N'hit_counter'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_files', @level2type=N'COLUMN',@level2name=N'file_url'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_files', @level2type=N'COLUMN',@level2name=N'description'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_files', @level2type=N'COLUMN',@level2name=N'title'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_files', @level2type=N'COLUMN',@level2name=N'page_id'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_events', @level2type=N'COLUMN',@level2name=N'time'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_events', @level2type=N'COLUMN',@level2name=N'date'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_events', @level2type=N'COLUMN',@level2name=N'hours'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_events', @level2type=N'COLUMN',@level2name=N'start_dt'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_events', @level2type=N'COLUMN',@level2name=N'end_dt'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_events', @level2type=N'COLUMN',@level2name=N'created_at'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_events', @level2type=N'COLUMN',@level2name=N'updated_at'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_events', @level2type=N'COLUMN',@level2name=N'description'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_events', @level2type=N'COLUMN',@level2name=N'title'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_events', @level2type=N'COLUMN',@level2name=N'id'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_contact', @level2type=N'COLUMN',@level2name=N'updated_at'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_contact', @level2type=N'COLUMN',@level2name=N'created_at'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_contact', @level2type=N'COLUMN',@level2name=N'detail'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_contact', @level2type=N'COLUMN',@level2name=N'subject'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_contact', @level2type=N'COLUMN',@level2name=N'email'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_contact', @level2type=N'COLUMN',@level2name=N'phone'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_contact', @level2type=N'COLUMN',@level2name=N'identification'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_contact', @level2type=N'COLUMN',@level2name=N'last_name'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_contact', @level2type=N'COLUMN',@level2name=N'first_name'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_contact', @level2type=N'COLUMN',@level2name=N'id'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'witness_location_03'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'witness_location_02'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'witness_location_01'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'witness_03'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'witness_02'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'witness_01'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'evidence_location_03'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'evidence_location_02'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'evidence_location_01'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'evidence_03'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'evidence_02'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'evidence_01'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'reported_location_03'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'reported_location_02'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'reported_location_01'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'reported_name_03'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'reported_name_02'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'reported_name_01'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'details'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'place_of_work'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'fax'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'email'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'address'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'phone'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'identification_card'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'last_name'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'first_name'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'id'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_category', @level2type=N'COLUMN',@level2name=N'slug'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_category', @level2type=N'COLUMN',@level2name=N'title'

GO
ALTER TABLE [dbo].[fn_videos] DROP CONSTRAINT [FK__fn_videos__page___6774552F]
GO
ALTER TABLE [dbo].[fn_token] DROP CONSTRAINT [FK__fn_token__user_i__668030F6]
GO
ALTER TABLE [dbo].[fn_static_pages] DROP CONSTRAINT [FK__fn_static__categ__7CA47C3F]
GO
ALTER TABLE [dbo].[fn_static_pages] DROP CONSTRAINT [FK__fn_static__categ__6FE99F9F]
GO
ALTER TABLE [dbo].[fn_static_pages] DROP CONSTRAINT [FK__fn_static__categ__658C0CBD]
GO
ALTER TABLE [dbo].[fn_static_pages] DROP CONSTRAINT [FK__fn_static__categ__4D5F7D71]
GO
ALTER TABLE [dbo].[fn_static_pages] DROP CONSTRAINT [FK__fn_static__categ__2AD55B43]
GO
ALTER TABLE [dbo].[fn_static_pages] DROP CONSTRAINT [FK__fn_static__categ__13BCEBC1]
GO
ALTER TABLE [dbo].[fn_social_account] DROP CONSTRAINT [FK__fn_social__user___6497E884]
GO
ALTER TABLE [dbo].[fn_profile] DROP CONSTRAINT [FK__fn_profil__user___63A3C44B]
GO
ALTER TABLE [dbo].[fn_info] DROP CONSTRAINT [FK__fn_info__page_id__62AFA012]
GO
ALTER TABLE [dbo].[fn_files] DROP CONSTRAINT [FK__fn_files__page_i__61BB7BD9]
GO
ALTER TABLE [dbo].[fn_auth_item_child] DROP CONSTRAINT [FK__fn_auth_i__paren__60C757A0]
GO
ALTER TABLE [dbo].[fn_auth_item_child] DROP CONSTRAINT [FK__fn_auth_i__child__5FD33367]
GO
ALTER TABLE [dbo].[fn_auth_item] DROP CONSTRAINT [FK__fn_auth_i__rule___6A30C649]
GO
ALTER TABLE [dbo].[fn_auth_assignment] DROP CONSTRAINT [FK__fn_auth_a__item___5DEAEAF5]
GO
ALTER TABLE [dbo].[fn_account] DROP CONSTRAINT [FK__fn_accoun__user___5CF6C6BC]
GO
ALTER TABLE [dbo].[fn_videos] DROP CONSTRAINT [DF__fn_videos__publi__22CA2527]
GO
ALTER TABLE [dbo].[fn_user] DROP CONSTRAINT [DF__fn_user__blocked__20E1DCB5]
GO
ALTER TABLE [dbo].[fn_user] DROP CONSTRAINT [DF__fn_user__unconfi__1FEDB87C]
GO
ALTER TABLE [dbo].[fn_user] DROP CONSTRAINT [DF__fn_user__confirm__1EF99443]
GO
ALTER TABLE [dbo].[fn_static_pages] DROP CONSTRAINT [DF__fn_static__has_m__24927208]
GO
ALTER TABLE [dbo].[fn_static_pages] DROP CONSTRAINT [DF__fn_static__has_m__239E4DCF]
GO
ALTER TABLE [dbo].[fn_static_pages] DROP CONSTRAINT [DF__fn_static__has_m__22AA2996]
GO
ALTER TABLE [dbo].[fn_static_pages] DROP CONSTRAINT [DF__fn_static__order__21B6055D]
GO
ALTER TABLE [dbo].[fn_static_pages] DROP CONSTRAINT [DF__fn_static__edita__20C1E124]
GO
ALTER TABLE [dbo].[fn_setting] DROP CONSTRAINT [DF__fn_settin__descr__1940BAED]
GO
ALTER TABLE [dbo].[fn_setting] DROP CONSTRAINT [DF__fn_settin__statu__184C96B4]
GO
ALTER TABLE [dbo].[fn_profile] DROP CONSTRAINT [DF__fn_profile__bio__16644E42]
GO
ALTER TABLE [dbo].[fn_profile] DROP CONSTRAINT [DF__fn_profil__websi__15702A09]
GO
ALTER TABLE [dbo].[fn_profile] DROP CONSTRAINT [DF__fn_profil__locat__147C05D0]
GO
ALTER TABLE [dbo].[fn_profile] DROP CONSTRAINT [DF__fn_profil__grava__1387E197]
GO
ALTER TABLE [dbo].[fn_profile] DROP CONSTRAINT [DF__fn_profil__grava__1293BD5E]
GO
ALTER TABLE [dbo].[fn_profile] DROP CONSTRAINT [DF__fn_profil__publi__119F9925]
GO
ALTER TABLE [dbo].[fn_profile] DROP CONSTRAINT [DF__fn_profile__name__10AB74EC]
GO
ALTER TABLE [dbo].[fn_news] DROP CONSTRAINT [DF__fn_news__publish__0EC32C7A]
GO
ALTER TABLE [dbo].[fn_events] DROP CONSTRAINT [DF__fn_events__hours__09FE775D]
GO
ALTER TABLE [dbo].[fn_auth_rule] DROP CONSTRAINT [DF__fn_auth_ru__data__0AD2A005]
GO
ALTER TABLE [dbo].[fn_auth_rule] DROP CONSTRAINT [DF__fn_auth_r__updat__09DE7BCC]
GO
ALTER TABLE [dbo].[fn_auth_rule] DROP CONSTRAINT [DF__fn_auth_r__creat__08EA5793]
GO
ALTER TABLE [dbo].[fn_auth_item] DROP CONSTRAINT [DF__fn_auth_it__data__060DEAE8]
GO
ALTER TABLE [dbo].[fn_auth_item] DROP CONSTRAINT [DF__fn_auth_i__updat__0519C6AF]
GO
ALTER TABLE [dbo].[fn_auth_item] DROP CONSTRAINT [DF__fn_auth_i__creat__0425A276]
GO
ALTER TABLE [dbo].[fn_auth_item] DROP CONSTRAINT [DF__fn_auth_i__rule___03317E3D]
GO
ALTER TABLE [dbo].[fn_auth_item] DROP CONSTRAINT [DF__fn_auth_i__descr__023D5A04]
GO
ALTER TABLE [dbo].[fn_account] DROP CONSTRAINT [DF__fn_accoun__prope__0169315C]
GO
ALTER TABLE [dbo].[fn_account] DROP CONSTRAINT [DF__fn_accoun__user___00750D23]
GO
/****** Object:  Index [UQ__fn_user__F3DBC5725832119F]    Script Date: 06/04/2018 03:36:38 p.m. ******/
ALTER TABLE [dbo].[fn_user] DROP CONSTRAINT [UQ__fn_user__F3DBC5725832119F]
GO
/****** Object:  Index [UQ__fn_user__AB6E61645555A4F4]    Script Date: 06/04/2018 03:36:38 p.m. ******/
ALTER TABLE [dbo].[fn_user] DROP CONSTRAINT [UQ__fn_user__AB6E61645555A4F4]
GO
/****** Object:  Index [UQ__fn_histo__809A238B3B95D2F1]    Script Date: 06/04/2018 03:36:38 p.m. ******/
ALTER TABLE [dbo].[fn_history] DROP CONSTRAINT [UQ__fn_histo__809A238B3B95D2F1]
GO
/****** Object:  Table [dbo].[fn_videos]    Script Date: 06/04/2018 03:36:38 p.m. ******/
DROP TABLE [dbo].[fn_videos]
GO
/****** Object:  Table [dbo].[fn_user]    Script Date: 06/04/2018 03:36:38 p.m. ******/
DROP TABLE [dbo].[fn_user]
GO
/****** Object:  Table [dbo].[fn_token]    Script Date: 06/04/2018 03:36:38 p.m. ******/
DROP TABLE [dbo].[fn_token]
GO
/****** Object:  Table [dbo].[fn_static_pages]    Script Date: 06/04/2018 03:36:38 p.m. ******/
DROP TABLE [dbo].[fn_static_pages]
GO
/****** Object:  Table [dbo].[fn_social_account]    Script Date: 06/04/2018 03:36:38 p.m. ******/
DROP TABLE [dbo].[fn_social_account]
GO
/****** Object:  Table [dbo].[fn_slider_images]    Script Date: 06/04/2018 03:36:38 p.m. ******/
DROP TABLE [dbo].[fn_slider_images]
GO
/****** Object:  Table [dbo].[fn_setting]    Script Date: 06/04/2018 03:36:38 p.m. ******/
DROP TABLE [dbo].[fn_setting]
GO
/****** Object:  Table [dbo].[fn_profile]    Script Date: 06/04/2018 03:36:38 p.m. ******/
DROP TABLE [dbo].[fn_profile]
GO
/****** Object:  Table [dbo].[fn_news]    Script Date: 06/04/2018 03:36:38 p.m. ******/
DROP TABLE [dbo].[fn_news]
GO
/****** Object:  Table [dbo].[fn_info]    Script Date: 06/04/2018 03:36:38 p.m. ******/
DROP TABLE [dbo].[fn_info]
GO
/****** Object:  Table [dbo].[fn_history]    Script Date: 06/04/2018 03:36:38 p.m. ******/
DROP TABLE [dbo].[fn_history]
GO
/****** Object:  Table [dbo].[fn_files]    Script Date: 06/04/2018 03:36:38 p.m. ******/
DROP TABLE [dbo].[fn_files]
GO
/****** Object:  Table [dbo].[fn_events]    Script Date: 06/04/2018 03:36:38 p.m. ******/
DROP TABLE [dbo].[fn_events]
GO
/****** Object:  Table [dbo].[fn_contact]    Script Date: 06/04/2018 03:36:38 p.m. ******/
DROP TABLE [dbo].[fn_contact]
GO
/****** Object:  Table [dbo].[fn_complaints]    Script Date: 06/04/2018 03:36:38 p.m. ******/
DROP TABLE [dbo].[fn_complaints]
GO
/****** Object:  Table [dbo].[fn_category]    Script Date: 06/04/2018 03:36:38 p.m. ******/
DROP TABLE [dbo].[fn_category]
GO
/****** Object:  Table [dbo].[fn_auth_rule]    Script Date: 06/04/2018 03:36:38 p.m. ******/
DROP TABLE [dbo].[fn_auth_rule]
GO
/****** Object:  Table [dbo].[fn_auth_item_child]    Script Date: 06/04/2018 03:36:38 p.m. ******/
DROP TABLE [dbo].[fn_auth_item_child]
GO
/****** Object:  Table [dbo].[fn_auth_item]    Script Date: 06/04/2018 03:36:38 p.m. ******/
DROP TABLE [dbo].[fn_auth_item]
GO
/****** Object:  Table [dbo].[fn_auth_assignment]    Script Date: 06/04/2018 03:36:38 p.m. ******/
DROP TABLE [dbo].[fn_auth_assignment]
GO
/****** Object:  Table [dbo].[fn_account]    Script Date: 06/04/2018 03:36:38 p.m. ******/
DROP TABLE [dbo].[fn_account]
GO
/****** Object:  FullTextCatalog [nombre]    Script Date: 06/04/2018 03:36:38 p.m. ******/
GO
DROP FULLTEXT CATALOG [nombre]
GO
/****** Object:  FullTextCatalog [nombre]    Script Date: 06/04/2018 03:36:38 p.m. ******/
CREATE FULLTEXT CATALOG [nombre]WITH ACCENT_SENSITIVITY = ON

GO
/****** Object:  Table [dbo].[fn_account]    Script Date: 06/04/2018 03:36:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fn_account](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NULL,
	[provider] [nvarchar](255) NOT NULL,
	[client_id] [nvarchar](255) NOT NULL,
	[properties] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[fn_auth_assignment]    Script Date: 06/04/2018 03:36:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fn_auth_assignment](
	[item_name] [nvarchar](64) NOT NULL,
	[user_id] [nvarchar](64) NOT NULL,
	[created_at] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[item_name] ASC,
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[fn_auth_item]    Script Date: 06/04/2018 03:36:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fn_auth_item](
	[name] [nvarchar](64) NOT NULL,
	[type] [smallint] NOT NULL,
	[description] [varchar](max) NULL,
	[rule_name] [nvarchar](64) NULL,
	[created_at] [int] NULL,
	[updated_at] [int] NULL,
	[data] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[fn_auth_item_child]    Script Date: 06/04/2018 03:36:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fn_auth_item_child](
	[parent] [nvarchar](64) NOT NULL,
	[child] [nvarchar](64) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[parent] ASC,
	[child] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[fn_auth_rule]    Script Date: 06/04/2018 03:36:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fn_auth_rule](
	[name] [nvarchar](64) NOT NULL,
	[created_at] [int] NULL,
	[updated_at] [int] NULL,
	[data] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[fn_category]    Script Date: 06/04/2018 03:36:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fn_category](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](255) NULL,
	[slug] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[fn_complaints]    Script Date: 06/04/2018 03:36:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fn_complaints](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[first_name] [varchar](255) NOT NULL,
	[last_name] [varchar](255) NOT NULL,
	[identification_card] [varchar](40) NOT NULL,
	[phone] [varchar](50) NOT NULL,
	[address] [varchar](500) NOT NULL,
	[email] [varchar](150) NOT NULL,
	[fax] [varchar](150) NULL,
	[place_of_work] [varchar](500) NULL,
	[details] [varchar](2500) NOT NULL,
	[reported_name_01] [varchar](500) NOT NULL,
	[reported_name_02] [varchar](500) NULL,
	[reported_name_03] [varchar](500) NULL,
	[reported_location_01] [varchar](500) NOT NULL,
	[reported_location_02] [varchar](500) NULL,
	[reported_location_03] [varchar](500) NULL,
	[evidence_01] [varchar](2000) NOT NULL,
	[evidence_02] [varchar](2000) NULL,
	[evidence_03] [varchar](2000) NULL,
	[evidence_location_01] [varchar](500) NOT NULL,
	[evidence_location_02] [varchar](500) NULL,
	[evidence_location_03] [varchar](500) NULL,
	[witness_01] [varchar](500) NOT NULL,
	[witness_02] [varchar](500) NULL,
	[witness_03] [varchar](500) NULL,
	[witness_location_01] [varchar](500) NOT NULL,
	[witness_location_02] [varchar](500) NULL,
	[witness_location_03] [varchar](500) NULL,
	[created_at] [int] NULL,
	[updated_at] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[fn_contact]    Script Date: 06/04/2018 03:36:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fn_contact](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[first_name] [varchar](255) NOT NULL,
	[last_name] [varchar](255) NOT NULL,
	[identification] [varchar](255) NOT NULL,
	[phone] [varchar](255) NULL,
	[email] [varchar](255) NOT NULL,
	[subject] [varchar](500) NOT NULL,
	[detail] [varchar](2500) NOT NULL,
	[created_at] [int] NULL,
	[updated_at] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[fn_events]    Script Date: 06/04/2018 03:36:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fn_events](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](500) NOT NULL,
	[description] [varchar](2000) NOT NULL,
	[updated_at] [int] NULL,
	[created_at] [int] NULL,
	[end_dt] [datetime2](7) NULL,
	[start_dt] [datetime2](7) NULL,
	[hours] [int] NULL,
	[date] [date] NULL,
	[time] [varchar](35) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[fn_files]    Script Date: 06/04/2018 03:36:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fn_files](
	[page_id] [int] NULL,
	[title] [varchar](500) NULL,
	[description] [varchar](2500) NULL,
	[file_url] [varchar](500) NULL,
	[hit_counter] [int] NULL,
	[download_counter] [int] NULL,
	[created_at] [int] NULL,
	[updated_at] [int] NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[file_date] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[fn_history]    Script Date: 06/04/2018 03:36:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fn_history](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[year] [varchar](4) NULL,
	[title] [varchar](500) NULL,
	[content] [varchar](5000) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[fn_info]    Script Date: 06/04/2018 03:36:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fn_info](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[page_id] [int] NULL,
	[title] [varchar](500) NULL,
	[content] [varchar](5000) NULL,
	[excerpt] [varchar](450) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[fn_news]    Script Date: 06/04/2018 03:36:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fn_news](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](500) NOT NULL,
	[slug] [varchar](500) NOT NULL,
	[extract] [varchar](550) NULL,
	[content] [nvarchar](max) NULL,
	[author_name] [varchar](255) NULL,
	[url_imagen] [varchar](500) NULL,
	[updated_at] [int] NULL,
	[created_at] [int] NULL,
	[published] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[fn_profile]    Script Date: 06/04/2018 03:36:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fn_profile](
	[user_id] [int] NOT NULL,
	[name] [nvarchar](255) NULL,
	[public_email] [nvarchar](255) NULL,
	[gravatar_email] [nvarchar](255) NULL,
	[gravatar_id] [nvarchar](32) NULL,
	[location] [nvarchar](255) NULL,
	[website] [nvarchar](255) NULL,
	[bio] [nvarchar](max) NULL,
	[timezone] [nvarchar](40) NULL,
PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[fn_setting]    Script Date: 06/04/2018 03:36:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fn_setting](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[type] [nvarchar](10) NOT NULL,
	[section] [nvarchar](255) NOT NULL,
	[key] [nvarchar](255) NOT NULL,
	[value] [nvarchar](max) NOT NULL,
	[status] [smallint] NOT NULL,
	[description] [nvarchar](255) NULL,
	[created_at] [int] NOT NULL,
	[updated_at] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[fn_slider_images]    Script Date: 06/04/2018 03:36:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fn_slider_images](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[image_url] [varchar](500) NOT NULL,
	[image_link] [varchar](500) NOT NULL,
	[slide_text] [varchar](500) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[fn_social_account]    Script Date: 06/04/2018 03:36:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fn_social_account](
	[id] [int] NOT NULL,
	[user_id] [int] NULL,
	[provider] [nvarchar](255) NULL,
	[client_id] [nvarchar](255) NULL,
	[data] [nvarchar](max) NULL,
	[code] [nvarchar](32) NULL,
	[created_at] [int] NULL,
	[email] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[fn_static_pages]    Script Date: 06/04/2018 03:36:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fn_static_pages](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[slug] [varchar](255) NOT NULL,
	[title] [varchar](255) NOT NULL,
	[excerpt] [varchar](500) NULL,
	[content] [nvarchar](max) NULL,
	[created_at] [int] NULL,
	[updated_at] [int] NULL,
	[category_id] [int] NULL,
	[editable] [bit] NULL,
	[icon] [varchar](32) NULL,
	[order] [int] NOT NULL,
	[has_md_documents] [bit] NOT NULL,
	[has_md_video] [bit] NOT NULL,
	[has_md_info] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[fn_token]    Script Date: 06/04/2018 03:36:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fn_token](
	[user_id] [int] NOT NULL,
	[code] [nvarchar](32) NOT NULL,
	[created_at] [int] NULL,
	[type] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[user_id] ASC,
	[code] ASC,
	[type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[fn_user]    Script Date: 06/04/2018 03:36:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fn_user](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](255) NOT NULL,
	[email] [nvarchar](255) NOT NULL,
	[password_hash] [nvarchar](60) NOT NULL,
	[auth_key] [nvarchar](32) NOT NULL,
	[confirmed_at] [int] NULL,
	[unconfirmed_email] [nvarchar](255) NULL,
	[blocked_at] [int] NULL,
	[created_at] [int] NOT NULL,
	[updated_at] [int] NOT NULL,
	[registration_ip] [nvarchar](45) NULL,
	[last_login_at] [int] NULL,
	[flags] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[fn_videos]    Script Date: 06/04/2018 03:36:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fn_videos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](500) NOT NULL,
	[slug] [varchar](500) NOT NULL,
	[extract] [varchar](550) NULL,
	[content] [nvarchar](max) NULL,
	[author_name] [varchar](255) NULL,
	[url_video] [varchar](500) NULL,
	[published] [bit] NOT NULL,
	[created_at] [int] NULL,
	[updated_at] [int] NULL,
	[page_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[fn_auth_assignment] ([item_name], [user_id], [created_at]) VALUES (N'ADMIN', N'12', 1520784717)
INSERT [dbo].[fn_auth_assignment] ([item_name], [user_id], [created_at]) VALUES (N'ADMIN', N'16', 1521583649)
INSERT [dbo].[fn_auth_assignment] ([item_name], [user_id], [created_at]) VALUES (N'SUPERADMIN', N'13', 1520784349)
INSERT [dbo].[fn_auth_item] ([name], [type], [description], [rule_name], [created_at], [updated_at], [data]) VALUES (N'ADMIN', 1, N'', NULL, 1519917471, 1522887799, NULL)
INSERT [dbo].[fn_auth_item] ([name], [type], [description], [rule_name], [created_at], [updated_at], [data]) VALUES (N'manageCategory', 2, N'Gestión de  categorías', NULL, 1520965935, 1520966007, NULL)
INSERT [dbo].[fn_auth_item] ([name], [type], [description], [rule_name], [created_at], [updated_at], [data]) VALUES (N'manageStaticPages', 2, N'Gestión de Páginas Estáticas', NULL, 1520965990, 1520965990, NULL)
INSERT [dbo].[fn_auth_item] ([name], [type], [description], [rule_name], [created_at], [updated_at], [data]) VALUES (N'SUPERADMIN', 1, N'Acceso a toda la administración', NULL, 1519917448, 1522881792, NULL)
INSERT [dbo].[fn_auth_item_child] ([parent], [child]) VALUES (N'ADMIN', N'manageCategory')
INSERT [dbo].[fn_auth_item_child] ([parent], [child]) VALUES (N'ADMIN', N'manageStaticPages')
INSERT [dbo].[fn_auth_item_child] ([parent], [child]) VALUES (N'SUPERADMIN', N'manageCategory')
INSERT [dbo].[fn_auth_item_child] ([parent], [child]) VALUES (N'SUPERADMIN', N'manageStaticPages')
SET IDENTITY_INSERT [dbo].[fn_category] ON 

INSERT [dbo].[fn_category] ([id], [title], [slug]) VALUES (1, N'Centro Educativo', N'centro-educativo')
INSERT [dbo].[fn_category] ([id], [title], [slug]) VALUES (2, N'Universidad', N'universidad')
INSERT [dbo].[fn_category] ([id], [title], [slug]) VALUES (3, N'Educación abierta', N'educacion-abierta')
INSERT [dbo].[fn_category] ([id], [title], [slug]) VALUES (4, N'Noticias', N'noticias')
INSERT [dbo].[fn_category] ([id], [title], [slug]) VALUES (5, N'Transparencia', N'transparencia')
INSERT [dbo].[fn_category] ([id], [title], [slug]) VALUES (6, N'FONABE', N'fonabe')
INSERT [dbo].[fn_category] ([id], [title], [slug]) VALUES (7, N'Contáctenos', N'contactenos')
INSERT [dbo].[fn_category] ([id], [title], [slug]) VALUES (8, N'Principal', N'principal')
INSERT [dbo].[fn_category] ([id], [title], [slug]) VALUES (9, N'Otros', N'pagina')
SET IDENTITY_INSERT [dbo].[fn_category] OFF
SET IDENTITY_INSERT [dbo].[fn_events] ON 

INSERT [dbo].[fn_events] ([id], [title], [description], [updated_at], [created_at], [end_dt], [start_dt], [hours], [date], [time]) VALUES (22, N'Apertura del sistema de citas', N'Apertura del sistema de citas para los meses de febrero, marzo y abril.', NULL, NULL, NULL, NULL, NULL, CAST(0xCB3D0B00 AS Date), N'03:30')
INSERT [dbo].[fn_events] ([id], [title], [description], [updated_at], [created_at], [end_dt], [start_dt], [hours], [date], [time]) VALUES (23, N'Inicia recepcion de solicitudes nuevas de beca (periodo ordinario)', N' Recepción de solicitudes nuevas a becas (periodo ordinario)', NULL, NULL, NULL, NULL, NULL, CAST(0xBD3D0B00 AS Date), N'04:00')
INSERT [dbo].[fn_events] ([id], [title], [description], [updated_at], [created_at], [end_dt], [start_dt], [hours], [date], [time]) VALUES (25, N'Proceso de recepcion de listados de postulantes 2017', N'Proceso de  recepción de los listados de postulantes 2017 enviados por FONABE a los Centros Educativos', NULL, NULL, NULL, NULL, NULL, CAST(0xE73D0B00 AS Date), N'04:00')
INSERT [dbo].[fn_events] ([id], [title], [description], [updated_at], [created_at], [end_dt], [start_dt], [hours], [date], [time]) VALUES (26, N'Inicia el proceso de recepción de prórroga de preescolar y primaria.', N'Inicia el proceso de recepción de prórroga de preescolar y primaria.', NULL, NULL, NULL, NULL, NULL, CAST(0xE73D0B00 AS Date), N'04:00')
INSERT [dbo].[fn_events] ([id], [title], [description], [updated_at], [created_at], [end_dt], [start_dt], [hours], [date], [time]) VALUES (27, N'Inicia el proceso de seguimiento y control', N'Inicia el proceso de seguimiento y control', NULL, NULL, NULL, NULL, NULL, CAST(0xF53D0B00 AS Date), N'04:00')
INSERT [dbo].[fn_events] ([id], [title], [description], [updated_at], [created_at], [end_dt], [start_dt], [hours], [date], [time]) VALUES (28, N'Finaliza el proceso de recepción de  los listados de postulantes 2017 enviados por FONABE a los Centros Educativos', N'Finaliza el proceso de recepción de  los listados de postulantes 2017 enviados por FONABE a los Centros Educativos', NULL, NULL, NULL, NULL, NULL, CAST(0xFC3D0B00 AS Date), N'04:00')
INSERT [dbo].[fn_events] ([id], [title], [description], [updated_at], [created_at], [end_dt], [start_dt], [hours], [date], [time]) VALUES (29, N'Inicia el primer proceso de corroboración en línea', N'Inicia el primer proceso de corroboración en línea', NULL, NULL, NULL, NULL, NULL, CAST(0xFC3D0B00 AS Date), N'04:00')
INSERT [dbo].[fn_events] ([id], [title], [description], [updated_at], [created_at], [end_dt], [start_dt], [hours], [date], [time]) VALUES (30, N'Capacitación a Comité de Becas en FONABE ', N'Capacitación a Comité de Becas en FONABE ', NULL, NULL, NULL, NULL, NULL, CAST(0xFE3D0B00 AS Date), N'04:00')
INSERT [dbo].[fn_events] ([id], [title], [description], [updated_at], [created_at], [end_dt], [start_dt], [hours], [date], [time]) VALUES (32, N'Inicia el  proceso de capacitación a los Comités de Becas en las Direcciones Regionales', N'Inicia el  proceso de capacitación a los Comités de Becas en las Direcciones Regionales', NULL, NULL, NULL, NULL, NULL, CAST(0x183E0B00 AS Date), N'04:00')
INSERT [dbo].[fn_events] ([id], [title], [description], [updated_at], [created_at], [end_dt], [start_dt], [hours], [date], [time]) VALUES (33, N'Finaliza el primer proceso de corroboración en línea.', N'Finaliza el primer proceso de corroboración en línea.', NULL, NULL, NULL, NULL, NULL, CAST(0x1C3E0B00 AS Date), N'04:00')
INSERT [dbo].[fn_events] ([id], [title], [description], [updated_at], [created_at], [end_dt], [start_dt], [hours], [date], [time]) VALUES (34, N'Apertura del sistema de citas para los meses de mayo, junio y julio.', N'Apertura del sistema de citas para los meses de mayo, junio y julio.', NULL, NULL, NULL, NULL, NULL, CAST(0x263E0B00 AS Date), N'04:00')
INSERT [dbo].[fn_events] ([id], [title], [description], [updated_at], [created_at], [end_dt], [start_dt], [hours], [date], [time]) VALUES (35, N'Capacitación a Comité de Becas en FONABE', N'Capacitación a Comité de Becas en FONABE', NULL, NULL, NULL, NULL, NULL, CAST(0x283E0B00 AS Date), N'04:00')
INSERT [dbo].[fn_events] ([id], [title], [description], [updated_at], [created_at], [end_dt], [start_dt], [hours], [date], [time]) VALUES (36, N' Finaliza el proceso de recepción de las prórrogas de Preescolar y Primaria', N'
Finaliza el proceso de recepción de las prórrogas de Preescolar y Primaria
', NULL, NULL, NULL, NULL, NULL, CAST(0x2D3E0B00 AS Date), N'04:00')
INSERT [dbo].[fn_events] ([id], [title], [description], [updated_at], [created_at], [end_dt], [start_dt], [hours], [date], [time]) VALUES (37, N'Finaliza la recepción de solicitudes de casos nuevos (período ordinario)', N'Finaliza la recepción de solicitudes de casos nuevos (período ordinario)', NULL, NULL, NULL, NULL, NULL, CAST(0x2D3E0B00 AS Date), N'04:00')
INSERT [dbo].[fn_events] ([id], [title], [description], [updated_at], [created_at], [end_dt], [start_dt], [hours], [date], [time]) VALUES (38, N'Recepción de solicitudes nuevas a becas (periodo extraordinario)', N'Recepción de solicitudes nuevas a becas (periodo extraordinario)', NULL, NULL, NULL, NULL, NULL, CAST(0x323E0B00 AS Date), N'04:00')
INSERT [dbo].[fn_events] ([id], [title], [description], [updated_at], [created_at], [end_dt], [start_dt], [hours], [date], [time]) VALUES (39, N'Capacitación a Comité de Becas en FONABE ', N'Capacitación a Comité de Becas en FONABE ', NULL, NULL, NULL, NULL, NULL, CAST(0x3D3E0B00 AS Date), N'04:00')
INSERT [dbo].[fn_events] ([id], [title], [description], [updated_at], [created_at], [end_dt], [start_dt], [hours], [date], [time]) VALUES (40, N'Capacitación a Comité de Becas en FONABE ', N'Capacitación a Comité de Becas en FONABE ', NULL, NULL, NULL, NULL, NULL, CAST(0x4B3E0B00 AS Date), N'04:00')
INSERT [dbo].[fn_events] ([id], [title], [description], [updated_at], [created_at], [end_dt], [start_dt], [hours], [date], [time]) VALUES (41, N'Capacitación a Comité de Becas en FONABE ', N'Capacitación a Comité de Becas en FONABE ', NULL, NULL, NULL, NULL, NULL, CAST(0x593E0B00 AS Date), N'04:00')
INSERT [dbo].[fn_events] ([id], [title], [description], [updated_at], [created_at], [end_dt], [start_dt], [hours], [date], [time]) VALUES (42, N'Capacitación a Comité de Becas en FONABE ', N'Capacitación a Comité de Becas en FONABE ', NULL, NULL, NULL, NULL, NULL, CAST(0x673E0B00 AS Date), N'04:00')
SET IDENTITY_INSERT [dbo].[fn_events] OFF
SET IDENTITY_INSERT [dbo].[fn_files] ON 

INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-002-2016 Niños Trabajadores', N'Información  sobre  el  proceso  de  verificación  de  matrícula  para  el  producto Niños, niñas, adolescentes y jóvenes Trabajadores 2016 .', N'5abaad0f6790b2.72713490.pdf', NULL, NULL, 1522183439, 1522183439, 23, CAST(0xEF3A0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-003-2016  Trámite prórroga 2016 producto Merito', N'Trámite  de  prórroga  2016  de  becas  producto  Mérito  Personal y  sus Distinciones', N'5abaadafd68b00.03058973.pdf', NULL, NULL, 1522183599, 1522183599, 24, CAST(0x013B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-005-2016  Prórroga becas madres, padres y adolescentes', N'Prórroga  de  las  becas  del  Programa  Adolescentes  y  Jóvenes  Madre - Padre menor 21 años.', N'5abaae4f818324.01636224.pdf', NULL, NULL, 1522183759, 1522183759, 25, CAST(0x013B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-006-2016 Formato de constancias de matricula', N'Formato de  Constancias de Matrícula  ( trámites ante  F ONABE )', N'5abaaeaed51432.40822719.pdf', NULL, NULL, 1522183854, 1522183854, 26, CAST(0x1D3B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-007-2016 Notificación del Acta de Comunicacion', N'Acta de Comunicación previa a suspensión de  Beneficio FONABE', N'5abaaef51a0ca8.18739177.pdf', NULL, NULL, 1522183925, 1522183925, 27, CAST(0x423B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-008-2016 Recepción casos nuevos Indigenas', N'', N'5abaaf54e9c1d3.09025571.pdf', NULL, NULL, 1522184020, 1522184020, 28, CAST(0x483B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-009-2016  Gestion becas Madres y Postsecundaria', N'Gestión de Becas Madre Adolescente y Post secundaria', N'5abaaf89945039.41211633.pdf', NULL, NULL, 1522184073, 1522184073, 29, CAST(0x593B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-011-2016 Información sobre el proceso de verificación de matrícula para el producto ', N'Información  sobre  el  proceso  de  verificación  de  matrícula  para  el  producto Niños, niñas, adolescentes y jóvenes Trabajadores 2016 .', N'5abab055be9103.69292867.pdf', NULL, NULL, 1522184277, 1522184277, 30, CAST(0x663B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-013-2016  Gestion becas Madres  Padres 2016', N'Gestión de Becas Madre /Padre Adolescente  2016', N'5abab094eda055.36672504.pdf', NULL, NULL, 1522184340, 1522184340, 31, CAST(0x723B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-014-2016  Gestión becas Madres  Padres DRE Guápiles 2016', N'Gestión de Becas Madre /Padre Adolescente ', N'5abab0e9d42644.86696566.pdf', NULL, NULL, 1522184425, 1522184425, 32, CAST(0x743B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-015-2016  Gestion becas Madres  Padres DRE Limón 2016', N'Gestión de Becas Madre /Padre Adolescente ', N'5abab175d896a7.44658895.pdf', NULL, NULL, 1522184565, 1522184565, 33, CAST(0x743B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-016-2016  Gestion becas Madres  Padres DRE Norte Norte 2016', N'Gestión de Becas Madre /Padre Adolescente ', N'5abab1ef7fcbd8.70884459.pdf', NULL, NULL, 1522184687, 1522184687, 34, CAST(0x743B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-017-2016  Gestion becas Madres  Padres DRE Sula 2016', N'Gestión de Becas Madre /Padre Adolescente ', N'5abab2285539a7.76181168.pdf', NULL, NULL, 1522184744, 1522184744, 35, CAST(0x743B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-018-2016  Actualización de Formularios en DRE Aguirre', N'Actualización de expedientes  – Ingreso por nomina -', N'5abab2c6832397.24875010.pdf', NULL, NULL, 1522184902, 1522184902, 36, CAST(0x853B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-019-2016  Actualización de Formularios en DRE Limón', N'Actualización de expedientes  – Ingreso por nomina -', N'5abab30ed32fe5.83017879.pdf', NULL, NULL, 1522184974, 1522184974, 37, CAST(0x853B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-020-2016  Actualización de Formularios en DRE Santos', N'Actualización de expedientes  - Ingreso por nomina -', N'5abbc6fa299d56.23777059.pdf', NULL, NULL, 1522255610, 1522255610, 38, CAST(0x7B3B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-021-2016  Actualización de Formularios en DRE Puntarenas', N'Actualización de expedientes  - Ingreso por nomina -', N'5abbc74c5f0556.96391097.pdf', NULL, NULL, 1522255692, 1522255692, 39, CAST(0x7B3B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-023-2016  Procedimiento proyecto Puente al desarrollo', N'Recordatorio del  Procedimiento del proyecto Puente al Desarrollo (Primaria)', N'5abbc796674de6.37243434.pdf', NULL, NULL, 1522255766, 1522255766, 40, CAST(0x9F3B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-024-2016  Gestion becas Madres  Padres DRE Limón 2016', N'Gestión de Becas Madre /Padre Adolescente ', N'5abbc7e9307743.59897196.pdf', NULL, NULL, 1522255849, 1522255849, 41, CAST(0xB73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-025-2016  Gestion becas Madres  Padres DRE Puntarenas 2016', N'Gestión de Becas Madre /Padre Adolescente ', N'5abbc867ea9513.82691152.pdf', NULL, NULL, 1522255975, 1522255975, 42, CAST(0xB73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-026-2016  Gestion becas Madres  Padres DRE Aguirres 2016', N'Gestión de Becas Madre /Padre Adolescente ', N'5abbc8b633ae88.62968849.pdf', NULL, NULL, 1522256054, 1522256054, 43, CAST(0xB73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-027-2016 DRE Alajuela Corroboración en Linea', N'Proceso de Corroboración Digital 2016', N'5abbc90c0128e1.66294723.pdf', NULL, NULL, 1522256140, 1522256140, 44, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-028-2016 DRE Aguirre Corroboración en Linea', N'Proceso de Corroboración Digital 2016', N'5abbc979845005.69451722.pdf', NULL, NULL, 1522256249, 1522256249, 45, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-029-2016 DRE Cañas Corroboración en Linea', N'Proceso de Corroboración Digital 2016', N'5abbd3e682c788.69424579.pdf', NULL, NULL, 1522258918, 1522258918, 46, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-030-2016 DRE Cartago Corroboración en Linea', N'Proceso de Corroboración Digital 2016', N'5abbd46f1db723.47239218.pdf', NULL, NULL, 1522259055, 1522259055, 47, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-031-2016 DRE Grande de Térraba Corroboración en Linea', N'Proceso de Corroboración Digital 2016', N'5abbd4d05f6597.31314174.pdf', NULL, NULL, 1522259152, 1522259152, 48, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-032-2016 DRE Coto Corroboración en Linea', N'Proceso de Corroboración Digital 2016', N'5abbd51b6996a4.05686700.pdf', NULL, NULL, 1522259227, 1522259227, 49, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-033-2016 DRE San Carlos Corroboración en Linea', N'Proceso de Corroboración Digital 2016', N'5abbd8f2e9db60.16680006.pdf', NULL, NULL, 1522260210, 1522260210, 50, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-034-2016 DRE Limón Corroboración en Linea', N'Proceso de Corroboración Digital 2016', N'5abbd9c8ac1dd9.38567291.pdf', NULL, NULL, 1522260424, 1522260424, 51, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-035-2016 DRE Occidente Corroboración en Linea', N'Proceso de Corroboración Digital 2016', N'5abbda2d891030.89988629.pdf', NULL, NULL, 1522260525, 1522260525, 52, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-036-2016 DRE Nicoya Corroboración en Linea', N'Proceso de Corroboración Digital 2016', N'5abbdc5bde3004.19082740.pdf', NULL, NULL, 1522261083, 1522261083, 53, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-037-2016 DRE Puntarenas Corroboración en Linea', N'Proceso de Corroboración Digital 2016', N'5abc08cf5f1802.07767129.pdf', NULL, NULL, 1522272463, 1522272463, 56, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-038-2016 DRE Pérez Zeledón Corroboración en Linea', N'Proceso de Corroboración Digital 2016', N'5abc090699a2b5.00269265.pdf', NULL, NULL, 1522272518, 1522272518, 57, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-039-2016 DRE Zona Norte-Norte Corroboración en Linea', N'Proceso de Corroboración Digital 2016', N'5abc09436b3cb7.71807983.pdf', NULL, NULL, 1522272579, 1522272579, 58, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-040-2016 DRE Sula Corroboración en Linea', N'Proceso de Corroboración Digital 2016', N'5abc0975996668.75772672.pdf', NULL, NULL, 1522272629, 1522272629, 59, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-041-2016 DRE Santa Cruz Corroboración en Linea', N'Proceso de Corroboración Digital 2016', N'5abc0a03b39808.46190396.pdf', NULL, NULL, 1522272771, 1522272771, 60, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-042-2016 DRE Liberia Corroboración en Linea', N'Proceso de Corroboración Digital 2016', N'5abc0a208d4560.50781584.pdf', NULL, NULL, 1522272800, 1522272800, 61, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-043-2016 DRE Turrialba Corroboración en Linea', N'Proceso de Corroboración Digital 2016', N'5abc0ad27c2168.54565969.pdf', NULL, NULL, 1522272978, 1522272978, 62, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-044-2016 DRE Heredia Corroboración en Linea', N'Proceso de Corroboración Digital 2016', N'5abc0b005862c6.58460866.pdf', NULL, NULL, 1522273024, 1522273024, 63, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-045-2016 DRE Desamparadosa Corroboración en Linea', N'Proceso de Corroboración Digital 2016', N'5abc0b3aa08f80.56269680.pdf', NULL, NULL, 1522273082, 1522273082, 64, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-046-2016 DRE Puriscal Corroboración en Linea', N'Proceso de Corroboración Digital 2016', N'5abc0bbd4c96d5.35600852.pdf', NULL, NULL, 1522273213, 1522273213, 65, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-047-2016 DRE San José Norte Corroboración en Linea', N'Proceso de Corroboración Digital 2016', N'5abc0bef6cb770.84372511.pdf', NULL, NULL, 1522273263, 1522273263, 66, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-048-2016 DRE San José Central Corroboración en Linea', N'Proceso de Corroboración Digital 2016', N'5abc0c20d30c38.61701338.pdf', NULL, NULL, 1522273312, 1522273312, 67, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-049-2016 DRE Guápilese Corroboración en Linea', N'Proceso de Corroboración Digital 2016', N'5abc0c4cbefe82.40873156.pdf', NULL, NULL, 1522273356, 1522273356, 68, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-050-2016 DRE San José Oeste Corroboración en Linea', N'Proceso de Corroboración Digital 2016', N'5abc0c76eaa9a4.98514983.pdf', NULL, NULL, 1522273398, 1522273398, 69, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-051-2016 DRE Peninsular Corroboración en Linea', N'Proceso de Corroboración Digital 2016', N'5abc0ca41deff1.61264181.pdf', NULL, NULL, 1522273444, 1522273444, 70, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-052-2016 DRE Sarapiqui Corroboración en Linea', N'Proceso de Corroboración Digital 2016', N'5abc0ccf24f2d7.46641122.pdf', NULL, NULL, 1522273487, 1522273487, 71, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-053-2016  Proceso de corroborac. digital 2016', N'Proceso de Corroboración Digital 2016', N'5abc0d065430f4.27554614.pdf', NULL, NULL, 1522273542, 1522273542, 72, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-054-2016  Proceso de corroborac. digital 2016', N'Proceso de Corroboración Digital 2016', N'5abc0d3491a0b3.62494935.pdf', NULL, NULL, 1522273588, 1522273588, 73, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-055-2016  Proceso de corrobora. digital 2016', N'Proceso de Corroboración Digital 2016', N'5abc0d60ef6268.50385711.pdf', NULL, NULL, 1522273632, 1522273632, 74, CAST(0xC73B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-056-2016 Asesores, Jefaturas y Comites Circuitales Capacitación Corroboración en Lin', N'Capacitación sobre la Corroboración Digital FONABE 2016', N'5abc0db10c8865.72097831.pdf', NULL, NULL, 1522273713, 1522273713, 75, CAST(0xCC3B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-057-2016 Devolución de expedientes', N'Devolución de Expedientes', N'5abc0ed073d842.21534877.pdf', NULL, NULL, 1522274000, 1522274000, 76, CAST(0xD63B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-058-2016  Corroboración en Línea', N'Proceso de Corroboración Digital 2016', N'5abc0f2f3b4123.37383458.pdf', NULL, NULL, 1522274095, 1522274095, 77, CAST(0xE93B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-059-2016  Gestion Madre Adolescente', N'Gestión de Becas Adolescente Madre o Padre', N'5abc0f73ca3d28.58033693.pdf', NULL, NULL, 1522274163, 1522274163, 78, CAST(0xF03B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-060-2016  Capacitación Proyecto Mesoamericano', N'Asesoramiento para la  Gestión de Becas Adolescente Madre o Padre', N'5abc0ff4d5e346.44872866.pdf', NULL, NULL, 1522274292, 1522274292, 79, CAST(0x0D3C0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-061-2016 Verificación de matricula 2017 de postulantes 2016', N'Verificación de Matrícula  201 7 de Postulantes 2016', N'5abc103a310695.82722223.pdf', NULL, NULL, 1522274362, 1522274362, 80, CAST(0x163C0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (5, N'7F03  Acta de Comunicaciòn', N'', N'5abc15a6da8cf6.55749929.pdf', NULL, NULL, 1522275750, 1522275750, 81, CAST(0x1A3C0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (5, N'7F04 Contrato de beca', N'', N'5abeeabc842128.72867441.pdf', 1, NULL, 1522460908, 1522703507, 82, CAST(0x873D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (5, N'7F10 Listado de suspensiones', N'', N'5abee99390ba26.56462161.docx', NULL, NULL, 1522461075, 1522461075, 83, CAST(0x873D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (5, N'7F21 Boleta de justificación de estudiantes reprobados', N'', N'5abee9e6be0db6.37357047.docx', NULL, NULL, 1522461158, 1522461158, 84, CAST(0x873D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (5, N'formato_renuncia_beca', N'', N'5abeea43077552.15608059.pdf', NULL, NULL, 1522461251, 1522461251, 86, CAST(0x873D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (5, N'Formulario 7F02 Solicitud de Beca Ant', N'', N'5abeea71c29f39.47180339.pdf', NULL, NULL, 1522461297, 1522461297, 87, CAST(0x873D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-001-2017 Formato de Formularios para Casos Nuevos 7F02', N'', N'5abef7ed43ad51.03997634.pdf', NULL, NULL, 1522464749, 1522464749, 88, CAST(0x4C3C0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-002-2017 Formato de Matricula/Circular', N'', N'5abf0226b7a609.60634711.pdf', NULL, NULL, 1522467366, 1522467366, 89, CAST(0x4C3C0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-003-2017 Información sobre tarjetas prepago', N'', N'5abf0295d38f60.90043161.pdf', NULL, NULL, 1522467477, 1522467477, 90, CAST(0x4C3C0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-004-2017 Devolución de Expedientes de beneficiarios y egresados.', N'', N'5abf032ab75889.95345181.pdf', NULL, NULL, 1522467626, 1522467626, 91, CAST(0x513C0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-006-2017 Capacitación Zona de Guanacaste', N'', N'5abf03a2136e41.98640883.pdf', NULL, NULL, 1522467746, 1522467746, 92, CAST(0x683C0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-007-2017 Capacitación Zona Norte-Norte', N'', N'5abf041d7e6992.65948219.pdf', NULL, NULL, 1522467869, 1522467869, 93, CAST(0x693C0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular DGB-008-2017 Proceso de Prórroga Madre Padre Adolescente', N'', N'5abf04c98f24d5.43026298.pdf', NULL, NULL, 1522468041, 1522468041, 94, CAST(0x693C0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular FONABE-CIR-DGB-001-2018 Prórroga de Mérito Personal 2018', N'Trámite  de  prórroga  de  becas  201 8 :  P roducto  Mérito  Personal  y  sus  Distinciones', N'5ac203d57bda50.85621570.pdf', NULL, NULL, 1522664405, 1522664405, 95, CAST(0xBD3D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular FONABE-DE-DGB-005-2018 Devolución de expedientes', N'Devolución de Expedientes de  postulantes,  beneficiarios y egresados', N'5ac20429ed54b5.21686732.pdf', NULL, NULL, 1522664489, 1522664489, 96, CAST(0xCB3D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular FONABE-DE-DGB-007-2018 Información sobre tarjetas prepago', N'Información sobre el tarjetas prepago  sin retirar', N'5ac204706240c5.10897398.pdf', NULL, NULL, 1522664560, 1522664560, 97, CAST(0xCE3D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular FONABE-DE-DGB-008-2018 Prórroga estudiantes con necesidades educativas especiales que cu', N'Prórroga  estudiantes  con  necesidades  educativas  especiales  que  cursarán  sétimo grado en el 2018', N'5ac204b6834c63.06652613.pdf', NULL, NULL, 1522664630, 1522664630, 98, CAST(0xCF3D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular FONABE-DE-DGB-009-2018 Prórroga a estudiantes que cursarán primer grado en el 2018', N'Prórroga  a  estudiantes que cursarán primer grado en el 2018', N'5ac204da6c6075.77178342.pdf', NULL, NULL, 1522664666, 1522664666, 99, CAST(0xD63D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (6, N'Circular FONABE-DE-DGB-010-2018 Proceso de prórroga manual 2018 a estudiantes que contaron con be', N'Proceso  de  prórroga  manual  2018  a  estudiantes  que  contaron  con  beneficio en el año 2017', N'5ac20525678aa9.53030514.pdf', 6, NULL, 1522664741, 1522861440, 100, CAST(0xE13D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (34, N'DEPARTAMENTO ARCHIVO INSTITUCIONAL', N'', N'5ac26adce80385.01062963.doc', 4, 2, 1522690780, 1522783336, 102, CAST(0x883D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (12, N'Circular DGB-028-2017 Nuevas fechas proceso de pro´rroga Postsecundaria', N'Nuevas fechas para el proceso de pro´rroga de Postsecundaria', N'5ac3b4cad13406.24806050.pdf', 1, NULL, 1522775242, 1522791567, 104, CAST(0x393D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (18, N'Circular DGB-004-2016  Pro´rroga becas educacio´n formal', N'Proceso de Pro´rrogas 2016 para Estudiantes de Colegios Nocturnos, Cindea, IPEC y Proyectos de Educacio´n Abierta Primaria', N'5ac3b7e0d64bd3.69940698.pdf', NULL, NULL, 1522776032, 1522776032, 105, CAST(0x023B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (18, N'Circular DGB-012-2016 Gestio´n Educacio´n Abierta 2016', N'Solicitudes de casos nuevos para becas Educacio´n Abierta 2016 (“Yo Me Apunto a la Educacio´n”).', N'5ac3b81fcb0dc9.32195398.pdf', NULL, NULL, 1522776095, 1522776095, 106, CAST(0x6A3B0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (18, N'Circular DGB-005-2017 I Pro´rroga Educacio´n Abierta', N'Proceso de Pro´rroga 2017, para Estudiantes de Educacio´n Abierta.', N'5ac3b8ae4936a9.59969085.pdf', NULL, NULL, 1522776238, 1522776238, 107, CAST(0x4E3C0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (18, N'Circular DGB-029-2017 II Proceso de Pro´rroga de Educacio´n Abierta y Corroboracio´n', N'Proceso de Prorroga de Proyectos Educacio´n Abierta 2017 y estudiantes de la modalidad de Educacio´n Abierta (Control de Calidad) y proceso de corroboracio´n de estudiantes que asisten a CINDEA e IPEC, fechas y requisitos.', N'5ac3b8f8bd1297.38359291.pdf', NULL, NULL, 1522776312, 1522776312, 108, CAST(0x0E3D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (18, N'Circular FONABE-DE-DGB-002-2018 Fechas y Requisitos I Pro´rroga de Educacio´n Abierta 2018', N'Fechas y Requisitos I Pro´rroga de Educacio´n Abierta 2018', N'5ac3b957a10a58.66806304.pdf', NULL, NULL, 1522776407, 1522776407, 109, CAST(0xBE3D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (18, N'Circular FONABE-DE-DGB-004-2018 Fechas y Requisitos I Pro´rroga de estudiantes cursan modalidad de', N'Fechas y Requisitos I Pro´rroga de estudiantes cursan modalidad de Educacio´n Abierta 2018', N'5ac3b98c5e7363.92307012.pdf', NULL, NULL, 1522776460, 1522776460, 110, CAST(0xC73D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (25, N'CÓDIGO DE ÉTICA', N'CÓDIGO DE ÉTICA ', N'5ac3bd50935958.26763761.pdf', 7, NULL, 1522777424, 1523024020, 111, CAST(0xCF3D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (25, N'LEY DE CREACION DEL FONDO NACIONAL DE BECAS, No. 7658', N'', N'5ac3bdeb8e35d5.78230518.pdf', NULL, NULL, 1522777579, 1522777579, 112, CAST(0xCF3D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (25, N'Reglamento a la Ley de Creacio´n del FONABE, Decreto No. 26496', N'', N'5ac3be0f6ade82.87003206.pdf', NULL, NULL, 1522777615, 1522777615, 113, CAST(0xCF3D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (28, N'FONABE-DAF-PI-001-2018, Ejecucio´n del programa de adquisiciones 2017', N'', N'5ac3c1a0086f07.97695728.doc', 1, NULL, 1522778528, 1522778540, 114, CAST(0xE33D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (28, N'Reglamento Proveedurias Institucionales', N'', N'5ac3c70b3d6206.67150023.pdf', NULL, NULL, 1522779915, 1522779915, 115, CAST(0xE33D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (29, N'Plan Estrate´gico Institucional 2017-2022', N'', N'5ac3c82ba349d9.66028960.pdf', NULL, NULL, 1522780203, 1522780203, 116, CAST(0x9E3D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (29, N'Informe PAO 2017 (DESAF-FONABE)', N'', N'5ac3c938349f69.72316076.pdf', NULL, NULL, 1522780472, 1522780472, 117, CAST(0x883D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (29, N'Presupuesto Extraordinario I-2017)', N'', N'5ac3ca6aeea032.56641237.pdf', NULL, NULL, 1522780778, 1522780778, 118, CAST(0x883D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (31, N'Balance General junio 2017', N'', N'5ac3cd805324a4.22446360.pdf', NULL, NULL, 1522781568, 1522781568, 119, CAST(0x013D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (31, N'Cambios en el Patrimonio Junio 2017', N'', N'5ac3cde32b6c22.14821806.pdf', NULL, NULL, 1522781667, 1522781667, 120, CAST(0x013D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (31, N'Deuda Publica Junio 2017', N'', N'5ac3ce1fe0d819.76660595.pdf', NULL, NULL, 1522781727, 1522781727, 121, CAST(0x023D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (31, N'Estado de Resultados', N'', N'5ac3ce5dc247f6.98782817.pdf', NULL, NULL, 1522781789, 1522781789, 122, CAST(0x023D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (31, N'ESTADO NOTAS CONTABLES NICSP JUNIO 2017', N'', N'5ac3ceab0f7af9.95272861.pdf', NULL, NULL, 1522781867, 1522781867, 123, CAST(0x033D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (31, N'Flujo de Efectivo Junio 2017', N'', N'5ac3ceeb055b12.00743392.pdf', NULL, NULL, 1522781931, 1522781931, 124, CAST(0x013D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (31, N'Segmentos Junio 2017', N'', N'5ac3cf0d6d7f94.00139228.pdf', NULL, NULL, 1522781965, 1522781965, 125, CAST(0x013D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (31, N'Situacion y evolucio´n de bienes Junio 2017', N'', N'5ac3cf5b921645.59650356.pdf', NULL, NULL, 1522782043, 1522782043, 126, CAST(0x013D0B00 AS Date))
GO
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (34, N'Informe Anual Desarrollo Archivi´stico 2016-2017', N'', N'5ac3d1d36fc276.06062720.pdf', NULL, NULL, 1522782675, 1522782675, 127, CAST(0x883D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (34, N'Acta eliminacio´n 001-2017', N'', N'5ac3d216dbef41.07794960.pdf', NULL, NULL, 1522782742, 1522782742, 128, CAST(0x873D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (34, N'Acta eliminacio´n 002-2017', N'', N'5ac3d4ea575876.24747551.pdf', NULL, NULL, 1522783466, 1522783466, 129, CAST(0x873D0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (27, N'8F10 Sugerencias de los usuarios', N'', N'5ac3d81fc3a419.57160278.pdf', NULL, NULL, 1522784287, 1522784287, 130, CAST(0x123E0B00 AS Date))
INSERT [dbo].[fn_files] ([page_id], [title], [description], [file_url], [hit_counter], [download_counter], [created_at], [updated_at], [id], [file_date]) VALUES (27, N'8F11 Atencion de quejas de los usuarios', N'', N'5ac3d848233a29.05527712.pdf', NULL, NULL, 1522784328, 1522784328, 131, CAST(0x123E0B00 AS Date))
SET IDENTITY_INSERT [dbo].[fn_files] OFF
SET IDENTITY_INSERT [dbo].[fn_history] ON 

INSERT [dbo].[fn_history] ([id], [year], [title], [content]) VALUES (1001, N'1996', N'Aprobación de proyecto de ley para la creación de un Fondo Nacional de Becas.', N'<p>En el a&ntilde;o 1996, se present&oacute; una iniciativa de ley para la creaci&oacute;n de un Fondo Nacional de Becas. El objetivo primordial de esta era contribuir a crear un mecanismo de movilidad social en Costa Rica, como soluci&oacute;n a la carencia de oportunidades para superarse, y a la vez resolver de forma integral la deserci&oacute;n estudiantil de aquellos que no pod&iacute;an continuar por carencia de recursos econ&oacute;micos.</p>

<p>Este proyecto cont&oacute; con la aprobaci&oacute;n un&aacute;nime de los diputados, situaci&oacute;n poco usual en el Congreso Nacional.</p>
')
INSERT [dbo].[fn_history] ([id], [year], [title], [content]) VALUES (1002, N'2018', N'Nuevos sistemas para realizar trámites en línea', N'<p>Aqui se ingresa informaci&oacute;n de los m&oacute;dulos de tr&aacute;mites en l&iacute;nea y el modulo de regionalizaci&oacute;n.&nbsp;</p>
')
INSERT [dbo].[fn_history] ([id], [year], [title], [content]) VALUES (1007, N'1997', N'Comienza a regir la ley de Creación del Fondo Nacional de Becas N°7658', N'<p>El 6 de febrero de 1997 se comunic&oacute; al Poder Ejecutivo la aprobaci&oacute;n de la ley, y el 11 de febrero de 1997, el Poder Ejecutivo firm&oacute; el ejec&uacute;tese y orden&oacute; su publicaci&oacute;n mediante la Gaceta N&ordm; 41 del 27 de febrero del mismo a&ntilde;o en que empez&oacute; a regir la Ley de Creaci&oacute;n del Fondo Nacional de Becas N&ordm; 7658.&nbsp;</p>

<p>En 1997, su primera Junta Directiva fue convocada y estuvo presidida por el Lic. Guillermo Barquero Chac&oacute;n, Presidente de la Federaci&oacute;n de Colegios Profesionales y asesor legislativo del Dr. Walter Coto.</p>
')
INSERT [dbo].[fn_history] ([id], [year], [title], [content]) VALUES (1008, N'2004', N'Ampliación de la cobertura de becas', N'<p>El 2 de junio del 2004 mediante Decreto Legislativo No. 8417, se ampl&iacute;a la cobertura de becas a la poblaci&oacute;n de Post secundaria; una siguiente modificaci&oacute;n al Fondo se har&iacute;a en el 2004, cuando por voto 7806, expediente 00-002675.007-CO de la Sala Constitucional de Costa Rica, se incluye a los &nbsp;extranjeros, en igualdad de condiciones de selecci&oacute;n, para ayudarles en los estudios , en el tanto que cumplan con ciertos requerimientos de migraci&oacute;n y residencia en el pa&iacute;s, adem&aacute;s de condiciones socioecon&oacute;micas pobres.</p>

<p>Posteriormente y de acuerdo con los informes DFOE-EC-23/2004 de la Contralor&iacute;a General de la Rep&uacute;blica, TN-1124-2004 estudio jur&iacute;dico de la Tesorer&iacute;a Nacional y C-297-2005 criterio de la Procuradur&iacute;a General de la Rep&uacute;blica de Costa Rica; el FONABE determin&oacute; realizar las gestiones necesarias para ejecutar el traspaso de los recursos al r&eacute;gimen de Caja &Uacute;nica del Estado. Esta decisi&oacute;n dio lugar a la finalizaci&oacute;n del Fideicomiso 478 con el Banco Nacional de Costa Rica, que trajo como consecuencia inmediata en materia laboral, un cambio en la estructura organizacional.</p>
')
INSERT [dbo].[fn_history] ([id], [year], [title], [content]) VALUES (1009, N'2000', N'Primera aprobación de la estructura organizacional', N'<p>La primera estructura organizacional fue aprobada por el Ministerio de Planificaci&oacute;n y Pol&iacute;tica Econ&oacute;mica mediante oficio DM-045-00 el 16 de mayo del 2000;</p>
')
INSERT [dbo].[fn_history] ([id], [year], [title], [content]) VALUES (1010, N'2006', N'Aprobación de reorganización y restructuración del Fondo.', N'<p>La reorganizaci&oacute;n y reestructuraci&oacute;n del Fondo se aprob&oacute; mediante oficio DM-0892-06 del 29 de agosto del 2006.</p>
')
INSERT [dbo].[fn_history] ([id], [year], [title], [content]) VALUES (1011, N'2011', N'Aprobada la estructura vigente de FONABE', N'<p>La estructura vigente de acuerdo a dos actualizaciones realizadas en el 2010 y el 2011, qued&oacute; aprobada mediante oficio DM-643-11, del 10 de noviembre del 2011.</p>

<p>Resultado de varios a&ntilde;os de operaci&oacute;n y consolidaci&oacute;n de algunos proyectos puestos en marcha, FONABE en el 2010 consider&oacute; la necesidad de dise&ntilde;ar un modelo propio para la selecci&oacute;n y calificaci&oacute;n de los estudiantes, que integre indicadores econ&oacute;micos y sociales que permita calificar a la poblaci&oacute;n meta de forma segura, eficiente y eficaz; complementando m&eacute;todos de medici&oacute;n de pobreza, satisfacci&oacute;n de necesidades b&aacute;sicas e &iacute;ndices de vulnerabilidad educativa. De ello nace el Modelo de Calificaci&oacute;n de la Beca (MICB) que se implement&oacute; entre el a&ntilde;o 2011 y 2012 integr&aacute;ndolo al nuevo sistema SICOB.</p>

<p>El proceso de Control y Seguimiento mejor&oacute; el protocolo de acci&oacute;n, incluyendo en las metas del POI las visitas domiciliares y aumentando la cobertura de estudiantes de la muestra por centro educativo, claro est&aacute;, partiendo de la capacidad instalada y los recursos disponibles en la Instituci&oacute;n para dicha labor.</p>
')
INSERT [dbo].[fn_history] ([id], [year], [title], [content]) VALUES (1012, N'2010', N'Reforma de Reglamento de FONABE', N'<p>El 6 de diciembre del 2010, con el Decreto Ejecutivo N&ordm; 36278-MEP, se reforma el Reglamento del FONABE, el cual se encuentra vigente en la actualidad.</p>
')
INSERT [dbo].[fn_history] ([id], [year], [title], [content]) VALUES (1013, N'1998', N'Periodo de organización 1998-1999', N'<p>Durante el per&iacute;odo de organizaci&oacute;n (1998-1999) la nueva Junta Directiva constituida recibe el apoyo tanto del Ministerio de Educaci&oacute;n P&uacute;blica por medio de la Oficina de Becas, as&iacute; como de otras entidades gubernamentales; sin espacio f&iacute;sico, falta de un sistema que administrara y controlara las solicitudes de becas y sin el recurso humano que hiciera frente al indiscutible crecimiento, la Junta Directiva realiza las acciones para poner en marcha la nueva estructura, que contaba apenas con dos coordinadores, preparando la contrataci&oacute;n del nuevo puesto de Direcci&oacute;n Ejecutiva, tres Trabajadoras Sociales y cuatro Oficinistas; as&iacute; inicia un viaje de consolidaci&oacute;n traslad&aacute;ndose de las oficinas del MEP en la antigua Embajada Americana, hacia el Edificio Langer, contiguo a la Casa Presidencial en Zapote hacia el a&ntilde;o 2000.&nbsp;</p>

<p>Por otra parte, para dotarlo de recursos se recurri&oacute; a un ahorro significativo del per&iacute;odo en el Congreso y a un porcentaje fijado por Ley; el 1% del Fondo de Desarrollo Social y Asignaciones Familiares (FODESAF). Este monto se constituy&oacute; en lo que se lleg&oacute; a conocer como el &ldquo;capital semilla&rdquo; del FONABE.</p>
')
INSERT [dbo].[fn_history] ([id], [year], [title], [content]) VALUES (1014, N'2012', N'Aprobada la nueva Estructura de Becas', N'<p>En el 2012, con la implementaci&oacute;n del SICOB, y la integraci&oacute;n del nuevo Modelo Integrado de Calificaci&oacute;n de la Beca (MICB), se controlaron las filtraciones seg&uacute;n los par&aacute;metros de medici&oacute;n de la l&iacute;nea de pobreza y la vulnerabilidad educativa. Adem&aacute;s, en ese a&ntilde;o se implement&oacute; la Nueva Estructura de Becas aprobadas mediante acuerdo No.189-2012 del 20 de setiembre del 2012 y para ese per&iacute;odo se aprob&oacute; tambi&eacute;n la modificaci&oacute;n de aumento de los montos del producto de Post-secundaria. Actualidad/&Aacute;reas de Acci&oacute;n</p>
')
INSERT [dbo].[fn_history] ([id], [year], [title], [content]) VALUES (1015, N'2007', N'Reemplazo del Sistema SIGBE', N'<p>El FONABE durante el 2007 inici&oacute; un proceso de remplazo del Sistema de Gesti&oacute;n de Becas (SIGBE) con el que se administraba el Proceso de Digitaci&oacute;n, Aprobaci&oacute;n y Pago de Becas, y en el 2008 se inici&oacute; una licitaci&oacute;n para el dise&ntilde;o de un desarrollo inform&aacute;tico que soportara todo el Proceso de Gesti&oacute;n de Becas de una forma integral y sistematizada que respondiera a la nueva estructura organizacional.</p>
')
SET IDENTITY_INSERT [dbo].[fn_history] OFF
SET IDENTITY_INSERT [dbo].[fn_news] ON 

INSERT [dbo].[fn_news] ([id], [title], [slug], [extract], [content], [author_name], [url_imagen], [updated_at], [created_at], [published]) VALUES (12, N'Comunicado FONABE 1 de Febrero 2018', N'comunicado-fonabe-1-de-febrero-2018', N'FONABE comunica que este año 132.127 estudiantes en condición de pobreza y pobreza extrema disponen en la actualidad de una beca estudiantil anual, que les permitirá ingresar de forma oportuna al curso lectivo 2018. Asimismo se informa que el mes de enero fue depositado en tiempo, en beneficio de todos estos estudiantes.', N'<p>FONABE comunica que este a&ntilde;o 132.127 estudiantes en condici&oacute;n de pobreza y pobreza extrema disponen en la actualidad de una beca estudiantil anual, que les permitir&aacute; ingresar de forma oportuna al curso lectivo 2018. Asimismo se informa que el mes de enero fue depositado en tiempo, en beneficio de todos estos estudiantes.</p>
', N'IGD', N'5ab5637d8fbe70.13664461.jpg', 1521836959, 1521836925, 1)
INSERT [dbo].[fn_news] ([id], [title], [slug], [extract], [content], [author_name], [url_imagen], [updated_at], [created_at], [published]) VALUES (13, N'Información sobre tarjetas prepago', N'informacion-sobre-tarjetas-prepago', N'FONABE comunica la existencia de tarjetas prepago sin retirar por beneficiarios de años anteriores', N'<p>FONABE comunica la existencia de tarjetas prepago sin retirar por beneficiarios de a&ntilde;os anteriores.&nbsp;</p>

<p>Ver detalle:&nbsp;&nbsp;<a href="http://www.fonabe.go.cr/Noticias/Documents/2018/FONABE-DE-DGB-007-2018.pdf">Aqui</a>&nbsp;</p>
', N'IGD', N'5ab5694430a771.06354599.jpg', 1521838404, 1521837982, 1)
INSERT [dbo].[fn_news] ([id], [title], [slug], [extract], [content], [author_name], [url_imagen], [updated_at], [created_at], [published]) VALUES (14, N'Prórroga a estudiantes que cursarán primer grado en el 2018 ', N'prorroga-a-estudiantes-que-cursaran-primer-grado-en-el-2018', N'Con el fin de realizar el proceso de prórroga a los estudiantes que en el año 2017 contaron con beca en el nivel de preescolar y que en el año 2018 cursarán primer grado', N'<p>Con el fin de realizar el proceso de pr&oacute;rroga a los estudiantes que en el a&ntilde;o 2017 contaron con beca en el nivel de preescolar y que en el a&ntilde;o 2018 cursar&aacute;n primer grado,&nbsp;ver detalle&nbsp;<a href="http://www.fonabe.go.cr/Noticias/Documents/2018/FONABE-DE-DGB-009-2018.pdf">Aqui</a>&nbsp;</p>
', N'IGD', N'5ab56a4a920855.21052186.jpg', 1521838677, 1521838666, 1)
INSERT [dbo].[fn_news] ([id], [title], [slug], [extract], [content], [author_name], [url_imagen], [updated_at], [created_at], [published]) VALUES (15, N'Fechas y Requisitos I Prórroga de Educación Abierta 2018 ', N'fechas-y-requisitos-i-prorroga-de-educacion-abierta-2018', N'Reciban un cordial saludo de parte del Fondo Nacional de Becas, con el fin de realizar el proceso de prórroga de los estudiantes beneficiarios pertenecientes a los diferentes programas de estudio de Educación Abierta de la Dirección de Gestión y Evaluación de la Calidad del Ministerio de Educación Pública.', N'<p>Reciban un cordial saludo de parte del Fondo Nacional de Becas, con el fin de realizar el proceso de pr&oacute;rroga de los estudiantes beneficiarios pertenecientes a los diferentes programas de estudio de Educaci&oacute;n Abierta de la Direcci&oacute;n de Gesti&oacute;n y Evaluaci&oacute;n de la Calidad del Ministerio de Educaci&oacute;n P&uacute;blica, ver detalle&nbsp;<a href="http://www.fonabe.go.cr/Noticias/Documents/2018/Circular%20FONABE-DE-DGB-002-2018%20Fechas%20y%20Requisitos%20I%20Pr%C3%B3rroga%20de%20Educaci%C3%B3n%20Abierta%202018.pdf">Aqui</a></p>
', N'IGD', N'5ab56c89ad60d0.38098382.jpg', 1521839252, 1521838928, 1)
INSERT [dbo].[fn_news] ([id], [title], [slug], [extract], [content], [author_name], [url_imagen], [updated_at], [created_at], [published]) VALUES (16, N'Primer período para el proceso de prórroga de Postsecundaria 2018 ', N'primer-periodo-para-el-proceso-de-prorroga-de-postsecundaria-2018', N'Se les informa a los estudiantes universitarios becados por FONABE, que se encuentra disponible para consulta el detalle de las fechas para el Primer período del proceso de prórroga de Postsecundaria 2018, las cuales están contempladas en la Circular DGB-040-2017. Para la modalidad de cuatrimestre se habilitará el sistema de citas a partir del 02 de enero del 2018, y para la modalidad de semestre el 15 de febrero del 2018. ', N'<p>Se les informa a los estudiantes universitarios becados por FONABE, que se encuentra disponible para consulta el detalle de las fechas para el Primer per&iacute;odo del proceso de pr&oacute;rroga de Postsecundaria 2018, las cuales est&aacute;n contempladas en la Circular DGB-040-2017. Para la modalidad de cuatrimestre se habilitar&aacute; el sistema de citas a partir del 02 de enero del 2018, y para la modalidad de semestre el 15 de febrero del 2018. Para ver la circular presione aqui&nbsp;<a href="http://www.fonabe.go.cr/Noticias/Documents/2017/Circular%20DGB-040-2017%20I%20Prorroga%20de%20postsecundaria%202018.pdf">Aqui</a></p>
', N'IGD', N'5ab56e1f59daa7.00411545.jpg', 1521839647, 1521839558, 1)
INSERT [dbo].[fn_news] ([id], [title], [slug], [extract], [content], [author_name], [url_imagen], [updated_at], [created_at], [published]) VALUES (17, N'Proceso de prórroga manual a estudiantes que contaron con beneficio en el año 2017', N'proceso-de-prorroga-manual-a-estudiantes-que-contaron-con-beneficio-en-el-ano-2017', N'Con el fin de realizar el proceso de prórroga manual 2018 a los estudiantes que contaron con un beneficio en el año 2017.', N'<p>Con el fin de realizar el proceso de pr&oacute;rroga manual 2018 a los estudiantes que contaron con un beneficio en el a&ntilde;o 2017, les comunicamos: ver detalle&nbsp;<a href="http://www.fonabe.go.cr/Noticias/Documents/2018/FONABE-DE-DGB-010-2018.pdf">Aqui</a></p>
', N'IGD', N'5ab56ed4d2a139.99947777.jpg', 1521839849, 1521839828, 1)
INSERT [dbo].[fn_news] ([id], [title], [slug], [extract], [content], [author_name], [url_imagen], [updated_at], [created_at], [published]) VALUES (18, N'Gestión para la recepción de solicitud de becas especiales DRE de Sulá. ', N'gestion-para-la-recepcion-de-solicitud-de-becas-especiales-dre-de-sula', N'Reciba un cordial saludo, nuevamente este año se continuará brindando prioridad a la población estudiantil indígena y con condiciones de vulnerabilidad.', N'<p>Reciba un cordial saludo, nuevamente este a&ntilde;o se continuar&aacute; brindando prioridad a la poblaci&oacute;n estudiantil ind&iacute;gena y con condiciones de vulnerabilidad.&nbsp;Se les informa: ver detalle&nbsp;<a href="http://www.fonabe.go.cr/Noticias/Documents/2018/FONABE-DE-DGB-011-2018.pdf">Aqui</a></p>
', N'IGD', N'5ab570289e4682.10331904.jpg', 1521840187, 1521840128, 1)
INSERT [dbo].[fn_news] ([id], [title], [slug], [extract], [content], [author_name], [url_imagen], [updated_at], [created_at], [published]) VALUES (19, N'IMPORTANTE: Pagos Mes de Diciembre ', N'importante-pagos-mes-de-diciembre', N'FONABE aclara que debido a una situación ajena a la institución, los pagos correspondientes al mes de diciembre se realizarán conforme a la liberación de recursos por parte del Ministerio de Hacienda. Cualquier cambio se estará comunicando oportunamente. Presentamos disculpas por los inconvenientes ocasionados.
', N'<p>FONABE aclara que debido a una situaci&oacute;n ajena a la instituci&oacute;n, los pagos correspondientes al mes de diciembre se realizar&aacute;n conforme a la liberaci&oacute;n de recursos por parte del Ministerio de Hacienda. Cualquier cambio se estar&aacute; comunicando oportunamente. Presentamos disculpas por los inconvenientes ocasionados.<br />
&nbsp;</p>
', N'IGD', N'5ab572f8c5ab61.81384297.jpg', 1521840898, 1521840888, 1)
INSERT [dbo].[fn_news] ([id], [title], [slug], [extract], [content], [author_name], [url_imagen], [updated_at], [created_at], [published]) VALUES (20, N'I Prórroga de estudiantes de los productos: Indígenas, Adolescentes madres y padres, Necesidades educativas especiales (Modalidad Educación Abierta 2018) ', N'i-prorroga-de-estudiantes-de-los-productos-indigenas-adolescentes-madres-y-padres-necesidades-educativas-especiales-modalidad-educacion-abierta-2018', N'Ver detalle ', N'<p>Ver detalle&nbsp;<a href="http://www.fonabe.go.cr/Noticias/Documents/2018/Circular%20FONABE-DE-DGB-004-2018%20Fechas%20y%20Requisitos%20I%20Pr%C3%B3rroga%20de%20estudiantes%20cursan%20modalidad%20de%20Educaci%C3%B3n%20Abierta%202018.pdf">Aqui </a></p>
', N'IGD', N'5ab5739555a776.01580672.jpg', 1521841060, 1521841045, 1)
INSERT [dbo].[fn_news] ([id], [title], [slug], [extract], [content], [author_name], [url_imagen], [updated_at], [created_at], [published]) VALUES (21, N'Información de Prorroga 2018 de producto “Apoyo a los programas Educativos de Adaptación Social"  ', N'informacion-de-prorroga-2018-de-producto-apoyo-a-los-programas-educativos-de-adaptacion-social', N' Ver datalle ', N'<p>Ver detalle&nbsp;<a href="http://www.fonabe.go.cr/Noticias/Documents/2018/Circular%20FONABE-DE-DGB-006-2018%20Pr%C3%B3rroga%20de%20Producto%20Apoyo%20a%20los%20programas%20Educativos%20de%20Adaptaci%C3%B3n%20Social%202018.pdf">Aqui</a></p>
', N'IGD', N'5ab5744f7cba22.28111273.jpg', 1521841241, 1521841231, 1)
INSERT [dbo].[fn_news] ([id], [title], [slug], [extract], [content], [author_name], [url_imagen], [updated_at], [created_at], [published]) VALUES (22, N'Cierre de oficinas FONABE 30 de Enero 2018 ', N'cierre-de-oficinas-fonabe-30-de-enero-2018', N'Estimado Usuario, Le comunicamos que conforme a lo informado por Acueductos y Alcantarillados (AyA) el día de mañana 30 de enero, no habrá suministro de agua potable en esta zona. ', N'<p>Estimado Usuario, Le comunicamos que conforme a lo informado por Acueductos y Alcantarillados (AyA) el d&iacute;a de ma&ntilde;ana 30 de enero, no habr&aacute; suministro de agua potable en esta zona. Es por ello que nuestras oficinas permanecer&aacute;n cerradas. Las citas que se encuentran asignadas estar&aacute;n siendo reprogramadas. Gracias por su comprensi&oacute;n.&nbsp;<br />
&nbsp;</p>
', N'IGD', N'5ab574dba53504.85620386.jpg', 1522967762, 1521841371, 1)
INSERT [dbo].[fn_news] ([id], [title], [slug], [extract], [content], [author_name], [url_imagen], [updated_at], [created_at], [published]) VALUES (23, N'Prórroga estudiantes con necesidades educativas especiales que cursarán sétimo grado en el 2018 ', N'prorroga-estudiantes-con-necesidades-educativas-especiales-que-cursaran-setimo-grado-en-el-2018', N'', N'<p>Ver detalle:&nbsp;&nbsp;<a href="http://www.fonabe.go.cr/Noticias/Documents/2018/Circular%20FONABE-DE-DGB-008-2018%20Pr%C3%B3rroga%20estudiantes%20con%20necesidades%20especiales%20s%C3%A9timo%20grado%20en%20el%202018.pdf">Aqui </a></p>
', N'IGD', N'5ab928cd397db2.50788770.jpg', 1522967757, 1521841550, 1)
SET IDENTITY_INSERT [dbo].[fn_news] OFF
INSERT [dbo].[fn_profile] ([user_id], [name], [public_email], [gravatar_email], [gravatar_id], [location], [website], [bio], [timezone]) VALUES (12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[fn_profile] ([user_id], [name], [public_email], [gravatar_email], [gravatar_id], [location], [website], [bio], [timezone]) VALUES (13, N'Admin', N'info@fonabe.go.cr', N'', N'd41d8cd98f00b204e9800998ecf8427e', N'SJ', N'http://www.fonabe.go.cr', N'', N'America/Costa_Rica')
INSERT [dbo].[fn_profile] ([user_id], [name], [public_email], [gravatar_email], [gravatar_id], [location], [website], [bio], [timezone]) VALUES (16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[fn_setting] ON 

INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (1, N'string', N'main', N'admin_url', N'http://ee828eff.ngrok.io', 1, N'Url general de Administración', 1519994056, 1522894728)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (2, N'string', N'main', N'correo_institucional_url', N'https://mail.fonabe.go.cr/owa/auth/logon.aspx?replaceCurrent=1&url=https%3a%2f%2fmail.fonabe.go.cr%2fowa', 1, N'Url de correo institucional', 1519994378, 1521825937)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (3, N'string', N'main', N'site_main_title', N'Fondo Nacional de Becas', 1, N'Titulo general del sitio', 1519997798, 1521825937)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (4, N'string', N'main', N'facebook_url', N'https://www.facebook.com/fonabe', 1, N'Url de facebook', 1520106143, 1521825937)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (5, N'string', N'main', N'twitter_url', N'https://twitter.com/fonabe', 1, N'Url de Twitter', 1520106182, 1521825937)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (6, N'string', N'main', N'copy_string', N'Derechos reservados © FONABE 2018', 1, N'Texto Derechos Reservados', 1520106345, 1521825937)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (10, N'string', N'main', N'frontend_url', N'http://e020371f.ngrok.io', 1, N'URL del frontend', 1520789093, 1522894683)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (11, N'string', N'data', N'link_url_1', N'http://fonabe.opendata.junar.com/dashboards/20155/focalizacion/', 1, N'Label Index , botones azules 1', 1520790296, 1522679543)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (12, N'string', N'data', N'link_url_2', N'#test2', 1, N'Label Index , botones azules 2', 1520790328, 1522166965)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (13, N'string', N'data', N'link_url_3', N'http://fonabe.opendata.junar.com/dashboards/20158/iii-trimestre-2017/', 1, N'Label Index , botones azules 3', 1520790344, 1522774104)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (14, N'string', N'data', N'link_label_1', N'Logros', 1, N'Label de botosnes azules 1', 1520790382, 1521825938)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (15, N'string', N'data', N'link_label_2', N'#OCULTO', 1, N'Label de botosnes azules 2', 1520790400, 1522090830)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (16, N'string', N'data', N'link_label_3', N'Demografia', 1, N'Label de botosnes azules 3', 1520790425, 1522682254)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (17, N'string', N'index', N'urlLink1', N' http://serviciosenlinea.fonabe.go.cr:8080/', 1, N'URL label 1 botones azules', 1520791175, 1522891791)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (18, N'string', N'index', N'urlLink2', N'http://e020371f.ngrok.io/pagina/tarjetas-prepago', 1, N'URL label 2 botones azules', 1520791175, 1522863442)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (19, N'string', N'index', N'urlLink3', N'http://reservarcitas.fonabe.go.cr/QSIGE/apps/citaPrevia/index.html#!/home', 1, N'URL label 2 botones azules', 1520791175, 1522891791)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (23, N'string', N'mail', N'email', N'lzunigam@fonabe.go.cr ', 1, N'Correo institucional para envío de solicitudes y mensajes', 1520958636, 1521825938)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (24, N'string', N'page', N'footer_code', N'<script type="text/javascript">
function add_chatinline(){var hccid=59724441;var nt=document.createElement("script");nt.async=true;nt.src="https://mylivechat.com/chatinline.aspx?hccid="+hccid;var ct=document.getElementsByTagName("script")[0];ct.parentNode.insertBefore(nt,ct);}
add_chatinline(); 
</script>', 1, N'Scripts del pie', 1521214286, 1521214286)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (25, N'string', N'forms', N'educational_center_name', N'Uno, Dos, tres', 1, N'separado por coma', 1521214764, 1521217710)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (26, N'string', N'forms', N'province', N'Heredia, San José, Cartago, Alajuela', 1, N'separado por coma', 1521216867, 1521217721)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (27, N'string', N'forms', N'district', N'San Francisco, San José, Zapote, Desamparados', 1, N'separado por coma', 1521217055, 1521217736)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (28, N'string', N'forms', N'canton', N'San francisco, San José, Zapote, Desamparados', 1, N'separado por coma', 1521217243, 1521217740)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (29, N'string', N'google', N'data-sitekey', N'6Ld5cVAUAAAAAGuApxAVSlrA5AKYqh8MXXVXGZ6y', 1, N'Google Captach Code', 1521238036, 1521238036)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (30, N'string', N'forms', N'idType', N'Cédula, Pasaporte', 1, N'Separado por comas', 1521238571, 1521238571)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (31, N'string', N'index', N'link1_label', N'Servicios en línea', 1, NULL, 1521825796, 1521831497)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (32, N'string', N'index', N'link2_label', N'Tarjetas prepago', 1, NULL, 1521825796, 1521825938)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (33, N'string', N'index', N'link3_label', N'Reservar cita', 1, NULL, 1521825796, 1521825938)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (34, N'string', N'index', N'urlLink0', N'http://179.0.212.19/portal/Account/Login', 1, NULL, 1521825938, 1522891791)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (35, N'string', N'index', N'link0_label', N'Fonabe digital', 1, NULL, 1521825938, 1521825938)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (36, N'string', N'main', N'pagination', N'8', 1, N'', 1521831859, 1522689252)
INSERT [dbo].[fn_setting] ([id], [type], [section], [key], [value], [status], [description], [created_at], [updated_at]) VALUES (1030, N'string', N'imagen-slider', N'1', N'imagen', 1, N'', 1522682802, 1522682802)
SET IDENTITY_INSERT [dbo].[fn_setting] OFF
SET IDENTITY_INSERT [dbo].[fn_slider_images] ON 

INSERT [dbo].[fn_slider_images] ([id], [image_url], [image_link], [slide_text]) VALUES (1, N'5ac586d99e86a3.64047523.jpg', N'http://e020371f.ngrok.io/fonabe/historia', N'Siempre ayudando a cristalizar sueños')
INSERT [dbo].[fn_slider_images] ([id], [image_url], [image_link], [slide_text]) VALUES (2, N'5ac1fd9b0d8520.73527082.jpg', N'http://179.0.212.19/portal/Account/Login', N'FONABE es la primera institucion en integrarse a SINIRUBE')
INSERT [dbo].[fn_slider_images] ([id], [image_url], [image_link], [slide_text]) VALUES (3, N'5ac1fdc596ecb9.88708706.jpg', N'http://e020371f.ngrok.io/centro-educativo/tipos-de-becas', N'Productos de becas')
INSERT [dbo].[fn_slider_images] ([id], [image_url], [image_link], [slide_text]) VALUES (4, N'5ac1fe557f6eb1.54873468.jpg', N'#', N'Cobertura del 80% de la poblacion indigena')
INSERT [dbo].[fn_slider_images] ([id], [image_url], [image_link], [slide_text]) VALUES (5, N'5ac1fdfc9ec420.89354344.jpg', N'http://e020371f.ngrok.io/centro-educativo/centros-educativos', N'  Consulte su beca')
SET IDENTITY_INSERT [dbo].[fn_slider_images] OFF
SET IDENTITY_INSERT [dbo].[fn_static_pages] ON 

INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (1, N'centros-educativos', N'Consulta Centros Educativos', N'', N'<h1><strong>Consulta Centros Educativos</strong></h1>

<hr />
<p>La b&uacute;squeda de los Centros Educativos se realiza digitando cualquiera de los siguientes campos.</p>
', 1519918469, 1519918469, 1, 0, N'icon-g-centros', 6, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (2, N'convocatorias', N'Convocatorias', N'', N'<h1>Convocatorias</h1>

<hr />
<p>&nbsp;</p>
', 1519918469, 1519918469, 1, 0, N'icon-g-calendario-fechas', 5, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (3, N'tramites', N'Trámites', N'', N'<h1><meta charset="utf-8" /><b id="docs-internal-guid-ec127034-8d62-22c4-2536-03b350febcd4">Tr&aacute;mites</b></h1>

<hr />
<h1 style="margin: 0cm 0cm 0.0001pt 36pt;">&nbsp;</h1>

<p style="margin-left:36.0pt; margin:0cm 0cm 8pt"><span style="font-size:11pt"><span style="line-height:107%"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:12.0pt"><span style="line-height:107%"><span style="color:#1f497d">Pr&oacute;rroga</span></span></span></b></span></span></span></p>

<p style="margin-left:36.0pt; margin:0cm 0cm 8pt"><span style="font-size:11pt"><span style="line-height:107%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:107%"><span style="color:#1f497d">Estudiantes</span></span></span><span style="font-size:12.0pt"><span style="line-height:107%"><span style="color:#1f497d"> que contaron con una beca en el a&ntilde;o anterior, y tienen pendiente la actualizaci&oacute;n de matr&iacute;cula para continuar con el proceso de Pr&oacute;rroga del a&ntilde;o actual. Para ingresar al sistema de tr&aacute;mites de Pr&oacute;rroga presione <a href="http://prorrogamanual.fonabe.go.cr/" onclick="window.open(this.href, '''', ''resizable=no,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=no,dependent=no''); return false;">aqu&iacute;</a></span></span></span></span></span></span></p>

<p style="margin-left:36.0pt; margin:0cm 0cm 8pt"><span style="font-size:11pt"><span style="line-height:107%"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:12.0pt"><span style="line-height:107%"><span style="color:#1f497d">Postulantes</span></span></span></b></span></span></span></p>

<p style="margin-left:36.0pt; margin:0cm 0cm 8pt"><span style="font-size:11pt"><span style="line-height:107%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:107%"><span style="color:#1f497d">Estudiantes que tramitaron una solicitud de beca durante el a&ntilde;o anterior y su estado es Postulante, deben de actualizar la matr&iacute;cula para continuar con el proceso del tr&aacute;mite de beca del a&ntilde;o actual. Para ingresar al sistema de tr&aacute;mites de Postulantes presione <a href="http://postulantes.fonabe.go.cr/" onclick="window.open(this.href, '''', ''resizable=no,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=no,dependent=no''); return false;">aqu&iacute;</a></span></span></span></span></span></span></p>

<p style="margin-left:36.0pt; margin:0cm 0cm 8pt"><span style="font-size:11pt"><span style="line-height:107%"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:12.0pt"><span style="line-height:107%"><span style="color:#1f497d">Traslados</span></span></span></b></span></span></span></p>

<p style="margin-left:36.0pt; margin:0cm 0cm 8pt"><span style="font-size:11pt"><span style="line-height:107%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:107%"><span style="color:#1f497d">Estudiantes que tienen estado Beneficiario en el a&ntilde;o actual y requieren un traslado de Centro Educativo.&nbsp;Para ingresar al sistema de tr&aacute;mites de Traslados presione <a href="http://traslados.fonabe.go.cr/" onclick="window.open(this.href, '''', ''resizable=no,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=no,dependent=no''); return false;">aqu&iacute;</a></span></span></span></span></span></span></p>

<p style="margin-left:36.0pt; margin:0cm 0cm 8pt"><span style="font-size:11pt"><span style="line-height:107%"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:12.0pt"><span style="line-height:107%"><span style="color:#1f497d">Corroboraci&oacute;n</span></span></span></b></span></span></span></p>

<p style="margin-left:36.0pt; margin:0cm 0cm 8pt"><span style="font-size:11pt"><span style="line-height:107%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:107%"><span style="color:#1f497d">Estudiantes que tienen estado Beneficiario en el a&ntilde;o actual y tienen que ser corroborados para la continuidad de la beca.&nbsp;</span></span></span></span></span></span></p>

<p>&nbsp;</p>
', 1519918469, 1519918469, 1, 0, N'icon-g-transaccion', 3, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (4, N'informacion', N'Información', N'', N'<h1><meta charset="utf-8" /><b id="docs-internal-guid-ec127034-8d62-d5b3-78c0-185f377731e4">Informaci&oacute;n</b></h1>

<hr />
<p>&nbsp;</p>
', 1519918469, 1519918469, 1, 0, N'icon-g-informacion', 0, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (5, N'tipos-de-becas', N'Tipos de becas', N'', N'<h1>Preescolar y Primaria</h1>

<hr />
<h3>Objetivo general:</h3>

<p>Brindar un aporte econ&oacute;mico de forma mensual, que les permita a los estudiantes y sus respectivos familiares, sufragar los gastos correspondientes a las necesidades escolares y que favorezcan del desarrollo educativo de los estudiantes.</p>

<h3>Poblaci&oacute;n meta:</h3>

<p>Estudiantes que cursen los niveles de materno, transici&oacute;n, pre k&iacute;nder, k&iacute;nder, preparatoria y primaria, que presenten vulnerabilidad econ&oacute;mica y educativa, y arriesgando con ello su permanencia en el sistema educativo.</p>

<p>Per&iacute;odo de implementaci&oacute;n:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; anual</p>

<p>Monto de beca: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &cent;18.000</p>

<p>Forma de pago:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; mensual</p>

<p>Rango de edad: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Preescolar: de 4 a 7 a&ntilde;os</p>

<p>Primaria: de 6 a 16 &nbsp;a&ntilde;os de edad</p>

<h3>Requisitos:&nbsp;&nbsp;</h3>

<ul>
	<li>Formulario de solicitud de beca 7F02. &nbsp;</li>
	<li>Documentos de identificaci&oacute;n.</li>
	<li>Documentos de certificaci&oacute;n de la condici&oacute;n acad&eacute;mica.</li>
	<li>Documentos de ingresos (constancias salariales, pensiones alimentarias, judiciales y/ o voluntarias, subsidios del estado (C.C.S.S, CONAPDIS, otros), as&iacute; como constancias de becas de universidades y municipalidades.</li>
	<li>Certificaci&oacute;n de notas del a&ntilde;o anterior.</li>
</ul>
', 1519918469, 1519918469, 1, 0, N'icon-g-tipo', 2, 1, 0, 1)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (6, N'circulares', N'Circulares', N'', N'<h1>Circulares</h1>

<hr />
<p>&nbsp;</p>
', 1519918469, 1519918469, 1, 0, N'icon-g-estado-financiero', 4, 1, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (7, N'tutoriales', N'Tutoriales', N'', N'<h1>Tutoriales</h1>

<p>&nbsp;</p>
', 1519918469, 1519918469, 1, 0, N'icon-g-videos', 8, 0, 1, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (8, N'mapa-del-sitio', N'Mapa del sitio', N'', N'', 1519918469, 1519918469, 8, 0, N'icon-g-centros', 0, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (9, N'privacidad', N'Privacidad', N'', N'<h2>T&eacute;rminos de Uso y Privacidad&nbsp;</h2>

<h3><strong>T&eacute;rminos y Condiciones</strong></h3>

<p>Por favor lea cuidadosamente estos T&eacute;rminos y Condiciones de Uso (T&eacute;rminos y Condiciones), antes de ingresar a cualquiera de nuestros servicios.</p>

<p>El acceso al Sitio Web de FONABE&nbsp; implica la aceptaci&oacute;n del usuario de las normas de uso que se establecen para el mismo, as&iacute; como la aceptaci&oacute;n del contenido de la cl&aacute;usula de Exenci&oacute;n de Responsabilidad, de los Derechos de la Propiedad Intelectual aplicables y de nuestra Pol&iacute;tica de Privacidad y Seguridad. El Usuario deber&aacute; consultar las condiciones espec&iacute;ficas que se establecen para el acceso y uso de determinados servicios dentro del Sitio Web de FONABE. El acceso a dichos servicios implica igualmente la aceptaci&oacute;n de dichas cl&aacute;usulas por parte del Usuario.</p>

<p><strong>Exenci&oacute;n de Responsabilidad</strong></p>

<p>El acceso al Sitio es gratuito y no genera relaci&oacute;n comercial, contractual o profesional alguna entre sus usuarios y Fondo Nacional de Becas.</p>

<p><strong>Reglas para la Conducta en L&iacute;nea</strong></p>

<p>El usuario est&aacute; de acuerdo con las normas de uso del Sitio, acorde con las leyes vigentes. Dado que FONABE es una organizaci&oacute;n no lucrativa, no est&aacute; autorizado para hacer uso del Sitio en la organizaci&oacute;n de actividades de partidos pol&iacute;ticos organizados.</p>

<p><strong>Permiso para el Uso de Materiales</strong></p>

<p>En consideraci&oacute;n al acuerdo del Usuario con los T&eacute;rminos y Condiciones contenidos aqu&iacute;, FONABE le garantiza una licencia de acceso (para las secciones que la requieran) personal e intransferible. El Usuario puede bajar la informaci&oacute;n del Sitio solamente para su uso personal o no comercial. El Usuario se compromete a no copiar, reproducir, retransmitir, distribuir, publicar, explotar comercialmente o transferir de cualquier otra forma ning&uacute;n material publicado en el Sitio, con fines diferentes a los publicados en la p&aacute;gina.</p>

<p><strong>Limitaciones de Uso</strong></p>

<p>El Usuario se compromete a usar el Sitio solamente para prop&oacute;sitos que sean legales. En caso de estar de acuerdo con lo anterior, si un tercero reclama que alg&uacute;n material publicado en el Sitio es ilegal, ser&aacute; responsabilidad del Usuario establecer si el material en cuesti&oacute;n corresponde con las leyes aplicables.</p>

<p><strong>Ligas a otros Sitios Web</strong></p>

<p>El Sitio contiene ligas a Sitios fuera del dominio de FONABE. La instituci&oacute;n no es responsable del contenido o pol&iacute;ticas de esos Sitios.</p>

<p><strong>Quejas relacionadas con Copyright</strong></p>

<p>FONABE respeta los derechos de propiedad de otros. Si usted considera que su copyright ha sido violado en el Sitio de la entidad, puede comunicarse con nosotros a trav&eacute;s de&nbsp;&nbsp;<a href="mailto:consultas@fonabe.go.cr">consultas@fonabe.go.cr</a>&nbsp;</p>

<p><strong>Pol&iacute;tica de Privacidad y Seguridad</strong></p>

<p>Fondo Nacional de becas, a trav&eacute;s de su Sitio Web, podr&aacute; solicitar informaci&oacute;n personal. Por lo tanto, la utilizaci&oacute;n de nuestro portal Web supone que usted reconoce haber le&iacute;do y aceptado nuestra Pol&iacute;tica de Privacidad y Seguridad que aqu&iacute; se presenta, as&iacute; como nuestros T&eacute;rminos y Condiciones.</p>

<p><strong>Recolecci&oacute;n y uso de la informaci&oacute;n</strong></p>

<p>La pol&iacute;tica de Privacidad y Seguridad de FONABE aplica a la recopilaci&oacute;n y uso de la informaci&oacute;n que usted nos suministre a trav&eacute;s de nuestro Sitio Web, dicha informaci&oacute;n ser&aacute; utilizada &uacute;nicamente por Fondo Nacional de Becas.<br />
La informaci&oacute;n se utilizar&aacute; con el prop&oacute;sito para la que fue solicitada. FONABE respeta su derecho a la privacidad, y no proveer&aacute; a terceras personas de la informaci&oacute;n personal de sus usuarios sin su consentimiento, a no ser que sea requerido por las leyes vigentes.<br />
Tampoco vender&aacute; informaci&oacute;n a terceras personas para prop&oacute;sitos de mercadeo, publicidad o promociones.</p>

<p><strong>Actualizaci&oacute;n de datos</strong></p>

<p>FONABE tiene y tendr&aacute; la facultad de efectuar, en cualquier momento y sin previo aviso, cualquier tipo de modificaci&oacute;n de este acuerdo, de modo que le recomendamos revisarlos peri&oacute;dicamente.</p>
', 1519918469, 1519918469, 8, 0, N'icon-g-centros', 0, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (10, N'sitios-de-interes', N'Sitios de Interes', N'', N'<h1>Sitios de interes&nbsp;</h1>

<hr />
<p>&nbsp;</p>

<p><a href="http://www.imas.go.cr/">Instituto mixto de ayuda social - IMAS</a></p>

<p><a href="http://www.pani.go.cr/">Patronato Nacional de la Infancia - PANI</a></p>

<p><a href="http://www.mep.go.cr/">Ministerio de Educaci&oacute;n P&uacute;blica - MEP</a></p>

<p><a href="https://www.cgr.go.cr/">Contralor&iacute;a General de la Rep&uacute;blica - CGR</a></p>

<p><a href="http://www.hacienda.go.cr/">Ministerio de Hacienda</a></p>

<p><a href="https://www.micit.go.cr/">Ministerio de Ciencia y Tecnolog&iacute;a - MICIT</a></p>

<p><a href="http://www.mideplan.go.cr/">Ministerio de Planificaci&oacute;n Nacional y Pol&iacute;tica Econ&oacute;mica - MIDEPLAN</a></p>

<p><a href="http://www.minae.go.cr/">Ministerio de Ambiente, Energ&iacute;a y Telecomunicaciones - MINAET</a></p>

<p><a href="http://www.estadonacion.or.cr/">Estado de la Naci&oacute;n</a></p>

<p><a href="http://www.ins-cr.com/index.html">Instituto Nacional de Seguros - INS</a></p>
', 1519918469, 1519918469, 8, 0, N'icon-g-centros', 0, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (11, N'preguntas-frecuentes', N'Preguntas frecuentes', N'', N'<p>Preguntas frecuentes:</p>

<p><br />
a.&nbsp;&nbsp; &nbsp;&iquest;C&oacute;mo se obtiene el c&oacute;digo presupuestario del centro educativo, ya que el mismo es nuevo?</p>

<p>El c&oacute;digo presupuestario es brindado por el MEP, es por ello que debe comunicarse directamente con ellos.</p>

<p><br />
b.&nbsp;&nbsp; &nbsp;Los padres de los solicitantes pueden presentarse a realizar la segunda revisi&oacute;n de los expedientes incompletos</p>

<p>No, &uacute;nicamente los puede presentar el encargado de becas y el director del centro educativo, en caso de que ninguno de los dos puede asistir, un miembro del comit&eacute; de becas lo puede presentar, con una autorizaci&oacute;n del encargado de becas o bien del director.</p>

<p>Solamente en el caso de que el solicitante presente sea de riesgo social, cuente con alg&uacute;n tipo de discapacidad, sea ind&iacute;gena o madre adolescente; &nbsp;deber&aacute;n descargar el formulario de la p&aacute;gina web FONABE, &nbsp;y reservar una cita en las fechas establecidas por el Fondo.&nbsp;</p>

<p><br />
c.&nbsp;&nbsp; &nbsp;Fechas en las que se habilita el sistema de citas</p>

<p>Las fechas ser&aacute;n publicadas en la p&aacute;gina de FONABE y &uacute;nicamente son otorgadas por medio de la p&aacute;gina de FONABE o comunic&aacute;ndose con el call center.</p>

<p><br />
d.&nbsp;&nbsp; &nbsp;&iquest;Por qu&eacute; motivo se suspenden las becas?</p>

<p>Entre las razones por las cu&aacute;les se suspende un beneficio, se encuentran:</p>

<p>&nbsp;&nbsp; &nbsp;Deserci&oacute;n: Es un mes de ausencia injustificada, el centro educativo &nbsp;puede enviar a suspender por este motivo en cualquier momento del a&ntilde;o, presentando la boleta de suspensi&oacute;n.</p>

<p>&nbsp;&nbsp; &nbsp;Reprobaci&oacute;n del a&ntilde;o sin justificaci&oacute;n: Aplica durante todo el a&ntilde;o, se incluye en el proceso de pr&oacute;rroga y de corroboraci&oacute;n; &nbsp;el centro educativo deber&aacute; justificar la reprobaci&oacute;n del estudiante con la respectiva boleta, la cual ser&aacute; &nbsp;valorada por el equipo de trabajo social.</p>

<p>&nbsp;&nbsp; &nbsp;Mejoramiento significativo de la condici&oacute;n socioecon&oacute;mica: El centro educativo puede comunicar en cualquier momento del a&ntilde;o esta condici&oacute;n, &nbsp;para realizar el an&aacute;lisis que corresponde. Se solicita el formulario del estudiante debidamente completo y actualizado que ser&aacute; valorado por el equipo de trabajo social.&nbsp;<br />
Asimismo en caso de ser una denuncia por este motivo, el equipo de trabajo social &nbsp;realizar&aacute; una investigaci&oacute;n de campo para la respectiva valoraci&oacute;n social.</p>

<p>&nbsp;&nbsp; &nbsp;Defunci&oacute;n: El centro educativo lo puede comunicar en cualquier momento del a&ntilde;o por medio de la boleta de suspensi&oacute;n.</p>

<p>&nbsp;&nbsp; &nbsp;Egresado: Aplica durante todo el a&ntilde;o, se incluye en el proceso de pr&oacute;rroga y de corroboraci&oacute;n.</p>

<p>&nbsp;&nbsp; &nbsp;M&eacute;rito Personal: El centro educativo puede comunicar en cualquier momento del a&ntilde;o por medio de un oficio donde describa a detalle la situaci&oacute;n, aportando los documentos probatorios del debido proceso el cual ser&aacute; valorado por el equipo de trabajo social de FONABE.&nbsp;</p>

<p>&nbsp;&nbsp; &nbsp;Traslado de centro educativo No reportado: El centro educativo informa que &eacute;l o la estudiante no pertenece a dicho centro educativo.</p>

<p>&nbsp;&nbsp; &nbsp;No prorrogado / corroborado por el centro educativo.</p>

<p>&nbsp;&nbsp; &nbsp;Renuncia: Se solicita una carta con la copia de la c&eacute;dula del padre, madre, o encargado del beneficiario; este proceso se puede realizar durante todo el a&ntilde;o.</p>

<p>e.&nbsp;&nbsp; &nbsp;&iquest;C&oacute;mo se tramita la solicitud de una beca de Puente al Desarrollo?</p>

<p><br />
El tr&aacute;mite debe ser realizado por medio de los cogestores del Instituto Mixto de Ayuda Social (IMAS).</p>

<p>f.&nbsp;&nbsp; &nbsp;&iquest;C&oacute;mo se puede gestionar una solicitud a beca, cuando el solicitante o los padres se encuentran en condici&oacute;n de indocumentados?</p>

<p>En el caso de que el solicitante sea indocumentado debe aportar alg&uacute;n tipo de documento que lo identifique, al igual que sus padres.</p>

<p>En caso de que el solicitante no pueda aportar ning&uacute;n tipo de documento que lo identifique, el encargado de becas del centro educativo deber&aacute; completar la boleta &ldquo;PREVENCI&Oacute;N DE DOCUMENTACI&Oacute;N FALTANTE&rdquo; ante FONABE. &nbsp;As&iacute; como tambi&eacute;n, el centro educativo debe emitir un oficio (firmado y sellado) por el director/a si los padres de familia son indocumentados, indicando que el n&uacute;cleo familiar est&aacute; conformado por dichas personas.</p>

<p>g.&nbsp;&nbsp; &nbsp;&iquest;Un estudiante indocumentado al igual que un estudiante nacional o con c&eacute;dula de residencia, debe aportar los mismos requisitos?</p>

<p>Si se deben aportar los mismos requisitos, sin excepci&oacute;n alguna.</p>

<p>h.&nbsp;&nbsp; &nbsp;&iquest;Por qu&eacute; raz&oacute;n un postulante que se visualizaba en la consulta realizada en la p&aacute;gina web, ahora no se refleja?</p>

<p><br />
Lo que sucede en estos casos es que el postulante a un beneficio ha sido tomado en cuenta en los procesos de asignaci&oacute;n y estar&aacute; siendo conocido por parte de la Junta Directiva en la sesi&oacute;n de cada mes, con el fin de que el mismo sea aprobado. Por lo que la condici&oacute;n es de Recomendado.</p>

<p>i.&nbsp;&nbsp; &nbsp;&iquest;Por qu&eacute; no se ha efectuado el dep&oacute;sito del beneficio?</p>

<p>En el caso de que el beneficiario no haya cambiado o extraviado la tarjeta, deber&aacute; dar un tiempo prudencial y comunicarse con el call center y verificar la situaci&oacute;n presentada&nbsp;</p>

<p>Si dado el caso, el beneficiario cambi&oacute; la tarjeta, deber&aacute; esperar hasta que la nueva tarjeta sea reflejada en el sistema, lo cual puede tardar aproximadamente tres meses.&nbsp;</p>

<p>j.&nbsp;&nbsp; &nbsp;&iquest;Qu&eacute; aspectos se valoran para la obtener un beneficio?</p>

<p>FONABE para la adjudicaci&oacute;n de becas, verifica la condici&oacute;n socioecon&oacute;mica de los solicitantes, bajo par&aacute;metros socioecon&oacute;micos establecidos por el Instituto Nacional de Estad&iacute;stica y Censos (pobreza extrema, pobreza, vulnerabilidad y no pobre); as&iacute; como tambi&eacute;n los de &iacute;ndole educativo relacionados al &iacute;ndice de vulnerabilidad educativa; &nbsp;para esto se utiliza el Modelo Integrado de Calificaci&oacute;n de Becas (MICB), de forma que, todas las solicitudes de becas se valoran, contempl&aacute;ndose los ingresos brutos obtenidos en el grupo familiar indicados en el formulario, no as&iacute; los egresos, los cuales no son objeto de valoraci&oacute;n. &nbsp;</p>

<p>k.&nbsp;&nbsp; &nbsp;&iquest;De qu&eacute; forma se pueden realizar las apelaciones de la beca de Postsecundaria?</p>

<p>El estudiante se debe presentar en FONABE con la carta de apelaci&oacute;n, en la cual se indique el motivo y los datos personales, as&iacute; como adjuntar la documentaci&oacute;n que certifique lo manifestado, en un tiempo de tres d&iacute;as h&aacute;biles a partir de la recepci&oacute;n de la notificaci&oacute;n de cierre, el tiempo corre a partir del d&iacute;a en que es enviado el correo electr&oacute;nico con dicha notificaci&oacute;n.</p>

<p><br />
l.&nbsp;&nbsp; &nbsp;&iquest;Qu&eacute; pasa si la cuenta que tiene asignada el beneficiario no existe en el sistema de FONABE?</p>

<p>Debe presentarse a FONABE con el documento del Banco Nacional (firmado y sellado), &nbsp;que indique que la cuenta est&aacute; cerrada.&nbsp;</p>
', 1519918469, 1519918469, 8, 0, N'icon-g-centros', 0, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (12, N'circulares', N'Circulares', N'circulares', N'<h1>Circulares</h1>

<hr />
<p>&nbsp;</p>
', 1519918469, 1519918469, 2, 0, N'icon-g-estado-financiero', 4, 1, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (13, N'tutoriales', N'Tutoriales', N'tutoriales', N'<h1><meta charset="utf-8" /><b id="docs-internal-guid-ec127034-8c7c-9fcd-d195-c35916dea602">Tutoriales</b></h1>

<hr />
<p>&nbsp;</p>
', 1519918469, 1519918469, 2, 0, N'icon-g-videos', 5, 0, 1, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (14, N'tramites', N'Trámites', N'tramites', N'<h1>Pr&oacute;rroga universidad</h1>

<hr />
<p>El proceso de pr&oacute;rroga de postsecundaria se realiza seg&uacute;n la modalidad de matr&iacute;cula de la universidad en la cual se encuentre empadronado el beneficiario, es decir, semestral y/o cuatrimestral. Los requisitos para este proceso son los siguientes:</p>

<ul>
	<li>Constancia de materias matriculadas (membretadas y selladas).</li>
	<li>Constancia de materias aprobadas en el &uacute;ltimo per&iacute;odo (membretadas y selladas).</li>
	<li>Carta de FONABE para la realizaci&oacute;n de horas de proyecci&oacute;n social al beneficiario.</li>
	<li>Carta de la instituci&oacute;n en donde el beneficiario ha realizado las horas de proyecci&oacute;n social, debe detallar la fecha de inicio, la fecha de finalizaci&oacute;n, las actividades realizadas y el total de las horas realizadas. La carta debe estar membretada, firmada y sellada por la m&aacute;xima autoridad.</li>
	<li>Control de horas de proyecci&oacute;n social (membretada, detallar las actividades realizadas, total de horas, firmada y sellada).</li>
	<li>Carta por incumplimiento del &ldquo;contrato de beca para estudios postsecundarios&rdquo;</li>
	<li>Contrato de beca postsecundaria.</li>
</ul>

<p>&nbsp;</p>
', 1519918469, 1519918469, 2, 0, N'icon-g-transaccion', 3, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (15, N'becas-universitarias', N'Becas Universitarias', N'Becas Universitarias', N'<h1>Becas Universitarias</h1>

<hr />
<h3>Objetivo general:</h3>

<p>Brindar un apoyo econ&oacute;mico a estudiantes que presenten condici&oacute;n de vulnerabilidad econ&oacute;mica y educativa, que cursan estudios en colegios universitarios, parauniversitarios, as&iacute; como en las universidades p&uacute;blicas y privadas, para la conclusi&oacute;n de sus estudios hasta el grado de bachillerato.</p>

<h3>Poblaci&oacute;n meta:</h3>

<p>Estudiantes matriculados en colegios universitarios o parauniversitarios, as&iacute; como en las universidades p&uacute;blicas y privadas todas reconocidas por el CONESUP, que presenten condici&oacute;n de vulnerabilidad econ&oacute;mica y educativa</p>

<p>&nbsp;</p>

<p>Per&iacute;odo de implementaci&oacute;n:&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;cuatrimestral, semestral</p>

<p>Monto de beca: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &cent;51.800 (dos o tres materias)</p>

<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&cent;83.000 (cuatro o m&aacute;s materias)</p>

<p>&nbsp;</p>

<p>Forma de pago:&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Mensual</p>

<p>Rango de edad: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 17 a&ntilde;os en adelante</p>

<p>&nbsp;</p>

<p>Requisitos para primer ingreso:</p>

<ul>
	<li>Formulario de solicitud de beca 7F02.</li>
	<li>C&eacute;dula de identidad.</li>
	<li>Documentos de ingresos (constancias salariales, pensiones alimentarias, judiciales y/ o voluntarias, subsidios del estado (C.C.S.S, CONAPDIS, otros), as&iacute; como constancias de becas de universidades y municipalidades.</li>
	<li>Copia de t&iacute;tulo de secundaria (o certificaci&oacute;n correspondiente)</li>
	<li>Plan de estudios (con c&oacute;digo, nombre de la materia, cr&eacute;ditos) sellado y firmado.</li>
	<li>Constancia de materias matriculadas (membretadas y selladas).</li>
	<li>Constancia de materias aprobadas en el &uacute;ltimo per&iacute;odo (membretadas&nbsp; selladas).</li>
	<li>Boleta de justificaci&oacute;n de reprobaci&oacute;n (en caso de reprobar un curso en el momento de presentar la solicitud por primera vez).</li>
</ul>
', 1519918469, 1519918469, 2, 0, N'icon-g-universidad', 1, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (16, N'solicitud-de-beca', N'Solicitud de Beca', N'Solicitud de Beca', N'<h1>Solicitud de Beca</h1>

<hr />
<p>&nbsp;</p>
', 1519918469, 1519918469, 2, 0, N'icon-g-solicitud', 2, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (17, N'estado-solicitud', N'Estado Solicitud', N'Estado Solicitud', N'', 1519918469, 1519918469, 2, 0, N'icon-g-estados', 0, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (18, N'circulares', N'Circulares', N'Circulares', N'<h1>Circulares</h1>

<hr />
<p>&nbsp;</p>
', 1519918469, 1519918469, 3, 0, N'icon-g-estado-financiero', 4, 1, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (19, N'tutoriales', N'Tutoriales', N'Tutoriales', N'<h1>Tutoriales</h1>

<hr />
<p>&nbsp;</p>
', 1519918469, 1519918469, 3, 0, N'icon-g-videos', 6, 0, 1, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (20, N'tramites', N'Trámites', N'Tramites', N'<h1><meta charset="utf-8" /><b id="docs-internal-guid-ec127034-8c8e-7e48-da52-af04fdb14a55">Tr&aacute;mites</b></h1>

<hr />
<p>&nbsp;</p>
', 1519918469, 1519918469, 3, 0, N'icon-g-transaccion', 3, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (21, N'becas-universitarias', N'Becas de Educación', N'Becas Universitarias', N'<h1><meta charset="utf-8" /><b id="docs-internal-guid-12a7eff4-8d68-328a-00b8-806e9f650cfc">Becas de educaci&oacute;n</b></h1>

<hr />
<p>&nbsp;</p>
', 1519918469, 1519918469, 3, 0, N'icon-g-universidad', 1, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (22, N'solicitud-de-beca', N'Solicitud de beca', N'Solicitud de beca', N'<h1>Solicitud de beca</h1>

<hr />
<p>&nbsp;</p>
', 1519918469, 1519918469, 3, 0, N'icon-g-solicitud', 2, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (23, N'estado-de-solicitd', N'Estado de solicitd', N'Estado de solicitd', N'<h1>Estado de solicitud</h1>

<hr />
<p>&nbsp;</p>
', 1519918469, 1519918469, 3, 0, N'icon-g-estados', 5, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (24, N'noticias', N'Noticias', N'Noticias del Fondo Nacional de Becas', N'', 1519918469, 1519918469, 4, 0, N'icon-g-centros', 0, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (25, N'normativa', N'Normativa', N'Normativa', N'<h1>Normativa</h1>

<hr />
<p>&nbsp;</p>
', 1519918469, 1519918469, 5, 0, N'icon-g-tipo', 0, 1, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (26, N'gestion-de-rrhh', N'Gestión de RRHH', N'Gestión de RRHH', N'<h1>Gesti&oacute;n de RRHH</h1>

<hr />
<p>&nbsp;</p>
', 1519918469, 1519918469, 5, 0, N'icon-g-comprometida', 0, 1, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (27, N'contraloria-de-servicios', N'Contraloria de servicios', N'Contraloria de servicios', N'<h1><big>Contraloria de servicios</big></h1>

<hr />
<p>Las Contralor&iacute;as de Servicios fueron creadas mediante la ley N&ordm; 9158, como un mecanismo para garantizar los derechos de las personas usurarias de los servicios que brindan organizaciones p&uacute;blicas y empresas privadas que brindan servicios, que est&eacute;n inscritos en el Sistema de conformidad con esta ley, coadyuvando con ello en la efectividad, mejora continua e innovaci&oacute;n en la prestaci&oacute;n de los servicios.</p>

<h3>Procedimiento</h3>

<p><strong>2.4</strong>&nbsp; Atenci&oacute;n de quejas, denuncias y sugerencias</p>

<p><strong>2.4.1</strong> Esta fase incluye la manera de informar al usuario sobre los puntos de contacto con FONABE a los cuales puede recurrir cuando lo requiera, cualquiera sea su gesti&oacute;n, especialmente, cuando se trata de una queja o denuncia o sugerencia</p>

<p><strong>2.4.2</strong> Una queja es todo comentario del usuario que manifieste un grado de inconformidad con relaci&oacute;n a los servicios ofrecidos por FONABE y cuya atenci&oacute;n permita mejorar la calidad del producto/servicio.</p>

<p><br />
<strong>2.4.3</strong> Las quejas, las denuncias y las sugerencias se pueden recibir por v&iacute;a telef&oacute;nica, correo electr&oacute;nico: consultas@fonabe.go.cr o al correo de cualquier funcionario de la organizaci&oacute;n, (en cuyo caso deber&aacute; de pasarlo a la Contralor&iacute;a de Servicios, para su respectivo control y asignaci&oacute;n, en casos simples podr&aacute;n contestarlos de inmediato notificando lo actuado en el formulario 8F15, Hoja de seguimiento), personalmente de forma verbal o escrita completando el 8F11, Atenci&oacute;n de quejas y denuncias de los usuarios y por medio del buz&oacute;n de sugerencias con el 8F10, Sugerencias de los usuarios, las cuales deber&aacute;n ser remitidas a la Contralor&iacute;a de Servicios para que esta las enumere con el consecutivo de control y las remita al funcionario encargado de darle tr&aacute;mite, seg&uacute;n corresponda. Los formularios deber&aacute;n venir con copia del correo electr&oacute;nico de la nota escrita, como soporte.</p>

<p><strong>2.4.4</strong>&nbsp;Cuando se reciba una queja, denuncia o sugerencia, debe ser remitida de inmediato al Contralor de Servicios donde se registra electr&oacute;nicamente en 8F09, Control de quejas y denuncias de los usuarios, 8F16, Control de denuncias o 8F17, Control de sugerencias, le informa por el medio que considere conveniente a la Direcci&oacute;n Ejecutiva y la asigna al responsable due&ntilde;o del proceso al que se refiere la queja, quien realiza un an&aacute;lisis de las causas y consecuencias, utilizando para ello un diagrama &ldquo;causa-efecto en el&nbsp; 8F05, Acci&oacute;n correctiva preventiva, siguiendo el procedimiento 8P02, Mejora continua.</p>

<p><strong>2.4.5 </strong>La Contralor&iacute;a de servicios recibe el 8F11, Atenci&oacute;n de quejas y&nbsp; de los usuarios y/o el 8F10, Sugerencias de los usuarios con el an&aacute;lisis de causas y consecuencias y si se declara procedente, lo registra utilizando en el formulario 8F05, Acci&oacute;n correctiva preventiva, siguiendo el apartado (2.4. y 2.5.) del procedimiento 8P02, Mejora continua.</p>

<p><strong>2.4.6</strong> La Contralor&iacute;a de servicios procede a comunicar al responsable del proceso las acciones por seguir, que deben ser atendidas en un tiempo de 5 d&iacute;as h&aacute;biles.</p>

<p><strong>2.4.7</strong>&nbsp;Si la queja, denuncia o sugerencia se declara no procedente no se genera ninguna documentaci&oacute;n.</p>

<p><strong>2.4.8</strong>&nbsp;El responsable de investigar la queja, denuncia o sugerencia comunica al usuario los resultados de la gesti&oacute;n, y le presenta las explicaciones pertinentes. Esta acci&oacute;n de respuesta se ejecuta en los casos simples en un plazo no mayor a 48 horas a partir del registro de la queja, denuncia o sugerencia.</p>

<p><strong>2.4.9</strong>&nbsp;Si la queja, denuncia o sugerencia requiere de mayor an&aacute;lisis para su atenci&oacute;n, el responsable de la gesti&oacute;n mantiene informado al usuario y a la Contralor&iacute;a de Servicios de los avances.</p>

<p><strong>2.4.10</strong>&nbsp;Una vez comunicado al usuario el an&aacute;lisis efectuado y las conclusiones, El funcionario a cargo de la gesti&oacute;n deber&aacute; devolver el formulario completo y firmado a la Contralor&iacute;a de Servicios, para completar la hoja de control, cerrar el caso y archivarlo.</p>

<p>&nbsp;</p>

<p>NOTA TODA QUEJA , CONSULTA O DENUNCIA DEBE DE INCLUIR LOS SIGUIENTES REQUISITOS:</p>

<p>NOMBRE DE LA PERSONA QUE REALIZA LA QUEJA, CONSULTA O DENUNCIA, N&Uacute;MERO DE C&Eacute;DULA, CORREO ELECTRONICO Y TEL&Eacute;FONO.</p>

<p>NOMBRE Y N&Uacute;MERO DE C&Eacute;DULA DEL BENEFICIARIOO PERSONA A QUE SE REFIERE LA QUEJA, CONSULTA O DENUNCIA</p>

<p>NOMBRE DE LA ESCUELA Y DIRECCION REGIONAL A QUE PERTENECE</p>

<p>Las denuncias son tratadas con absoluta confidencialidad</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>SOLICITUDES DE INFORMACI&Oacute;N P&Uacute;BLICA QUE QUIERA SOLICITAR Y ESTE CONTEMPLADA EN EL DECRETO&nbsp; EJECUTIVO N&ordm; 40200-MP-MEIC&mdash;MC O EN LA DIRECTRIZ EJECUTIVA N&ordm; 073-MP-MEIC-MC</p>

<p>Oficial de Acceso a la Informaci&oacute;n (OAI)<br />
Encargado: Lic. Orlando Miranda Mora</p>

<p>Correo: omiranda@fonabe.go.cr</p>

<p>Tel&eacute;fono: (+506) 2542-0215</p>

<p>&nbsp;</p>

<h3><strong>Formularios descargables</strong></h3>
', 1519918469, 1519918469, 5, 0, N'icon-g-contralor', 0, 1, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (28, N'contratacion-administrativa', N'Contratación administrativa', N'Contratación administrativa', N'<h1>Contrataci&oacute;n administrativa</h1>

<hr />
<h3>REGLAMENTO PARA EL FUNCIONAMIENTO DE LAS PROVEEDUR&Iacute;AS INSTITUCIONALES DE LOS MINISTERIOS DEL GOBIERNO</h3>

<p>Art&iacute;culo 1&ordm;&nbsp;&mdash;Definici&oacute;n funcional de Proveedur&iacute;a Institucional. Las Proveedur&iacute;as Institucionales ser&aacute;n las competentes para tramitar los procedimientos de contrataci&oacute;n administrativa que interesen al respectivo Ministerio, as&iacute; como para realizar los procesos de almacenamiento y distribuci&oacute;n o tr&aacute;fico de bienes y llevar un inventario permanente de todos sus bienes.</p>

<p>MISI&Oacute;N</p>

<p>Adquirir, dotar y administrar los bienes y servicios necesarios para atender las demandas de las dependencias que conforman la Instituci&oacute;n, dentro de un marco de calidad y oportunidad, de acuerdo con las pol&iacute;ticas institucionales, con un personal altamente motivado.</p>

<h3>VISI&Oacute;N</h3>

<p>Conformar una Proveedur&iacute;a Institucional que utiliza la m&aacute;s avanzada tecnolog&iacute;a en la ejecuci&oacute;n de sus labores, renovando permanentemente su gesti&oacute;n en sistemas, procesos y recursos para brindar un servicio eficiente y eficaz; siempre en busca de la satisfacci&oacute;n de nuestros clientes y proveedores.</p>

<h3>OBJETIVO GENERAL</h3>

<p>Planificar, dirigir y controlar las diferentes etapas del proceso de adquisici&oacute;n y distribuci&oacute;n de los bienes y servicios del Fondo Nacional de Becas; dentro de la normativa y procedimientos establecidos para este fin, manteniendo un registro actualizado y detallado de todos los activos de la Instituci&oacute;n, satisfaciendo el inter&eacute;s p&uacute;blico.</p>

<h3>OBJETIVOS ESPEC&Iacute;FICOS</h3>

<ol>
	<li>
	<p>&nbsp;Lograr un equipo de trabajo comprometido con la Instituci&oacute;n.</p>
	</li>
	<li>
	<p>&nbsp;Mantener un ambiente de motivaci&oacute;n y solidaridad.</p>
	</li>
	<li>
	<p>&nbsp;Mantener un programa de capacitaci&oacute;n y refrescamiento de la normativa.</p>
	</li>
</ol>
', 1519918469, 1519918469, 5, 0, N'icon-g-contratacion-admin', 0, 1, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (29, N'informes', N'Informes', N'Informes', N'<h1>Informes</h1>

<hr />
<h1>&nbsp;</h1>
', 1519918469, 1519918469, 5, 0, N'icon-g-informe', 0, 1, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (30, N'presupuestos', N'Presupuestos', N'Presupuestos', N'<h1>Presupuestos</h1>

<hr />
<h1>&nbsp;</h1>
', 1519918469, 1519918469, 5, 0, N'icon-g-moneda', 0, 1, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (31, N'estados-financieros', N'Estados Financieros', N'Estados Financieros', N'<h1>Estados Financieros</h1>

<hr />
<p>&nbsp;</p>
', 1519918469, 1519918469, 5, 0, N'icon-g-estado-financiero', 0, 1, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (32, N'liquidaciones', N'Liquidaciones', N'Liquidaciones', N'<h1>Liquidaciones</h1>

<hr />
<p>&nbsp;</p>
', 1519918469, 1519918469, 5, 0, N'icon-g-liquidacion', 0, 1, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (33, N'estadisticas', N'Estadisticas', N'Estadisticas', N'<h1>Estadisticas</h1>

<hr />
<p>&nbsp;</p>
', 1519918469, 1519918469, 5, 0, N'icon-g-estadística', 0, 1, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (34, N'archivo-institucional', N'Archivo Institucional', N'Archivo Institucional', N'<h1>Archivo Institucional</h1>

<hr />
<div>
<p>El Archivo Institucional es un departamento de la Direcci&oacute;n Administrativa Financiera, que centraliza el acervo documental que producen las oficinas de la instituci&oacute;n, de acuerdo con los plazos establecidos de remisi&oacute;n de documentos.</p>

<p>Se crea en el mes de abril de 2008 cuando por primera vez la instituci&oacute;n contrata a un profesional en Archiv&iacute;stica. Luego de varias gestiones de este profesional, en octubre de ese mismo a&ntilde;o, la instituci&oacute;n asigna el local para el Archivo y en &nbsp;diciembre, se adquiere estanter&iacute;a m&oacute;vil y cajas especiales para documentos, con lo cual se inicia el tratamiento archiv&iacute;stico y, por ende, la organizaci&oacute;n del archivo.</p>

<p>Desde su creaci&oacute;n es un departamento unipersonal y posee tres &aacute;reas:</p>

<p>a) &Aacute;rea Administrativa: Donde la encargada realiza labores archiv&iacute;sticas y administrativas.</p>

<p>b) &Aacute;rea de Dep&oacute;sito: Cuenta con archivos m&oacute;viles donde se custodia y conserva el acervo documental de la instituci&oacute;n.</p>

<p>c) &Aacute;rea de consulta: Donde los usuarios pueden consultar los documentos que solicitan.</p>

<p>El Archivo Institucional de Fonabe as&iacute; como los archivos de cada una de sus oficinas, forman parte del Sistema Nacional de Archivos de Costa Rica, por lo cual se rigen por la Ley N&ordm; 7202 del Sistema Nacional de Archivos y su Reglamento, cuyo ente rector es el Archivo Nacional de Costa Rica.</p>

<p>Entre las principales funciones que realiza el Archivo Institucional est&aacute;n:</p>

<p>a) Reunir, conservar, clasificar, ordenar, describir, seleccionar, administrar y facilitar el acervo documental producido por la instituci&oacute;n en sus diversos formatos.</p>

<p>b) Velar por la aplicaci&oacute;n de pol&iacute;ticas archiv&iacute;sticas y asesorar t&eacute;cnicamente al personal de la instituci&oacute;n que labore en los archivos de gesti&oacute;n.</p>

<p>c) Colaborar en la b&uacute;squeda de soluciones para el buen funcionamiento del archivo institucional y de los archivos de gesti&oacute;n de la entidad.</p>

<p>Los documentos m&aacute;s antiguos que custodia datan de mayo de 1951 y corresponden a las Actas del extinto Patronato de Estudiantes Costarricenses en el exterior, perteneciente al Ministerio de Educaci&oacute;n P&uacute;blica y que otorgaba becas a estudiantes costarricenses que cursaban sus estudios universitarios en el extranjero.</p>

<p>En julio de 2010 se le otorga al Archivo Institucional por parte de la Junta Administrativa del Archivo Nacional, el premio &ldquo;Luz Alba Chac&oacute;n de Uma&ntilde;a&rdquo; como archivo distinguido, por su destacada labor en la organizaci&oacute;n de su fondo documental.</p>

<p>Finalmente, su principal servicio es facilitar los documentos que custodia, con ello garantiza la transparencia, el acceso a la informaci&oacute;n p&uacute;blica y la rendici&oacute;n de cuentas.</p>
</div>
', 1519918469, 1519918469, 5, 0, N'icon-g-carpeta', 0, 1, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (35, N'junta-directiva', N'Junta Directiva', N'Junta Directiva', N'<h1>Junta Directiva</h1>

<hr />
<div>
<p><strong><big>Presidente</big></strong>: Msc. Rosa Adolio Cascante/Representante Ministerio de Educaci&oacute;n P&uacute;blica</p>

<p><strong><big>Vicepresidente</big></strong>: Lic.&nbsp; Arturo Miranda Chaves/Representante del Instituto Mixto de Ayuda Social</p>

<p><strong><big>Tesorera</big></strong>: Licda.Rosaura S&aacute;nchez Bola&ntilde;os/Representante de la Uni&oacute;n Costarricense de&nbsp; C&aacute;maras y Asociaciones del Sector Empresarial Privado (UCCAEP).&nbsp;</p>

<p><strong><big>Secretario</big></strong>: Arq. Roberto Hall Retana/Representante de la Federaci&oacute;n de Colegios Profesionales Universitarios de C.R</p>

<p><strong><big>Vocal</big></strong>: Dr. Mart&iacute;n Parada G&oacute;mez/Representante del Consejo Nacional de Rectores</p>

<p><strong><big>Fiscal</big></strong>: Lic. Jorge Oviedo &Aacute;lvarez/Representante de Procuradur&iacute;a General de la Rep&uacute;blica</p>
</div>
', 1519918469, 1519918469, 6, 0, N'icon-g-junta', 0, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (36, N'calendario', N'Calendario', N'Calendario', N'', 1519918469, 1519918469, 6, 0, N'icon-g-calendario-fechas', 0, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (37, N'multimedia', N'Multimedia', N'Multimedia', N'<h1>Multimedia</h1>

<hr />
<p>&nbsp;</p>
', 1519918469, 1519918469, 6, 0, N'icon-g-videos', 0, 0, 1, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (38, N'historia', N'Historia', N'Historia', N'<h1>Historia</h1>

<hr />
<h1><strong>FONDO NACIONALDE BECAS (FONABE)</strong></h1>

<h2><strong>Constituci&oacute;n</strong></h2>

<p>El Fondo Nacional de Becas (FONABE), es creado como una instituci&oacute;n sin fines de lucro, que brinda ayuda a estudiantes de familias de escasos recursos econ&oacute;micos, para que cursen y concluyan, con &eacute;xito, el proceso educativo.&nbsp;</p>

<p>As&iacute;, con la creaci&oacute;n del FONABE, se incorpor&oacute; la participaci&oacute;n y colaboraci&oacute;n de:<br />
&nbsp;&nbsp; &nbsp;Ministerio de Educaci&oacute;n P&uacute;blica (del que depender&iacute;a como &oacute;rgano adscrito).<br />
&nbsp;&nbsp; &nbsp;Uni&oacute;n Costarricense de C&aacute;maras de Empresas Privadas.<br />
&nbsp;&nbsp; &nbsp;Consejo Nacional de Rectores (mediante la representaci&oacute;n de una de sus universidades).&nbsp;<br />
&nbsp;&nbsp; &nbsp;Instituto Mixto de Ayuda Social&nbsp;<br />
&nbsp;&nbsp; &nbsp;Federaci&oacute;n de Colegios Profesionales Universitarios.&nbsp;</p>

<p>Todos ellos conforman la Junta Directiva del Fondo Nacional de Becas. La representaci&oacute;n de diferentes sectores dentro de la Junta Directiva tuvo como fin permitir un adecuado balance y con ello evitar en lo posible intromisiones pol&iacute;tico partidistas.</p>

<h2><strong>Per&iacute;odo de Consolidaci&oacute;n</strong></h2>

<p>El FONABE, en concordancia con lo se&ntilde;alado en la disposici&oacute;n a) del informe DFOE-SOC-13-2008, realiz&oacute; esfuerzos para establecer alianzas estrat&eacute;gicas con distintas instituciones del Sector Social, y con ello promover la suscripci&oacute;n de instrumentos formales.</p>

<p>Hasta esa fecha se hab&iacute;an concretado dos convenios con Instituciones pertenecientes al Sector Social entre ellos, el Patronato Nacional de la Infancia y la Direcci&oacute;n General de Desarrollo Social y Asignaciones Familiares.</p>

<p>Asimismo, se hab&iacute;an suscrito convenios con diferentes instituciones que aunque no pertenec&iacute;an al Sector Social, de alguna manera favorecieron el Programa de Becas del FONABE, entre las cuales se pudieron identificar el Banco Nacional de Costa Rica, Facultad Latinoamericana de Ciencias Sociales, EDUNAMICA y Tiendas Universal. El FONABE tambi&eacute;n implement&oacute; alianzas estrat&eacute;gicas con entidades como el Centro de Formaci&oacute;n en Tecnolog&iacute;as de Informaci&oacute;n, Centro de Transferencias de Tecnolog&iacute;a, Polit&eacute;cnico Internacional, INVENIO, Universidad Juan Pablo II, Consejo Nacional de Competitividad-Asociaci&oacute;n C&aacute;mara de Tecnolog&iacute;a de Informaci&oacute;n y Comunicaci&oacute;n, con el prop&oacute;sito de transferir becas del 100% a j&oacute;venes graduados de la educaci&oacute;n media para que se formen como especialistas en tecnolog&iacute;as de la informaci&oacute;n mediante las becas del Programa Costa Rica Especialista. As&iacute; mismo se establecieron alianzas con la Oficina de Erradicaci&oacute;n del Trabajo Infantil y Protecci&oacute;n al Adolescente Trabajador, Ministerio de Justicia con Adaptaci&oacute;n Social y la Comisi&oacute;n Nacional de Asuntos Ind&iacute;genas, esfuerzos que fortalecieron la gesti&oacute;n del FONABE dirigida a esta poblaci&oacute;n.</p>

<h2><strong>Actualidad/&Aacute;reas de Acci&oacute;n</strong></h2>

<p>El FONABE se ha dedicado, desde su creaci&oacute;n en 1997, a generar pol&iacute;ticas y acciones concretas que facilitan la permanencia de los estudiantes de bajos recursos econ&oacute;micos dentro del sistema educativo.</p>

<p>Perfilada como una instituci&oacute;n rectora en materia de becas para la educaci&oacute;n, ha experimentado un permanente crecimiento y madurez organizacional que le ha permitido establecer una estructura participativa y eficaz, la cual se rige por procesos agiles y eficientes, apoyados en normas t&eacute;cnicas que garantizan la transparencia en la asignaci&oacute;n de los recursos invertidos.</p>

<p><br />
Los resultados obtenidos son producto de un arduo trabajo para construir la instituci&oacute;n; la definici&oacute;n de metas y planes de trabajo; el establecimiento de la normativa interna necesaria y la consolidaci&oacute;n de un sistema seguro y eficiente para asignar y distribuir los subsidios a los beneficiarios.&nbsp;</p>

<p>El principal objetivo a lograr trav&eacute;s de las diferentes acciones, es orientar la gesti&oacute;n del Fondo Nacional de Becas hacia el cumplimiento de los planes y objetivos propuestos para los pr&oacute;ximos a&ntilde;os.</p>

<p>Actualmente las principales acciones estrat&eacute;gicas dentro de la gesti&oacute;n del fondo se encuentran: Actualizaci&oacute;n de la informaci&oacute;n por medio de herramientas tecnol&oacute;gicas y de f&aacute;cil acceso para los centros educativos, modificaciones presupuestarias las cuales permitieron contar con recursos econ&oacute;micos para la atenci&oacute;n de la demanda en los diferentes productos de beca, desarrollo e implementaci&oacute;n de estrategias en el campo para la recepci&oacute;n de casos nuevos en aquellos Centros Educativos que se encuentran ubicados en las zonas con &nbsp;menor &iacute;ndice de desarrollo social, visitas a las Direcciones Regionales del MEP a las que se les dio una atenci&oacute;n personalizada, capacitaci&oacute;n tanto en el campo como en las instalaciones del Fondo sobre el proceso de gesti&oacute;n, lo que gener&oacute; un aumento en la presentaci&oacute;n de solicitudes de formularios de casos nuevos completos, lo que se traduce en una mayor oportunidad en el otorgamiento del beneficio.</p>

<p>El esfuerzo que FONABE asumi&oacute; en este per&iacute;odo es fundamental para lograr la integraci&oacute;n con SINIRUBE. El avance tecnol&oacute;gico asumido por la instituci&oacute;n potencializa la capacidad instalada del Fondo para atender como corresponde el mandato legal de integraci&oacute;n social, a trav&eacute;s de este sistema &uacute;nico de beneficiarios.</p>

<p><br />
Hoy d&iacute;a, el FONABE se encuentra en una faceta dentro de la comunidad nacional: Consolidarse como una instituci&oacute;n madura, que brinda un servicio eficiente y que posee el conocimiento y la experiencia para convertirse en generadora de estudios, pol&iacute;ticas y procesos para la asignaci&oacute;n de becas.&nbsp;</p>

<h2><strong>Filosof&iacute;a del Trabajo</strong></h2>

<p>Abrigamos en nuestro quehacer institucional una filosof&iacute;a de mejora continua, para el mejoramiento permanentemente de nuestros procesos en funci&oacute;n de la atenci&oacute;n de las necesidades de estudio para todos aquellos estudiantes de escasos recursos que luchan incansablemente por alcanzar sus sue&ntilde;os y proyectos de vida por medio de la educaci&oacute;n como instrumento fundamental para el desarrollo de sus capacidades y que contribuya a la construcci&oacute;n de un pa&iacute;s con mayores niveles de bienestar social.&nbsp;</p>
', 1519918469, 1519918469, 6, 0, N'icon-g-historia', 0, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (39, N'gente-comprometida', N'Gente Comprometida', N'Gente Comprometida', N'<h1><strong>Gente comprometida</strong></h1>

<hr />
<h2><br />
Externa</h2>

<p><br />
En los centros educativos se conforman organismos auxiliares o comit&eacute;s de beca, los cuales son el puente de comunicaci&oacute;n entre la poblaci&oacute;n estudiantil y el Fondo Nacional de Becas. &nbsp;Ellos informan a la poblaci&oacute;n estudiantil sobre las becas y canalizan las solicitudes que cumplen con todos los requisitos de admisibilidad establecidos por el FONABE en los niveles de preescolar, primaria, secundaria, ense&ntilde;anza especial y Universidad.<br />
Los comit&eacute;s de becas son el principal apoyo con el que cuenta FONABE para captar a la poblaci&oacute;n con limitaciones econ&oacute;micas o de vulnerabilidad educativa; este esfuerzo desinteresado de los comit&eacute;s de becas permite contribuir con el otorgamiento de una beca para satisfacer parte de las necesidades escolares de los ni&ntilde;os y adolescentes del pa&iacute;s.</p>

<h2><br />
Interna</h2>

<p><br />
Equipo de trabajo conformado por funcionarios enfocados en realizar una labor de soporte y direcci&oacute;n en el proceso de gesti&oacute;n de becas, en la cual los valores de trabajo en equipo, honestidad, compromiso y transparencia;&nbsp;&nbsp;sean los pilares primordiales para el cumplimiento de la misi&oacute;n y visi&oacute;n de FONABE.</p>

<p>&nbsp;</p>

<p><a href="https://i.ytimg.com/vi/-8cXRWGlB3Q/maxresdefault.jpg" target="_self"><img alt="" src="https://i.ytimg.com/vi/-8cXRWGlB3Q/maxresdefault.jpg" style="float: left; width: 300px; height: 169px;" /></a><br />
&nbsp;</p>
', 1519918469, 1519918469, 6, 0, N'icon-g-comprometida', 0, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (40, N'mision-vision', N'Mision/Visión', N'Mision/Visión', N'<h1><strong>Mision</strong></h1>

<p>Contribuimos a la permanencia de estudiantes en condici&oacute;n de vulnerabilidad socio econ&oacute;mica, pobreza y pobreza extrema en el sistema educativo a trav&eacute;s del otorgamiento, administraci&oacute;n y seguimiento de becas</p>

<h1><strong>Vision</strong></h1>

<p><strong>Ser reconocidos a nivel nacional como la mejor alternativa para el otorgamiento, administraci&oacute;n y seguimiento de becas, tanto del Estado como de otros actores, que apoyen la inclusi&oacute;n, permanencia y &eacute;xito en el sistema educativo, para el combate a la pobreza y el desarrollo social</strong></p>
', 1519918469, 1519918469, 6, 0, N'icon-g-mision', 0, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (41, N'organigrama', N'Organigrama', N'Organigrama', N'<h1>Organigrama</h1>

<hr />
<p><img alt="" src="http://e020371f.ngrok.io/images/organigrama.png" style="width: 1025px; height: 651px;" /></p>
', 1519918469, 1519918469, 6, 0, N'icon-g-organigrama', 0, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (42, N'contacto', N'Contacto', N'Contacto', N'<h3>Para mayor informaci&oacute;n, comun&iacute;quese con nosotros por los siguientes medios:</h3>

<h6>Tel&eacute;fono:&nbsp;<span>2521-6322</span></h6>

<h6>Fax:<span>&nbsp;2221-5448</span></h6>

<h6>Localizaci&oacute;n <span>De Acueductos y Alcantarillados, Paseo de los Estudiantes, 200mts Este y 50mts Sur edificio Az&uacute;l, Av 10 y 12, Calle 13.</span></h6>

<h6>Correo institucional: <span>consultas@fonabe.go.cr</span></h6>

<h6>Horario de Atenci&oacute;n: <span>De lunes a Viernes, 7:00 am a 3:00 pm, Jornada Continua.</span></h6>

<p>&nbsp;</p>

<p><a class="btn btn-primary" href="/pagina/direcciones-funcionarios">Direcciones Electr&oacute;nicas Funcionarios</a></p>
', 1519918469, 1519918469, 7, 0, N'icon-g-contactos', 0, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (43, N'consultas', N'Consultas', N'Consultas', N'', 1519918469, 1519918469, 7, 0, N'icon-g-consultas', 0, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (44, N'denuncias', N'Denuncias', N'Denuncias', N'', 1519918469, 1519918469, 7, 0, N'icon-g-denuncia', 0, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (45, N'direcciones-funcionarios', N'Direcciones Electrónicas Funcionarios', N'direcciones electrónicas', N'<div style="column-count: 3;">
<p><strong><em>Secretaria</em></strong></p>

<p>Zeneida Mej&iacute;as,&nbsp;<a href="mailto:zmejias@fonabe.go.cr">zmejias@fonabe.go.cr</a></p>

<p><strong><em>Auditor&iacute;a Interna</em></strong></p>

<p>Roxana Rodr&iacute;guez.&nbsp;<a href="mailto:rrodriguez@fonabe.go.cr">rrodriguez@fonabe.go.cr</a></p>

<p><strong><em>Asistente de Auditor&iacute;a Interna</em></strong></p>

<p>Helin Piedra.&nbsp;<a href="mailto:hpiedra@fonabe.go.cr">hpiedra@fonabe.go.cr</a></p>

<p><strong>Direcci&oacute;n&nbsp;Ejecutiva</strong></p>

<p><strong><em>Director Ejecutivo</em></strong></p>

<p>Mauricio Donato,&nbsp;<a href="mailto:mdonato@fonabe.go.cr">mdonato@fonabe.go.cr</a></p>

<p><strong><em>Secretaria</em></strong></p>

<p>Adriana Navarro,&nbsp;<a href="mailto:anavarro@fonabe.go.cr">anavarro@fonabe.go.cr</a></p>

<p><strong><em>Asesor&iacute;a Legal</em></strong></p>

<p>Ana Mar&iacute;a Rivera,&nbsp;<a href="mailto:arivera@fonabe.go.cr">arivera@fonabe.go.cr</a></p>

<p><strong><em>Asistente de Asesor&iacute;a Legal</em></strong></p>

<p>Mar&iacute;a Fernanda Barquero,&nbsp;<a href="mailto:mbarquero@fonabe.go.cr">mbarquero@fonabe.go.cr</a></p>

<p>Natalia Castro,&nbsp;<a href="mailto:ncastro@fonabe.go.cr">ncastro@fonabe.go.cr</a></p>

<p><strong><em>Contralor&iacute;a de Servicios</em></strong></p>

<p>Orlando Miranda,&nbsp;<a href="mailto:omiranda@fonabe.go.cr">omiranda@fonabe.go.cr</a></p>

<p><strong><em>Jefe de&nbsp;&nbsp;Planificaci&oacute;n</em></strong>&nbsp;</p>

<p>Laura Mena,&nbsp;<a href="mailto:lmena@fonabe.go.cr">lmena@fonabe.go.cr</a></p>

<p><strong><em>Asistente de&nbsp;Planificaci&oacute;n</em></strong>&nbsp;</p>

<p>Gabriela Barahona,&nbsp;<a href="mailto:gbarahona@fonabe.go.cr">gbarahona@fonabe.go.cr</a></p>

<p>Francisco Vindas,&nbsp;<a href="mailto:fvindasc@fonabe.go.cr">fvindasc@fonabe.go.cr</a></p>

<p>Sof&iacute;a Bola&ntilde;os,&nbsp;<a href="mailto:sbolanos@fonabe.go.cr">sbolanos@fonabe.go.cr</a></p>

<p><strong>Direcci&oacute;n&nbsp;de Gesti&oacute;n de Becas</strong></p>

<p>Vanessa Ram&iacute;rez,&nbsp;<a href="mailto:vramirez@fonabe.go.cr">vramirez@fonabe.go.cr</a></p>

<p>Merl&iacute;n Mena,&nbsp;<a href="mailto:mmena@fonabe.go.cr">mmena@fonabe.go.cr</a></p>

<p>Adriana Le&oacute;n Sorio,&nbsp;<a href="mailto:aleon@fonabe.go.cr">aleon@fonabe.go.cr</a></p>

<p><em><strong>Jefe de Atenci&oacute;n al Becario</strong></em></p>

<p>Jansen Coto,&nbsp;<a href="mailto:jcoto@fonabe.go.cr">jcoto@fonabe.go.cr</a></p>

<p><strong><em>Asistentes de Atenci&oacute;n al Becario</em></strong></p>

<p>Gabriela Garc&iacute;a,&nbsp;<a href="mailto:ggarcia@fonabe.go.cr">ggarcia@fonabe.go.cr</a></p>

<p>Miriam Sequeira,&nbsp;<a href="mailto:msequeira@fonabe.go.cr">msequeira@fonabe.go.cr</a></p>

<p>Oscar Sanabria,&nbsp;<a href="mailto:osanabria@fonabe.go.cr">osanabria@fonabe.go.cr</a></p>

<p>Jacqueline Brenes&nbsp;<a href="mailto:jbrenes@fonabe.go.cr">jbrenes@fonabe.go.cr</a></p>

<p>Laura Rodr&iacute;guez,&nbsp;<a href="mailto:lrodriguezg@fonabe.go.cr">lrodriguezg@fonabe.go.cr</a></p>

<p>Eli&eacute;cer Aguilera,&nbsp;<a href="mailto:eaguilera@fonabe.go.cr">eaguilera@fonabe.go.cr</a></p>

<p>Diana Jim&eacute;nez<a href="mailto:djimenez@fonabe.go.cr">djimenez@fonabe.go.cr</a></p>

<p>Isis S&aacute;enz,&nbsp;<a href="mailto:isaenz@fonabe.go.cr">isaenz@fonabe.go.cr</a></p>

<p>Marilyn Alfaro,&nbsp;<a href="mailto:malfaro@fonabe.go.cr.go.cr">malfaro@fonabe.go.cr</a></p>

<p><strong><em>Jefe de Asignaci&oacute;n de Becas</em></strong></p>

<p>Nalda Hay L&oacute;pez,&nbsp;<a href="mailto:%20nhay@fonabe.go.cr">nhay@fonabe.go.cr</a></p>

<p>&nbsp;</p>

<p><strong><em>Equipo de Trabajo Social&nbsp;</em></strong></p>

<p>Adriana Arce,&nbsp;<a href="mailto:adarce@fonabe.go.cr">adarce@fonabe.go.cr</a></p>

<p>Andrea L&oacute;pez,&nbsp;<a href="mailto:alopez@fonabe.go.cr">alopez@fonabe.go.cr</a></p>

<p>Mar&iacute;a Fernanda Retana,&nbsp;<a href="mailto:mretana@fonabe.go.cr">mretana@fonabe.go.cr</a></p>

<p>Carmen Arias&nbsp;<a href="mailto:carias@fonabe.go.cr">carias@fonabe.go.cr</a></p>

<p>Andrea Ch&aacute;vez&nbsp;<a href="mailto:achavez@fonabe.go.cr">achavez@fonabe.go.cr</a></p>

<p>Karen Fern&aacute;ndez&nbsp;<a href="mailto:kfernandez@fonabe.go.cr">kfernandez@fonabe.go.cr</a></p>

<p>Yamileth Guardado&nbsp;<a href="mailto:yguradado@fonabe.go.cr">yguradado@fonabe.go.cr</a></p>

<p>Rossy M&eacute;ndez&nbsp;<a href="mailto:rmendez@fonabe.go.cr">rmendez@fonabe.go.cr</a></p>

<p><strong><em>Asistentes de Asignaci&oacute;n de Becas</em></strong></p>

<p>Grace Zamora,&nbsp;<a href="mailto:gzamora@fonabe.go.cr">gzamora@fonabe.go.cr</a></p>

<p>Maricel Acu&ntilde;a,&nbsp;<a href="mailto:macuna@fonabe.go.cr">macuna@fonabe.go.cr</a></p>

<p>Eugenia Cruz,&nbsp;<a href="mailto:ecruz@fonabe.go.cr">ecruz@fonabe.go.cr</a></p>

<p><strong>Direcci&oacute;n&nbsp;Administrativa - Financiera</strong></p>

<p><strong><em>Director Administrativo Financiero</em></strong></p>

<p>Lucrecia Rodr&iacute;guez,&nbsp;<a href="mailto:lrodriguez@fonabe.go.cr">lrodriguez@fonabe.go.cr</a></p>

<p><strong><em>Jefe de Contabilidad y Tesorer&iacute;a</em></strong></p>

<p>Adriana Velasco,&nbsp;<a href="mailto:avelasco@fonabe.go.cr">avelasco@fonabe.go.cr</a></p>

<p><strong><em>Analista&nbsp;Financiera</em></strong></p>

<p>Andrea G&oacute;mez,&nbsp;<a href="mailto:agomez@fonabe.go.cr">agomez@fonabe.go.cr</a></p>

<p><strong><em>Tesorer&iacute;a, Contabilidad y Presupuesto</em></strong></p>

<p>Mariela Hern&aacute;ndez Sol&iacute;s,&nbsp;<a href="mailto:mhernandez@fonabe.go.cr">mhernandez@fonabe.go.cr</a>&nbsp;</p>

<p>Giselle Dur&aacute;n Oviedo,&nbsp;<a href="mailto:gduran@fonabe.go.cr">gduran@fonabe.go.cr</a>&nbsp;</p>

<p>Ana Karina Fallas,&nbsp;<a href="mailto:afallas@fonabe.go.cr">afallas@fonabe.go.cr</a>&nbsp;</p>

<p><strong><em>Jefe de la Proveedur&iacute;a Institucional y Servicios Generales&nbsp;</em></strong></p>

<p>Luis Barrantes,&nbsp;<a href="mailto:lbarrantes@fonabe.go.cr">lbarrantes@fonabe.go.cr</a>&nbsp;</p>

<p><em><strong>Analista de Contrataciones</strong></em></p>

<p>Olman Bonilla,&nbsp;<a href="mailto:obonilla@fonabe.go.cr">obonilla@fonabe.go.cr</a></p>

<p><em><strong>Auxiliar de Almacenamiento y Distribuci&oacute;n</strong></em></p>

<p>Marta Gonz&aacute;lez,&nbsp;<a href="mailto:mgonzalez@fonabe.go.cr">mgonzalez@fonabe.go.cr</a></p>

<p><em><strong>Servicios Generales&nbsp;</strong></em></p>

<p>V&iacute;ctor Quesada ,&nbsp;<a href="mailto:vquesada@fonabe.go.cr">vquesada@fonabe.go.cr</a></p>

<p><em><strong>Mensajero&nbsp;</strong></em></p>

<p>Edwin Arauz,&nbsp;<a href="mailto:earauz@fonabe.go.cr">earauz@fonabe.go.cr</a></p>

<p><em><strong>Miscel&aacute;nea&nbsp;</strong></em></p>

<p>Sof&iacute;a V&iacute;quez,&nbsp;<a href="mailto:sviquez@fonabe.go.cr">sviquez@fonabe.go.cr</a></p>

<p><strong><em>Jefatura de Gesti&oacute;n Institucional de Recursos Humanos</em></strong></p>

<p>Karla Cubero,&nbsp;<a href="mailto:kcubero@fonabe.go.cr">kcubero@fonabe.go.cr</a></p>

<p><strong><em>Asistentes de Recursos Humanos</em></strong></p>

<p>Francine Cardoso,&nbsp;<a href="mailto:fcardoso@fonabe.go.cr">fcardoso@fonabe.go.cr</a></p>

<p>Irene Morales,&nbsp;<a href="mailto:imorales@fonabe.go.cr">imorales@fonabe.go.cr</a></p>

<p><strong><em>Archivo Institucional</em></strong></p>

<p>Ivania Vindas,&nbsp;<a href="mailto:ivindas@fonabe.go.cr">ivindas@fonabe.go.cr</a></p>

<p><strong>Direcci&oacute;n&nbsp;Desarrollo Tecnol&oacute;gico</strong></p>

<p>Ivannia Badilla,&nbsp;<a href="mailto:ibadilla@fonabe.go.cr">ibadilla@fonabe.go.cr</a></p>

<p><strong><em><em>Jefe de Desarrollo Tecnol&oacute;gico</em></em></strong></p>

<p>Laura Z&uacute;&ntilde;iga,&nbsp;<a href="mailto:lzunigam@fonabe.go.cr">lzunigam@fonabe.go.cr</a></p>

<p><strong><em>Administradores&nbsp;Desarrollo Tecnol&oacute;gico&nbsp;</em></strong></p>

<p>El&iacute;as Mora,&nbsp;<a href="mailto:emora@fonabe.go.cr">emora@fonabe.go.cr</a></p></div>
', 1519918469, 1519918469, 9, 0, N'icon-g-centros', 0, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (46, N'tarjetas-prepago', N'Tarjetas Prepago', N'Tarjetas Prepago pendientes de retiro De acuerdo con la Circular DGB-026-2015, puede consultar aquí si el beneficiario es parte del grupo que aún no ha retirado la Tarjeta Prepago del Banco Nacional.', N'<h2>Tarjetas Prepago pendientes de retiro</h2>

<p>De acuerdo con la Circular DGB-026-2015, puede consultar aqu&iacute; si el beneficiario es parte del grupo que a&uacute;n no ha retirado la Tarjeta Prepago del Banco Nacional.</p>

<h3>Recuerde:</h3>

<p>1. Si la tarjeta prepago no es retirada luego de dos meses de confeccionado (asignado el beneficio), por pol&iacute;ticas del Banco Nacional la misma ser&aacute; destruida.</p>

<p>2. Si la tarjeta no tiene movimientos en tres meses, por pol&iacute;ticas del Banco Nacional, autom&aacute;ticamente se cerrara.</p>

<p>3. En caso de Robo o Extravi&oacute;, debe reportarlo inmediatamente al Banco Nacional. De darse los punto 1 y 3, el costo de la reposici&oacute;n estar&aacute; a cargo del beneficiario.</p>
', NULL, NULL, 9, 0, NULL, 0, 0, 0, 0)
INSERT [dbo].[fn_static_pages] ([id], [slug], [title], [excerpt], [content], [created_at], [updated_at], [category_id], [editable], [icon], [order], [has_md_documents], [has_md_video], [has_md_info]) VALUES (47, N'estado-solicitud', N'Estado Solicitud', N'Estado Solicitud', N'', 1519918469, 1519918469, 1, 0, N'icon-g-estados', 7, 0, 0, 0)
SET IDENTITY_INSERT [dbo].[fn_static_pages] OFF
SET IDENTITY_INSERT [dbo].[fn_user] ON 

INSERT [dbo].[fn_user] ([id], [username], [email], [password_hash], [auth_key], [confirmed_at], [unconfirmed_email], [blocked_at], [created_at], [updated_at], [registration_ip], [last_login_at], [flags]) VALUES (12, N'fonabe', N'admin@moplin.com', N'$2y$12$NNt1IdiizYLutoLYEu3I4.4NqrP6wEyE3RaYKDM0A3CDmzBCKQQRG', N'5r4FrdwQZueQ6PiitbxoDQEpD9Knyj-V', 1520784697, NULL, NULL, 1520784697, 1520784697, N'127.0.0.1', 1520784948, NULL)
INSERT [dbo].[fn_user] ([id], [username], [email], [password_hash], [auth_key], [confirmed_at], [unconfirmed_email], [blocked_at], [created_at], [updated_at], [registration_ip], [last_login_at], [flags]) VALUES (13, N'admin', N'pp@moplin.com', N'$2y$12$iairvYCmuOYvncvcTvtuquH4aeX12u9kVgvEUBFdAKwxvnrObJ0iO', N'1BoJx992Sxmrm-3TsTFpP4DYol3g0Gyi', 1519843968, NULL, NULL, 1519843944, 1522894301, N'127.0.0.1', 1522984252, NULL)
INSERT [dbo].[fn_user] ([id], [username], [email], [password_hash], [auth_key], [confirmed_at], [unconfirmed_email], [blocked_at], [created_at], [updated_at], [registration_ip], [last_login_at], [flags]) VALUES (16, N'FONABE09', N'fonabe@correo.com', N'$2y$12$QjN/0HnWjQ3ZFze3ApTL2.ZAAutDuHGwf4O2eiU0W4ydQC0ofmVu6', N'S7PaeqaDyXCPLccpYrahVtnBsqIwmLla', 1521583577, NULL, NULL, 1521583576, 1521583576, N'127.0.0.1', 1523029942, NULL)
SET IDENTITY_INSERT [dbo].[fn_user] OFF
SET IDENTITY_INSERT [dbo].[fn_videos] ON 

INSERT [dbo].[fn_videos] ([id], [title], [slug], [extract], [content], [author_name], [url_video], [published], [created_at], [updated_at], [page_id]) VALUES (13, N'Historia de FONABE', N'historia-de-fonabe', N'Historia de FONABE', N'', NULL, N'mB6MwAB-I9I', 1, 1522665664, 1522665759, 7)
SET IDENTITY_INSERT [dbo].[fn_videos] OFF
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__fn_histo__809A238B3B95D2F1]    Script Date: 06/04/2018 03:36:39 p.m. ******/
ALTER TABLE [dbo].[fn_history] ADD UNIQUE NONCLUSTERED 
(
	[year] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__fn_user__AB6E61645555A4F4]    Script Date: 06/04/2018 03:36:39 p.m. ******/
ALTER TABLE [dbo].[fn_user] ADD UNIQUE NONCLUSTERED 
(
	[email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__fn_user__F3DBC5725832119F]    Script Date: 06/04/2018 03:36:39 p.m. ******/
ALTER TABLE [dbo].[fn_user] ADD UNIQUE NONCLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[fn_account] ADD  DEFAULT (NULL) FOR [user_id]
GO
ALTER TABLE [dbo].[fn_account] ADD  DEFAULT (NULL) FOR [properties]
GO
ALTER TABLE [dbo].[fn_auth_item] ADD  DEFAULT (NULL) FOR [description]
GO
ALTER TABLE [dbo].[fn_auth_item] ADD  DEFAULT (NULL) FOR [rule_name]
GO
ALTER TABLE [dbo].[fn_auth_item] ADD  DEFAULT (NULL) FOR [created_at]
GO
ALTER TABLE [dbo].[fn_auth_item] ADD  DEFAULT (NULL) FOR [updated_at]
GO
ALTER TABLE [dbo].[fn_auth_item] ADD  DEFAULT (NULL) FOR [data]
GO
ALTER TABLE [dbo].[fn_auth_rule] ADD  DEFAULT (NULL) FOR [created_at]
GO
ALTER TABLE [dbo].[fn_auth_rule] ADD  DEFAULT (NULL) FOR [updated_at]
GO
ALTER TABLE [dbo].[fn_auth_rule] ADD  DEFAULT (NULL) FOR [data]
GO
ALTER TABLE [dbo].[fn_events] ADD  DEFAULT ((1)) FOR [hours]
GO
ALTER TABLE [dbo].[fn_news] ADD  DEFAULT ((1)) FOR [published]
GO
ALTER TABLE [dbo].[fn_profile] ADD  DEFAULT (NULL) FOR [name]
GO
ALTER TABLE [dbo].[fn_profile] ADD  DEFAULT (NULL) FOR [public_email]
GO
ALTER TABLE [dbo].[fn_profile] ADD  DEFAULT (NULL) FOR [gravatar_email]
GO
ALTER TABLE [dbo].[fn_profile] ADD  DEFAULT (NULL) FOR [gravatar_id]
GO
ALTER TABLE [dbo].[fn_profile] ADD  DEFAULT (NULL) FOR [location]
GO
ALTER TABLE [dbo].[fn_profile] ADD  DEFAULT (NULL) FOR [website]
GO
ALTER TABLE [dbo].[fn_profile] ADD  DEFAULT (NULL) FOR [bio]
GO
ALTER TABLE [dbo].[fn_setting] ADD  DEFAULT ((1)) FOR [status]
GO
ALTER TABLE [dbo].[fn_setting] ADD  DEFAULT (NULL) FOR [description]
GO
ALTER TABLE [dbo].[fn_static_pages] ADD  DEFAULT ((1)) FOR [editable]
GO
ALTER TABLE [dbo].[fn_static_pages] ADD  DEFAULT ((0)) FOR [order]
GO
ALTER TABLE [dbo].[fn_static_pages] ADD  DEFAULT ((0)) FOR [has_md_documents]
GO
ALTER TABLE [dbo].[fn_static_pages] ADD  DEFAULT ((0)) FOR [has_md_video]
GO
ALTER TABLE [dbo].[fn_static_pages] ADD  DEFAULT ((0)) FOR [has_md_info]
GO
ALTER TABLE [dbo].[fn_user] ADD  DEFAULT (NULL) FOR [confirmed_at]
GO
ALTER TABLE [dbo].[fn_user] ADD  DEFAULT (NULL) FOR [unconfirmed_email]
GO
ALTER TABLE [dbo].[fn_user] ADD  DEFAULT (NULL) FOR [blocked_at]
GO
ALTER TABLE [dbo].[fn_videos] ADD  DEFAULT ((1)) FOR [published]
GO
ALTER TABLE [dbo].[fn_account]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[fn_user] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[fn_auth_assignment]  WITH CHECK ADD FOREIGN KEY([item_name])
REFERENCES [dbo].[fn_auth_item] ([name])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[fn_auth_item]  WITH CHECK ADD FOREIGN KEY([rule_name])
REFERENCES [dbo].[fn_auth_rule] ([name])
ON UPDATE CASCADE
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[fn_auth_item_child]  WITH CHECK ADD FOREIGN KEY([child])
REFERENCES [dbo].[fn_auth_item] ([name])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[fn_auth_item_child]  WITH CHECK ADD FOREIGN KEY([parent])
REFERENCES [dbo].[fn_auth_item] ([name])
GO
ALTER TABLE [dbo].[fn_files]  WITH CHECK ADD FOREIGN KEY([page_id])
REFERENCES [dbo].[fn_static_pages] ([id])
GO
ALTER TABLE [dbo].[fn_info]  WITH CHECK ADD FOREIGN KEY([page_id])
REFERENCES [dbo].[fn_static_pages] ([id])
GO
ALTER TABLE [dbo].[fn_profile]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[fn_user] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[fn_social_account]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[fn_user] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[fn_static_pages]  WITH CHECK ADD FOREIGN KEY([category_id])
REFERENCES [dbo].[fn_category] ([id])
GO
ALTER TABLE [dbo].[fn_static_pages]  WITH CHECK ADD FOREIGN KEY([category_id])
REFERENCES [dbo].[fn_category] ([id])
GO
ALTER TABLE [dbo].[fn_static_pages]  WITH CHECK ADD FOREIGN KEY([category_id])
REFERENCES [dbo].[fn_category] ([id])
GO
ALTER TABLE [dbo].[fn_static_pages]  WITH CHECK ADD FOREIGN KEY([category_id])
REFERENCES [dbo].[fn_category] ([id])
GO
ALTER TABLE [dbo].[fn_static_pages]  WITH CHECK ADD FOREIGN KEY([category_id])
REFERENCES [dbo].[fn_category] ([id])
GO
ALTER TABLE [dbo].[fn_static_pages]  WITH CHECK ADD FOREIGN KEY([category_id])
REFERENCES [dbo].[fn_category] ([id])
GO
ALTER TABLE [dbo].[fn_token]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[fn_user] ([id])
GO
ALTER TABLE [dbo].[fn_videos]  WITH CHECK ADD FOREIGN KEY([page_id])
REFERENCES [dbo].[fn_static_pages] ([id])
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Título categoría' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_category', @level2type=N'COLUMN',@level2name=N'title'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slug para el URL' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_category', @level2type=N'COLUMN',@level2name=N'slug'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id de las quejas' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'first_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Apellido' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'last_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número de identificación' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'identification_card'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Teléfono' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'phone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dirección' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'address'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Correo electrónico' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número de Fax' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'fax'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Lugar de trabajo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'place_of_work'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Detalles' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'details'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'reported_name_01'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'reported_name_02'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'reported_name_03'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ubicación' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'reported_location_01'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ubicación' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'reported_location_02'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ubicación' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'reported_location_03'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Evidencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'evidence_01'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Evidencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'evidence_02'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Evidencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'evidence_03'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ubicación' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'evidence_location_01'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ubicación' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'evidence_location_02'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ubicación' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'evidence_location_03'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Testigo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'witness_01'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Testigo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'witness_02'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Testigo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'witness_03'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ubicación' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'witness_location_01'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ubicación' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'witness_location_02'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ubicación' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_complaints', @level2type=N'COLUMN',@level2name=N'witness_location_03'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id de contacto' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_contact', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_contact', @level2type=N'COLUMN',@level2name=N'first_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Apellido' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_contact', @level2type=N'COLUMN',@level2name=N'last_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificación' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_contact', @level2type=N'COLUMN',@level2name=N'identification'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Teléfono' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_contact', @level2type=N'COLUMN',@level2name=N'phone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Correo electrónico' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_contact', @level2type=N'COLUMN',@level2name=N'email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tema' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_contact', @level2type=N'COLUMN',@level2name=N'subject'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Detalles' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_contact', @level2type=N'COLUMN',@level2name=N'detail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de creación' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_contact', @level2type=N'COLUMN',@level2name=N'created_at'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de actualización' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_contact', @level2type=N'COLUMN',@level2name=N'updated_at'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id Eventos' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_events', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Título del evento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_events', @level2type=N'COLUMN',@level2name=N'title'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descripción del evento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_events', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha actualización' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_events', @level2type=N'COLUMN',@level2name=N'updated_at'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha creación' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_events', @level2type=N'COLUMN',@level2name=N'created_at'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hora y fecha de fin' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_events', @level2type=N'COLUMN',@level2name=N'end_dt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hora y fecha de inicio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_events', @level2type=N'COLUMN',@level2name=N'start_dt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Duración del evento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_events', @level2type=N'COLUMN',@level2name=N'hours'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha del evento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_events', @level2type=N'COLUMN',@level2name=N'date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hora de inicio del evento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_events', @level2type=N'COLUMN',@level2name=N'time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id página' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_files', @level2type=N'COLUMN',@level2name=N'page_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Título documento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_files', @level2type=N'COLUMN',@level2name=N'title'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descripción documento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_files', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Url documento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_files', @level2type=N'COLUMN',@level2name=N'file_url'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contador hits' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_files', @level2type=N'COLUMN',@level2name=N'hit_counter'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contador downloads' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_files', @level2type=N'COLUMN',@level2name=N'download_counter'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FEcha de creación' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_files', @level2type=N'COLUMN',@level2name=N'created_at'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha actualización' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_files', @level2type=N'COLUMN',@level2name=N'updated_at'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id documento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_files', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha del documento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_files', @level2type=N'COLUMN',@level2name=N'file_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del Histórico' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_history', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Año' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_history', @level2type=N'COLUMN',@level2name=N'year'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Título' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_history', @level2type=N'COLUMN',@level2name=N'title'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contenido' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_history', @level2type=N'COLUMN',@level2name=N'content'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_info', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ide de la página relacionada' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_info', @level2type=N'COLUMN',@level2name=N'page_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Título' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_info', @level2type=N'COLUMN',@level2name=N'title'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contenido' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_info', @level2type=N'COLUMN',@level2name=N'content'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Resumen' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_info', @level2type=N'COLUMN',@level2name=N'excerpt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id de noticia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_news', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Título de la noticia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_news', @level2type=N'COLUMN',@level2name=N'title'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slug para URL' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_news', @level2type=N'COLUMN',@level2name=N'slug'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Resúmen' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_news', @level2type=N'COLUMN',@level2name=N'extract'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contenido' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_news', @level2type=N'COLUMN',@level2name=N'content'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre del autor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_news', @level2type=N'COLUMN',@level2name=N'author_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de ultima actualización' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_news', @level2type=N'COLUMN',@level2name=N'updated_at'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de creación' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_news', @level2type=N'COLUMN',@level2name=N'created_at'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Publicado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_news', @level2type=N'COLUMN',@level2name=N'published'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id Settings' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_setting', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tipo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_setting', @level2type=N'COLUMN',@level2name=N'type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Seccion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_setting', @level2type=N'COLUMN',@level2name=N'section'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Llave elemento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_setting', @level2type=N'COLUMN',@level2name=N'key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor elemento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_setting', @level2type=N'COLUMN',@level2name=N'value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Estado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_setting', @level2type=N'COLUMN',@level2name=N'status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descripción' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_setting', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de creación' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_setting', @level2type=N'COLUMN',@level2name=N'created_at'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha actualización' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_setting', @level2type=N'COLUMN',@level2name=N'updated_at'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id Slide' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_slider_images', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Url imagen del slide' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_slider_images', @level2type=N'COLUMN',@level2name=N'image_url'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Link del slide' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_slider_images', @level2type=N'COLUMN',@level2name=N'image_link'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Texto del slide' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_slider_images', @level2type=N'COLUMN',@level2name=N'slide_text'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slug para URL limpia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'slug'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Título de la página' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'title'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Resúmen del contenido' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'excerpt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contenido de la página' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'content'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de creación' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'created_at'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de actualización' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'updated_at'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id Categoría' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'category_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Es editable?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'editable'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Icono de menú' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'icon'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Orden de las páginas' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'order'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tiene maestro/detalle de documentos?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'has_md_documents'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tienes maestro/detalle de videos?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'has_md_video'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tienes maestro/detalle de información?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_static_pages', @level2type=N'COLUMN',@level2name=N'has_md_info'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_token', @level2type=N'COLUMN',@level2name=N'user_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Código' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_token', @level2type=N'COLUMN',@level2name=N'code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de creación' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_token', @level2type=N'COLUMN',@level2name=N'created_at'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tipo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_token', @level2type=N'COLUMN',@level2name=N'type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla de gestion de tokens de autenticación' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_token'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_user', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre de Usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_user', @level2type=N'COLUMN',@level2name=N'username'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Correo del Usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_user', @level2type=N'COLUMN',@level2name=N'email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hash de contraseña' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_user', @level2type=N'COLUMN',@level2name=N'password_hash'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Llave de atorización' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_user', @level2type=N'COLUMN',@level2name=N'auth_key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de confirmacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_user', @level2type=N'COLUMN',@level2name=N'confirmed_at'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Correo electronico no confirmado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_user', @level2type=N'COLUMN',@level2name=N'unconfirmed_email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de bloqueo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_user', @level2type=N'COLUMN',@level2name=N'blocked_at'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FEcha de creación' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_user', @level2type=N'COLUMN',@level2name=N'created_at'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No IP del registro' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_user', @level2type=N'COLUMN',@level2name=N'registration_ip'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de últimop registro' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_user', @level2type=N'COLUMN',@level2name=N'last_login_at'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Banderas' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_user', @level2type=N'COLUMN',@level2name=N'flags'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla de usuarios del sistema' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_user'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id de video' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_videos', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Título del video' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_videos', @level2type=N'COLUMN',@level2name=N'title'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slug para URL' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_videos', @level2type=N'COLUMN',@level2name=N'slug'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Resúmen' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_videos', @level2type=N'COLUMN',@level2name=N'extract'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contenido' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_videos', @level2type=N'COLUMN',@level2name=N'content'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre del autor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_videos', @level2type=N'COLUMN',@level2name=N'author_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Publicado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_videos', @level2type=N'COLUMN',@level2name=N'published'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de creación' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_videos', @level2type=N'COLUMN',@level2name=N'created_at'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha actualización' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fn_videos', @level2type=N'COLUMN',@level2name=N'updated_at'
GO
